<?php

/**
 * @file
 * landing_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function landing_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function landing_page_image_default_styles() {
  $styles = array();

  // Exported image style: half_screen.
  $styles['half_screen'] = array(
    'label' => 'Half Screen',
    'effects' => array(
      12 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 765,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function landing_page_node_info() {
  $items = array(
    'landing_page' => array(
      'name' => t('Landing Page'),
      'base' => 'node_content',
      'description' => t('A full width page displaying dynamic components.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function landing_page_paragraphs_info() {
  $items = array(
    '1_column_content' => array(
      'name' => '1 column content',
      'bundle' => '1_column_content',
      'locked' => '1',
    ),
    '2_column_image_content' => array(
      'name' => '2 Column Image & Content',
      'bundle' => '2_column_image_content',
      'locked' => '1',
    ),
    'button' => array(
      'name' => 'Button',
      'bundle' => 'button',
      'locked' => '1',
    ),
    'content_strap' => array(
      'name' => 'Content Strap',
      'bundle' => 'content_strap',
      'locked' => '1',
    ),
    'content_strap_column' => array(
      'name' => 'Content Strap Column',
      'bundle' => 'content_strap_column',
      'locked' => '1',
    ),
    'cta' => array(
      'name' => 'CTA',
      'bundle' => 'cta',
      'locked' => '1',
    ),
    'cta_strap' => array(
      'name' => 'CTA Strap',
      'bundle' => 'cta_strap',
      'locked' => '1',
    ),
    'featured_products' => array(
      'name' => 'Featured Products',
      'bundle' => 'featured_products',
      'locked' => '1',
    ),
    'image_content' => array(
      'name' => 'Image + Content',
      'bundle' => 'image_content',
      'locked' => '1',
    ),
    'image_strap' => array(
      'name' => 'Image Strap',
      'bundle' => 'image_strap',
      'locked' => '1',
    ),
    'standard_content' => array(
      'name' => 'Standard Content',
      'bundle' => 'standard_content',
      'locked' => '1',
    ),
    'strap' => array(
      'name' => 'Strap',
      'bundle' => 'strap',
      'locked' => '1',
    ),
    'webform' => array(
      'name' => 'Webform',
      'bundle' => 'webform',
      'locked' => '1',
    ),
    'webform_content' => array(
      'name' => 'Webform + Content',
      'bundle' => 'webform_content',
      'locked' => '1',
    ),
  );
  return $items;
}
