<?php

/**
 * @file
 * landing_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function landing_page_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_component_details|paragraphs_item|image_content|form';
  $field_group->group_name = 'group_component_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Component Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_content_layout',
      1 => 'field_bg_color',
      2 => 'field_color',
      3 => 'group_image',
      4 => 'group_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Component Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-component-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_component_details|paragraphs_item|image_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_component_details|paragraphs_item|webform_content|form';
  $field_group->group_name = 'group_component_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'webform_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Component Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_content_layout',
      1 => 'group_webform',
      2 => 'group_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-component-details field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_component_details|paragraphs_item|webform_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_strap_details|paragraphs_item|content_strap_column|form';
  $field_group->group_name = 'group_content_strap_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'content_strap_column';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content Strap Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_bg_color',
      1 => 'field_image',
      2 => 'field_opacity',
      3 => 'field_color',
      4 => 'field_grid_system_classes',
      5 => 'group_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-content-strap-details field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_content_strap_details|paragraphs_item|content_strap_column|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content|paragraphs_item|content_strap_column|form';
  $field_group->group_name = 'group_content';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'content_strap_column';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_content_strap_details';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '10',
    'children' => array(
      0 => 'field_paragraphs_reference',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-content field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_content|paragraphs_item|content_strap_column|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content|paragraphs_item|image_content|form';
  $field_group->group_name = 'group_content';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_content';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_component_details';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '5',
    'children' => array(
      0 => 'field_paragraphs_reference',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Content',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-content field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_content|paragraphs_item|image_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content|paragraphs_item|webform_content|form';
  $field_group->group_name = 'group_content';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'webform_content';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_component_details';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '20',
    'children' => array(
      0 => 'field_paragraphs_reference',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-content field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_content|paragraphs_item|webform_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image|paragraphs_item|image_content|form';
  $field_group->group_name = 'group_image';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_content';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_component_details';
  $field_group->data = array(
    'label' => 'Image',
    'weight' => '4',
    'children' => array(
      0 => 'field_alignment',
      1 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Image',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_image|paragraphs_item|image_content|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image|paragraphs_item|image_strap|form';
  $field_group->group_name = 'group_image';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_strap';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_strap_details';
  $field_group->data = array(
    'label' => 'Image',
    'weight' => '6',
    'children' => array(
      0 => 'field_alignment',
      1 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Image',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_image|paragraphs_item|image_strap|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_strap_content|paragraphs_item|image_strap|form';
  $field_group->group_name = 'group_strap_content';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_strap';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_strap_details';
  $field_group->data = array(
    'label' => 'Strap Content',
    'weight' => '7',
    'children' => array(
      0 => 'field_paragraphs_reference',
      1 => 'field_paragraphs_reference_2',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Strap Content',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-strap-content field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_strap_content|paragraphs_item|image_strap|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_strap_details|paragraphs_item|image_strap|form';
  $field_group->group_name = 'group_strap_details';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_strap';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Strap Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_bg_color',
      1 => 'field_color',
      2 => 'group_image',
      3 => 'group_strap_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Strap Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-strap-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_strap_details|paragraphs_item|image_strap|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_webform|paragraphs_item|webform_content|form';
  $field_group->group_name = 'group_webform';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'webform_content';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_component_details';
  $field_group->data = array(
    'label' => 'Webform',
    'weight' => '19',
    'children' => array(
      0 => 'field_alignment',
      1 => 'field_webform',
      2 => 'field_text_long',
      3 => 'field_grid_system_classes',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-webform field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_webform|paragraphs_item|webform_content|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Component Details');
  t('Content');
  t('Content Strap Details');
  t('Image');
  t('Strap Content');
  t('Strap Details');
  t('Webform');

  return $field_groups;
}
