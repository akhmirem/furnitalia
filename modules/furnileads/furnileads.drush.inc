<?php
/**
 * Drush commands for Furnitalia Leads
 */

/**
 * Implements hook_drush_command().
 */
function furnileads_drush_command()
{

  $items['furnileads-drush-leads'] = array(
    'description' => 'Transfer Furnitalia leads info into amoCRM.',
    'aliases' => array('fdl'),
    //'bootstrap' => DRUSH_BOOTSTRAP_SITE,//DRUSH_BOOTSTRAP_MAX, //DRUSH_BOOTSTRAP_DRUPAL_FULL
    'bootstrap' => DRUSH_BOOTSTRAP_MAX,
  );

  return $items;
}

/**
 * Drush command callback to extract leads that need to be transferred into amoCRM
 */
function drush_furnileads_drush_leads()
{

  $result = db_query("SELECT lid
                            , created
                            , first_name
                            , last_name
                            , email
                            , substr(body, 1, 900) as short_descr
                            , category
                            , ip_address
                        FROM {furnileads} fl
                        WHERE fl.amocrm_status = :amocrm_status
                              AND IFNULL(fl.category, '') IN (:categories)
                        ORDER BY created ASC
                        LIMIT 5"
    , array(':amocrm_status' => 0, ':categories' => array('REQUEST', 'CONTACT', 'VIP')))->fetchAll(PDO::FETCH_ASSOC);

  if (count($result) > 0) {
    $leads = array();
    foreach ($result as $lead) {

      $item = '';
      try {
        $body_lines = explode("\n", $lead['short_descr']);
        if (is_array($body_lines) && count($body_lines) > 0) {
          $item = substr($body_lines[0],0,20);
        }
      } catch (Exception $e) {}

      // Update Lead Address based on IP Address
      if (strlen($lead['ip_address'])) {
        try {
          $location = furnileads_get_location_from_ip($lead['ip_address']);

          if (is_array($location)) {
            $location_str = "IP Address Estimated Location: " .
              $location['country'] . ", " .
              $location["city"] . ", " .
              $location["region"] . ', ' .
              $location["postal"];

            $lead['location'] = $location;
            $lead['short_descr'] = $location_str .
              "\nIP address: " . $lead['ip_address'] .
              "\n-----------------------------------\n" .
              $lead['short_descr'];

            update_lead_address($lead['lid'], $location_str);
          }

        } catch (Exception $e) {
          watchdog('furnitalia', "Exception while getting Geo information for lead: " . $e->getMessage(),
            null, WATCHDOG_DEBUG);
        }

      }

      $leads [] = array(
        'lid' => $lead['lid'],
        'email' => strtolower(trim($lead['email'])),
        'body' => $lead['short_descr'],
        'item' => $item,
        'name' => $lead['first_name'],
        'created' => $lead['created'],
        'category' => $lead['category'],
        'location' => $lead['location'],
      );

    }

    watchdog('furnitalia', "There are " . count($leads) . ' Contacts to be sent to AmoCRM:', null, WATCHDOG_DEBUG);
    furnileads_create_amocrm_lead($leads);

  } else {
    watchdog('furnitalia', "No new leads available to be sent to AmoCRM", null, WATCHDOG_DEBUG);
  }

}

/**
 * Send leads/contacts to amoCRM using REST API
 * @param array $leads
 * @return bool
 */
function furnileads_create_amocrm_lead($leads = array())
{

  if (!module_exists('amocrm_api')) {
    watchdog('furnitalia', "Error: amocrm_api module must be enabled", null, WATCHDOG_ERROR);
    return FALSE;
  }

  if (count($leads) == 0) {
    return FALSE;
  }

  $client = amocrm_api_get_default_client();
  if ($client) {
    $client_info = amocrm_api_account_load($client);
    foreach ($leads as $lead_info) {

      if ($lead_info['category'] == 'VIP') {

        _process_vip_contact($client, $client_info, $lead_info);

      } else {

        $amocrm_lead_id = _create_amocrm_lead($client, $lead_info);
        if ($amocrm_lead_id) {
          _create_amocrm_todo_for_lead($client, $amocrm_lead_id);
          _create_amocrm_lead_note($client, $amocrm_lead_id, $lead_info);

          $existing_contact = find_existing_amocrm_contact($client, $lead_info);
          if ($existing_contact) {

            // associate lead to contact
            if (isset($existing_contact['linked_leads_id']) &&
              is_array($existing_contact['linked_leads_id'])
            ) {
              $existing_contact['linked_leads_id'][] = $amocrm_lead_id;
            } else {
              $existing_contact['linked_leads_id'] = array($amocrm_lead_id);
            }

            // update address from IP address
            // if contact already has address entered, don't update it.
            $contact_address_entered = FALSE;
            if (!empty($existing_contact['custom_fields'])) {
              foreach ($existing_contact['custom_fields'] as $field) {
                if (!empty($field['name']) && $field['name'] == 'Address') {
                  if (!empty($field['values']) && is_array($field['values'])) {
                    $contact_address_entered = TRUE;
                  }
                }
              }
            }
            if (!$contact_address_entered) {
              $custom_fields_address = furnileads_create_address_amocrm_field($client_info, $lead_info);
              if (!empty($existing_contact['custom_fields']) && is_array($existing_contact['custom_fields'])) {
                $existing_contact['custom_fields'] = array_merge(
                  $existing_contact['custom_fields'],
                  $custom_fields_address
                );
              } else {
                $existing_contact['custom_fields'] = $custom_fields_address;
              }
            }

            try {
              $response = amocrm_api_contacts_update($client, array($existing_contact));
            } catch (Exception $E) {
              watchdog('furnitalia', "Failed to update Contact in amoCRM; " . $E->getMessage(),
                array(), WATCHDOG_DEBUG);
            }

          } else {
            _create_amocrm_contact($client, $client_info, $amocrm_lead_id, $lead_info);
          }
        }
      }
    }

  } else {
    watchdog('furnitalia', 'Not filled with data to connect to amoCRM', NULL, WATCHDOG_ERROR);
    return FALSE;
  }

  return TRUE;
}

/**
 * Query amoCRM REST API to fetch existing contact by e-mail
 * @param $client
 * @param array $lead_info
 * @return array - Contact object array or null
 */
function find_existing_amocrm_contact($client, array $lead_info = array())
{
  $params = array(
    'query' => $lead_info['email'],
  );
  $response = amocrm_api_contact_load_multiple($client, $params);

  $var = var_export($response, TRUE);
  watchdog('furnitalia', 'AmoCRM query status for existing contact: %res', array('%res' => $var), WATCHDOG_INFO);

  if (count($response) > 0 && is_array($response[0])) {
    return $response[0];
  }

  return null;
}

/**
 * @param $client - amoCRM client object
 * @param array $lead_info - array of lead fields
 * @return int - Lead ID in amoCRM
 * @internal param $contact_id - amoCRM contact object id
 */
function _create_amocrm_lead($client, array $lead_info)
{

  //Prepare lead structure
  $lead_title = 'Web Request Lead';
  if (!empty($lead_info['name'])) {
    $lead_title = $lead_info['name'];
    if (!empty($lead_info['item'])) {
      $lead_title .= " - " . $lead_info['item'];
    }
  }
  $lead = array(
    'name' => $lead_title,
    'date_create' => $lead_info['created'],
    'last_modified' => $lead_info['created'],
    'status_id' => '10739429' // 'Unprocessed' Lead status
  );

  /*// fill in Contact's e-mail
  $custom_fields = array();
  if (!empty($client_info['custom_fields']['leads'])) {
      foreach ($client_info['custom_fields']['leads'] as $field) {
          if ($field['id'] == '1522440') { // referred by
              $custom_fields[] = array(
                  'id' => $field['id'],
                  'values' => array(
                      '3639914', //web
                  )
              );
              break;
          }
      }
  }
  $lead['custom_fields'] = $custom_fields;*/

  $lead['tags'] = strtolower($lead_info['category']);
  //request to create a lead in amoCRM
  try {
    $var = var_export($lead, TRUE);
    watchdog('furnitalia', 'Lead to be created: %lead', array('%lead' => $var), WATCHDOG_INFO);
    $response = amocrm_api_leads_add($client, array($lead));
  } catch (Exception $e) {
    watchdog('furnitalia', 'AmoCRM: exception occured when creating new Lead . Message: %res', array('%res' => $e->getMessage()), WATCHDOG_ERROR);
  }
  $var = var_export($response, TRUE);
  watchdog('furnitalia', 'AmoCRM Lead creation call. Result: %res', array('%res' => $var), WATCHDOG_INFO);

  $lead_id = 0;
  if (is_array($response) && count($response) > 0) {

    $lead_id = $response[0]['id'];

  } else {
    $var = var_export($response, TRUE);
    watchdog('furnitalia', 'Failed to create Lead in amoCRM', array($var), WATCHDOG_ERROR);
  }

  return $lead_id;
}

/**
 * @param $client - amoCRM client object
 * @param $lead_id
 * @param array $lead_info - array of Lead fields
 */
function _create_amocrm_lead_note($client, $lead_id, array $lead_info)
{
  //Prepare note for amoCRM request
  $note = array(
    'element_id' => $lead_id,
    'element_type' => 2, //lead
    'note_type' => 4, //standard note type
    'date_create' => $lead_info['created'],
    'last_modified' => $lead_info['created'],
    'text' => $lead_info['body'],
  );

  $added_notes = amocrm_api_notes_add($client, array($note));
  if (isset($added_notes['notes']['add']) &&
    is_array($added_notes['notes']['add']) &&
    count($added_notes['notes']['add']) > 0) {

    watchdog('furnitalia', 'AmoCRM Note created; id= %lead', array('%lead' => $lead_id), WATCHDOG_INFO);

    // mark as 'sent' to amoCRM
    update_lead_amocrm_status($lead_info['lid']);

  } else {
    watchdog('furnitalia', 'Failed to create a note for Lead %lead in amoCRM; response:%response',
      array(
        '%lead' => $lead_id,
        '%response' => $added_notes
      ),
      WATCHDOG_ERROR);
  }
}

/**
 * @param $client - amoCRM client object
 * @param $lead_id
 */
function _create_amocrm_todo_for_lead($client, $lead_id)
{
  $lead_followup_due = new DateTime();
  if ($lead_followup_due->format("H") >= 17) {
    // if lead came after 5pm, make follow up due date to be next day
    $interval = new DateInterval("P1D");
    $lead_followup_due->add($interval);
  }
  $lead_followup_due->setTime(23, 59, 59); // all day
  $lead_followup_due = $lead_followup_due->getTimestamp();

  //Prepare task for amoCRM request
  $task = array(
    'element_id' => $lead_id,
    'element_type' => 2, //lead
    'task_type' => 4, //follow up
    'text' => 'Please follow up with a website lead.',
    'complete_till' => $lead_followup_due,
  );

  $added_tasks = amocrm_api_tasks_add($client, array($task));
  if (isset($added_tasks['tasks']['add']) &&
    is_array($added_tasks['tasks']['add']) &&
    count($added_tasks['tasks']['add']) > 0) {

    watchdog('furnitalia', 'AmoCRM task created; lead id= %lead', array('%lead' => $lead_id), WATCHDOG_INFO);

  } else {
    watchdog('furnitalia', 'Failed to create a task for Lead %lead in amoCRM; response:%response',
      array(
        '%lead' => $lead_id,
        '%response' => $added_tasks
      ),
      WATCHDOG_ERROR);
  }
}

/**
 * @param $client - amoCRM client object
 * @param $lead_id
 * @param array $lead_info
 * @return int - contact ID
 */
function _create_amocrm_contact($client, $client_info, $lead_id = NULL, array $lead_info)
{
  // build amoCRM Contact object
  $contact = array(
    'name' => !empty($lead_info['name']) ? $lead_info['name'] : '',
    'date_create' => $lead_info['created'],
    'last_modified' => $lead_info['created'],
  );

  if ($lead_id) {
    $contact['linked_leads_id'] = array($lead_id); //link lead to contact
  }

  // fill in Contact's e-mail
  $custom_fields = array();
  if (!empty($client_info['custom_fields']['contacts'])) {
    foreach ($client_info['custom_fields']['contacts'] as $field) {
      if (!empty($field['code'])) {
        if ($field['code'] == 'EMAIL' && !empty($lead_info['email'])) {
          $custom_fields[] = array(
            'id' => $field['id'],
            'values' => array(
              array(
                'value' => $lead_info['email'],
                'enum' => 'OTHER',
              ),
            ),
          );
          continue;
        }
      }
    }
  }

  $custom_fields_address = furnileads_create_address_amocrm_field($client_info, $lead_info);

  if (!empty($custom_fields_address)) {
    $custom_fields = array_merge($custom_fields, $custom_fields_address);
  }

  $contact['custom_fields'] = $custom_fields;

  // add tags
/*  $tags = array();
  if (is_array($contact['tags']) && count($contact['tags']) > 0) {
    foreach($contact['tags'] as $tag) {
      $tags []= $tag['id'];
    }
  }
  $tags []= strtolower($lead_info['category']);
  $contact['tags'] = $tags;*/

  // make an API request to create a contact in amoCRM
  $response = amocrm_api_contacts_add($client, array($contact));
  $var = var_export($response, TRUE);
  watchdog('furnitalia', "Contact creation in amoCRM, response: %resp", array('%resp' => $var), WATCHDOG_DEBUG);

  $contact_id = 0;
  $contact_ok = isset($response['contacts']['add']) &&
    is_array($response['contacts']['add']) &&
    count($response['contacts']['add']) > 0;
  if ($contact_ok) {
    $contact_id = $response['contacts']['add'][0]['id'];
  }
  return $contact_id;
}

function furnileads_create_address_amocrm_field($client_info, array $lead_info)
{

  $custom_fields = array();
  if (!empty($client_info['custom_fields']['contacts'])) {

    foreach ($client_info['custom_fields']['contacts'] as $field) {

      // set address based on ip
      if (!empty($field['name']) && $field['name'] == 'Address') {
        if (!empty($lead_info['location']) && !empty($lead_info['location']['city'])) {
          $custom_fields[] = array(
            'id' => $field['id'],
            'values' => array(
              array(
                'value' => $lead_info['location']['city'],
                'subtype' => '3',
              ),
              array(
                'value' => $lead_info['location']['region'],
                'subtype' => '4',
              ),
              array(
                'value' => $lead_info['location']['postal'],
                'subtype' => '5',
              ),
              array(
                'value' => $lead_info['location']['country'],
                'subtype' => '6',
              ),
            ),
          );
        }
      }

    }
  }

  return $custom_fields;
}

/**
 * @param $client - amoCRM client object
 * @param $contact_id
 * @param array $lead_info - array of Lead fields
 */
function _create_amocrm_contact_note($client, $contact_id, array $lead_info)
{
  //Prepare note for amoCRM request
  $note_body = '';
  $note_body .= "VIP (Preferred Contact) Form Submission received on " . format_date($lead_info['created'], 'custom', 'm-d-Y') . "\n";
  $note_body .= $lead_info['body'];
  $note = array(
    'element_id' => $contact_id,
    'element_type' => 1, //contact
    'note_type' => 4,    //standard note type
    'date_create' => $lead_info['created'],
    'last_modified' => $lead_info['created'],
    'text' => $note_body,
  );

  $added_notes = amocrm_api_notes_add($client, array($note));
  if (isset($added_notes['notes']['add']) &&
    is_array($added_notes['notes']['add']) &&
    count($added_notes['notes']['add']) > 0) {

    watchdog('furnitalia', 'AmoCRM Note created; id= %lead', array('%lead' => $contact_id), WATCHDOG_INFO);

    // mark as 'sent' to amoCRM
    update_lead_amocrm_status($lead_info['lid']);

  } else {
    watchdog('furnitalia', 'Failed to create a note for VIP Contact %lead in amoCRM; response:%response',
      array(
        '%lead' => $contact_id,
        '%response' => $added_notes
      ),
      WATCHDOG_ERROR);
  }
}

/**
 * Function to set the amoCRM status for lead to indicate that lead as been sent to CRM
 */
function update_lead_amocrm_status($lid)
{
  db_update('furnileads')
    ->fields(array('amocrm_status' => 1))
    ->condition('lid', $lid)
    ->execute();
}

/**
 * Function to update the Address for Lead
 */
function update_lead_address($lid, $new_address)
{
  db_update('furnileads')
    ->fields(array('address' => $new_address))
    ->condition('lid', $lid)
    ->execute();
}


/**
 * @param $client - amoCRM client object
 * @param $client_info
 * @param array $lead_info
 */
function _process_vip_contact($client, $client_info, array $lead_info)
{
  $contact = find_existing_amocrm_contact($client, $lead_info);
  if ($contact) {
    $contact_id = $contact['id'];

    // add VIP tag to contact
    $tags = array();
    if (is_array($contact['tags']) && count($contact['tags']) > 0) {
      foreach($contact['tags'] as $tag) {
        $tags []= $tag['id'];
      }
    }
    $tags []= 1514828; // 1514828 - VIP
    $contact['tags'] = $tags;

    try {
      $response = amocrm_api_contacts_update($client, array($contact));
    } catch (Exception $E) {
      watchdog('furnitalia', "Failed to update Contact in amoCRM; " . $E->getMessage(),
        array(), WATCHDOG_DEBUG);
    }

  } else {

    // create new VIP contact
    $contact_id = _create_amocrm_contact($client, $client_info, NULL, $lead_info);
  }

  // add note for VIP contact
  if ($contact_id && strlen(trim($lead_info['body']))) {
    _create_amocrm_contact_note($client, $contact_id, $lead_info);
  }
}
