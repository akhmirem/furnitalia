<?php
/**
 * My custom filter handler
 */
class furnileads_handler_filter_status_select extends views_handler_filter_string {
	

  /**
   * Shortcut to display the exposed options form.
   */
  function value_form(&$form, &$form_state) {
  
    $lead_status_options = array (
        'N' => 'Open',
        'O' => 'Follow Up Required',
        'P' => 'Communicated',
        'S' => 'Sold',
        'X' => 'Closed',
    );
    
	
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => $lead_status_options,
    );
 
    return $form;
  }
  
}