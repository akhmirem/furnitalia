<?php

define('LEADS_UNPROCESSED', '10739429');

function run_amocrm_unprocessed_leads_cron() {

    if (!module_exists('amocrm_api')) {
        watchdog('furnitalia', "Error: amocrm_api module must be enabled", null, WATCHDOG_ERROR);
        return FALSE;
    }

    $client = amocrm_api_get_default_client();
    if ($client) {
        $client_info = amocrm_api_account_load($client);
        //dsm($client_info);

        // get list of associate names
        $users = array();
        foreach($client_info['users'] as $user) {
            $users[$user['id']] = $user['name'];
        }

        $lead_full_info = array();

        $params = array(
            'status' => array(LEADS_UNPROCESSED),
        );
        $leads = amocrm_api_lead_load_multiple($client, $params);
        //dsm($leads);

        // exit if there are no unprocessed leads
        if (empty($leads)) {
            return TRUE;
        }

        $contact_ids = array();
        foreach($leads as $lead) {
            $contact_ids []= $lead['main_contact_id'];
            $lead_full_info[$lead['id']] = $lead;
            $lead_full_info[$lead['id']]['last_modified'] = format_date($lead['last_modified'], 'custom', 'm-d-Y H:i');
            $lead_full_info[$lead['id']]['date_create'] = format_date($lead['date_create'], 'custom', 'm-d-Y H:i');
            $lead_full_info[$lead['id']]['date_create_raw'] = isset($lead['date_create']) ? $lead['date_create'] : (string) time();
            $lead_full_info[$lead['id']]['owner'] = $users[$lead['responsible_user_id']];
            $lead_full_info[$lead['id']]['name'] = $lead['name'];
            $lead_full_info[$lead['id']]['contact_info']['name'] = $lead['name'];
        }

        // load contacts
        $params = array(
            'ids' => $contact_ids,
        );
        $contacts = amocrm_api_contact_load_multiple($client, $params);
        //dsm($contacts);

        // attach contact info to lead list
        foreach($contacts as $contact) {
            foreach ($contact['linked_leads_id'] as $lid) {
                if (array_key_exists($lid, $lead_full_info)) {
                    $lead_full_info[$lid]['contact_info'] = $contact;
                }
            }
        }
        //dsm($lead_full_info);

        usort($lead_full_info, "compare_lead_dates");

        $output = theme('furnileads_unprocessed_list_email_tpl', array('leads' => $lead_full_info));

        // send email notification
        $to_user_mail = 'emil@furnitalia.com';
        $module = 'furnileads';
        $key = 'notice';
        $from = 'admin@furnitalia.com';

        // build the email
        $message = drupal_mail($module, $key, $to_user_mail, LANGUAGE_NONE, array(), $from, FALSE);

        $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
        $message['headers']['Cc'] = 'd.aks@furnitalia.com,marie@furnitalia.com,becca@furnitalia.com,greg@furnitalia.com,inna@furnitalia.com';
        //$message['headers']['Reply-To'] = 'reply_to_email@test.com';

        $current_date = format_date(REQUEST_TIME, 'custom', 'm-d-Y');
        $message['subject'] = 'Furnitalia - ' . count($lead_full_info) . ' Unprocessed Leads in AmoCRM - ' . $current_date;

        $message['body'] = array();
        $message['body'][] =  $output;

        // retrieve the responsible implementation for this message.
        $system = drupal_mail_system($module, $key);

        // format the message body.
        $message = $system->format($message);

        watchdog('furnitalia', 'Furnileads email notification %email', array('%email' => '<pre>' . print_r($lead_full_info, TRUE) . '</pre>'), WATCHDOG_DEBUG, $link = NULL);

        // send e-mail
        $message['result'] = $system->mail($message);

        watchdog('furnitalia', 'Furnileads email notification status %status', array('%status' => $message['result']), WATCHDOG_DEBUG, $link = NULL);
        //watchdog('furnitalia', 'Furnileads email notification last run %lastrun , %seconds_ago seconds ago', array('%lastrun' => format_date($furnilead_notification_last_run, 'custom', 'm-d-Y H:i'), '%seconds_ago' => $notification_seconds_ago), WATCHDOG_DEBUG, $link = NULL);

        variable_set('furnileads_email_notification_last_run', date('Y-m-d H:i', time() ));

    } else {
        watchdog('furnitalia', "Error: failed to get amocrm_api client connection", null, WATCHDOG_ERROR);
        return FALSE;
    }

    return TRUE;

}


/*function run_unprocessed_leads_cron() {
    $result = db_query("SELECT lid
                            , created
                            , first_name
                            , last_name
                            , email
                            , substr(body, 1, 900) as short_descr
                        FROM {furnileads} fl
                        WHERE fl.status = :lead_status
                              AND IFNULL(fl.category, '') not in (:category, :coupon, :vip)
                        ORDER BY created ASC "
        , array(':lead_status' => 'N', ':category' => 'NEWSLETTER', ':coupon' => 'COUPON', ':vip' => 'VIP'))->fetchAll(PDO::FETCH_ASSOC);


        if (count($result) > 0) {

        $module = 'furnileads';
        $key = 'notice';

        $from = 'admin@furnitalia.com';

        $output = "<table border=\"1\" cellpadding=\"5\">";
        $output .= "<th>Lead Date</th><th>Lead Contact</th><th>Short description</th><th>Open</th><th>Mark Status</th>";
        foreach ($result as $lead) {
            $temp = "<tr>";
            $temp .= "<td>" . format_date($lead['created'], 'custom', 'm-d-Y H:i') . "</td>";
            $temp .= "<td>" . $lead['first_name'] . ' ' . $lead['last_name'] . ' ' . $lead['email'] . '</td>';
            $temp .= "<td>" . check_plain($lead['short_descr']) . '</td>';
            $temp .= "<td>" . l("Open", "admin/leads/" . $lead['lid'],  array('absolute' => true)) . '</td>';
            $temp .= "<td>" . l("Mark as Communicated", "admin/leads/" . $lead['lid'] . '/mark-status',  array('absolute' => true)) . '</td>';
            $temp .= "</tr>";

            $output .= $temp;
        }
        $output .= "</table>";


        $to_user_mail = 'emil@furnitalia.com';

        // build the email
        $message = drupal_mail($module, $key, $to_user_mail, LANGUAGE_NONE, array(), $from, FALSE);

        $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
        $message['headers']['Cc'] = 'd.aks@furnitalia.com,pearl@furnitalia.com,ruth@furnitalia.com,jenn@furnitalia.com,audrey@furnitalia.com,david@furnitalia.com,kurt@furnitalia.com';
        //$message['headers']['Reply-To'] = 'reply_to_email@test.com';

        $message['subject'] = 'Furnitalia Leads: ' . count($result) . ' new lead requests';

        $message['body'] = array();
        $message['body'][] = 'There are ' . count($result) . ' new lead requests:<br/>';
        $message['body'][] =  $output;
        $message['body'][] =  '<br/><p>Status as of ' . format_date(REQUEST_TIME, 'custom', 'm-d-Y H:i') . '.</p>';

        // retrieve the responsible implementation for this message.
        $system = drupal_mail_system($module, $key);

        // format the message body.
        $message = $system->format($message);

        watchdog('furnitalia', 'Furnileads email notification %email', array('%email' => '<pre>' . print_r($message, TRUE) . '</pre>'), WATCHDOG_DEBUG, $link = NULL);

        // send e-mail
        $message['result'] = $system->mail($message);

        watchdog('furnitalia', 'Furnileads email notification status %status', array('%status' => $message['result']), WATCHDOG_DEBUG, $link = NULL);
        //watchdog('furnitalia', 'Furnileads email notification last run %lastrun , %seconds_ago seconds ago', array('%lastrun' => format_date($furnilead_notification_last_run, 'custom', 'm-d-Y H:i'), '%seconds_ago' => $notification_seconds_ago), WATCHDOG_DEBUG, $link = NULL);

        variable_set('furnileads_email_notification_last_run', date('Y-m-d H:i', time() ));
    }
}*/

/*function run_followup_leads_cron() {
    $result = db_query("SELECT lid
                            , created
                            , updated
                            , first_name
                            , last_name
                            , email
                            , substr(body, 1, 900) as short_descr
                            , uid
                        FROM {furnileads} fl
                        WHERE fl.status = :lead_status
                            AND IFNULL(fl.category, '') != :category
                        ORDER BY uid,created DESC", array(':lead_status' => 'O', ':category' => 'NEWSLETTER'))->fetchAll(PDO::FETCH_ASSOC);


    if (count($result) > 0) {

        $module = 'furnileads';
        $key = 'notice';

        $from = 'admin@furnitalia.com';

        $followup_list = array();
        foreach ($result as $lead) {

            watchdog('furnitalia', 'Lead: %lead', array('%lead' => '<pre>' . print_r($lead, TRUE) . '</pre>'), WATCHDOG_DEBUG, $link = NULL);

            if (!isset($lead['uid'])) {
                $owner = "unk";
            } else {
                $owner = $lead['uid'];
            }

            if (!is_array($followup_list[$owner])) {
                $followup_list[$owner] = array();
            }

			$cur_date = new DateTime();
            $date_last_updated = new DateTime(date('Y-m-d H:i', $lead['updated']));
            $date_diff = date_diff($date_last_updated, $cur_date);
            if ($date_diff->d >= 7) {
                $followup_list[$owner][] = array(
                    'created' => format_date($lead['created'], 'custom', 'm-d-Y H:i'),
                    'updated' => format_date($lead['updated'], 'custom', 'm-d-Y H:i'),
                    'lead_contact_info' => $lead['first_name'] . ' ' . $lead['last_name'] . ' ' . $lead['email'],
                    'short_descr' => check_plain($lead['short_descr']),
                );
            }
        }

        if (count($followup_list) > 0) {
            watchdog('furnitalia', 'Followup list: %list', array('%list' => '<pre>' . print_r($followup_list, TRUE) . '</pre>'), WATCHDOG_DEBUG, $link = NULL);

            foreach ($followup_list as $owner => $lead_list) {
                if ($owner == 'unk')
                    continue;

                $u = user_load($owner);
                $to_user_mail = $u->mail; //'emil@furnitalia.com';
                $first_name = $u->field_first_name['und'][0]['value'];
                $output = "<table border=\"1\">";
                $output .= "<th>Lead Date</th><th>Last Action Date</th><th>Lead Owner</th><th>Lead Contact</th><th>Short description</th>";
                foreach ($lead_list as $lead) {
                    $temp = "<tr>";
                    $temp .= "<td>" . $lead['created'] . "</td>";
                    $temp .= "<td>" . $lead['updated'] . "</td>";
                    $temp .= "<td>" . $first_name . "</td>";
                    $temp .= "<td>" . $lead['lead_contact_info'] . '</td>';
                    $temp .= "<td>" . $lead['short_descr'] . '</td>';
                    $temp .= "</tr>";

                    $output .= $temp;
                }
                $output .= "</table>";

                // build the email
                $message = drupal_mail($module, $key, $to_user_mail, LANGUAGE_NONE, array(), $from, FALSE);
                $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
                $message['headers']['Cc'] = 'emil@furnitalia.com';
                $message['subject'] = 'Furnitalia Leads: ' . count($lead_list) . ' leads pending follow-up';
                $message['body'] = array();
                $message['body'][] = 'You have ' . count($lead_list) . ' leads that are pending follow-up:<br/>';
                $message['body'][] = $output;
                $message['body'][] = '<br/><p>Status as of ' . format_date(REQUEST_TIME, 'custom', 'm-d-Y H:i') . '.</p>';

                // retrieve the responsible implementation for this message.
                $system = drupal_mail_system($module, $key);

                // format the message body.
                $message = $system->format($message);

                // send e-mail
                $message['result'] = $system->mail($message);

            }

            //variable_set('furnileads_followup_notification_last_run', date('Y-m-d H:i', time()));
        }
    }
}*/

/**
 * Comparator Function
 * @param $lead_info_a
 * @param $lead_info_b
 */
function compare_lead_dates($lead_info_a, $lead_info_b) {
    $default = time();
    $d1 = (isset($lead_info_a['date_create_raw']) ? $lead_info_a['date_create_raw'] : $default);
    $d2 = (isset($lead_info_b['date_create_raw']) ? $lead_info_b['date_create_raw'] : $default);
    $d1 = new DateTime('@' . $d1);
    $d2 = new DateTime('@' . $d2);

    return $d1 <= $d2 ? -1 : 1;
}
