<?php
//Misc website pages callbacks

function _get_category_image_map()
{
  $category_img_map = array(
      'all' => array(
          '1' => 'all_collections/all_collections_living.jpg',
          '16' => 'all_collections/all_collections_dining.jpg',
          '15' => 'all_collections/all_collections_accessories.jpg',
          '14' => 'all_collections/all_collections_bedroom.jpg',
          '19' => 'all_collections/all_collections_lighting.jpg',
          '65' => 'all_collections/all_collections_occasional.jpg',
          '18' => 'all_collections/all_collections_rugs_pillows.jpg',
          '73' => 'all_collections/all_collections_kitchen.jpg',
          '17' => 'all_collections/all_collections_office.jpg',
          'headers' => array(
              array(
                  'path' => 'all_collections/all_collections_header.jpg',
                  'title' => 'Furnitalia - Modern Italian Furniture Collections',
                  'link' => '',
              ),
          ),
      ),
      'italia' => array(
          '1' => 'natuzzi_italia/natuzzi_italia_living.jpg',
          '16' => 'natuzzi_italia/natuzzi_italia_dining.jpg',
          '15' => 'natuzzi_italia/natuzzi_italia_accessories.jpg',
          '14' => 'natuzzi_italia/natuzzi_italia_bedroom.jpg',
          '19' => 'natuzzi_italia/natuzzi_italia_lighting.jpg',
          '65' => 'natuzzi_italia/natuzzi_italia_occasional.jpg',
          '18' => 'natuzzi_italia/natuzzi_italia_rugs_pillows.jpg',
          'headers' => array(
              array(
                // 'path' => 'natuzzi_italia/natuzzi_italia_header.jpg',
                  'path' => 'natuzzi_italia/natuzzi_italia_summer_sale_header.jpg',
                //'path' => 'natuzzi_italia/natuzzi_italia_memorial_day_header.jpg',
                  'title' => 'Natuzzi Italia - Modern Italian Furniture Collections',
                  'link' => '',
              ),
          ),
      ),
      'editions' => array(
          '1' => 'natuzzi_editions/natuzzi_editions_living.jpg',
          '65' => 'natuzzi_editions/natuzzi_editions_occasional.jpg',
          '18' => 'natuzzi_editions/natuzzi_editions_rugs_pillows.jpg',
          'headers' => array(
              array(
                  'path' => 'natuzzi_editions/natuzzi_editions_header.jpg',
                  'title' => 'Natuzzi Editions - Modern Italian Furniture Collections',
                  'link' => '',
              ),
          ),
      )
  );

  return $category_img_map;
}

/**
 * Collections page handler
 */
function furn_global_collections_page($term = NULL)
{

  global $conf;

  if (arg(1) == "") {   //if it's just /collections/ page


    /*$output = '<ul id="categoryList">';

    $thumbs = _get_category_list('all');
    $cat_terms = taxonomy_term_load_multiple($thumbs);
    $cat_terms = taxonomy_term_view_multiple($cat_terms);

    foreach($cat_terms['taxonomy_terms'] as $cat_term) {
      $output .= '<li>' . drupal_render($cat_term)  . '</li>';
    }

    $output .= '</ul>';
    $build = array();
    $build['desc'] = array(
      '#markup' => $output, //theme('all-cats'),
    );*/

    if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'desktop') {
      return furn_global_brand_categories_page('all');
    } else {
      $build = array();
      $build['desc'] = array(
          '#markup' => theme('all-cats'),
      );
    }


    //if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'desktop') {
    //  drupal_add_js(drupal_get_path('theme', 'furnitheme') . "/js/jquery.bgpos.js");
    //  drupal_add_library('system', 'drupal.ajax');

    /*$build['add'] = array(
      '#markup' => '<div id="menu-pic-wrapper">
                      <div id="menu-pic" class="menu-hover loading">&nbsp;</div>
                    </div>',
    );*/
    //}

    return $build;

  } else {        //something else is after "collections/"

    $path = explode("/", current_path());
    array_shift($path);       // remove the 'collections' part
    $path = implode("/", $path);  // get normal path without "collections"

    $new_path = drupal_lookup_path("source", $path); //attempt to find normal path
    if ($new_path) {
      drupal_goto($path);
    } else {
      drupal_goto('collections'); //redirect to root collections page
    }
  }
}

function furn_global_brand_categories_page($brand, $include_header=TRUE)
{

  $category_img_map = _get_category_image_map();

  //Natuzzi Italia categories
  $output = '';

  $thumbs = _get_category_list($brand);

  $cat_terms = taxonomy_term_load_multiple($thumbs);
  $cat_terms = taxonomy_term_view_multiple($cat_terms);

  $files_dir = variable_get('file_public_path', conf_path() . '/files');
  $category_img_path = $files_dir . '/category_images';

  //$i = true;

  $header_img_html = '';
  if ($include_header) {
    if (isset($category_img_map[$brand]['headers']) && is_array($category_img_map[$brand]['headers'])) {

      if (count($category_img_map[$brand]['headers']) > 1) {
        $header_img_html .= '<ul id="categoryTopImg" class="bxslider" style="margin:0 0 0 0;">';
        foreach ($category_img_map[$brand]['headers'] as $header_img) {

          $header_img_html .= '<li>';

          if (!empty($header_img['link'])) {
            $header_img_html .= l('<img src="' . $category_img_path . '/' . $header_img['path'] . '" alt="' . $header_img['title'] . '" />',//theme("image", array("path" => $category_img_path . '/' . $header_img['path'], "alt" => $header_img['title'], "attributes" => array("ref" => ""))),
              $header_img['link'],
              array("html" => TRUE));
          } else {
            $header_img_html .= '<img src="' . $category_img_path . '/' . $header_img['path'] . '" alt="' . $header_img['title'] . '" />';//theme("image", array("path" => $header_img['path'], "title" => $header_img['title'], "attributes" => array("ref" => "")));
          }
          $header_img_html .= '</li>';
        }
        $header_img_html .= '</ul>';
      } else {

        $header_img_html = '<img src="' . $category_img_path . '/' . $category_img_map[$brand]['headers'][0]['path'] . '" alt="' . $category_img_map[$brand]['headers'][0]['title'] . '" />';

      }
    }
    $output .= $header_img_html;
  }


  /*$promo_text = "<section class='memorial_day_vibes' style='border: 3px #70a8b5 solid;margin:auto;width:93%;bottom: 2.5em;position: relative;'><div class='text' style='padding:25px;text-align: center;box-sizing: border-box'>";
  $mobile_h2_css = '';

  if ($is_mobile) {
      $mobile_h2_css = 'font-size: 1.36em;margin-bottom: .5em;';
  }
  $promo_text .= "<h2 style='font-weight: bold;text-transform: uppercase;color: #2d2c2c;letter-spacing: 3px;font-size: 2em;margin-bottom:0; ". $mobile_h2_css  ."'>Memorial Day Vibes</h2>";
  $promo_text .= "<h3 style='margin-top:0;font-weight: 400;font-size: 1.5em;line-height: 130%;color: #4c4545;'>Until May 31st, get up to <span style='font-weight: bold;color:#000'>20% off</span> all Natuzzi Italia.</h3>";
  $promo_text .= "<div style='color: #4c4545;'>Limited-time special vendor participation pricing. <a class='furn-red' href='/contact-us'>Contact us</a> for promo details.</div>";
  $promo_text .= "</div></section>";*/
  $promo_text = "";

  $output .= $promo_text;

  $output .= '<ul id="categoryList">';

  foreach ($cat_terms['taxonomy_terms'] as $cat_term) {

    $term = $cat_term['#term'];

    if (!is_object($term)) {
      continue;
    }

    $cat_url = drupal_get_path_alias("taxonomy/term/$term->tid"); //url($uri['path'], $uri['options']);

    //!! Tweak term URL depending on if current page is natuzzi brand page
    if ($brand == 'italia') {
      $cat_url = base_path() . 'natuzzi-italia/' . $cat_url;
    } else if ($brand == 'editions') {
      $cat_url = base_path() . 'natuzzi-editions/' . $cat_url;
    }

    $category_name = check_plain($term->name);

    if (isset($category_img_map[$brand][$term->tid])) {
      $output .= '<li>';
      $output .= '<a href="' . $cat_url . '" title="' . $category_name . '">';
      $output .= '<img src="' . $category_img_path . '/' . $category_img_map[$brand][$term->tid] . '" alt="Category: ' . $category_name . '" class="brand-category-img" />';
      $output .= '</a></li>';
    } else {
      if (is_array($cat_term)) {
        $output .= '<li>' . drupal_render($cat_term) . ' ' . $cat_term['#term']->tid . '</li>';
      }
    }
  }

  $output .= '</ul>';

  // add Re-Vive banner for Italia landing page
  if ($brand == 'italia') {
    drupal_add_css(drupal_get_path("theme", "furnitheme")."/fonts/natuzzi-landing-fonts.css");
    $output .= '<div class="re-vive-banner"><a href="re-vive" title="Re-Vive Recliners by Natuzzi">
        <img src="'.$category_img_path.'/all_collections/Natuzzi_Re-Vive_recliners.png" alt="Re-Vive Recliners by Natuzzi"/>
        <span class="caption"><span>Natuzzi Re-Vive</span><br/>the world\'s first performance recliner</span>
        <span class="learn-more">Learn More »</span>
        </a></div>';
  }


  $build = array();
  $build['desc'] = array(
      '#markup' => $output, //theme('all-cats'),
  );

  $build['brand_info'] = array(
      '#markup' => theme('natuzzi-cat'),
      '#prefix' => '<br/>',
  );

  return $build;

}

function _get_category_list($brand = 'all')
{

  $res = array();

  $all_items = array();
  $italia_items = array();
  $editions_items = array();

  _generate_menu_items($all_items, $italia_items, $editions_items);

  $cat_array = array();
  if ($brand == 'all') {
    $cat_array = &$all_items;
  } else if ($brand == 'italia') {
    $cat_array = &$italia_items;
  } else if ($brand == 'editions') {
    $cat_array = &$editions_items;
  }

  uasort($cat_array, 'category_cmp');    //sort categories array by weight
  foreach ($cat_array as $tid => $term_detail) {
    $res [] = $tid;
  }

  return $res;
}

/*function _get_subcategory_list($brand = 'all', $category) {

  $res = array();

  $all_items = array();
  $italia_items = array();
  $editions_items = array();

  _generate_menu_items($all_items, $italia_items, $editions_items);

  if ($brand == 'all') {
    uasort($all_items, 'category_cmp');    //sort categories array by weight
    if (count($all_items[$category->tid])) {
      foreach ($all_items[$category->tid]['sub'] as $tid => $term_detail) {  
        $res []= $tid;
      }
    }
  } else if ($brand == 'italia') {
    uasort($italia_items, 'category_cmp');    //sort categories array by weight
    if (count($italia_items[$category->tid])) {
      foreach ($italia_items[$category->tid]['sub'] as $tid => $term_detail) {  
        $res []= $tid;
      }
    }    
  } else if ($brand == 'editions') {
    uasort($editions_items, 'category_cmp');    //sort categories array by weight
    if (count($editions_items[$category->tid])) {
      foreach ($editions_items[$category->tid]['sub'] as $tid => $term_detail) {  
        $res []= $tid;
      }
    }   
  }

  return $res;

}*/

/**
 * Page callback for coupon page
 */
function furn_global_coupon_page()
{

  //drupal_set_title('Spring into SALE event at Furnitalia');
  $query_params = drupal_get_query_parameters();
  if (isset($query_params['success']) && isset($query_params['sid'])) {
    module_load_include("inc", 'webform', 'includes/webform.submissions');
    $submissions = webform_get_submissions(array('nid' => 834, 'sid' => $query_params['sid']));

    if (!empty($submissions)) {
      $build['message'] = array(
          '#markup' => "<div class=\"thanks-message furn-red\" style=\"font-size:1.5em\">Your personal 10% OFF coupon will be e-mailed to you. Please contact us if you don't receive your coupon within 24 hours.</div>",
      );

      $build['sharetext'] = array(
          '#markup' => '<h3>Share the offer, <br/>your friends will thank you for it</h3>',
      );


      // First get all of the options for the sharethis widget from the database:
      $data_options = sharethis_get_options_array();

      // Get the full path to insert into the Share Buttons.
      $mPath = url('coupon', array('absolute' => TRUE));
      $mTitle = "Get 10% Discount Coupon on Contemporary Furniture at Furnitalia!";
      $build['share'] = array(
          '#tag' => 'div', // Wrap it in a div.
          '#type' => 'html_tag',
          '#attributes' => array('class' => 'sharethis-buttons'),
          '#value' => theme('sharethis', array('data_options' => $data_options, 'm_path' => $mPath, 'm_title' => $mTitle)),
          '#weight' => intval(variable_get('sharethis_weight', 10)),
      );
      return $build;
    }
  }

  $webform_nid = 834;
  $node = node_load($webform_nid);

  module_load_include("inc", "furn_global", "furn_global");

  $coupon_code = "SPRING" . get_random_string("123456789", 4);
  $node->webform['components'][2]['value'] = $coupon_code;

  $submission = (object)array();
  $enabled = TRUE;
  $preview = FALSE;
  $form = drupal_get_form('webform_client_form_' . $webform_nid, $node, $submission, $enabled, $preview);

  unset($form['submitted']['email']['#title']);
  $form['submitted']['email']['#attributes'] = array('autofocus' => array(''));
  $form['actions']['submit']['#theme_wrappers'] = array("image_button");
  $form['actions']['submit']['#button_type'] = "image";
  $form['actions']['submit']['#src'] = base_path() . 'sites/default/files/promo/coupons/submit_button.png';

  /*$build['coupon'] = array(
    '#markup' => '<img src="' . base_path() . 'sites/default/files/promo/spring_sale/SpringSale_coupon_header.png' . '"/>',
    '#prefix' => '<div id="coupon-form">',
  );
  $build ['form'] = array(
    '#markup' => drupal_render($form),
  );
  $build['disclaimer'] = array(
    '#markup' => '<div id="coupon-disclaimer" class="antialias"> * OFFER APPLIES TO NEW EMAIL SUBSCRIPTIONS ONLY.  OFFER IS NOT APPLICABLE 
    ON PRIOR PURCHASES AND CLEARANCE ITEMS AND CANNOT BE COMBINED WITH ANY 
    OTHER OFFERS.  BRAND MRP RESPRICTIONS MAY APPLY. BDI, RE-VIVE AND NATUZZI 
    ITALIA ORDERS ARE EXCLUDED FROM THIS OFFER. 
    DISCOUNT WILL BE DELIVERED TO YOUR EMAIL ADDRESS</div>',
    '#suffix' => '</div>',
);*/

  $build['coupon'] = array(
      '#markup' => 'Subscribe to Furnitalia newsletter to receive coupons and special promotions!',
  );

  drupal_add_library('system', 'drupal.ajax');
  $webform_nid = 33;
  $node = node_load($webform_nid);
  //$node->webform['components'][3]['value'] = 'stressless';
  $node->title = 'Sign up for news and promotions';
  $submission = (object)array();
  $enabled = TRUE;
  $preview = FALSE;
  $form = drupal_get_form('webform_client_form_' . $webform_nid, $node, $submission, $enabled, $preview);
  $form['submitted']['label']['#markup'] = 'Sign up for news and promotions';
  $form['submitted']['label']['#title_display'] = 'invisible';
  $form['submitted']['email']['#title_display'] = 'invisible';
  $form['#prefix'] = '<div id="promoEmailSignupForm">';
  $form['#suffix'] = '</div>';

  $build['coupon'] = $form;

  return $build;

}

/**
 * Page callback for coupon page
 */
function furn_global_promo_page($promo_name)
{

  $promos = furn_global_get_promos();

  // default settings
  $form_type = 'default'; // webform layout
  $template_name = "";
  $campaign_channel = "";
  $webform_nid = 32;
  $webform_title = 'CONTACT';
  $webform_subtitle = 'Receive more info, pricing, or book an in-store appointment';

  if (!array_key_exists($promo_name, $promos)) {
    return '<p style="font-size:1.2em;">This promotion has expired. <br/>Please sign up for our newsletter to receive special discounts and promotions.</p>';
  }
  $promo = $promos[$promo_name];

  if (isset($promo['landing']) && $promo['landing']) {
    drupal_set_title($promo['title']);
  }

  // get additional promo page parameters
  if (isset($promo['form_type'])) {
    $form_type = $promo['form_type'];
  }

  if (isset($promo['template_name']) && !empty($promo['template_name'])) {
    $template_name = $promo['template_name'];
  }

  if (isset($promo['webform_nid'])) {
    $webform_nid = $promo['webform_nid'];
  }
  if (isset($promo['webform_title'])) {
    $webform_title = $promo['webform_title'];
  }
  if (isset($promo['webform_subtitle'])) {
    $webform_subtitle = $promo['webform_subtitle'];
  }

  if (isset($promo['bootstrap'])) {
    drupal_add_css(drupal_get_path('theme', 'furnitheme') . "/css/bootstrap.css", array(
      'weight' => -10,
      'every_page' => FALSE,
    ));
  }


  drupal_add_library('system', 'drupal.ajax');

  // WEBFORM
  //---------------------------------


  $is_renovation_sale = FALSE;
  $is_black_friday_sale = false;
  if (stristr(current_path(), 'black-friday-sale') ||
      stristr(current_path(), 'december-clearance')) {
    $is_black_friday_sale = TRUE;
    // $webform_nid = 834;
    $form_caption = "December Clearance SALE";
  }

  // prepare the webform
  $node = node_load($webform_nid);
  $submission = (object)array();
  $enabled = TRUE;
  $preview = TRUE;
  $resume_draft = FALSE;
  $form = drupal_get_form('webform_client_form_' . $webform_nid, $node, $submission, $resume_draft);

  // customize webform markup and controls
  $is_natuzzi_promo_page = stristr(current_path(), 'natuzzi-harmony-friday-sale');
  if ($is_natuzzi_promo_page) {

    $form = update_form_landing_page_layout($form);

  } else if ($is_renovation_sale) {

    $form = update_special_sale_form_layout($form);
    $form['#prefix'] = '<div id="renovationSaleForm">';
    $form['#suffix'] = '</div>';

    $form['submitted']['submission_code']['#value'] = $campaign_channel;
    //dsm($form);
  } else if ($is_black_friday_sale) {

    //$form = update_black_friday_coupon_form_layout($form);
    $markup = '<h3 class="furn-red" style="text-align: center;font-size: 2em;padding: 0;margin: 0 0 .5em 0;">' . $form_caption . '</h3>';
    $markup .= '<div style="padding: 0 0 1.5em 0;font-size: 1.4em;text-align: center;">Receive more info, pricing, or book an in-store appointment</div>';
    $form['submitted']['label']['#markup'] = $markup;
    $form['#prefix'] = '<div id="requestFormContainer" class="coupon-form">';
    $form['#suffix'] = '</div>';


  } else {

    if ($form_type === 'natuzzi_side_form') {

      $form = update_form_landing_page_layout($form);

    } else {

      $markup = '<h3 class="furn-red" style="text-align: left;font-size: 2rem;padding: 0;margin: 0 0 .5em 0;letter-spacing:11px">' . $webform_title . '</h3>';
      $markup .= '<div style="padding: 0 0 1.5em 0;font-size: 1.4rem;text-align: left;">'. $webform_subtitle .'</div>';
      $form['submitted']['label']['#markup'] = $markup;
      $form['submitted']['label']['#weight'] = -1;
      if (isset($form['submitted']['message']['#attributes'])) {
        $form['submitted']['message']['#attributes']['placeholder'] = array(
            'Your mesage here',
        );
      }
      $form['#prefix'] = '<div id="requestFormContainer">'; //promoContactForm
      $form['#suffix'] = '</div>';
    }

  }

  // webform render array to be passed to template
  $build['webform'] = array (
      '#theme' => 'webform_view',
      '#node' => $node,
      '#teaser' => $preview,
      '#page' => true,
      '#form' => $form,
      '#enabled' => $enabled,
  );


  // variables for template
  $variables = [
    'promo_name' => $promo_name,
    'content' => $build,
  ];

  // if a specific template file name was specified
  if ($template_name) {
    $variables['template_name'] = $template_name;
  }

  return theme('furn_promo', $variables);

}

function update_form_landing_page_layout($form)
{

  $form['submitted']['first_name']['#container_class'] = 'form-group';
//    $form['submitted']['first_name']['#value'] = '';
  $form['submitted']['first_name']['#attributes'] = array(
      'class' => array('form-control', 'form-input'),
      'required' => 'true',
      'placeholder' => "Name",
      'value' => "",
  );

  $form['submitted']['email']['#container_class'] = 'form-group';
//    $form['submitted']['email']['#value'] = '';
  $form['submitted']['email']['#attributes'] = array(
      'class' => array('form-control', 'form-input'),
      'required' => 'true',
      'placeholder' => "E-mail",
  );

  $form['submitted']['message']['#container_class'] = 'form-group';
//    $form['submitted']['message']['#value'] = '';
  $form['submitted']['message']['#attributes'] = array(
      'class' => array('form-control', 'form-input'),
      'required' => '',
      'placeholder' => "Comments",
      'value' => "",
  );
  $form['submitted']['first_name']['#title_display'] = 'invisible';
  $form['submitted']['email']['#title_display'] = 'invisible';
  $form['submitted']['message']['#title_display'] = 'invisible';
  $form['#attributes']['class'][] = 'form-data';

  return $form;
}


function update_special_sale_form_layout($form)
{

//  dpm($form);

  $form['actions']['submit']['#attributes']['class'] []= 'btn btn-primary';

  /*$form['submitted']['city']['#prefix'] = '<div class="form-items-group clearfix">';
  $form['submitted']['city']['#container_class'] = "form-item-inline";
  $form['submitted']['state']['#container_class'] = "form-item-inline";
  $form['submitted']['zip']['#container_class'] = "form-item-inline";
  $form['submitted']['zip']['#suffix'] = '</div>';

  $form_elements = array('first_name', 'address', 'city', 'state', 'zip', 'email', 'phone');
  foreach ($form['submitted'] as $key => $elem) {
    if (in_array($key, $form_elements)) {
      $form['submitted'][$key]['#theme_wrappers'] = array('renovation_sale_webform_element');
    }
  }*/

  return $form;
}

function update_black_friday_coupon_form_layout($form)
{

  $form['submitted']['zip']['#container_class'] = "form-item-inline";
  $form['submitted']['zip']['#suffix'] = '</div>';

  $form_elements = array('full_name', 'zip', 'email');
  //$form_elements = [];
  foreach ($form['submitted'] as $key => $elem) {
    if (in_array($key, $form_elements)) {
      $form['submitted'][$key]['#theme_wrappers'] = array('renovation_sale_webform_element');
    }
  }

  return $form;
}

/**
 * Display the list of items that are in current user's favorites list
 */
function furn_global_favorites_list()
{
  global $user;

  /*if ($user->uid == 0) {
    //not authenticated user
    drupal_set_message('Please log in to access your favorites list');
    drupal_goto('user/login');
    return;
    }*/

  $favorites = flag_get_user_flags('node', NULL, $user->uid);

  if (!isset($favorites['favorites']) || empty($favorites['favorites'])) {
    return "Your don't have any favorites yet. <br/> You can add items to favorites by clicking on 'Add to favorites' button when viewing product descriptions.";
  }

  $nids = array();

  foreach ($favorites['favorites'] as $nid => $flag_obj) {
    $nids [] = $nid;
  }

  $nodes = node_load_multiple($nids);
  $build = furn_global_prepare_node_attributes($nodes);

  unset($build['options']);

  //print flag_create_link('favorites', $node->nid);

  return $build;
}

/**
 * Shows the Natuzzi Editions quick-delivery program information
 * @throws Exception
 */
function furn_global_quick_delivery_page($brand, $ajax = FALSE)
{

  if (!isset($brand)) {
    drupal_goto('shipping-deliveries');
  }

  $output = "";
  switch ($brand) {
    case "italia":
      $output = theme('natuzzi_italia_qt', array('ajax' => $ajax));
      break;
    case "editions":
      $output = theme('natuzzi_editions_qt', array('ajax' => $ajax));
      break;
    default:
      drupal_goto("shipping-deliveries");
      break;
  }

  if (!$ajax) {
    drupal_add_css(drupal_get_path('theme', 'furnitheme') . '/css/qt_page_responsive.css', array(
        'group' => 101
    ));
    return $output;
  } else {
    print $output;
    exit();
  }
}

/**
 * Prepares virtual showroom page
 * @throws Exception
 */
function furn_global_virtual_showroom_page()
{
  drupal_set_title("Visit Furnitalia 3D Virtual Showroom");
  $output = theme('virtual_showroom');
  return $output;
}

/*function furn_global_collection_subcategoriezs_page($category) {
  $output = '<ul id="categoryList">';

  $thumbs = _get_subcategory_list('all', $category);
  $cat_terms = taxonomy_term_load_multiple($thumbs);
  $cat_terms = taxonomy_term_view_multiple($cat_terms);

  foreach($cat_terms['taxonomy_terms'] as $cat_term) {
    $output .= '<li>' . drupal_render($cat_term) . '</li>';
  }

  $output .= '</ul>';
  $build = array();
  $build['desc'] = array(
    '#markup' => $output, //theme('all-cats'),
  );

  return $build;

}*/

/*function furn_global_brand_subcategories_page($brand, $category) {
  $output = '<ul id="categoryList">';

  $thumbs = _get_subcategory_list($brand, $category);
  $cat_terms = taxonomy_term_load_multiple($thumbs);
  $cat_terms = taxonomy_term_view_multiple($cat_terms);

  foreach($cat_terms['taxonomy_terms'] as $cat_term) {
    $output .= '<li>' . drupal_render($cat_term) . '</li>';
  }

  $output .= '</ul>';
  $build = array();
  $build['desc'] = array(
    '#markup' => $output, //theme('all-cats'),
  );

  return $build;
}
*/
