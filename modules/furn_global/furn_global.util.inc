<?php

function _process_item_node_presave($node) {

	//if items is unpublished
	$node_is_published = TRUE;
	if ($node->status == 0) {
		$node_is_published = FALSE;
	}

	$show_sale_prices = variable_get('show_sale_prices', FALSE);

	if ($node_is_published) {

		//sync special price and sell price
		$special_price = field_get_items('node', $node, 'field_special_price');
		if (!$special_price) {
			//set default special price equal to sell price
			$node->field_special_price = array('und' => array(array('value' => $node->sell_price))); 
		} else {
			if (intval($special_price) == 0) {
				//if special price is 0, reset to sell price
				$node->field_special_price['und'][0]['value'] = $node->sell_price;
			}
		}
	  
	  	//determine item final price
	  	if ($show_sale_prices) {
	  		$item_price = $node->field_special_price['und'][0]['value'];
	  	} else {
	  		$item_price = $node->sell_price;
	  	}
	  	

	  	//sync FINAL price and sell/special price
	  	$final_price = field_get_items('node', $node, 'field_final_price');	  	

	  	//update item's final price
		if ($final_price) {
			$node->field_final_price['und'][0]['value'] = $item_price;
		} else {
			$node->field_final_price = array(
		  		'und' => array(0 => array('value' => $item_price))
			);
		}
	}

	//------------------------------------------------------------------------
	//sync price, categories, and availability info in the related collection
	//------------------------------------------------------------------------
	
	$collection_old = FALSE;
	$collection_new = field_get_items('node', $node, 'field_collection');

	if (isset($node->original)) {
		$collection_old = field_get_items('node', $node->original, 'field_collection');
	}

	//item was switched to a different collection
	//need to update old collection as well
	if ($collection_old && $collection_old != $collection_new) {
		//reset collection info
		_update_collection_info($collection_old[0]['nid'], null);
	}

	if ($collection_new) { //if item belongs to a collection, update collection info
		//set updated values to the collection
		_update_collection_info($collection_new[0]['nid'], $node);	
	}

  	return $node;
}

/**
 * Update collection info (min_price, category, availability) to stay in sync with items belonging to the collection
 */
function _update_collection_info($collection_id, $updated_item) {
	
	$show_sale_prices = variable_get('show_sale_prices', FALSE);
	$collection_node = node_load($collection_id);    
	$categories = array();
	$availabilities = array();

	if ($updated_item && $updated_item->status) {
		$coll_price = $updated_item->field_final_price['und'][0]['value'];
	} else {
		$coll_price = 99999.0;	//default max
	}

	$item_nodes = _get_items_belonging_to_collection($collection_node->nid);
	//dsm($item_nodes);
	
    if (count($item_nodes) > 1) {

		//make collection published if there are more than 1 items in a collection
		$collection_node->status = 1;

      	foreach($item_nodes as $nid => $temp_node) {

      		if ($updated_item && $updated_item->nid == $nid) {
      			continue;
      		}

     		//get price for other items in the collection
        	if ($show_sale_prices) {
            	$price = $temp_node->field_special_price['und'][0]['value'];
          	} else {
            	$price = $temp_node->sell_price;
          	}

          	//pick min price
          	if ($price < $coll_price) {
            	$coll_price = $price;
          	}

      		//get all categories from related nodes
      		foreach($temp_node->field_category['und'] as $k => $value) {
        		$categories [$value['tid']]= $value['tid'];
      		}

      		//get all availabilities for related nodes
      		foreach($temp_node->field_availability['und'] as $k => $value) {
        		$availabilities [$value['value']] = $value['value'];
      		}

        }

      //if there is only 1 item in collection and item is unpublished, collection
      //needs to be unpublished also
    } else {   

		if ($updated_item) {
			//only 1 item in collection
			//unpublish collection if item is unpublished
			if ($updated_item->status == 0) {
				$collection_node->status = 0;
			} else {
				$collection_node->status = 1;
			}
		} else {
			$collection_node->status = 0;	
		}    	
    }


    if ($updated_item && $updated_item->status) {
	    //include categories of the currently updated item
	    foreach($updated_item->field_category['und'] as $k => $value) {
	      $categories [$value['tid']]= $value['tid'];
	    }

	    //include availabilities of the currently updated item
	    foreach($updated_item->field_availability['und'] as $k => $value) {
	      $availabilities [$value['value']] = $value['value'];
	    }
	}

    //prepare updated list of categories for the collection
    $collection_new_categories = array();
    foreach($categories as $key => $tid) {
      $collection_new_categories []= array('tid' => $tid);
    }
    if (count($collection_new_categories) > 0) {
    	$collection_node->field_category['und'] = $collection_new_categories;	
    }

    //prepare updated list of availabilities for the collection
    $collection_availabilities = array();
    foreach($availabilities as $key => $value) {
      $collection_availabilities []= array('value' => $value);
    }
    if (count($collection_availabilities) > 0) {
    	$collection_node->field_availability['und'] = $collection_availabilities;	
    }    

    //set price for collection
    $coll_final_price = field_get_items('node', $collection_node, 'field_final_price');	  	
    if ($coll_final_price) {
      $collection_node->field_final_price['und'][0]['value'] = $coll_price;           				//update price
    } else {
      $collection_node->field_final_price = array('und' => array(array('value' => $coll_price)));   //set price
    }

    //dsm('Updated collection:');
    //dsm($collection_node);

    node_save($collection_node);
}

/**
 * Get items associated to a given collection
 */
function _get_items_belonging_to_collection($coll_nid) {
  $query = db_select('field_data_field_collection', 'fc');
  //$query->join('field_revision_field_special_price', 'sp', 'sp.entity_id = fc.entity_id');
  $query->join('node', 'n', 'n.nid = fc.entity_id');
  $query->fields('fc', array('entity_id'));
  //$query->fields('sp', array('field_special_price_value'));
  $query->condition('n.status', '1', '=');
  $query->condition('fc.field_collection_nid', $coll_nid, '=');
  //$query->orderBy('field_special_price_value', 'ASC');

  $res = $query->execute();

  $nids = array();
  $item_nodes = array();
  
  foreach($res as $record) {
      $nids[] = $record->entity_id;
  }
  
  if (count($nids)) {
  	$item_nodes = node_load_multiple($nids);
  }

  return $item_nodes;
}

/**
 * Execution of the request.
 *
 * @param string $url
 * @param string $method
 * @param string $parameters
 * @param array $headers
 * @param string $cookie
 * @param integer $timeout
 *
 * @return array
 *
 * @access protected
 *
 * @throws \Exception
 */
function curlRequest($url, $method = 'GET', $parameters = '', $headers = array(), $cookie = '/tmp/cookie.txt', $timeout = 30) {
    if ($method == 'GET' && $parameters) {
        $url .= "?$parameters";
    }

    // Get curl handler or initiate it.
    $curl = curl_init();

    // Set general arguments.
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_FAILONERROR, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie);
    curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie);

    // Reset some arguments, in order to avoid use some from previous request.
    curl_setopt($curl, CURLOPT_POST, FALSE);

    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    if ($method == 'POST' && $parameters) {
        curl_setopt($curl, CURLOPT_POST, TRUE);

        // Encode parameters if them already not encoded in json.
        //        if (!$this->isJson($parameters)) {
        $parameters = http_build_query($parameters);
        //        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
    }

    // receive server response ...
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    $errno = curl_errno($curl);
    $error = curl_error($curl);

    curl_close ($curl);

    if ($errno) {
        throw new Exception($error, $errno);
    }

    $var = var_export($response, TRUE);
    $status = var_export($statusCode, TRUE);
    watchdog('furnitalia', 'Natuzzi get products call. Raw Result: %status ; %res', array('%status' => $status, '%res' => $var), WATCHDOG_INFO);

    $result = drupal_json_decode($response);

    if ($statusCode >= 400) {
        throw new Exception('Error', $statusCode);
    }

    return $result;

    return isset($result['template']) ? array($result['template']) : array();
}
