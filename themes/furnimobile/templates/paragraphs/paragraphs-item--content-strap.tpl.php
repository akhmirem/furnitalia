<div class="<?php print $classes; ?> component-content-strap"<?php print $attributes; ?>>
  <div class="container-fluid">
    <div class="row">
      <?php foreach ($columns as $key => $col): ?>
          <?php print render($col); ?>
      <?php endforeach; ?>
    </div>
  </div>
</div>
