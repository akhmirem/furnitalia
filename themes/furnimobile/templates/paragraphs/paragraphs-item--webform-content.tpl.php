
<div class="<?php print $classes; ?> component-webform-content" <?php print $attributes; ?>>

  <div class="component-webform-content__container">
    <div class="row">

      <?php
        $col_class = 'col-md-6';
        $col2_class = 'col-md-6';
        $content_body_class = '';

        /*if ($paragraph->field_alignment['und'][0]['value'] && ($paragraph->field_alignment['und'][0]['value'] == 'right')) {
          $col_class = 'col-md-6 col-md-push-6';
          $col2_class = 'col-md-6 col-md-pull-6';
          $content_body_class = 'component-webform-content__body--webform-right';
        }*/

        if ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '1_4')) {
          $col_class = 'col-md-3';
          $col2_class = 'col-md-9';
        } elseif ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '2_3')) {
          $col_class = 'col-md-4';
          $col2_class = 'col-md-8';
        }
        if ($paragraph->field_alignment['und'][0]['value'] && ($paragraph->field_alignment['und'][0]['value'] == 'right')) {
          if ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '1_4')) {
            $col_class = 'col-md-3 col-md-push-9';
            $col2_class = 'col-md-9 col-md-pull-3';
          } elseif ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '2_3')) {
            $col_class = 'col-md-4 col-md-push-8';
            $col2_class = 'col-md-8 col-md-pull-4';
          } else {
            $col_class = 'col-md-6 col-md-push-6';
            $col2_class = 'col-md-6 col-md-pull-6';
          }
          $content_body_class = 'component-webform-content__body--webform-right';
        }

      ?>

      <div class="component-webform-content__col <?php print $col_class; ?>">
        <?php if ($paragraph->field_text_long['und'][0]['value']): ?>
          <div class="component-webform-content__intro">
            <?php print render($content['field_text_long']); ?>
          </div>
        <?php endif; ?>

        <?php if ($paragraph->field_webform['und'][0]['nid']): ?>
          <div class="component-webform-content__form">
            <?php print render($content['field_webform']); ?>
          </div>
        <?php endif; ?>

      </div>
      <div class="component-webform-content__col <?php print $col2_class; ?>">
        <?php if ($paragraph->field_paragraphs_reference['und'][0]['value']): ?>
          <div class="component-webform-content__body <?php print $content_body_class; ?>">
            <?php print render($content['field_paragraphs_reference']); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </div>
</div>