<?php $body_attributes = array() ?>
<?php $overlay_attributes = array() ?>
<?php $extra_attributes = array() ?>
<?php $style_value = '' ?>

<!-- Background Color. -->
<?php if (field_get_items('paragraphs_item', $paragraphs_item, 'field_bg_color')): ?>
  <?php $bg_color = $paragraphs_item->field_bg_color['und'][0]['rgb'] ?>
  <?php $style_value = $style_value . 'background-color: ' . $bg_color . ';' ?>
  <?php $extra_attributes['style'] = $style_value ?>
<?php endif; ?>

<!-- Overlay / Background Image. -->
<?php if ($paragraphs_item->field_image && $paragraphs_item->field_image['und'][0]['fid']): ?>
  <?php $opacity_value = 1; ?>
  <?php if ($paragraphs_item->field_opacity['und'][0]['value']): ?>
    <?php $opacity_value = $paragraphs_item->field_opacity['und'][0]['value'] / 100 ?>
  <?php endif; ?>

  <?php $overlay_attributes['class'] = ['component-content-strap-column__overlay']; ?>

  <?php
      $style = "background";
      $derivative_uri = image_style_path($style, $paragraphs_item->field_image['und'][0]['uri']);
      if (!file_exists($derivative_uri)) {
          $display_style = image_style_load($style);
          image_style_create_derivative($display_style, $paragraphs_item->field_image['und'][0]['uri'], $derivative_uri);
      }
      $bg_image = file_create_url($derivative_uri);
  ?>
  <?php $overlay_style_value .= 'background-image: url(' . $bg_image . ');' ?>
  <?php $overlay_style_value .= ' opacity: ' . $opacity_value . ';' ?>

  <?php $overlay_attributes['style'] = $overlay_style_value; ?>
  <?php $classes .= 'component-content-strap-column--image'; ?>
<?php endif ?>

 <?php if ($paragraphs_item->field_color['und'][0]['rgb']): ?>
  <?php $color_id = 'paragraph-' . $paragraphs_item->item_id . '-color' ?>
  <?php $body_attributes['id'] = $color_id; ?>
  <?php $body_attributes['class'] = ['override-text-color']; ?>
  <?php $tags = ['h1','h2','h3','h4','h5','h6','h7','h8','div','span','p','a','blockquote','ul','ol','label','table','tr','td','.base-accordion__header::before'] ?>
   <style type="text/css">
     <?php foreach($tags as $tag): ?>
     <?php print "#" . $color_id. ' ' . $tag; ?><?php if( next( $tags ) ): ?>, <?php endif; ?>
     <?php endforeach; ?>
     {
       color: <?php print $paragraphs_item->field_color['und'][0]['rgb']; ?>;
     }
   </style>
<?php endif; ?>

<!-- Grid System Class -->
<?php if ($paragraphs_item->field_grid_system_classes['und'][0]['value']): ?>
  <?php $classes .= $paragraphs_item->field_grid_system_classes['und'][0]['value']; ?>
<?php else: ?>
  <?php $classes .= 'col-xs-12 col-md-6 col-lg-4'; ?>
<?php endif; ?>

<?php $overlay_attributes = drupal_attributes($overlay_attributes); ?>

<?php $body_attributes['class'] []= 'component-content-strap-column__body';?>
<?php $body_attributes = drupal_attributes($body_attributes); ?>

<?php $extra_attributes = drupal_attributes($extra_attributes); ?>

<div class="<?php print $classes; ?> component-content-strap-column"<?php print $attributes; print $extra_attributes; ?>>
  <?php  if ($paragraphs_item->field_image && $paragraphs_item->field_image['und'][0]['fid']): ?>
    <div<?php print $overlay_attributes; ?>></div>
  <?php endif; ?>
  <div class="component-content-strap-column__content" data-aos="fade-up">
    <div<?php print $body_attributes; ?>>
      <?php if ($content['field_paragraphs_reference']): ?>
        <?php print render($content['field_paragraphs_reference']); ?>
      <?php endif ?>
    </div>
  </div>
</div>
