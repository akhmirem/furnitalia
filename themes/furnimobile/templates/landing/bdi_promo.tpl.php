

<?php
global $theme_path;
$bdi_landing = base_path() . "sites/default/files/promo/bdi/media_sale";
?>

<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=PT+Sans|PT+Sans+Narrow|Raleway:400,200,300,500,600,100"
      xmlns="http://www.w3.org/1999/html"/>
<style type="text/css">
    .bdi-box {
        font-family: Raleway, sans-serif;
        font-size: 20px;
        letter-spacing: .07em;
        width: 100%;
        padding: 20px 0;
        float: left;
        margin-right: 15px;
        text-transform: uppercase;
        color: black;
        font-weight: 400;
        text-align: center;
    }
    .bdi-box .orange {
        color: #F15D2f;
        letter-spacing: 0.05em;
    }
    p.promo-copy {
        font-family: 'PT Sans', sans-serif;
        color: black;
        font-size: 1.2em;
    }
</style>

<h1 style="    text-align: center;
    font-family: Raleway, sans-serif;
    letter-spacing: .05em;
    font-size: 28px;
    margin: 0;"><span class="bolded" style="    font-weight: bold;
    vertical-align: middle;
    display: inline-block;"> <img src="https://www.bdiusa.com/graphics/logo.png"/></span>
    <span style="        display: inline-block;
    background: #F15D2F;
    color: white;
    padding: 18px 20px;
    position: relative;
    line-height:130%;
    margin: 15px 0 40px 0;">Media Room Furniture Sale</span>
</h1>


<h3 style="text-align: center;
    margin: 0;
    font-size: 1.5em;color: black;">Save 15%* on ALL BDI Media Furniture</h3>
<h4 style="text-align: center;
    margin: 20px 0 30px 0;">February 16 &mdash; March 7</h4>

<aside class="bdi-box">
    <span class="orange">Innovative Designs</span><br/> For Modern Living
</aside>

<p class="promo-copy">
    Your media center is the entertainment hub of your home, and BDI offers an affordable and innovative home theatre and
    media furniture. BDI is an industry leader in media furniture and office systems. Always more than meets the eye, BDI
    merges innovative engineering and original design to seamlessly integrate technology into the home.
</p>

<!--BDI’s furniture combines great design with innovative-->
<!--functionality to seamlessly integrate technology into the home and office environments.-->
<!--Whether you are looking to house a TV and soundbar or-->
<!--a full blown multi-channel system, make sure the furniture that you-->
<!--select meets your needs.-->
<br/>

<div style="text-align: center;">
    <img src="<?php print $bdi_landing; ?>/BDI-spring-sale-promo-mobile-v2.jpg" alt="BDI Furniture - The Annual Media Furniture Event" />
</div>

<div style="text-align: center">
<!--    <iframe width="420" height="320" src="https://www.youtube.com/embed/pr9IzQnpy4k" frameborder="0" allowfullscreen></iframe>-->
    <iframe width="420" height="236" src="https://www.youtube.com/embed/0pUQzqkyJG8" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

<hr class="grey-sep"/>

<p style="color: black;text-align: center">
    CALL TODAY FOR MORE INFORMATION ABOUT THIS EVENT! <br/>
    <a href="tel:+19163329000" style="border-bottom: 1px dashed #981b1e; color;#981b1e !important; text-decoration: none">916-332-9000</a><br/>
    <a href="<?php print base_path();?>request/ajax/bdi" class="request" id="request-info" title="Request BDI Product Information"
       style="padding: 17px 10px;
        background: #981b1e;
        display: inline-block;
        color: white;
        text-decoration: none;
        text-rendering: optimizeLegibility;
        font-size: 22px;
        width: 170px;
        margin: 10px auto;">
        Request Info
    </a> &nbsp;
    <a href="<?php print base_path();?>occasional/tv-media-storage?brand=29" class="request"  title="Shop BDI"
       style="padding: 17px 10px;
        background: #981b1e;
        display: inline-block;
        color: white;
        text-decoration: none;
        text-rendering: optimizeLegibility;
        font-size: 22px;
        width: 170px;
        margin: 10px auto;">
        Shop BDI
    </a>
</p>

<p style="    text-align: center;
    font-size: 1em;
    text-transform: uppercase;color:black">
*Website prices are as marked and already include 15% discount.
</p>

<!--<video class="wp-video-shortcode" id="video-10923-1" width="640" height="360" preload="metadata" src="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4?_=1" style="width: 100%; height: 100%;"><source type="video/mp4" src="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4?_=1"><a href="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4">http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4</a></video>-->

<section id="requestFormSection"></section>

