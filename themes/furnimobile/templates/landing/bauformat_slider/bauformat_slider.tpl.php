<?php

    $files_path = base_path() . 'sites/default/files/landing/bauformat/slider';
?>
	<!-- Stylesheets
	============================================= -->
	<link rel="stylesheet" href="<?php print $files_path; ?>/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="<?php print $files_path; ?>/style.css" type="text/css">
	<link rel="stylesheet" href="<?php print $files_path; ?>/swiper.css" type="text/css">
	<link rel="stylesheet" href="<?php print $files_path; ?>/dark.css" type="text/css">
	<link rel="stylesheet" href="<?php print $files_path; ?>/font-icons.css" type="text/css">
	<link rel="stylesheet" href="<?php print $files_path; ?>/animate.css" type="text/css">
	<link rel="stylesheet" href="<?php print $files_path; ?>/magnific-popup.css" type="text/css">

	<link rel="stylesheet" href="<?php print $files_path; ?>/responsive.css" type="text/css">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->


<style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>

<div class="stretched dark device-lg">

	<!-- Content
		============================================= -->
	<section id="Bauformatcontent" style="margin-bottom: 0;">

		<div class="content-wrap">

			<div id="kuechen" class="container clearfix bottommargin-lg">

				<!-- Portfolio Filter
				============================================= -->
				<!--<ul id="portfolio-filter" class="portfolio-filter clearfix de" data-container="#portfolio">

					<li class="en"><a href="#" data-filter="*">Show all</a></li>
					<li class="fr"><a href="#" data-filter="*">Voir tous</a></li>
					<li class="activeFilter"><a href="#" data-filter=".bauformat">bau-for-mat</a></li>
					<li><a href="#" data-filter=".burger">Burger</a></li>

				</ul>-->
				<!-- #portfolio-filter end -->

				<!--<div id="portfolio-shuffle" class="portfolio-shuffle de" data-container="#portfolio">
					<i class="icon-random"></i>
				</div>-->

				<!--<div class="clear"></div>-->

				<!-- Portfolio Items
                ============================================= -->
                <div id="portfolio" class="portfolio grid-container portfolio-3 portfolio-masonry clearfix">

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mailand2-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mailand1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mailand3-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mailand4-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mailand5-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mailand6-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mailand2.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mailand1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mailand3.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mailand4.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mailand5.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mailand6.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbfm_Mailand03-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/Mailand_MIGA_Studio 01 Detail 04-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/dbfm_Mailand08-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/dbfm_Mailand07-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/dbfm_mailand06-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbfm_Mailand03.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/Mailand_MIGA_Studio_2001_Detail_04.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/dbfm_Mailand08.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/dbfm_Mailand07.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/dbfm_mailand06.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbfm_Mailand02-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbfm_Mailand02_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbfm_Mailand02_2-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbfm_Mailand02.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mbfm_Mailand02_1.jpg" data-lightbox="gallery-item">
                                <a href="<?php print $files_path; ?>/mbfm_Mailand02_2.jpg" data-lightbox="gallery-item">
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_01_02_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_01_02_03-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_01_02_04-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_01_02_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_01_02_03.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_01_02_04.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_01_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_01_02-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_01_03-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_02_01_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_02_01_02.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_02_01_03.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_03_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_03_02-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_03_03_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_02_03_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_02_03_02.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_02_03_03_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_02_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_02_02_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_02_03_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_03_02_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_03_02_02_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_03_02_03_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg177_612-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg177_612_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg177_612_2-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg177_612_3-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbrg177_612.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mbrg177_612_1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mbrg177_612_2.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mbrg177_612_3.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="3500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mlp239_338-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mlp239_338_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mlp239_338.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mlp239_338_1.jpg" class="hidden" data-lightbox="gallery-item"></a>

                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_03_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_03_02-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_03_03_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_03_03_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_03_03_02.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_03_03_03_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="3000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_01_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_01_02_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_03_01_03_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_03_01_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_03_01_02_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_03_01_03_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="3500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mlp380-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mlp380_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mlp380_2-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mlp380.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mlp380_1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mlp380_2.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="3500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg437-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg437_01-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg437_01_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbrg437.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mbrg437_01.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mbrg437_01_1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="3500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg178_612-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg178_612_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbrg178_612.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mbrg178_612_1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_02_01_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_02_02_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_02_02_03_fogra39-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_02_02_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_02_02_02_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_02_02_03_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="3500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg435_06-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg435_06_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg435_06_2-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbrg435_06.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mbrg435_06_1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/mbrg435_06_2.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item burger">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="4000" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg435_08-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/mbrg435_08_1-w400.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/mbrg435_08.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/mbrg435_08_1.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                    <article class="portfolio-item bauformat">
                        <div class="portfolio-image">
                            <div class="fslider" data-arrows="false" data-speed="1000" data-pause="5500" data-animation="fade">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_01_01_01_fogra39-w400-h3000.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_01_01_02_fogra39_freigabe-w400-h3000.jpg" alt="bauformat Burger"></a>
                                        </div>
                                        <div class="slide">
                                            <a href="#"><img src="<?php print $files_path; ?>/6108_01_01_03_fogra39-w400-h3000.jpg" alt="bauformat Burger"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-overlay" data-lightbox="gallery">
                                <div class="portfolio-desc">
                                    <h3><a href="#"></a></h3>
                                </div>
                                <a href="<?php print $files_path; ?>/6108_01_01_01_fogra39.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                <a href="<?php print $files_path; ?>/6108_01_01_02_fogra39_freigabe.jpg" class="hidden" data-lightbox="gallery-item"></a>
                                <a href="<?php print $files_path; ?>/6108_01_01_03_fogra39.jpg" class="hidden" data-lightbox="gallery-item"></a>
                            </div>
                        </div>
                    </article>

                </div>

				<div class="clear"></div>

			</div>

			<div class="clear"></div>

	</div></section>
	<!-- #content end -->

	
	<!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
<!--	<div id="gotoTop" class="icon-angle-up" style="display: block;"></div>-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php print $files_path; ?>/jquery.js"></script>
	<script type="text/javascript" src="<?php print $files_path; ?>/plugins.js"></script>
	<script type="text/javascript" src="<?php print $files_path; ?>/timeline.js"></script>
	<script type="text/javascript" src="<?php print $files_path; ?>/jquery.mobile.custom.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php print $files_path; ?>/functions.js"></script>

	<script>
		/*jQuery(window).load(function() {
			var swiperParent = new Swiper('.swiper-parent', {
				slidesPerView: 3,
			});
		});*/
	</script>




</div>