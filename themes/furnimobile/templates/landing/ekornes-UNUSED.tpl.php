

<?php global $theme_path;?>

<style>
    div.stressless-items-link {
        padding: 15px 0 30px 0;
    }
    div.stressless-items-link a.more-info {
        font-size: 1.5em;
        border-bottom: 1px dotted;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>
<h1>Stressless&reg; Ekornes Recliners and Sofas</h1>

<!--<img src="<?php print $theme_path;?>/images/landing/stressless/mobile_banner_stressless_w320.jpg" alt="Stressless Ekornes Free Leather Upgrade Event" style="text-align:center;"/>
<p style="font-size: 1.2em;
    font-weight: 500;">
    Envision saving up to $500 on Stressless®. During our FREE Leather Upgrade Event you can.
</p>
<p>
During our FREE Leather Upgrade event you can save up to $500 on Stressless®. recliners and sofas. Pay for fabric and receive leather at no extra cost or pay for leather and receive a smoother, softer grade of leather for free. Offer ends March 21.
Imagine a recliner or sofa so comfortable, it takes life’s fast pace down to a speed that relaxes and rejuvenates you. Stressless is the only furniture capable of providing such comfort. You’ll feel it in our patented Plus™-system, which provides exceptional lumbar support by interpreting your every movement. Once you sit in a Stressless chair or a Stressless couch, you’ll wonder how you’ve been able to relax and unwind without it.    
</p>

<aside class="contact" style="text-align:center;">
    Call us at <a href="tel:+19163329000">916-332-9000</a> (Sacramento, CA) | <a href="tel:+19163329000">916-332-9000</a> (Roseville, CA) 
    <a href="<?php print base_path();?>request/ajax/stressless" class="request" id="request-info" title="Request Stressless Product Information"
    style="    padding: 17px 10px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: 300px;
    margin: 10px auto;
    border-radius: 7px;">
        CONTACT
    </a>    
    <?php if (isset($view_items_link)) : ?>
        <?php print render($view_items_link); ?>
    <?php endif; ?>
</aside>

<section id="requestFormSection"></section>
-->

<script language="javascript"> 
    function scrollToTop() { 
        //scroll(0,0); 
        var offset = jQuery("#ekornesIframe").offset();
        var scrollTarget = jQuery("#ekornesIframe");
        while (scrollTarget.scrollTop() == 0 && scrollTarget.parent()) {
            scrollTarget = scrollTarget.parent();
        }
        // Only scroll upward
        scrollTarget.animate({scrollTop: (offset.top - 5)}, 500);
    } 
</script>

<iframe id="ekornesIframe" src="https://secure.ekornes.com/?d=1&sc_lang=en" class="loading loading-top" frameborder="0" width="100%" height="3500" scrolling="no" id="ekornes_stressless" onload="scrollToTop();"></iframe> 
<!-- <hr/>
<a href="http://www.ekornes.com/us/e-catalog" target="_blank"><img src="http://www.stresslessdigital.com/media/1378/onlinecatus.png"/></a> -->
<br/>
<aside class="contact" style="text-align:center;">
    Call us at <a href="tel:+19163329000">916-332-9000</a> (Sacramento, CA) | <a href="tel:+19163329000">916-332-9000</a> (Roseville, CA) 
    <a href="<?php print base_path();?>request/ajax/stressless" class="request request-info" title="Request Stressless Product Information"
    style="    padding: 17px 10px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: 300px;
    margin: 10px auto;
    border-radius: 7px;">
        CONTACT
    </a>    
    <?php if (isset($view_items_link)) : ?>
        <?php print render($view_items_link); ?>
    <?php endif; ?>
</aside>

<section id="requestFormSection"></section>

