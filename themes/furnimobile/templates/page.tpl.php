<?php global $base_path, $theme_path, $conf;
$furnitheme = drupal_get_path("theme", "furnitheme");
?>

<link type="text/css" rel="stylesheet"
      href="<?php print $base_path . $theme_path; ?>/lib/mmenu/dist/css/jquery.mmenu.css"/>

<script type="text/javascript"
        src="<?php print $base_path . $theme_path; ?>/lib/mmenu/dist/js/jquery.mmenu.min.umd.js"></script>

<style>
  .headroom {
    will-change: transform;
    transition: transform 200ms linear;
  }

  .headroom--pinned {
    transform: translateY(0%);
    background: #e6e7e8;
  }

  .headroom--top {
    background: transparent;
  }

  .headroom--unpinned {
    transform: translateY(-100%);
  }

  #container-inner #top-header.headroom-pinned {
    position: fixed;
    z-index: 10;
    right: 0;
    left: 0;
    top: 4.5em;
  }
  #container-inner #top-header.headroom--top {
    height: 12em;
    position: static;
  }
  #container-inner #top-header.headroom--not-top {
    top: 0;
  }
</style>

<nav id="main-nav" class="accordion clearfix" role="navigation">
  <!-- <img src="<?php print $base_path . $theme_path ?>/images/icons_logos/furnitalia_logo_white.png" alt="Furnitalia" style="padding:10px 0"/>
    <a href="#" id="close-toggle">CLOSE <img src="<?php print $base_path . $theme_path ?>/images/icons_logos/close_icon_18x18.png"/></a> -->

  <?php print render($page['nav']); ?>

  <!-- <nav id="footer-info-menu"><?php print render($footer_info_menu); ?></nav>
    <nav id="footer-user-menu"><?php print render($footer_user_menu); ?></nav>
    <nav id="footer-policy-menu"><?php print render($footer_policy_menu); ?></nav>  
    <?php print  l("Catalogs", "catalogs", array('attributes' => array('id' => 'catalogs'))); ?>  
    <?php print  l("Full Site", "http://www.furnitalia.com/?desktop=1", array('attributes' => array('id' => 'full-site'))); ?>   -->

</nav>

<div id="container">

  <div id="container-inner">

    <?php include("header_promo.tpl.php"); ?>

    <!-- HEADER
    ======================================================================================================= -->
    <header role="banner" id="top-header">

      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>

      <hgroup>
        <?php if ($site_name): ?>
          <h1 id="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"
               rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 id="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>
      </hgroup>

      <section id="header-phone">
        <a href="tel:+19163329000" class="main-phone furn-red">916-332-9000</a>
        &nbsp; | &nbsp;
        <?php print l("Store Locations", 'store-locations', array('attributes' => array('class' => array('furn-grey', 'header-link', (arg(0) == 'contact-us') ? " active" : ""), 'style' => 'text-transform: uppercase;'))); ?>

        <?php if (FALSE) : ?>
          <!--<a href="tel:+18883874825" class="main-phone furn-red">1-888-387-4825</a>-->
          <!-- &nbsp; | &nbsp;
					<a href="tel:+19164840333">916-484-0333</a> <a href="<?php print url('stores') ?>"> (Sacramento, CA)</a> &nbsp; | &nbsp;
					<a href="tel:+19163329000">916-332-9000</a> <a href="<?php print url('stores') ?>"> (Roseville, CA)</a>		-->
        <?php endif; ?>
      </section>

      <!-- <a href="#main-nav" style="background: center center no-repeat transparent;
   background-image: url( data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADhJREFUeNpi/P//PwOtARMDHQBdLGFBYtMq3BiHT3DRPU4YR4NrNAmPJuHRJDyahEeT8Ii3BCDAAF0WBj5Er5idAAAAAElFTkSuQmCC );

   display: block;
   width: 40px;
   height: 40px;
   position: absolute;
   top: 0;
   left: 10px;"></a>  -->

      <a id="hamburger" class="mm-s lideout" href="#"><span></span></a>

      <?php //print render($page['page_top']); ?>

      <!-- TEMPORARY PUT HIGHLIGHTED HERE -->
      <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>

    </header>

    <!-- \HEADER
    ======================================================================================================= -->

    <!-- MAIN Section
    ======================================================================================================= -->
    <!-- style="margin-top: 145px;"-->
    <main role="main" id="main-content" style="">


      <?php print $messages; ?>

      <?php if ($breadcrumb): ?>
        <nav id="breadcrumb"><?php print $breadcrumb; ?></nav>
      <?php endif; ?>

      <?php /*if (false && $tabs): */?><!--
        <div class="tabs"><?php /*print render($tabs); */?></div>
      --><?php /*endif; */?>

      <a id="main-content"></a>

      <?php if ($title && $show_title): ?>

          <!--    <?php
        /*    // open article tag if page is a node
            if (($page) && (arg(0) == 'node')): */ ?>
    <article role="article">
      --><?php /*endif; */ ?>


        <?php print render($title_prefix); ?>
        <h1 class="title furn-red furn-ucase furn-e3" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php print render($page['help']); ?>

      <?php if ($action_links): ?>
        <nav>
        <ul class="action-links"><?php print render($action_links); ?></ul></nav><?php endif; ?>


      <!-- Banner Section
      ======================================================================================================= -->
      <?php if (isset($page['banner']) && !empty($page['banner'])) : ?>
        <section id="banner">
          <?php print render($page['banner']); ?>
        </section>
      <?php endif; ?>
      <!-- \Banner Section
      ======================================================================================================= -->

      <!-- CONTENT
            ======================================================================================================= -->
      <?php print render($page['content']); ?>
      <!-- \CONTENT
            ======================================================================================================= -->

    </main>
    <!-- \MAIN Section
    ======================================================================================================= -->

    <!-- Supplementary Section
======================================================================================================= -->
    <?php if ($page['supplementary']): ?>
      <aside id="supplementary" class="column sidebar" role="complementary">
        <?php print render($page['supplementary']); ?>
      </aside>
    <?php endif; ?>
    <!-- \Supplementary Section
======================================================================================================= -->


    <!-- Footer
======================================================================================================= -->
    <?php if ($page['footer']): ?>
      <footer role="contentinfo" id="footer">

       <?php /*print render($page['footer']); */?>

        <!-- Newsletter subscription
  ============================================================================-->
        <!-- Begin Mailchimp Signup Form -->
        <!-- <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css"> -->
        <?php //include_once "$furnitheme/css/mailchimp_signup_form.css" ?>
        <div id="mc_embed_signup" class="clearfix">
          <form action="https://furnitalia.us13.list-manage.com/subscribe/post?u=66b55ad437d9bfa2366d0d5e2&amp;id=280637c773" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
              <h2>Get On the List!</h2>
              <div class="signup-text" style="">Be the first to know about special offers, new products and store events. Plus, get dècor inspiration and advice from our design team.</div>
              <div class="mc-field-group">
                <label for="mce-EMAIL" style="display: none">Email Address </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
              </div>
              <div class="mc-field-group input-group" style="display:none">
                <ul>
                  <li><input type="checkbox" value="512" name="group[2125][512]" id="mce-group[2125]-2125-11" checked><label for="mce-group[2125]-2125-11">Newsletter Subscribers</label></li>
                </ul>
              </div>
              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_66b55ad437d9bfa2366d0d5e2_280637c773" tabindex="-1" value=""></div>
              <div class="clear"><input type="submit" value="Sign Me Up" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
            </div>
          </form>
        </div>
        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';fnames[5]='MMERGE5';ftypes[5]='text';fnames[6]='MMERGE6';ftypes[6]='text';fnames[7]='MMERGE7';ftypes[7]='text';fnames[8]='MMERGE8';ftypes[8]='text';fnames[9]='MMERGE9';ftypes[9]='text';fnames[10]='MMERGE10';ftypes[10]='text';fnames[11]='MMERGE11';ftypes[11]='text';fnames[12]='MMERGE12';ftypes[12]='text';fnames[13]='MMERGE13';ftypes[13]='text';fnames[14]='MMERGE14';ftypes[14]='text';fnames[15]='MMERGE15';ftypes[15]='text';fnames[16]='MMERGE16';ftypes[16]='text';fnames[17]='MMERGE17';ftypes[17]='text';fnames[18]='MMERGE18';ftypes[18]='text';fnames[19]='UNIQUECODE';ftypes[19]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
        <!--End mc_embed_signup-->

        <!-- /Newsletter subscription -->


        <!--<section id="email-us">
                    <a href="mailto:admin@furnitalia.com?Subject=Inquiry%20from%20Furnitalia%20website" target="_top" title="E-mail us for further assistance"><img src="<?php print $base_path . $theme_path; ?>/images/icons_logos/email_icon_31x34.png" alt="E-Mail Us"/><span class="furn-grey furn-ucase">E-mail us for further assistance</span></a>
                </section>-->

        <!-- Social Icons
    ======================================================================================================= -->
        <section id="social-icons">
          <!--<span style="text-transform: uppercase">Connect with Furnitalia</span>-->
          <ul>
            <li><a href="https://www.facebook.com/furnitalia.sacramento" class="facebook" target="_blank">&nbsp;</a>
            </li>
            <li><a href="https://twitter.com/furnitalia" class="twitter" target="_blank">&nbsp;</a></li>
            <li><a href="https://pinterest.com/furnitalia" class="pinterest " target="_blank">&nbsp;</a></li>
            <li><a href="https://instagram.com/furnitalia" class="instagram" target="_blank">&nbsp;</a></li>
            <li><a href="https://www.houzz.com/pro/furnitalia/furnitalia" class="houzz"
                   style="padding-left:2px;position:relative;top:-20px" target="_blank"> </a></li>
          </ul>

        </section>
        <!-- \Social Icons
    ======================================================================================================= -->

        <!-- VIP Customer Program
        ============================================================================-->
        <div id="vipProgramBlock" class="furn-red" style="
                font-size: 1em;
                letter-spacing: 1px;
                text-transform: uppercase;
                padding-top:1em;
                text-align:center;
            ">
          <h4 style="display: inline;">
            <?php print l("Join VIP Customer Program", "vip",
                array("attributes" =>
                    array(
                        "title" => "Sign up to become a preferred customer for exclusive benefits from Furnitalia!",
                        "style" => "border: 1px solid #ea2328;padding: .5em 1em;text-transform: none;font-weight: normal;",
                        "class" => "furn-red",
                    ))); ?>
          </h4>
        </div>
        <!-- /VIP Customer Program -->

      </footer>
    <?php endif; ?>
    <!-- \Footer
======================================================================================================= -->

    <!--<div class="mobile-menu-background"></div>-->

  </div>

</div>


<?php if (false && isset($conf['production']) && $conf['production']) : ?>
  <!-- BEGIN JIVOSITE CODE {literal} -->
  <script type='text/javascript'>
    (function () {
      var widget_id = 'wJpNauYRQr';
      var d = document;
      var w = window;

      function l() {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = '//code.jivosite.com/script/geo-widget/' + widget_id;
        var ss = document.getElementsByTagName('script')[0];
        ss.parentNode.insertBefore(s, ss);
      }

      if (d.readyState == 'complete') {
        l();
      } else {
        if (w.attachEvent) {
          w.attachEvent('onload', l);
        } else {
          w.addEventListener('load', l, false);
        }
      }
    })();</script>
  <!-- {/literal} END JIVOSITE CODE -->

<?php endif; ?>
