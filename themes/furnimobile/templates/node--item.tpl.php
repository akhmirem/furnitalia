<?php
/**
 * @file
 * Zen theme's implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
?>

<?php

global $base_path, $base_url, $theme_path;

$promo_dir = base_path() . "sites/default/files/promo";

// Don't show availability icon
/*if (isset($content['field_availability']) && is_array($content['field_availability'])) {
	$availability_icon = '<img src="' . base_path() . path_to_theme() . '/images/availability-icon.png"/>';
	$content['field_availability'][0]['#markup'] = $availability_icon . $content['field_availability'][0]['#markup'];
}*/

$pdf_icon = '<img src="' . $base_path . $theme_path . '/images/icons_logos/pdf-icon.png" />';
$brand_title = $content['field_brand'][0]['#title'];


if ($teaser) { //item teaser view

	$classes .= " gallery-item";
	$classes .= ' brand' . ((isset($node->field_brand['und'])) ? $node->field_brand['und'][0]['tid'] : '');

	$additional_attribs = '';
?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	<?php

	unset($content['field_availability']['#title']);

	$image = $node->field_image['und'][0];
	if(isset($image['uri']) && !empty($image['uri'])) {
		$image_html = theme('image_style', array(
			//'style_name' => 'mobile_gallery_thumb',
			'style_name' => 'mobile_large',
			'path' => $image['uri'],
			'alt' => $image['alt'],
            'attributes' => array(
                'property' => "schema:image",
            ),
		));
		$image_html_rendered = render($image_html);

        $image_linked = l($image_html_rendered, "node/" . $node->nid,
            array('html' => true,
                'attributes' => array(
                    'property' => "schema:url",
                    'title' => $title . ' by ' . $content['field_brand'][0]['#title'],
                )
            )
        );

		print $image_linked;
	}

	?>

	<header>
		<h2<?php print $title_attributes; ?>>
            <a href="<?php print $node_url; ?>" class="title furn-red" property="schema:url">
                <span property="schema:name"><span class="brand-subtitle"><?php print $brand_title; ?></span> <?php print $title; ?></span>
            </a>
        </h2>
	</header>

	<?php if(isset($content['sale_price']) && !empty($content['sale_price'])) : ?>
		<!--<span class="item-clearance"><img src="<?php print base_path() . path_to_theme(); ?>/images/Sale_Icon_Red_40x24.png" alt="Item on clearance"/></span>	-->
	<?php endif; ?>

	<div class="item-details">

		<?php //print render($content['list_price']); ?>
		<?php print render($content['sell_price']); ?>
		<?php print render($content['sale_price']); ?>
		<?php //print render($content['final_price']); ?>

	</div>

</article><!-- /.node -->

<?php
} else { //full page view
?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>


    <div id="item-images">

	    <header id="product-header">
            <h1 class="title furn-ucase furn-red furn-e2-a" id="page-title" property="schema:name">
                <?php print $brand_title; ?> <?php print $node->title; ?>
            </h1>
		    <?php $content['field_alu']['#title'] = 'Model'; ?>
		    <?php print render($content['field_alu']); ?>
		    <?php print render($content['model']); ?>
	    </header>

	    <!--<div id="image-gallery" class="mightyslider_modern_skin horizontal">
		<div class="frame" data-mightyslider="width:320,height:240">
			<div class="slide_element">
				<?php foreach($content['field_image']['#items'] as $i => $image) :?>

			  		<?php
			  			$style = "mobile_large";
			  			$derivative_uri = image_style_path($style, $image['uri']);
			  			if (!file_exists($derivative_uri)) {
			  				$display_style = image_style_load($style);
			  				image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
			  			}
			  			$img_url  = file_create_url($derivative_uri);

			  			$style = "mobile_thumb";
			  			$derivative_uri = image_style_path($style, $image['uri']);
			  			if (!file_exists($derivative_uri)) {
			  				$display_style = image_style_load($style);
			  				image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
			  			}
			  			$thumb_url  = file_create_url($derivative_uri);

			  			$full_img_url = file_create_url($image['uri']);

			  		?>

					<div class="slide" data-mightyslider="cover: '<?php print $img_url; ?>', link: { url: '<?php print $full_img_url; ?>', thumbnail: '<?php print $thumb_url; ?>', target: '_blank' }, thumbnail: '<?php print $thumb_url; ?>' ">
						<!--<div class="mSCaption infoBlock infoBlockLeftBlack" data-msanimation="{ speed: 700, easing: 'easeOutQuint', style: { left: 30, opacity: 1 } }">
							<h4>This is an animated block, add any number of them to any type of slide</h4>
							<p>Put completely anything inside - text, images, inputs, links, buttons.</p>
						</div>-->
					<!--</div>

				<?php endforeach; ?>
			</div>
            </div>
            <ul class="gal-pager"><li></li></ul>
	    </div>
	    <div id="thumbs-wrapper" ><ul id="thumbs"></ul></div>
         -->

        <div id="image-gallery" class="gallery-container">
            <ul class="slickslide">
                <?php foreach($content['field_image']['#items'] as $i => $image) :?>

                    <?php

                        $style = "mobile_large";
                        $derivative_uri = image_style_path($style, $image['uri']);
                        if (!file_exists($derivative_uri)) {
                            $display_style = image_style_load($style);
                            image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
                        }
                        $img_url  = file_create_url($derivative_uri);

                        $full_img_url = file_create_url($image['uri']);

                    ?>

                    <li>
                        <a href="<?php print $full_img_url; ?>" data-rel="lightcase:mycollection:slideshow">
                            <?php print theme("image", array(
                                "path" => $img_url,
                                "attributes" => array(
                                    "ref" => $img_url,
                                    "property" => 'schema:image',
                                )
                            )); ?>

                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="slick-thumbs">
                <ul>
                    <?php foreach($content['field_image']['#items'] as $i => $image) :?>

                        <?php

                        $style = "mobile_thumb";
                        $derivative_uri = image_style_path($style, $image['uri']);
                        if (!file_exists($derivative_uri)) {
                            $display_style = image_style_load($style);
                            image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
                        }
                        $thumb_url  = file_create_url($derivative_uri);

                        ?>
                        <li>
                            <img src=" <?php print $thumb_url; ?>"/>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

        </div>

    </div>

  <section id="product-info">
	  <section id="pricing">
		  <div class="item-info-p">

		  	<label class="furn-red furn-ucase">Price:</label>
		  	<?php print render($content['sell_price']); ?>
		  	<?php print render($content['sale_price']); ?>
		  	<?php //print render($content['final_price']); ?>

		  </div>

		  <?php if (isset($content['add_to_cart'])) : ?>
			  <p class="item-info-p"><?php print render($content['add_to_cart']);?></p>
		  <?php endif;?>

		  <?php print l("Request info", "request/$node->nid/ajax", array('attributes' => array('id' => 'request-quote', 'class' => array('furn-button-text')))); ?>
	  </section>

	  <div id="item-brand">
          <?php if (isset($content['field_availability'])) : ?>
				<?php print render($content['field_availability']); ?>
		  <?php endif; ?>

		  <?php
          if (is_array($content['field_brand']['#object']->field_brand['und'][0]['taxonomy_term']->field_brand_image)) {
              $brand_image = $content['field_brand']['#object']->field_brand['und'][0]['taxonomy_term']->field_brand_image['und'][0];

              $style = "mobile_brand_img";
              $derivative_uri = image_style_path($style, $brand_image['uri']);
              if (!file_exists($derivative_uri)) {
                  $display_style = image_style_load($style);
                  image_style_create_derivative($display_style, $brand_image['uri'], $derivative_uri);
              }
              $brand_img_url = file_create_url($derivative_uri);


              $brand_display = theme("image",
                  array(
                      "path" => $derivative_uri,
                      'attributes' => array(
                          "alt" => $brand_title,
                          "title" => $brand_title,
                          "class" => array("brand-img"),
                          "property" => "schema:logo"
                      )
                  )
              );
              $brand_display .= "<div class='brand-title'>(<span property='schema:name'>" . $content['field_brand'][0]['#title'] . "</span>)</div>";

              print "<div typeof='schema:Brand' property='schema:brand' class='product-brand'>" .
                  l($brand_display,
                      $content['field_brand'][0]['#href'],
                      array(
                          "html" => TRUE,
                          'attributes' => array(
                              "property" => "schema:url",
                          ),
                      )
                  ) .
                  "</div>";
          }
		  ?>
	  </div>

    <?php if ($content['bdi_media_furniture']): ?>
      <br/>
      <div class="price-explanation furn-red" style="font-size: 1.2em;
    line-height: 1.2;
    margin: 1em 0 .5em 0;">
        *Save additional 15% OFF, available through Feb. 5, 2020 <br/>
      </div>
      <img src="<?php print $promo_dir; ?>/bdi/entertainment-sale/BDi-entertainment-sale-banner-2020.jpg"
           alt="BDI Entertainment Furniture Sale!" style="clear: both;">
    <?php endif; ?>

	  <div id="product-additional" class="accordion clearfix">
	  	<h3 class="furn-ucase furn-red">Description</h3>
	  	<div>
	  		<?php print render($content['body']); ?>
	  		<?php print render($content['dimensions']);?>
	  		<?php if (isset($content['field_details']) && is_array($content['field_details'])):?>
				<p class="item-info-p">
					<?php $content['field_details'][0]['#markup'] =  nl2br($content['field_details']['#items'][0]['value']); ?>
					<?php print render($content['field_details']);?>
				</p>
        <p class="item-info-p furn-red furn-ucase">*Special financing is available. Call for more information.</p>
			<?php endif; ?>

	  	</div>

	  	<!--<h3 class="furn-ucase furn-red">*Call for information on 24 months special financing</h3>
	  	<div>
	  		<img src="<?php print $base_path . 'sites/default/files/promo/spring2015/financing_popup_mobile.png';?>" alt="24 months special financing"/>
	  	</div>-->

	  	<h3 class="furn-ucase furn-red">Schematics PDF</h3>
	  	<div>
	  		 <?php if(isset($content['field_product_pdf']) && count($content['field_product_pdf']['#items']) > 0) : ?>
	  		 	<span class="favorites">
	  		 		<a href="<?php print file_create_url($content['field_product_pdf']['#items'][0]['uri']);?>"><?php print $pdf_icon; ?> VIEW SCHEMATICS </a>
	  		 	</span>
	  <?php endif; ?>
	  	</div>

	  </div>




	  <span class="favorites"><img src="<?php print base_path() . path_to_theme(); ?>/images/icons_logos/favorites_star_icon_16x16.png"/>
		  <?php
		  	/*global $user;
  			if(!$user->uid) {
  			    print l(t('Add to favorites'), 'user/login' , array('query'=> array('destination' => 'node/' . $node->nid)));
  			} else {
  				print $content['links']['flag']['#links']['flag-favorites']['title']; //render($content['links']['flag']);
            }*/
            print $content['links']['flag']['#links']['flag-favorites']['title'];
		  ?>
	  </span>

	  <div id="scroll-top"><img src="<?php print $base_path . $theme_path?>/images/icons_logos/link_arrow_previous_6x9.png"/>&nbsp;<a href="#" class="furn-grey">BACK TO TOP</a></div>

  </section>


  <?php if ($content['has_video']): ?>
<!-- 	  <div id="hidden-video"> -->
	  	  <!-- This is a hidden container for item video, its contents will appear in popup box -->
<!-- 		  <?php print render($content['field_video']); ?>-->
<!-- 	  </div> -->
  <?php endif; ?>

  <?php if ($display_collection_items) : ?>
	<hr class="gradient title-sep"/>
  	<h3 class="title">More from <span style="font-style: italic;"><?php print $collection_title;?></span> collection</h3>
  	<div id="collection-items-gallery">

		<?php
			$display_id = 'block_collection_items';
			$view_name = 'taxonomy_term';
			$args = array($collection_nid);

			//print views_embed_view($view_name, $display_id, $args);
			$view = views_get_view($view_name);
			$view->set_arguments($args);
			$view->set_display($display_id);
			$view->is_cacheable = FALSE;

			//exclude current zitem from the list of related items
			$view->add_item($view->current_display, 'filter', 'node', 'nid', array('operator' => '!=','value' => array('value' => $node->nid), 'group' => 1));

			$view->pre_execute();
			$view->execute();
			$view->post_execute();
			print $view->preview($display_id);
			$view->destroy();

		?>
  	</div>
    <?php endif; ?>

 <?php //print render($content); ?>

</article><!-- /.node -->


<?php } ?>
