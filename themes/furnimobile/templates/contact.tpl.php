<div class="field-label store-phone"><a href="tel:+18003874825" class="main-phone furn-grey">1-800-387-4825</a></div>
<section id="email-us-contact">
	<a href="mailto:admin@furnitalia.com?Subject=Inquiry%20from%20Furnitalia%20website" target="_top" title="E-mail us for further assistance"><img src="<?php print base_path() . path_to_theme(); ?>/images/icons_logos/email_icon_31x34.png" alt="E-Mail Us"/><span class="furn-grey furn-ucase">E-mail us for further assistance</span></a>
</section>
<br/>

<?php if (FALSE): ?>
<section class="hours-update" style="padding: 50px 5px 10px;font-size: 1.3em;color: #ea2328;text-transform: uppercase;text-align: center;">
Our stores are closed on <strong>December 24-25, 2014</strong> for Christmas holidays and <strong>December 30-31, 2014 and January 1, 2015</strong> for New Year observance.
</section>
<?php endif; ?>

<?php if (isset($webform)): ?>


    <div id="amoCrmFormWrapper" class="loading" style="">
        <!-- AmoCRM Form -->

        <script>var amo_forms_params = {"id":44610,"hash":"a53ea91b1dd3ea8df8ab7dfbd05e23f6","locale":"en"};</script>
        <script id="amoforms_script" async="async" charset="utf-8" src="https://forms.amocrm.com/forms/assets/js/amoforms.js"></script>

        <!-- End AmoCRM Form -->
        <div class="dots-loader"></div>
    </div>

    <hr/>
    <br/>



<?php endif; ?>

<h2 class="block-title furn-red">Furnitalia Showroom Locations</h2>
<section class="store-info clearfix">
	<h2 class="field-label furn-e3">Furnitalia Sacramento (main store)</h2>
	<div class="map"><iframe width="99%" height="223" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=211487104845968262296.0004968861c94e20ef05a&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=38.664335,-121.349316&amp;spn=0.014945,0.025234&amp;z=14&amp;output=embed"></iframe></div>
	<div class="store-descr">


		<p class="info info-left"><span class="store-address">
			5252 Auburn Boulevard <br/>
			Sacramento, California 95841</span><br/>
			<span class="location-label">37k sq ft contemporary furniture showroom,
			 located close to the corner of Madison Avenue and Auburn Boulevard (next to the U-haul Rentals on Auburn Blvd).</span>
		</p>

		<p class="info info-right">
			Phone: <br/><a href="tel:+19163329000" class="phone furn-grey">(916) 332–9000</a> <br/>
			<span class="store-subtext">Hours: <span class="working-hours"><br/>Monday – Sunday <br/>
			10:00 am – 6:00 pm</span></span>
			<a href="https://maps.google.com/maps?saddr=&daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&z=11" target="_blank" class="directions-link furn-button-text">GET DIRECTIONS</a>
		</p>

	</div>
</section>


<section class="store-info clearfix">
	<h2 class="field-label furn-e3">Furnitalia Roseville (a boutique location)</h2>
	<div class="map"><iframe width="99%" height="223" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=211487104845968262296.0004968861c94e20ef05a&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=38.769543,-121.266747&amp;spn=0.007462,0.012617&amp;z=13&amp;output=embed"></iframe></div>
	<div class="store-descr">

		<p class="info info-left"><span class="store-address">
			1198 Roseville Parkway, #120 <br/>
			Roseville, California 95678</span><br/>
			<span class="location-label">Located at the Fountains at Roseville, between New Balance and Aveda Salon.</span>
		</p>

		<p class="info info-right">
			Phone: <br/><a href="tel:+19163329000" class="phone furn-grey">(916) 332-9000</a> <br/>
			<span class="store-subtext">Hours: <span class="working-hours"><br/>Monday – Saturday <br/>
			10:00 am – 6:00 pm <br/>
			Sunday <br/>
			11:00 am – 6:00 pm</span></span> <br/>
			<a href="https://maps.google.com/maps?saddr=&daddr=Furnitalia,+1198+Roseville+Pkwy+#120,+Roseville,+CA+95678&z=11" target="_blank" class="directions-link furn-button-text">GET DIRECTIONS</a>
		</p>

	</div>
</section>
