
<?php
	$theme_path = base_path() . drupal_get_path('theme', 'furnitheme');
?>
<article class="interior-design">
<img src="<?php print $theme_path; ?>/images/interior-design-top-left.png"/>
<p>Whether you need help selecting the perfect armchair or putting together an entire room, Furnitalia provides an arsenal of qualified designers waiting to help you. We offer personalized design services for your home or business.</p>

<p><span class="what-how">What We Do:</span> Our designers will help you create a comfortable space with a sense of style and sophistication that will represent the modern you. With an in-home visit, the specialist can evaluate your space from a professional point of view and provide a personalized design plan.</p>

<p><span class="what-how">How We Do It:</span> Our process is methodical. We will measure your space, take the appropriate photographs, conduct a comprehensive lifestyle analysis. Moving forward, we will discuss the color schemes, upholstery selection, furniture configurations and the wide spectrum of accessories we offer, suitable for you. Furnitalia prides itself in developing a beautiful new space you will want to spend time in. Our designers work to ensure that your vision comes to life while you still enjoy the comforts of being in your own home!</p>

<p><span class="what-how">Design Services Include*:</span><br />
&mdash; Visit to your home or business<br />
&mdash; Identifying the design and stylistic preferences<br />
&mdash; Furniture, upholstery and finishes selection<br />
&mdash; Coordinating with existing furniture and accessories<br />
&mdash; Drafting a floor plan and furniture layout</p>

<p>We also offer optional 3-D modeling services. We can recreate your space using 3-D modeling software that will give you an ability to see what your new furniture would look like in your space**</p>

<p style="text-align:center"><a href="<?php print url('contact-us');?>" class="appointment furn-red" title="Set up appointment">Set up your appointment today!</a></p>

<p class="disclaimer">*$125 Retainer Fee will be applied to a minimum store purchase of $2000 within 60 days of agreement and will be waived with the furniture purchase. The retainer does not apply to prior purchases and must be paid prior to on-site visit. Furnitalia will provide floor plans and custom drawings to client upon placement of order. There will be no exceptions.</p>
<p class="disclaimer">
**The cost of 3-D modeling services will vary depending on the space size and number of furniture pieces that will be modeled. Please contact our designers for details.</p>

<img class="image-right" src="<?php print $theme_path;?>/images/interior-design-right.png"/>

</article>

<br/>
<hr class="gradient" style="width:100%"/>
<br/>

<a name="1"></a>
<div id="requestFormSection"></div>
<br/>
<section id="expertise" style="clear:both;text-align:center">
    <!--<a href="https://www.expertise.com/ca/sacramento/interior-design" style="display:inline-block; border:0;"><img style="width:200px; display:block;" width="200" height="160" src="https://cdn.expertise.com/awards/ca_sacramento_interior-design_2017.svg" alt="Best Interior Designers in Sacramento" /></a>-->

    <table style="width: 200px;
    display: inline;
    margin: 0px 10px;
    position: relative;
    top: 5px;" cellpadding="0" cellspacing="0"><tr><td><a href="https://www.houzz.com/pro/furnitalia/furnitalia"><img src="https://st.hzcdn.com/static/badge_32_9@2x.png" alt="furnitalia in Sacramento, CA on Houzz" width="96" height="96" border="0" style="width: auto;height: 160px;"/></a></td></tr></table>

</section>
