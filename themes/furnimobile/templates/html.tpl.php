<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
	<?php print $head; ?>
	<title><?php print $head_title; ?></title>
  <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <meta name="MobileOptimized" content="width">
  <meta name="HandheldFriendly" content="true">
  <!--<meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.68, user-scalable=yes">-->
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">
  <?php if (isset($conf['production']) && !$conf['production']) : ?>
      <meta name="robots" content="noindex, nofollow">
  <?php endif; ?>
  <meta http-equiv="cleartype" content="on">
  <?php if ($meta_mobilewebapp): ?>
  	  <meta name="apple-mobile-web-app-capable" content="yes">
  <?php endif; ?>

  <!-- Favicon & app shortcut icons -->
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#b91d47">
  <meta name="msapplication-TileImage" content="/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <meta property="fb:pages" content="65283884934">

  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php if (isset($conf['production']) && $conf['production']) : ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M9VVZLG');</script>
    <!-- End Google Tag Manager -->
  <?php endif; ?>

  <?php if (FALSE && isset($conf['production']) && $conf['production']) : ?>
    <!-- Global site tag (gtag.js) - Google Ads: 1026222283 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1026222283"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-1026222283');
    </script>
  <?php endif; ?>

</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>

  <?php print $page_bottom; ?>

  <?php if (isset($conf['production']) && $conf['production']) : ?>

    <?php if (TRUE): ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9VVZLG"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php endif; ?>

    <?php if (FALSE): ?>
      <!-- Facebook Pixel Code -->
      <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '256029765185769');
        fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=256029765185769&ev=PageView&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
    <?php endif; ?>

  <?php endif;?>
  <script src="//d12ue6f2329cfl.cloudfront.net/resources/utm_form-1.0.4.min.js" async></script>


</body>
</html>
