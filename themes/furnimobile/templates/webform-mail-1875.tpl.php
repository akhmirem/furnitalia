<?php
// $Id: webform-mail.tpl.php,v 1.3.2.3 2010/08/30 20:22:15 quicksketch Exp $

/**
 * @file
 * Customize the e-mails sent by Webform after successful submission.
 *
 * This file may be renamed "webform-mail-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-mail.tpl.php" to affect all webform e-mails on your site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The webform submission.
 * - $email: The entire e-mail configuration settings.
 * - $user: The current user submitting the form.
 * - $ip_address: The IP address of the user submitting the form.
 *
 * The $email['email'] variable can be used to send different e-mails to different users
 * when using the "default" e-mail template.
 */
?>

<?php

global $base_url, $theme_path, $conf;

//$data = '<pre>' . print_r($submission, TRUE) .'</pre>';
//$data .= '<br/><br/><pre>Email var:<br/>' . print_r($email, TRUE) .'</pre>';

$is_user = $email['email'] == 3;

$theme = $base_url . '/' . $theme_path;

// TEMP
$theme = 'https://www.furnitalia.com/sites/all/themes/furnitheme';
// TEMP

$site = 'desktop site';
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $site = 'mobile site';
}


//------------------------
// ATTACHMENTS
/*
global $drupal_hash_salt;
$hash = md5($drupal_hash_salt);
$mime_boundary = "==Multipart_Boundary_x{$hash}x";
ob_start();
print "--" . $mime_boundary . "\r\n" . "Content-Type: text/html; charset=\"UTF-8\"; format=flowed; delsp=yes" . "\r\n";*/
//------------------------

?>

<?php if ($is_user) : ?>

    <div>

        <table cellpadding="0" cellspacing="0" width="600" align="center" style="border-collapse:collapse;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="100%"
                           style="border-collapse:collapse;border-spacing:0;">

                        <!-- LOGO -->
                        <tr>
                            <td style="text-align:center;"><span><a href="http://www.furnitalia.com" target="_blank"
                                                                    rel="nofollow" title="Furnitalia"
                                                                    style="text-decoration:none"><img
                                                src="<?php print $theme; ?>/images/webmail/furnitalia_logo.jpg" alt="Furnitalia"
                                                border="0" width="599" height="100"/></a></span></td>
                        </tr>

                        <tr>
                            <td style="height:15px">
                                <span><img src="<?php print $theme; ?>/images/webmail/space.gif" style="height:15px;"
                                           border="0"/></span>
                            </td>
                        </tr>
                        <!-- CONTENT -->

                        <tr>
                            <td align="center"
                                style="text-align:center;width:688px;border:2px solid #CCC;padding:10px;">

                                <table width="600" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td width="600" align="left" valign="top">
                                            <table width="600" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                <tr>
                                                    <td width="600" align="left" valign="top"
                                                        style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:14px;line-height:130%;font-weight:normal;text-decoration:none;color:#555555;">

                                                      <!--<img src="<?php print $theme; ?>/images/webmail/spring-sale-coupon.png"
                                                             alt="Here is your exclusive $250 discount coupon!"/> -->

                                                      <br/>
                                                      <p>[submission:values:first_name]</p>,
                                                      <p>You have signed up to attend a Grand Re-Opening event at Furnitalia
                                                        Sacramento showroom.</p>
                                                      <p>We look forward to seeing you!</p>
                                                      <p>
                                                        <span style="font-weight: bold;">Location:</span><br/>
                                                        5252 Auburn Blvd<br/>
                                                        Sacramento, CA 95841
                                                      </p>

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>

                        <!-- \CONTENT -->
                        <tr>
                            <td style="height:15px">
                                <span><img src="<?php print $theme; ?>/images/webmail/space.gif" style="height:40px;"
                                           border="0"/></span>
                            </td>
                        </tr>
                        <!-- STORES -->
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%"
                                       style="border-collapse:collapse; border-spacing:0;">
                                    <tr style="text-align:center">
                                        <td>
                  <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:13px;">
                    <a href="http://www.furnitalia.com/contact" rel="nofollow" title="Locate Furnitalia Stores"
                       target="_blank"
                       style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-weight:bold; font-size:14px;text-decoration:none;">Furnitalia Main Store</a>
                  </span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial,Verdana,sans-serif; font-size:11px;">5252 Auburn Blvd.</span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial,Verdana,sans-serif; font-size:11px;">Sacramento, CA 95841</span><br/>
                                            <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:14px; font-weight:bold;">916.332.9000</span>
                                        </td>
                                        <td>
                  <span>
                    <a href="http://www.furnitalia.com" title="Furnitalia" rel="nofollow" target="_blank"
                       style="text-decoration:none"><img src="<?php print $theme; ?>/images/webmail/website.jpg"
                                                         style="height:41px;width:181px" border="0"
                                                         alt="Furnitalia.com"/></a>
                  </span>
                                        </td>
                                        <td>
                  <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:13px">
                    <a href="http://www.furnitalia.com/contact" rel="nofollow" title="Locate Furnitalia Stores"
                       target="_blank"
                       style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-weight:bold; font-size:14px; text-decoration:none;">Fountains at Roseville</a>
                  </span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial, Verdana, sans-serif; font-size:11px;">1198 Roseville Pkwy</span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial, Verdana, sans-serif; font-size:11px;">Roseville, CA 95678</span><br/>
                                            <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:14px; font-weight:bold;">916.332.9000</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- \STORES -->
                        <tr>
                            <td>&nbsp;</td>
                        </tr>


                    </table>
                </td>
            </tr>
        </table>


    </div>

    <?php
    //------------------------
    //  ATTACHMENTS
    /*print "--" . $mime_boundary . "\r\n"
      ."Content-Type: application/pdf; name=\"Furnitalia_moving_sale_coupon.pdf\"" . "\r\n"
      ."Content-Disposition: attachment; filename=\"Furnitalia_moving_sale_coupon.pdf" ."\"\r\n"
      ."Content-Transfer-Encoding: base64\r\n\r\n"
      .chunk_split(base64_encode(file_get_contents($theme . '/files/landing/Furnitalia_moving_sale_coupon.pdf')))."\r\n";
   print "--" . $mime_boundary . "--\r\n";
   print(ob_get_clean());*/
    //------------------------
    ?>

<?php else: ?>

    <div class="request">

        <p><b>A new Grand Reopening webform registration was received on <?php print $site; ?> on [submission:date:long]</b></p>

        <p>[submission:values:first_name]</p>
        <p>[submission:values:last_name]</p>
        <p>[submission:values:e_mail]</p>
        <p>[submission:values:zip]</p>
        <p>[submission:values:number_of_people_attending]</p>
        <p>[submission:values:utm]</p>
        <p>[current-user:ip-address]</p>

    </div>

    <?php
    //------------------------
    //print(ob_get_clean());
    //------------------------
    ?>


<?php endif; ?>


