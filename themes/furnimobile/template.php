<?php

/**
** Override or insert variables into the page templates.
**
** @param $variables
**   An array of variables to pass to the theme template.
** @param $hook
**   The name of the template being rendered ("page" in this case.)
**/
//!PreprocessPage
function furnimobile_preprocess_page(&$vars) {

	global $theme_path;

	drupal_add_library('system', 'ui.accordion');

	drupal_add_library('system', 'drupal.ajax');	    
    drupal_add_library('system', 'ui.tooltip');
	drupal_add_js(drupal_get_path('module', 'webform_ajax') . '/js/webform_ajax.js', 'file');

	// set up specific template based on page
	$vip_nid = variable_get('furn_preferred_customer_nid', 1237);
	if (isset($vars['node']) && $vars['node']->nid == $vip_nid || arg(0) == 'vip') {

		drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.tools.min.js");
		drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.watermark.min.js");
		drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.maskedinput.min.js");
		drupal_add_js($theme_path . "/templates/vip_program/media/js/lpg/_init.js");
		drupal_add_js($theme_path . "/templates/vip_program/media/js/lpg/extensions/array.js");
		drupal_add_js($theme_path . "/templates/vip_program/media/js/lpg/utils/elements.js");
		drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.tools.min.js");


		drupal_add_css("//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css", 'external');
		drupal_add_css($theme_path . "/templates/vip_program/media/css/global/screen.css", array(
            //'media' => 'screen,projection',
            'group' => 100,
            'weight' => 8,
            'preprocess' => FALSE,
		));
		drupal_add_css($theme_path . "/templates/vip_program/media/css/print.css", array(
            'media' => 'print',
            'weight' => 9,
            'preprocess' => FALSE,
		));
		drupal_add_css($theme_path . "/templates/vip_program/media/css/public.css", array(
            //'media' => 'screen,projection',
            'group' => 100,
            'weight' => 10,
            'preprocess' => FALSE,
		));
		drupal_add_css("https://fonts.googleapis.com/css?family=Neucha&subset=latin,cyrillic", array(
            'type' => 'external',
            'preprocess' => FALSE,
            //'media' => 'screen,projection',
		));
		drupal_add_css("https://fonts.googleapis.com/css?family=Montserrat:400,700", "external");

		$vars['theme_hook_suggestions'][] = "page__vip";
		return;
	}

	//drupal_add_css(drupal_get_path("theme", "furnimobile"). "/lib/fancybox3/jquery.fancybox.min.css");
	//drupal_add_js(drupal_get_path("theme", "furnimobile"). "/lib/fancybox3/jquery.fancybox.js");

    drupal_add_css(drupal_get_path("theme", "furnimobile"). "/lib/lightcase/css/lightcase.min.css");
    drupal_add_js(drupal_get_path("theme", "furnimobile"). "/lib/lightcase/js/lightcase.min.js");
    drupal_add_js(drupal_get_path("theme", "furnimobile"). "/lib/lightcase/jquery.events.touch.js");

    // BXSlider
    //drupal_add_js($theme_path . '/lib/bxslider/jquery.bxslider.min.js');
    //drupal_add_css($theme_path . '/lib/bxslider/jquery.bxslider.css');

    // Slick
    drupal_add_js($theme_path . '/lib/slick/slick.min.js');
    drupal_add_css($theme_path . '/lib/slick/slick.css');
    drupal_add_css($theme_path . '/lib/slick/slick-theme.css');

	//google Adwords tracking
	//drupal_add_js('http://www.googleadservices.com/pagead/conversion_async.js', 'external');

	//set up top menu
	//furnimobile_set_up_top_menu($vars);
	
	//navigation menu items
	//furnimobile_set_up_footer_menu($vars);
	
	$italia_editions_gallery_paths = array("natuzzi-italia", "natuzzi-italia/*", "natuzzi-italia/*/*", "natuzzi-editions", "natuzzi-editions/*", "natuzzi-editions/*/*");
	$paths_no_title = array_merge(array("<front>", "home", "node/*", "sale", "collections", "taxonomy/term/*", "moving-sale","bauformat-kitchen-and-bath", "modern-european-bathroom-cabinets", "modern-european-kitchen-cabinets", "miele-appliances", "ekornes-stressless-recliners-sofas", "stressless-ekornes-promotion", "bdi-furniture-media-event", "promo/*"), $italia_editions_gallery_paths);
	$vars['show_title'] = !drupal_match_path(current_path(), implode("\n", $paths_no_title));
	
	if(drupal_match_path(current_path(), implode("\n", array("<front>", "front", "node/*","bauformat-kitchen-and-bath")))) {
		_page_include_mightyslider_resources();
	}
	
	//!BANNER promo
  $show_promo = FALSE;
	if (variable_get('show_banner', FALSE)) {

        //$show_banner = furn_global_show_promo();
		$promos = furn_global_get_promos();
		$vars['promos'] = $promos;

    // get header promo text
    $header_promo = furn_global_get_header_promo();
    if (is_array($header_promo) && count($header_promo) > 0) {
        $vars['header_promo'] = $header_promo;
    }


		if (drupal_match_path(current_path(), implode("\n", array_merge(
                array('node/*', 'clearance', 're-vive', 'collections', 'taxonomy/term/*', 'brands'),
                $italia_editions_gallery_paths
            )))
        ) {
          $show_promo = TRUE;
    }
  }


  if ($show_promo) {

    $generic_banner = TRUE;

    $item_brand = 0;
    $brand_italia = 21;
    $brand_editions = 22;
    $brand_calligaris = 31;
    if (isset($vars['node']) && $vars['node']->type == 'item') {
      $item_brand = field_get_items('node', $vars['node'], 'field_brand');
      if ($item_brand) {
        $item_brand = $item_brand['0']['tid'];
      }
    }

    if ($item_brand == $brand_editions || drupal_match_path(current_path(), implode("\n", array("natuzzi-editions",
        "natuzzi-editions/*", "natuzzi-editions/*/*", "taxonomy/term/22")))) {

      $vars['page']['description_section'] = 'Natuzzi Editions is a modern/contemporary furniture line, designed in 
      Italy. All modern collections are upholstered with genuine Italian leather and fabric. Natuzzi Editions sofas, 
      sectionals, and armchairs are well-known for their complete comfort, style, and versatile configurations &mdash; a 
      perfect fit for American lifestyles. <br/> Furnitalia is the largest Natuzzi retailer in N California. We offer 
      large selection of Natuzzi furniture in our 35,000 sq ft showroom. Exceptional service and best prices are 
      guaranteed.';
      $vars['show_title'] = TRUE;
//      drupal_set_title('Natuzzi Editions Leather & Gabric Living Room Furniture');

      if (isset($promos['natuzzi-editions'])) {
        $vars['page']['banner'] = theme("natuzzi-editions-promo-block");
        $generic_banner = FALSE;
      }

    } elseif ($item_brand == $brand_italia || drupal_match_path(current_path(), implode("\n", array("natuzzi-italia", "natuzzi-italia/*", "natuzzi-italia/*/*", "taxonomy/term/21")))) {
      if (isset($promos['natuzzi-italia'])) {
        $vars['page']['banner'] = theme("natuzzi-italia-promo-block");
        $generic_banner = FALSE;
      }
    } elseif ($item_brand == $brand_calligaris || drupal_match_path(current_path(), implode("\n", array("taxonomy/term/$brand_calligaris")))) {
      if (isset($promos['calligaris'])) {
        $vars['page']['banner'] = theme("calligaris-promo-block");
        $generic_banner = FALSE;
      }
    }

    if ($generic_banner) {



            $promo_html = '';
            $promo_html .= '<ul class="bxslider" style="margin:0;padding:0;list-style-type:none">';
            foreach($promos as $name => $promo) {
                foreach ($promo['banners'] as $key => $banner) {

                    $promo_html .= '<li >';
                    $banner_img = theme("image", array(
                            "path" => $banner['image'],
                            "title" => $banner['alt'],
                            "attributes" => array("ref" => "", "align" => "center")
                        )
                    );
                    if (isset($banner['link'])) {
                        $promo_html .= '<a href="' . $banner['link'] . '" title="' . $banner['alt'] . '">' . $banner_img . '</a>';
                    } else {
                        $promo_html .= $banner_img;
                    }
                    $promo_html .= '</li>';
                }
            }
            $promo_html .= '</ul>';
            $banner_html = '<div id="promo-slider">
                                <div id="category-image-pane"> ' . $promo_html . ' </div>
                            </div>';
      

		  	//add 10% OFF coupon banner
		  	/*$banner_html .= l('<img src="' . $files_dir . '/promo/coupons/10_percent_coupon_mobile.jpg"/>',
				"coupon",
				array(
				  'html' => TRUE,
				  'attributes' => array(
					'class' => array('coupon-link')
				  ),
				)
		  	);*/
      
			$vars['page']['banner'] = array(
				'#markup' => $banner_html,
			);
		}
	}
	
	if (isset($vars['node']) && arg(2) != 'edit') {
		$node = $vars['node'];
		if ($node->type == 'item') {
			
			_page_include_mightyslider_resources();
		
			$content = $vars['page']['content']['system_main']['nodes'][$node->nid];

			furn_global_preprocess_node_common_fields($content, 'page', $node);
						
			$vars['page']['content']['system_main']['nodes'][$node->nid] = $content;			
				
		} else if ($node->type == 'blog') {
			$vars['show_title'] = TRUE;

    } else if ($node->type == 'landing_page') {

      $furnitheme_path = drupal_get_path('theme', 'furnitheme');

      drupal_add_css($furnitheme_path . "/css/bootstrap.css", array(
        'weight' => -10,
        'every_page' => FALSE,
      ));

      drupal_add_css($furnitheme_path . "/css/paragraphs.css", array(
        'weight' => 0,
        'every_page' => FALSE,
      ));

      $vars['page']['banner'] = [];

    }
	}
	
	if (arg(0) == "search") {
		$vars['page']['highlighted']['#access'] = FALSE;
	}
	
	if (arg(0) == 'node' && arg(1) == '33') {
		//if we are on newsletter subscription page, don't display subscription block
		$vars['page']['footer']['webform_client-block-33'] = "";
	}

  if (arg(0) === 'miele-appliances') {
      drupal_add_library('system', 'ui.tabs');
  }
	
	_page_set_breadcrumbs($vars);

  $is_editions_promo_page = stristr(current_path(), 'natuzzi-editions-promo-2018');
  if ($is_editions_promo_page) {
      drupal_add_css($theme_path . "/styles/bootstrap.css", array(
          'weight' => -10,
          'every_page' => FALSE,
      ));
  }

/*  $is_italia_promo_page = stristr(current_path(), 'promo/natuzzi');
  if ($is_italia_promo_page) {
    drupal_add_css("https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css", array(
        'weight' => -10,
        'every_page' => FALSE,
        'type' => "external",
    ));
  }*/

    $is_renovation_promo_page = stristr(current_path(), 'renovation-private-sale')
        || stristr(current_path(), 'renovation-sale-discount')
        || stristr(current_path(), 'renovation-sale-exclusive-discount')
        || stristr(current_path(), 'renovation-sale-discount-adv') ;
    if ($is_renovation_promo_page) {
        drupal_add_css($theme_path . "/styles/request-form.css", array(
            'weight' => -10,
            'every_page' => FALSE,
        ));
    }
	

}

function furnimobile_webform_mail_headers($variables) {
  $headers = array(
    'Content-Type'  => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'X-Mailer'      => 'Drupal Webform (PHP/'. phpversion() .')',
  );
  return $headers;
}

/*function furnimobile_set_up_footer_menu (&$vars) {
	$attrs = array('attributes' => array('class'=>array('furn-grey')));
		
	$info_menu = array(
		'<span class="menu-label">About Furnitalia</span>',
		l("About us", 'about', $attrs),
		//l("Contact us", "contact", $attrs), 
		l("Store Locations", 'store-locations', $attrs),
		l("Blog", "blog", $attrs),
	);
	$vars['footer_info_menu'] = array(
		'#theme' => 'item_list',
		'#items' => array_values($info_menu),
		'#type' => 'ul',
		'#attributes' => array('class' => 'links'),
	);
	
	$user_menu = array();	
	$user_menu []= '<span class="menu-label">Account information</span>';
	$user_menu []= l("Account Details", 'user', $attrs);
	$user_menu []= l("Favorites", 'my-favorites', $attrs);		
	//$user_menu []= l("Order status", 'my-orders', $attrs);		
	//$user_menu []= l("My Cart", 'cart', $attrs);
	
	$vars['footer_user_menu'] = array(
		'#theme' => 'item_list',
		'#items' => array_values($user_menu),
		'#type' => 'ul',
		'#attributes' => array('class' => 'links'),
	);
	
	$policies_menu = array(
		'<span class="menu-label">Policies and information</span>',
		l("FAQ", 'faq', $attrs),
		l("Shipping and Delivery", "shipping-deliveries", $attrs), 
		l("Terms of Service", "service-terms", $attrs),
		l("Privacy Policy", "privacy-policy", $attrs),	
	);
	$vars['footer_policy_menu'] = array(
		'#theme' => 'item_list',
		'#items' => array_values($policies_menu),
		'#type' => 'ul',
		'#attributes' => array('class' => 'links'),
	);
}
*/
/*
function furnimobile_set_up_top_menu (&$vars) {
	global $user;
	
	$top_menu = array();

	//$top_menu []= "<a href=\"#\" title=\"menu\" id=\"menu-toggle\">MENU</a>";
	$attrs = array('attributes' => array('class'=>array('furn-grey')));
    $top_menu []= l("Store Locations", 'store-locations', $attrs);
	$top_menu []= l("Favorites", 'my-favorites', $attrs);
	//$top_menu []= l("My Cart", 'cart', $attrs);
	
	
	$vars['page']['page_top']['top_menu'] = array(
		'#theme' => 'item_list',
		'#items' => array_values($top_menu),
		'#type' => 'ul',
		'#attributes' => array('class' => 'menu furn-ucase'),
	);
}*/

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/*function furnimobile_preprocess_node(&$vars, $hook) {
	if ($vars['type'] == 'item') {
		preprocess_node_common_fields($vars['content'], 'page', $vars['node']);	
	}
}*/

/**
 * Implements hook_form_alter().
 *
 * Alter search form
 */
function furnimobile_form_webform_client_form_33_alter(&$form, &$form_state, $form_id) {
	
	global $base_path, $theme_path;
	
	$path = $base_path . $theme_path;
	
	$form['actions']['submit']['#theme_wrappers'] = array("image_button");
	$form['actions']['submit']['#button_type'] = "image";
	$form['actions']['submit']['#src'] = $path . "/images/buttons/JOIN_Button_50x29.png";
}


/**
 * Implements hook_form_alter().
 *
 * Alter search form
 */
function furnimobile_form_search_form_alter(&$form, &$form_state, $form_id) {

	global $base_path, $theme_path;
	
	$path = $base_path . $theme_path;
	
	unset($form['advanced']);

	
	$form['basic']['submit']['#theme_wrappers'] = array("image_button");
	$form['basic']['submit']['#button_type'] = "image";
	$form['basic']['submit']['#src'] = $path . "/images/buttons/GO_Button_50x29.png";

}

/**
 * Implements hook_form_alter().
 *
 * Alter search form
 */
function furnimobile_form_search_block_form_alter(&$form, &$form_state, $form_id) {
	
	global $base_path, $theme_path;
	
	$path = $base_path . $theme_path;
	
	$form['search_block_form']['#default_value'] = "SEARCH";
	
	$form['actions']['submit']['#theme_wrappers'] = array("image_button");
	$form['actions']['submit']['#button_type'] = "image";
	$form['actions']['submit']['#src'] = $path . "/images/buttons/GO_Button_50x29.png";

}

function furnimobile_views_mini_pager($vars) {
  global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  if ($pager_total[$element] > 1) {

    $li_previous = theme('pager_previous',
      array(
        'text' => (isset($tags[1]) ? $tags[1] : t('‹‹')),
        'element' => $element,
        'interval' => 1,
        'parameters' => $parameters,
      )
    );
    if (empty($li_previous)) {
      $li_previous = "&nbsp;";
    }

    $li_next = theme('pager_next',
      array(
        'text' => (isset($tags[3]) ? $tags[3] : t('››')),
        'element' => $element,
        'interval' => 1,
        'parameters' => $parameters,
      )
    );

    if (empty($li_next)) {
      $li_next = "&nbsp;";
    }

    $items[] = array(
      'class' => array('pager-previous'),
      'data' => $li_previous,
    );

    $items[] = array(
      'class' => array('pager-current'),
      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );
    

    // Set up link to view all results in one page - taken from theme_pager_link
    $query = array();
    if (count($parameters)) {
      $query = drupal_get_query_parameters($parameters, array());
    }
    if ($query_pager = pager_get_query_parameters()) {
      $query = array_merge($query, $query_pager);
    }
    $query['items_per_page'] = 'All';
    $attributes['href'] = url($_GET['q'], array('query' => $query));
    $li_view_all = '<a' . drupal_attributes($attributes) . '>' . 'View All' . '</a>';
    
    $items[] = $li_view_all;
    
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => array('pager')),
      )
    );
  } else {
  
    // Set up link to view results by pages
    $query = array();
    if (count($parameters)) {
      $query = drupal_get_query_parameters($parameters, array());
    }
    if ($query_pager = pager_get_query_parameters()) {
      $query = array_merge($query, $query_pager);
    }
    $query['items_per_page'] = '9';
    $attributes['href'] = url($_GET['q'], array('query' => $query));
    $li_view_paged = '<a' . drupal_attributes($attributes) . '>' . 'View by pages' . '</a>';
    
    $items[] = $li_view_paged;

    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => array('pager')),
      )
    );
  }
}

/**
 * Custom theme for pager links
 */
function furnimobile_pager($variables) {

  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }

  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.
  
  $first_item_text = '1';
  $first_item_class = "";
  if ($i < 2) {
	  $first_item_class = "with-separator";	//show border separator if current page is 1
  }
  
  $li_first = theme('pager_first', array('text' =>  $first_item_text, 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next ›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' =>  $pager_max, 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }
    
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first', $first_item_class),
        'data' => $li_first,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {

      	$cur_text = $i;
      	$sep_class = 'with-separator';  	
      	if ($i == $pager_last && $pager_last + 1 != $pager_max || $i == $pager_max) {
    		//unset separator if ... follows current  		
			$cur_text = $i;
			$sep_class = '';
        }
        
        //if last item in the range, unset the explicit 'last' pager
        if ($i == $pager_max) {
        	$li_last = "";
    	}
          
        if ($i < $pager_current && $i > 1) {
          $items[] = array(
            'class' => array('pager-item', $sep_class),
            'data' => theme('pager_previous', array('text' => $cur_text, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {         
          $items[] = array(
            'class' => array('pager-current', $sep_class),
            'data' => $cur_text,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item', $sep_class),
            'data' => theme('pager_next', array('text' => $cur_text, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
    	
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

function _page_include_mightyslider_resources() {
	//add  gallery JS/css files
	//drupal_add_css(drupal_get_path('theme', 'furnimobile') . "/lib/mightyslider/src/css/mightyslider.css", array('group' => CSS_DEFAULT, 'every_page' => FALSE));
	//drupal_add_css(drupal_get_path('theme', 'furnimobile') . "/styles/gallery.css", array('group' => CSS_THEME, 'every_page' => FALSE));			
	
	
	//drupal_add_js(drupal_get_path('theme', 'furnimobile') . "/lib/mightyslider/src/js/mightyslider.min.js", array('group' => JS_THEME));
	drupal_add_js(drupal_get_path('theme', 'furnimobile') . "/lib/mightyslider/src/js/mightyslider_new.min.js", array('group' => JS_THEME));
	drupal_add_js(drupal_get_path('theme', 'furnimobile') . "/lib/mightyslider/js/jquery.easing.1.3.js", array('group' => JS_THEME));

	// !!! Disable touch/swipe events for now
	//drupal_add_js(drupal_get_path('theme', 'furnimobile') . "/lib/mightyslider/js/jquery.mobile.just-touch.js");
}

function _page_set_breadcrumbs(&$vars) {

	$natuzzi_italia = 21;
	$natuzzi_editions = 22;
	
	$attrs = array('attributes' => array('class'=>array('furn-grey', 'furn-e2-a')));
	$active_class= "active-breadcrumb furn-red furn-ucase furn-e2-a";

	#Process breadcrumbs	
	$links = array();
	
	if (isset($vars['node']) && $vars['node']->type == 'item'){
		#item page breadcrumbs

		#get most relevant category of the item
		$node = $vars['node'];
		$weight = 1000;
		$term_index = -1;
		foreach($node->field_category['und'] as $i => $cat) {
			if ($weight > $cat['taxonomy_term']->weight) {
				$term_index = $i;
			}
		}
		$category = $node->field_category['und'][$term_index]['taxonomy_term'];
		
		#get item's parent category
		$parent_term = "";
		$parent_term_tid = furn_global_get_parent_category($category->tid);
		if ($parent_term_tid) {
			$parent_term = taxonomy_term_load($parent_term_tid);
		}
		
		#determine if it falls under ALL categories or Natuzzi Italia or Natuzzi Editions
		$brand = $node->field_brand['und'][0]['tid'];
		$path_prefix = "";
		if ($brand == $natuzzi_italia) {
			$top_breadcrumb = l("Natuzzi Italia", "natuzzi-italia", $attrs);
			$path_prefix = "natuzzi-italia";
		} else if ($brand == $natuzzi_editions) {
			$top_breadcrumb = l("Natuzzi Editions", "natuzzi-editions", $attrs);
			$path_prefix = "natuzzi-editions";
		} else {
			$top_breadcrumb = l("All Collections", "collections", $attrs);
		}
		
		$links[] = $top_breadcrumb;
		if ($parent_term && $parent_term_tid != $category->tid) {
			$links[] = l($parent_term->name, (empty($path_prefix) ? "" : $path_prefix . "/") . drupal_get_path_alias('taxonomy/term/' . $parent_term->tid), $attrs);
		}
		$links[] = l($category->name, (empty($path_prefix) ? "" : $path_prefix . "/") . drupal_get_path_alias("taxonomy/term/$category->tid"), $attrs);

	}
	else if (arg(0) == 'natuzzi-italia'  || arg(0) == 'natuzzi-editions' || (arg(0) == 'taxonomy' && arg(1) == 'term')) {
		
		
		if (arg(0) == 'natuzzi-italia' || arg(0) == 'natuzzi-editions') {
			if(arg(1) == "") {
				$links[] = l("Home", "<front>", $attrs);
				$links[] = "<span class=\"". $active_class . "\">". ucwords(str_replace("-", " ", arg(0))) ."</span>";
			} else {
				$alias = arg(1);
				if (arg(2) != "") {
					$alias .= "/" . arg(2);
				}
				$normal_path = explode('/', drupal_get_normal_path($alias)); //TO-DO check for validity
				if (is_array($normal_path) && count($normal_path) == 3) { //taxonomy/term/xxx
					$active_cat_tid = $normal_path[2];
					$active_parent_tid = furn_global_get_parent_category($active_cat_tid);
				}
				
				if (arg(0) == 'natuzzi-italia') {
					$top_breadcrumb = l("Natuzzi Italia", "natuzzi-italia", $attrs);
				} else if (arg(0) == 'natuzzi-editions') {
					$top_breadcrumb = l("Natuzzi Editions", "natuzzi-editions", $attrs);
				}
				
				$parent_term = taxonomy_term_load($active_parent_tid);
				$term = taxonomy_term_load($active_cat_tid);
	
				$links[] = $top_breadcrumb;			
				if ($active_cat_tid != $active_parent_tid) {
					$links[] = l($parent_term->name, arg(0) . "/" . drupal_get_path_alias('taxonomy/term/' . $parent_term->tid), $attrs);
					$links[] = "<span class=\"". $active_class . "\">$term->name</span>";		
				} else {
					$links[] = "<span class=\"". $active_class . "\">$parent_term->name</span>";
				}
			}
			
		} else {
			$active_cat_tid = arg(2);
			$term = taxonomy_term_load($active_cat_tid);
			
			if ($term->vid == 3) { 
				//currently brand page is in focus
				$links[] = l("All Brands", "brands", $attrs);	
				$links[] = "<span class=\"". $active_class . "\">$term->name</span>";
			} else {	
				//category page is in focus
				$active_parent_tid = furn_global_get_parent_category($active_cat_tid);
				$links[] = l("All Collections", "collections", $attrs);
				if ($active_cat_tid != $active_parent_tid) {
					$parent_term = taxonomy_term_load($active_parent_tid);
					$links[] = l($parent_term->name, drupal_get_path_alias('taxonomy/term/' . $parent_term->tid), $attrs);
				}
				$links[] = "<span class=\"". $active_class . "\">$term->name</span>";
			}
		}
	}
	
	if (count($links) > 1){
		drupal_set_breadcrumb($links);
		$vars['breadcrumb'] = theme('breadcrumb', $links);
	} else {
		drupal_set_breadcrumb(array());
	}
	
	
}

function furnimobile_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' <span class="furn-e2-a separator">/</span> ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function furnimobile_preprocess_html(&$variables, $hook) {
	$italia_editions_gallery_paths = array("natuzzi-italia", "natuzzi-italia/*", "natuzzi-italia/*/*", "natuzzi-editions", "natuzzi-editions/*", "natuzzi-editions/*/*");
	$paths = array_merge(array("sale", "clearance", "collections", "taxonomy/term/*"), $italia_editions_gallery_paths);
	if(drupal_match_path(current_path(), implode("\n", $paths))) {
		 $variables['classes_array'][] = 'gallery-grid';
	}
	if (in_array(arg(0), array("coupon", "re-vive", "vip", "promo", "home"))) {
        $variables['classes_array'][] = 'revive';
        $variables['classes_array'][] = 'full-width';
	}
}

/**
 * Implements hook_html_head_later(...)
 */
function furnimobile_html_head_alter(&$head_elements) {

	//dsm($head_elements);
	//foreach (preg_grep('/^drupal_add_html_head_link:canonical:/', array_keys($head_elements)) as $key) {
	foreach (preg_grep('/^metatag_canonical/', array_keys($head_elements)) as $key) {
		
		//if (strpos($key,'</') !== false) {
			//remove canonical urls of the form ...</item etc.
		//	unset($head_elements[$key]);

		//} else {

			try {
				//$new_canonical_href = preg_replace('/m\.furnitalia/i', 'www.furnitalia', $head_elements[$key]['#attributes']['href']);
				$new_canonical_href = preg_replace('/m\.furnitalia/i', 'www.furnitalia', $head_elements[$key]['#value']);
				$head_elements[$key]['#value']= $new_canonical_href;
                //dsm('new canonical: ' . $new_canonical_href);
			} catch (Exception $e) {
				$link = current_path();
				//dsm("Error setting canonical URL, key=" . $key);
				watchdog('furnitalia', 'Error setting canonical meta tag, key=%key', array('%key' => $key), WATCHDOG_ERROR, $link);	
			}	
		//}
	}

    // custom description headers
    if (arg(0) == 'miele-appliances') {

        $metatag_description = array(
            '#type' => 'html_tag',
            '#tag' => 'meta',
            '#attributes' => array(
                'name' => 'description',
                'content' => "Furnitalia is an authorized direct reseller of Miele in California. 
                Miele - the world's most inspiring kitchen, at an inspiring price. 
                Built to last. Engineered to amaze.",
            ),
        );
        $head_elements['metatag_description'] = $metatag_description;
    }

}

/**
 * Implementation of hook_js_alter
 */
function furnimobile_js_alter(&$javascript) {

	if (isset($javascript['misc/jquery.form.js'])) {
		$jquery_path = drupal_get_path('theme','furnitheme') . '/js/jquery.form.min.js';

		//We duplicate the important information from the Drupal one
		$javascript[$jquery_path] = $javascript['misc/jquery.form.js'];
		//..and we update the information that we care about
		$javascript[$jquery_path]['version'] = '3.35';
		$javascript[$jquery_path]['data'] = $jquery_path;

		unset($javascript['misc/jquery.form.js']);

	}

    if (arg(0) == 'vip') {
        unset($javascript["sites/all/themes/furnimobile/script.js"]);
    }
}

/**
 * @param $form
 * @param $form_state
 * @param $form_id
 * Implement hook_form_alter
 */
function furnimobile_form_webform_client_form_alter(&$form, &$form_state, $form_id) {
    // see if webform_client_form_ is in the form_id
    $vip_nid = variable_get('furn_preferred_customer_nid', 1237);
    if(strstr($form_id, 'webform_client_form_' . $vip_nid)) {

        $form['#attributes']['class'][] = 'our_form';
        $form['submitted']['name']['#prefix'] = '<div class="cont" data-name="submitted[name]" data-first_name="1" data-ordering="0" data-required="1" data-label="First name" data-type="input">';
        $form['submitted']['name']['#title_display'] = 'invisible';
        $form['submitted']['name']['#attributes'] = array(
            'placeholder' => 'Name*',
            'class' => array('input'),
        );
        $form['submitted']['name']['#suffix'] = '</div>';

        $form['submitted']['phone']['#prefix'] = '<div class="cont " data-name="submitted[phone]" data-ordering="1" data-pattern="\(\d\d\d\) \d\d\d-\d\d\d\d" data-required="1" data-mask="(999) 999-9999" data-phone="1" data-label="Phone number" data-type="tel">';
//        <input type="tel" pattern="\d \(\d\d\d\) \d\d\d-\d\d-\d\d" data-mask="9 (999) 999-99-99" class="input" name="widget_1" value="" required placeholder="Phone*" />
//        $form['submitted']['phone']['#type'] = 'tel';
        $form['submitted']['phone']['#title_display'] = 'invisible';
        $form['submitted']['phone']['#attributes'] = array(
            'placeholder' => 'Phone*',
            'class' => array('input'),
            'type' => 'tel',
            'pattern' => "\(\d\d\d\) \d\d\d-\d\d\d\d",
            'data-mask' => "(999) 999-9999",
        );
        $form['submitted']['phone']['#suffix'] = '</div>';

        $form['submitted']['email']['#prefix'] = '<div class="cont " data-name="submitted[email]" data-ordering="2" data-required="1" data-label="Your e-mail" data-type="email" data-email="1">';
//            <input type="email" class="input" name="widget_2" value="" required placeholder="E-mail*" />
        $form['submitted']['email']['#title_display'] = 'invisible';
        $form['submitted']['email']['#attributes'] = array(
            'placeholder' => 'E-mail*',
            'class' => array('input'),
        );
        $form['submitted']['email']['#suffix'] = '</div>';

        $form['submitted']['birth_date']['#prefix'] = '<div class="cont " data-name="submitted[birth_date]" data-ordering="4" data-label="Birth Date" data-type="bday">';
//            <input type="text" class="input" name="widget_4" value="" required placeholder="Birth date" pattern="\d\d-\d\d-\d\d\d\d" data-mask="99-99-9999"/>
        $form['submitted']['birth_date']['#title_display'] = 'invisible';
        $form['submitted']['birth_date']['#attributes'] = array(
            'placeholder' => 'Birth date',
            'class' => array('input'),
            'pattern' => "\d\d-\d\d-\d\d\d\d",
            'data-mask' => "99-99-9999",
        );
        $form['submitted']['birth_date']['#suffix'] = '</div>';

        $form['submitted']['note']['#prefix'] = '<div class="cont ">';
//            <input type="text" class="input" name="widget_5" value="" required placeholder="Note"   />
        $form['submitted']['note']['#title_display'] = 'invisible';
        $form['submitted']['note']['#attributes'] = array(
            'placeholder' => 'Note',
            'class' => array('input'),
        );
        $form['submitted']['note']['#suffix'] = '</div>';

        $form['actions']['submit']['#prefix'] = '<div class="block block-button is-submit" id="block-new26" >';
        $form['actions']['submit']['#suffix'] = '</div>';

        $form['actions']['submit']['#ajax'] = array(
            'callback' => 'furn_global_webform_js_submit',
            'wrapper' => $form['#id'],
            'method' => 'replace',
            'effect' => 'fade',
			'event' => 'click',
        );

    }
}

function furnimobile_uc_product_model($variables) {
    $model = $variables['model'];
    $attributes = $variables['attributes'];
    $attributes['class'][] = "product-info";
    $attributes['class'][] = "model";

    $output = '<div ' . drupal_attributes($attributes) . '>';
    $output .= '<span class="product-info-label">' . t('SKU') . ':</span> ';
    $output .= '<span class="product-info-value" property="schema:model">' . check_plain($model) . '</span>';
    $output .= '</div>';

    return $output;
}

/**
 * Process variables for paragraphs-items.tpl.php
 */
function furnimobile_preprocess_paragraphs_items(&$variables, $hook)
{
//  if ($variables['element']['#bundle'] == 'content_strap') {
//  }
  $variables['theme_hook_suggestions'][] = 'paragraphs_items__' . $variables['element']['#field_name'] . '__' . $variables['element']['#bundle'];
  $variables['theme_hook_suggestions'][] = 'paragraphs_items__' . $variables['element']['#field_name'] . '__' . $variables['element']['#bundle'] . '__' . $variables['view_mode'];
}

/**
 * Process variables for paragraphs-items.tpl.php
 */
function furnimobile_preprocess_entity(&$variables, $hook)
{
  if ($variables['entity_type'] == 'paragraphs_item') {
    if (in_array($variables['elements']['#bundle'], ['image_content', 'webform_content', 'content_strap'])) {
//      dpm($variables);
      $variables['paragraph'] = $variables['elements']['#entity'];
    }

    if ($variables['elements']['#bundle'] == 'content_strap') {
//      $variables['test'] = field_view_field('paragraphs_item', $variables['paragraphs_item'], 'field_paragraphs_reference');
      $strap_column_fields = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_paragraphs_reference');
      $strap_columns = [];
      foreach ($strap_column_fields as $col) {
        $strap_columns [] = field_view_value('paragraphs_item', $variables['paragraphs_item'], 'field_paragraphs_reference', $col);
      }
      $variables['columns'] = $strap_columns;
//      dpm($variables);
    }
  }
}

function furnimobile_preprocess_field(&$variables)
{
  if ($variables['element']['#field_name'] == 'field_paragraphs_reference') {
    if ($variables['element']['#bundle'] == 'content_strap') {
//      dpm("content strap field preprocess");
//      dpm($variables);
//      krumo($variables);
    }
  }

  // add responsive image class to image strap
  if ($variables['element']['#field_name'] == 'field_image') {
    if ($variables['element']['#bundle'] == 'image_strap') {
      foreach ($variables['items'] as $key => $item) {
        $variables['items'][$key]['#item']['attributes']['class'][] = 'img-responsive';
      }
    }
  }
}
