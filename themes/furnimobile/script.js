/*!
 * headroom.js v0.9.3 - Give your page some headroom. Hide your header until you need it
 * Copyright (c) 2016 Nick Williams - http://wicky.nillia.ms/headroom.js
 * License: MIT
 */

(function (root, factory) {
    'use strict';

    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    }
    else if (typeof exports === 'object') {
        // COMMONJS
        module.exports = factory();
    }
    else {
        // BROWSER
        root.Headroom = factory();
    }
}(this, function () {
    'use strict';

    /* exported features */

    var features = {
        bind: !!(function () {
        }.bind),
        classList: 'classList' in document.documentElement,
        rAF: !!(window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame)
    };
    window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame;

    /**
     * Handles debouncing of events via requestAnimationFrame
     * @see http://www.html5rocks.com/en/tutorials/speed/animations/
     * @param {Function} callback The callback to handle whichever event
     */
    function Debouncer(callback) {
        this.callback = callback;
        this.ticking = false;
    }

    Debouncer.prototype = {
        constructor: Debouncer,

        /**
         * dispatches the event to the supplied callback
         * @private
         */
        update: function () {
            this.callback && this.callback();
            this.ticking = false;
        },

        /**
         * ensures events don't get stacked
         * @private
         */
        requestTick: function () {
            if (!this.ticking) {
                requestAnimationFrame(this.rafCallback || (this.rafCallback = this.update.bind(this)));
                this.ticking = true;
            }
        },

        /**
         * Attach this as the event listeners
         */
        handleEvent: function () {
            this.requestTick();
        }
    };
    /**
     * Check if object is part of the DOM
     * @constructor
     * @param {Object} obj element to check
     */
    function isDOMElement(obj) {
        return obj && typeof window !== 'undefined' && (obj === window || obj.nodeType);
    }

    /**
     * Helper function for extending objects
     */
    function extend(object /*, objectN ... */) {
        if (arguments.length <= 0) {
            throw new Error('Missing arguments in extend function');
        }

        var result = object || {},
            key,
            i;

        for (i = 1; i < arguments.length; i++) {
            var replacement = arguments[i] || {};

            for (key in replacement) {
                // Recurse into object except if the object is a DOM element
                if (typeof result[key] === 'object' && !isDOMElement(result[key])) {
                    result[key] = extend(result[key], replacement[key]);
                }
                else {
                    result[key] = result[key] || replacement[key];
                }
            }
        }

        return result;
    }

    /**
     * Helper function for normalizing tolerance option to object format
     */
    function normalizeTolerance(t) {
        return t === Object(t) ? t : {down: t, up: t};
    }

    /**
     * UI enhancement for fixed headers.
     * Hides header when scrolling down
     * Shows header when scrolling up
     * @constructor
     * @param {DOMElement} elem the header element
     * @param {Object} options options for the widget
     */
    function Headroom(elem, options) {
        options = extend(options, Headroom.options);

        this.lastKnownScrollY = 0;
        this.elem = elem;
        this.tolerance = normalizeTolerance(options.tolerance);
        this.classes = options.classes;
        this.offset = options.offset;
        this.scroller = options.scroller;
        this.initialised = false;
        this.onPin = options.onPin;
        this.onUnpin = options.onUnpin;
        this.onTop = options.onTop;
        this.onNotTop = options.onNotTop;
        this.onBottom = options.onBottom;
        this.onNotBottom = options.onNotBottom;
    }

    Headroom.prototype = {
        constructor: Headroom,

        /**
         * Initialises the widget
         */
        init: function () {
            if (!Headroom.cutsTheMustard) {
                return;
            }

            this.debouncer = new Debouncer(this.update.bind(this));
            this.elem.classList.add(this.classes.initial);

            // defer event registration to handle browser
            // potentially restoring previous scroll position
            setTimeout(this.attachEvent.bind(this), 100);

            return this;
        },

        /**
         * Unattaches events and removes any classes that were added
         */
        destroy: function () {
            var classes = this.classes;

            this.initialised = false;
            this.elem.classList.remove(classes.unpinned, classes.pinned, classes.top, classes.notTop, classes.initial);
            this.scroller.removeEventListener('scroll', this.debouncer, false);
        },

        /**
         * Attaches the scroll event
         * @private
         */
        attachEvent: function () {
            if (!this.initialised) {
                this.lastKnownScrollY = this.getScrollY();
                this.initialised = true;
                this.scroller.addEventListener('scroll', this.debouncer, false);

                this.debouncer.handleEvent();
            }
        },

        /**
         * Unpins the header if it's currently pinned
         */
        unpin: function () {
            var classList = this.elem.classList,
                classes = this.classes;

            if (classList.contains(classes.pinned) || !classList.contains(classes.unpinned)) {
                classList.add(classes.unpinned);
                classList.remove(classes.pinned);
                this.onUnpin && this.onUnpin.call(this);
            }
        },

        /**
         * Pins the header if it's currently unpinned
         */
        pin: function () {
            var classList = this.elem.classList,
                classes = this.classes;

            if (classList.contains(classes.unpinned)) {
                classList.remove(classes.unpinned);
                classList.add(classes.pinned);
                this.onPin && this.onPin.call(this);
            }
        },

        /**
         * Handles the top states
         */
        top: function () {
            var classList = this.elem.classList,
                classes = this.classes;

            if (!classList.contains(classes.top)) {
                classList.add(classes.top);
                classList.remove(classes.notTop);
                this.onTop && this.onTop.call(this);
            }
        },

        /**
         * Handles the not top state
         */
        notTop: function () {
            var classList = this.elem.classList,
                classes = this.classes;

            if (!classList.contains(classes.notTop)) {
                classList.add(classes.notTop);
                classList.remove(classes.top);
                this.onNotTop && this.onNotTop.call(this);
            }
        },

        bottom: function () {
            var classList = this.elem.classList,
                classes = this.classes;

            if (!classList.contains(classes.bottom)) {
                classList.add(classes.bottom);
                classList.remove(classes.notBottom);
                this.onBottom && this.onBottom.call(this);
            }
        },

        /**
         * Handles the not top state
         */
        notBottom: function () {
            var classList = this.elem.classList,
                classes = this.classes;

            if (!classList.contains(classes.notBottom)) {
                classList.add(classes.notBottom);
                classList.remove(classes.bottom);
                this.onNotBottom && this.onNotBottom.call(this);
            }
        },

        /**
         * Gets the Y scroll position
         * @see https://developer.mozilla.org/en-US/docs/Web/API/Window.scrollY
         * @return {Number} pixels the page has scrolled along the Y-axis
         */
        getScrollY: function () {
            return (this.scroller.pageYOffset !== undefined)
                ? this.scroller.pageYOffset
                : (this.scroller.scrollTop !== undefined)
                ? this.scroller.scrollTop
                : (document.documentElement || document.body.parentNode || document.body).scrollTop;
        },

        /**
         * Gets the height of the viewport
         * @see http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
         * @return {int} the height of the viewport in pixels
         */
        getViewportHeight: function () {
            return window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;
        },

        /**
         * Gets the physical height of the DOM element
         * @param  {Object}  elm the element to calculate the physical height of which
         * @return {int}     the physical height of the element in pixels
         */
        getElementPhysicalHeight: function (elm) {
            return Math.max(
                elm.offsetHeight,
                elm.clientHeight
            );
        },

        /**
         * Gets the physical height of the scroller element
         * @return {int} the physical height of the scroller element in pixels
         */
        getScrollerPhysicalHeight: function () {
            return (this.scroller === window || this.scroller === document.body)
                ? this.getViewportHeight()
                : this.getElementPhysicalHeight(this.scroller);
        },

        /**
         * Gets the height of the document
         * @see http://james.padolsey.com/javascript/get-document-height-cross-browser/
         * @return {int} the height of the document in pixels
         */
        getDocumentHeight: function () {
            var body = document.body,
                documentElement = document.documentElement;

            return Math.max(
                body.scrollHeight, documentElement.scrollHeight,
                body.offsetHeight, documentElement.offsetHeight,
                body.clientHeight, documentElement.clientHeight
            );
        },

        /**
         * Gets the height of the DOM element
         * @param  {Object}  elm the element to calculate the height of which
         * @return {int}     the height of the element in pixels
         */
        getElementHeight: function (elm) {
            return Math.max(
                elm.scrollHeight,
                elm.offsetHeight,
                elm.clientHeight
            );
        },

        /**
         * Gets the height of the scroller element
         * @return {int} the height of the scroller element in pixels
         */
        getScrollerHeight: function () {
            return (this.scroller === window || this.scroller === document.body)
                ? this.getDocumentHeight()
                : this.getElementHeight(this.scroller);
        },

        /**
         * determines if the scroll position is outside of document boundaries
         * @param  {int}  currentScrollY the current y scroll position
         * @return {bool} true if out of bounds, false otherwise
         */
        isOutOfBounds: function (currentScrollY) {
            var pastTop = currentScrollY < 0,
                pastBottom = currentScrollY + this.getScrollerPhysicalHeight() > this.getScrollerHeight();

            return pastTop || pastBottom;
        },

        /**
         * determines if the tolerance has been exceeded
         * @param  {int} currentScrollY the current scroll y position
         * @return {bool} true if tolerance exceeded, false otherwise
         */
        toleranceExceeded: function (currentScrollY, direction) {
            return Math.abs(currentScrollY - this.lastKnownScrollY) >= this.tolerance[direction];
        },

        /**
         * determine if it is appropriate to unpin
         * @param  {int} currentScrollY the current y scroll position
         * @param  {bool} toleranceExceeded has the tolerance been exceeded?
         * @return {bool} true if should unpin, false otherwise
         */
        shouldUnpin: function (currentScrollY, toleranceExceeded) {
            var scrollingDown = currentScrollY > this.lastKnownScrollY,
                pastOffset = currentScrollY >= this.offset;

            return scrollingDown && pastOffset && toleranceExceeded;
        },

        /**
         * determine if it is appropriate to pin
         * @param  {int} currentScrollY the current y scroll position
         * @param  {bool} toleranceExceeded has the tolerance been exceeded?
         * @return {bool} true if should pin, false otherwise
         */
        shouldPin: function (currentScrollY, toleranceExceeded) {
            var scrollingUp = currentScrollY < this.lastKnownScrollY,
                pastOffset = currentScrollY <= this.offset;

            return (scrollingUp && toleranceExceeded) || pastOffset;
        },

        /**
         * Handles updating the state of the widget
         */
        update: function () {
            var currentScrollY = this.getScrollY(),
                scrollDirection = currentScrollY > this.lastKnownScrollY ? 'down' : 'up',
                toleranceExceeded = this.toleranceExceeded(currentScrollY, scrollDirection);

            if (this.isOutOfBounds(currentScrollY)) { // Ignore bouncy scrolling in OSX
                return;
            }

            if (currentScrollY <= this.offset) {
                this.top();
            } else {
                this.notTop();
            }

            if (currentScrollY + this.getViewportHeight() >= this.getScrollerHeight()) {
                this.bottom();
            }
            else {
                this.notBottom();
            }

            if (this.shouldUnpin(currentScrollY, toleranceExceeded)) {
                this.unpin();
            }
            else if (this.shouldPin(currentScrollY, toleranceExceeded)) {
                this.pin();
            }

            this.lastKnownScrollY = currentScrollY;
        }
    };
    /**
     * Default options
     * @type {Object}
     */
    Headroom.options = {
        tolerance: {
            up: 0,
            down: 0
        },
        offset: 0,
        scroller: window,
        classes: {
            pinned: 'headroom--pinned',
            unpinned: 'headroom--unpinned',
            top: 'headroom--top',
            notTop: 'headroom--not-top',
            bottom: 'headroom--bottom',
            notBottom: 'headroom--not-bottom',
            initial: 'headroom'
        }
    };
    Headroom.cutsTheMustard = typeof features !== 'undefined' && features.rAF && features.bind && features.classList;

    return Headroom;
}));


(function ($) {

    var menuStatus;
    var galleryView = "grid";
    var $html = $('html');

    Drupal.behaviors.furnitalia = {
        attach: function (context, settings) {

            /*var requestFormId = 'webform-client-form-34';
             $('#' + requestFormId).ajaxComplete(function(event, xhr, settings) {
             if ($(event.target.id) == requestFormId) {
             if($('div.webform-confirmation').length) {
             //record conversion
             trackAdwordsConversion('request_form');
             }
             }
             });*/
            $('form.webform-client-form input.form-submit', context).click(function () {
               var formId = $(this).parents('form').find('input[name=form_id]').attr('value') || 'unknown';

               //trigger GA tracking event if request went through OK
               if (typeof gtag !== "undefined") {
                   /*gtag('event', 'Form Submission', {
                     'event_category': formId, 
                     'event_label': '',
                     'value': ''
                   });*/
               }
               else if (typeof ga !== "undefined") {
                    //ga('send', 'event', 'Form Submission', formId);
               }
               if (typeof fbq !== "undefined") {
                    fbq('track', 'Lead');
               }

                //record conversion
                //trackAdwordsConversion('request_form');

            });

            $('#mc-embedded-subscribe-form input.button', context).click(function() {
                 //trigger GA tracking event if request went through OK
                 if (typeof ga !== "undefined") {
                     ga('send', 'event', 'Form', 'Click Submit', 'Mailchimp');
                     // ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);

                 }
                 if (typeof fbq !== "undefined") {
                     fbq('track', 'Lead');
                 }

             });


            InitAccordonMenu();
            InitItemPageGallery();
            InitDropDownMenu();
            InitGalleryControls();

            // ----------- SCROLL TO TOP LINK --------------------
            $("#scroll-top a").click(function () {

                var offset = $("#main-content").offset();
                var scrollTarget = $("#main-content");
                while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
                    scrollTarget = $(scrollTarget).parent();
                }
                // Only scroll upward
                if (offset.top - 10 < $(scrollTarget).scrollTop()) {
                    $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
                }

                return false;

            });

            var element_settings = {};
            element_settings.event = "click";
            element_settings.progress = {type: false};

            $("#request-quote").once(function () {
                element_settings.url = $(this).attr('href');

                var ajax = new Drupal.ajax("#request-quote", $('#request-quote')[0], element_settings);

                /*$(this).click(function(e) {
                 $("#product-additional").append('<h3 class="furn-ucase furn-red">REQUEST QUOTE</h3><div id="request-data"></div>').accordion('destroy');

                 e.preventDefault();
                 });*/

            });


            $('a.request-info').once(function () {
                element_settings.url = $(this).attr('href');
                var ajax = new Drupal.ajax("a.request-info", $(this)[0], element_settings);
            });

            $("#request-info").once(function () {
                element_settings.url = $(this).attr('href');

                var ajax = new Drupal.ajax("#request-info", $('#request-info')[0], element_settings);

                /*$(this).on('click', function(e) {
                 var requestionSecion = $('section.request-form-section');
                 var url = $(this).attr('href');
                 console.log(url);
                 $.get(url, function(response, status, xhr) {
                 requestionSecion.html(response);
                 var settings = response.settings || Drupal.settings;
                 Drupal.attachBehaviors(requestionSecion, settings);
                 console.log("here");
                 if (status != "error") {
                 var offset = requestionSecion.offset();
                 var scrollTarget = requestionSecion;
                 while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
                 scrollTarget = $(scrollTarget).parent();
                 }
                 // Only scroll upward
                 //if (offset.top - 10 < $(scrollTarget).scrollTop()) {
                 $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
                 }
                 });
                 e.preventDefault();
                 e.stopImmediatePropagation();
                 });*/


                /*Drupal.ajax['request-info'].commands.insert = function (ajax, response, status) {
                 var new_content = $(response.data);
                 $('section.request-form-section').html(new_content);
                 console.log('New content was appended to #my-wrapper');
                 };*/
                /*$(this).click(function(e) {
                 $("#product-additional").append('<h3 class="furn-ucase furn-red">REQUEST QUOTE</h3><div id="request-data"></div>').accordion('destroy');

                 e.preventDefault();
                 });*/

            });

            ProcessTxtToTruncate();

            /*$("article a.promo-link").on("click", function(e) {
             var msg = '<div style="width:240px; height:300px"><p style="font-size:1.3em;line-height:135%" class="furn-ucase furn-red">We move, you save!</p><p>Furnitalia is going through exciting renovations and we are launching MOVING SALE starting <span class="furn-red">January 28, 2014</span>. Now is the time to shop and save!</p> <p style="font-size:1.2em" class="furn-ucase furn-red">SALE 20% - 70% OFF IN STORE ONLY.</p> <p>Click <a href="/moving-sale" style="text-decoration:underline;color:blue;">here</a> for more info</p></div>';
             $.fancybox.open(msg,
             {
             closeBtn:true,
             closeClick:false,
             autoDimensions:false
             }).center();

             e.preventDefault();

             });*/

            InitSrchInputEvents();


            $("a.promo-link").tooltip({
                content: function () {
                    return "Due to custom options available for this product, the price varies upon your preferences. <br/><br/>Please send us request for information or call at: <span style=\"font-weight:bold\">916-332-9000</span><br/>We will happily assist you.";
                },
                open: function (e, ui) {
                    ui.tooltip.animate({top: ui.tooltip.position().top + 10}, "fast");
                    $('body').on('touchend', closeTooltipOnClick);
                }
            }).click(function (e) {
                e.stopPropagation();
                $(this).tooltip({
                    position: {my: "left+15 center", at: "center+20 center"}
                });
                $(this).tooltip("open");
                return false;
            }).focusout(function () {
                $(this).tooltip("close");
            });


            var myElement = document.querySelector("#container-inner header");
            // construct an instance of Headroom, passing the element
            var headroom = new Headroom(myElement);
            // initialise
            headroom.init();

            if ($.fn.mmenu) {
                var menuAPI = $('nav#main-nav').mmenu().data('mmenu');

                $('#hamburger')
                    .on('click',
                        function (e) {
                            e.preventDefault();
                            // console.log('click');
                            if ($html.hasClass('mm-opened')) {
                                menuAPI.close();
                            }
                            else {
                                menuAPI.open();
                            }
                        }
                    );
            }

            // Add Tab effect to content with tabs markup
            if (typeof $.ui !== "undefined" && typeof $.ui.tabs !== "undefined") {
                $( "#tabs" ).tabs();
            }

        }
    };

    function closeTooltipOnClick(e) {
        $('body').off('touchend', closeTooltipOnClick);
        $("a.promo-link").tooltip("close");
    }


    function InitAccordonMenu() {

        if (!jQuery().accordion) {
            return;
        }

        //main navigation menu accordeon
        $("#supplementary").once(function () {

            $(".accordion-inner").accordion({
                icons: false,
                collapsible: true,
                active: false,
                heightStyle: 'content',
                animate: 300
            });

            $("div.accordion-inner", $(this)).each(function (index, val) {
                if ($(this).find('a.active-menu').length) {
                    $(this).accordion("option", "active", 0);
                }
            });

        });

        $("#product-additional").once(function () {
            $(this).accordion({
                icons: false,
                collapsible: true,
                active: 0,
                heightStyle: 'content',
                animate: 300
            });
        });

    }

    function InitItemPageGallery() {

        // BXSlider
        $('.bxslider:not(.slick-initialized)').each(function (i, elem) {
            $(elem).once(function () {
                var bxMultipleSlides = $(this).find('li').length > 1;
                var dots = $(this).attr('data-slick-nodots') === undefined;
                var isFadeSetting = $(this).attr('data-slick-scroll') === undefined;
                var isAdaptiveHeight = $(this).attr('data-slick-adaptive') !== undefined;
                var bxShowArrows = $(this).attr('data-slick-arrows') !== undefined;
                var bxSlidesToShow = $(this).attr('data-slick-slidesToShow') !== undefined ? $(this).attr('data-slick-slidesToShow') : 1;
                var disableAutoPlay = $(this).attr('data-slick-noAutoplay') !== undefined;

                $(this).on('init', function(event, slick, direction) {
                    $('a[data-rel^=lightcase]', slick.parent).lightcase();
                });
                $(this).slick({
                    adaptiveHeight: isAdaptiveHeight,
                    autoplay: bxMultipleSlides && !disableAutoPlay,
                    autoplaySpeed: 4000,
                    arrows: bxShowArrows,
                    dots: bxMultipleSlides && dots,
                    /*cssEase: 'linear',*/
                    fade: isFadeSetting,
                    slidesToShow: bxSlidesToShow || 1,
                    /*slidesPerRow: 3,*/
                    infinite: true/*,
                    centerMode: true,
                    centerPadding: '60px'*/
                });
            });
        });

        $('.slickslide:not(.slick-initialized)').each(function (i, elem) {

            var bxMultipleSlides = $(this).find('li').length > 1;
            if (!bxMultipleSlides) {
                return;
            }
            var dots = $(this).attr('data-slick-nodots') === undefined;
            var bxShowArrows = $(this).attr('data-slick-arrows') !== undefined;
            var bxSlidesToShow = $(this).attr('data-slick-slidesToShow') !== undefined ? $(this).attr('data-slick-slidesToShow') : 1;
            var disableAutoPlay = $(this).attr('data-slick-noAutoplay') !== undefined;

            $(elem).on('init', function (event, slick, direction) {
                $('a[data-rel^=lightcase]', slick.parent).lightcase();
            });

            $(elem).slick({
                dots: dots,
                infinite: true,
                speed: 500,
                fade: false,
                slide: 'li',
                cssEase: 'linear',
                centerMode: true,
                centerPadding: "0px",
                slidesToShow: bxSlidesToShow || 1,
                variableWidth: true,
                autoplay: bxMultipleSlides && !disableAutoPlay,
                autoplaySpeed: 4000,
                responsive: [{
                    breakpoint: 800,
                    settings: {
                        arrows: bxShowArrows,
                        //centerMode: false,
                        //centerPadding: '40px',
                        variableWidth: false,
                        slidesToShow: bxSlidesToShow || 1,
                        dots: dots,
                        centerMode: true,
                        centerPadding: '0px'
                    },
                    breakpoint: 1200,
                    settings: {
                        arrows: bxShowArrows,
                        centerMode: true,
                        //centerMode: false,
                        //centerPadding: '40px',
                        centerPadding: '0px',
                        variableWidth: false,
                        slidesToShow: bxSlidesToShow || 1,
                        dots: dots

                    }
                }],
                customPaging: function (slider, i) {
                    return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
                }
            });
        });

        $('#natuzzi-editions-quick-delivery, #natuzzi-italia-quick-delivery').once(function() {
            var ajaxUrl = $(this).attr('href') + "/ajax";
            $(this).attr("href", ajaxUrl);
            $(this).lightcase({
                showSequenceInfo: false,
                fixedRatio: false,
                showTitle: false,
                swipe:false
            });
        });

    }

    function InitDropDownMenu() {

        if (jQuery().dropkick) {

            $('main select').each(function (index) {
                //var id = $(this).attr('id');
                var sel = $(this);
                $(this).attr('tabindex', index).dropkick({
                    change: function (value, label) {
                        sel.trigger('change');
                    }
                });

            });

            $('article.node select').each(function (index) {
                //var id = $(this).attr('id');
                var sel = $(this);
                $(this).attr('tabindex', index).dropkick({
                    change: function (value, label) {
                        sel.trigger('change');
                    }
                });

            });

        }

    }

    function InitGalleryControls() {
        $("#view-grid").click(function (e) {
            if (galleryView != "grid") {
                console.log("Switched to grid view");
                galleryView = "grid";
                $('body').removeClass("gallery-list").addClass("gallery-grid");
            }
            e.preventDefault();
        });

        $("#view-list").click(function (e) {
            if (galleryView != "list") {
                console.log("Switched to list view");
                galleryView = "list";
                $('body').removeClass("gallery-grid").addClass("gallery-list");
            }
            e.preventDefault();
        });
    }

    function ProcessTxtToTruncate() {
        var maxheight = 218;
        var showText = "Expand";
        var hideText = "Show Less";

        $('.furn-truncate').each(function () {
            var text = $(this);
            maxheight = (!isNaN($(this).data('maxheight')) ? $(this).data('maxheight') : maxheight);

            if (text.height() > maxheight) {
                text.css({'overflow': 'hidden', 'height': maxheight + 'px'});

                var link = $('<a href="#" style="color:#ea2328;border-bottom:1px dashed #981b1e;float:right">' + showText + '</a>');
                var linkDiv = $('<div class="furn-ucase"></div>');
                linkDiv.append(link);
                $(this).after(linkDiv);

                link.click(function (event) {
                    event.preventDefault();
                    if (text.height() > maxheight) {
                        $(this).html(showText);
                        text.css('height', maxheight + 'px');
                    } else {
                        $(this).html(hideText);
                        text.css('height', 'auto');
                    }
                });
            }
        });
    }

    function InitSrchInputEvents() {
        $("#search-form #edit-keys").focus(function () {
            $(this).val("");
        }).blur(function () {
            if ($(this).val().trim() == "") {
                $(this).val("Search");
            }
        });
        $("#search-block-form input.form-text").focus(function () {
            $(this).val("");
        }).blur(function () {
            if ($(this).val().trim() == "") {
                $(this).val("SEARCH");
            }
        });

        $("#webform-client-form-33 input.form-email").focus(function () {
            $(this).val("");
        }).blur(function () {
            if ($(this).val().trim() == "") {
                $(this).val("EMAIL");
            }
        });
    }


    //--------   G O O G L E   A D W O R D S
    function trackAdwordsConversion(lbl) {

        var opt = {
            google_remarketing_only: false,
            google_is_call: true,
            google_conversion_language: 'en'
        }

        if (lbl === 'request_form' || lbl == '') {
            opt['google_conversion_id'] = 1063723018;
            opt['google_conversion_label'] = "4cmqCJuE4VgQisCc-wM";
            opt['google_remarketing_only'] = false;
        }
        var conv_handler = window['google_trackConversion'];
        if (typeof(conv_handler) == 'function') {
            conv_handler(opt);
        }

    }

    //-------------------------------------------------------------------------------------------
    /**
     * Ajax delivery command to switch among tabs by ID.
     */
    if (Drupal.ajax) {
        Drupal.ajax.prototype.commands.furnAjax = function (ajax, response, status) {
            // response.data is a value setted in 'data' key of command on PHP side.

            //don't process response multiple times
            if ($("#request-data").length > 0) {
                return;
            }

            // Get information from the response. If it is not there, default to
            // our presets.
            var wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
            var method = response.method || ajax.method;
            var effect = ajax.getEffect(response);

            // We don't know what response.data contains: it might be a string of text
            // without HTML, so don't rely on jQuery correctly iterpreting
            // $(response.data) as new HTML rather than a CSS selector. Also, if
            // response.data contains top-level text nodes, they get lost with either
            // $(response.data) or $('<div></div>').replaceWith(response.data).
            //var new_content_wrapped = $('<div></div>').html(response.data);
            //var new_content = new_content_wrapped.contents();
            var new_content = $(response.data);

            // For legacy reasons, the effects processing code assumes that new_content
            // consists of a single top-level element. Also, it has not been
            // sufficiently tested whether attachBehaviors() can be successfully called
            // with a context object that includes top-level text nodes. However, to
            // give developers full control of the HTML appearing in the page, and to
            // enable Ajax content to be inserted in places where DIV elements are not
            // allowed (e.g., within TABLE, TR, and SPAN parents), we check if the new
            // content satisfies the requirement of a single top-level element, and
            // only use the container DIV created above when it doesn't. For more
            // information, please see http://drupal.org/node/736066.
            //if (new_content.length != 1 || new_content.get(0).nodeType != 1) {
            //  new_content = new_content_wrapped;
            //}

            // If removing content from the wrapper, detach behaviors first.
            switch (method) {
                case 'html':
                case 'replaceWith':
                case 'replaceAll':
                case 'empty':
                case 'remove':
                    var settings = response.settings || ajax.settings || Drupal.settings;
                    Drupal.detachBehaviors(wrapper, settings);
            }

            // Add the new content to the page.
            wrapper[method](new_content);

            // Immediately hide the new content if we're using any effects.
            if (effect.showEffect != 'show') {
                new_content.hide();
            }

            // Determine which effect to use and what content will receive the
            // effect, then show the new content.
            if ($('.ajax-new-content', new_content).length > 0) {
                $('.ajax-new-content', new_content).hide();
                new_content.show();
                $('.ajax-new-content', new_content)[effect.showEffect](effect.showSpeed);
            }
            else if (effect.showEffect != 'show') {
                new_content[effect.showEffect](effect.showSpeed);
            }

            //$("#product-additional").append('<h3 class="furn-ucase furn-red">REQUEST QUOTE</h3><div id="request-data">' + response.data + '</div>').accordion('destroy').accordion();
            $("#product-additional").accordion('destroy').accordion({
                icons: false,
                collapsible: true,
                heightStyle: 'content',
                animate: 300,
                active: $('h3.furn-ucase', $(this)).length - 1
            });

            // Attach all JavaScript behaviors to the new content, if it was successfully
            // added to the page, this if statement allows #ajax['wrapper'] to be
            // optional.
            if (new_content.parents('html').length > 0) {
                // Apply any settings from the returned JSON if available.
                var settings = response.settings || ajax.settings || Drupal.settings;
                Drupal.attachBehaviors(new_content, settings);
            }

            //scroll to newly inserted content
            var $requestData = $('#request-data');
            var offset = $requestData.offset();
            var scrollTarget = $requestData;
            while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
                scrollTarget = $(scrollTarget).parent();
            }
            $('html,body').animate({scrollTop: (offset.top - 10)}, 500, function () {
                $('#request-data').find('input').eq(0).focus();
            });

        };

        Drupal.ajax.prototype.commands.FocusOnRequestForm = function (ajax, response, status) {
            var $requestData = $('#requestFormSection');
            var offset = $requestData.offset();
            var scrollTarget = $requestData;
            while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
                scrollTarget = $(scrollTarget).parent();
            }
            $('html,body').animate({scrollTop: (offset.top - 10)}, 500, function () {
                $('#request-data').find('input').eq(0).focus();
            });
        };

    }


})(jQuery);


	
