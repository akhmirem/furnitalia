/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

(function($) {

    var EMAIL_REGEX = regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    var fancyGallery = []; // fancybox gallery group

	Drupal.behaviors.furnitalia = {
		attach: function(context, settings) {

            HamburgerMenu();

            $('form.webform-client-form input.form-submit', context).click(function() {
                 var formId = $(this).parents('form').find('input[name=form_id]').attr('value') || 'unknown';
                 //trigger GA tracking event if request went through OK
                 if (typeof gtag !== "undefined") {
                   /*gtag('event', 'Form Submission', {
                     'event_category': formId,
                     'event_label': '',
                     'value': ''
                   });*/
                 }
                 else if (typeof ga !== "undefined") {
                     //ga('send', 'event', 'Form Submission', formId);
                 }
                 if (typeof fbq !== "undefined") {
                     fbq('track', 'Lead');
                 }

                 //record conversion
                 //trackAdwordsConversion('request_form');

             });

            $('#mc-embedded-subscribe-form input.button', context).click(function() {
                 //trigger GA tracking event if request went through OK
                 if (typeof ga !== "undefined") {
                     //ga('send', 'event', 'Form', 'Click Submit', 'Mailchimp');
                     // ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);

                 }
                 if (typeof fbq !== "undefined") {
                     fbq('track', 'Lead');
                 }

                 //record conversion
                 //trackAdwordsConversion('request_form');

             });

            InitNavMenuAnimation();


         /*   $('ul.sf-menu').superfish({
              animation: {height:'show'},	// slide-down effect without fade-in
              delay: 200,			            // 200 ms delay on mouseout
              speed: 'fast',
              speedOut: '100',
              disableHI: true
            });*/

            /*$("#horizontalNav > ul > li > ul").addClass("mega-menu").removeClass("menu sm");
            var $mainMenu = $('#horizontalNav > ul');
            $mainMenu.addClass('sm sm-clean');
            $mainMenu.smartmenus({
              subMenusSubOffsetX: 1,
              subMenusSubOffsetY: 0,
              showDuration: 0,
              showTimeout: 0,
              showFunction: null
            });*/

            $('.bxslider').each(function (i, elem) {
                $(elem).once(function () {
                    var bxMultipleSlides = $(this).find('li').length > 1;
                    var isFadeSetting = $(this).attr('data-slick-scroll') === undefined;
                    var dots = $(this).attr('data-slick-nodots') === undefined;
                    var showArrows = $(this).attr('data-slick-arrows') !== undefined;
                    var isAdaptiveHeight = $(this).attr('data-slick-adaptive') !== undefined;
                    var isLazyLoad = $(this).attr('data-slick-lazy') !== undefined;
                    //console.log("Adaptive:" + isAdaptiveHeight.toString());

                    $(this).slick({
                        adaptiveHeight: isAdaptiveHeight,
                        autoplay: bxMultipleSlides,
                        autoplaySpeed: 4000,
                        arrows: showArrows,
                        dots: bxMultipleSlides && dots,
                        /*cssEase: 'linear',*/
                        fade: isFadeSetting,
                        slidesToShow: 1,
                        infinite: true,
                        lazy: 'progressive',
                        centerMode: true
                    });
                });
            });


            //mark li active if link inside has class 'active'
            $('a.active-menu').closest('li').addClass('active-menu');


            initCustomDropDown();


            // ------------ SET UP REQUEST/CONTACT POPUP AJAX EVENTS
            var element_settings = {};
            element_settings.event = "click";
            element_settings.progress = {type: false};

            $('#contact-us').once(function () {
                element_settings.url = Drupal.settings.basePath + 'contact/ajax/';

                var ajax = new Drupal.ajax("#contact-us", $('#contact-us')[0], element_settings);
            });
            //$('article a.appointment').once(function () {
            //    element_settings.url = Drupal.settings.basePath + 'contact/ajax/';

            //     var ajax = new Drupal.ajax("article a.appointment", $('article a.appointment')[0], element_settings);
            // });
            $('a.contact').once(function () {
                element_settings.url = Drupal.settings.basePath + 'contact/ajax/';

                var ajax = new Drupal.ajax("a.contact", $('a.contact')[0], element_settings);
            });

            $('#request-quote').once(function () {
                element_settings.url = $(this).attr('href');
                var ajax = new Drupal.ajax("#request-quote", $('#request-quote')[0], element_settings);
                $(this).on('click', function request() {
                    //console.log("Request Button clicked");
                    //trigger GA tracking event once request button is clicked
                    //if (typeof ga !== "undefined") {
                    //    ga('send', 'event', 'Form Submission', 'Request Form opened');
                    //}

                    /*$.fancybox.open($('#amoforms_iframe_44610'),
                    {
                        width: 450,
                        height: 350,
                        closeBtn: false,
                        closeClick: false,
                        fitToView: false,
                        autoWidth: false,
                        autoSize: false,
                        autoHeight:false,
                        autoResize: false,
                        afterShow: function(opts) {
                            console.log("on before load");
                            $('#amoforms_iframe_44610').fadeIn(300);
                           // $.fancybox.update();
                        }
                    });*/
                    //return false;
                });
            });

            $('#natuzzi-editions-quick-delivery, #natuzzi-italia-quick-delivery').once(function () {
                var url = $(this).attr('id');
                $(this).on('click', function request(e) {
                    e.preventDefault();
                    $.fancybox.open({
                        'width': 600,
                        'href' : Drupal.settings.basePath + url + '/ajax',
                        'type': 'iframe',
                        'afterShow': function(current, previous) {
                            console.log('Quick Delivery program details have loaded');
                        }

                    });
                });
            });


            $('a.request-info').once(function () {
                element_settings.url = $(this).attr('href');
                var ajax = new Drupal.ajax("a.request-info", $(this)[0], element_settings);
            });

            $('#request-info').once(function () {
                element_settings.url = $(this).attr('href');
                var ajax = new Drupal.ajax("#request-info", $('#request-info')[0], element_settings);
                $(this).on('click', function request() {
                    //console.log("Request Button clicked");
                    //trigger GA tracking event once request button is clicked
                    //if (typeof ga !== "undefined") {
                    //    ga('send', 'event', 'Form Submission', 'Request Form opened');
                    // }

                    /*$.fancybox.open($('#amoforms_iframe_44610'),
                        {
                            width: 450,
                            height: 350,
                            closeBtn: false,
                            closeClick: false,
                            fitToView: false,
                            autoWidth: false,
                            autoSize: false,
                            autoHeight:false,
                            autoResize: false,
                            afterShow: function(opts) {
                                console.log("on before load");
                                $('#amoforms_iframe_44610').fadeIn(300);
                                // $.fancybox.update();
                            }
                        });*/
                    //return false;
                });
            });

            /*$('#coupon').once(function () {
                element_settings.url = Drupal.settings.basePath + 'coupon/ajax/';

                var ajax = new Drupal.ajax("#coupon", $('#coupon')[0], element_settings);
            }); */

            $('#virtGallery').once(function () {
                $(this).on('click', function () {
                    //width="853" height="480"
                    var d3GalleryFrame = '<iframe width="920" height="540" src="https://my.matterport.com/show/?m=4WmF8UxXhx9" frameborder="0" allowfullscreen></iframe>';
                    $.fancybox.open(d3GalleryFrame,
                        {
                            width: 940,
                            height: 560,
                            closeBtn: true,
                            closeClick: false
                        });

                    if (typeof ga !== "undefined") {
                        ga('send', 'event', 'Virtual showroom', 'Open Virtual Showroom');
                    }

                    return false;

                });

            });


            //item description page thumbnail gallery
            // build fancybox group
            $("#pikame").once(function () {
                fancyGallery = [];
                $(this).find("a").each(function (i) {
                    // build fancybox gallery
                    fancyGallery[i] = {"href": this.href, "title": this.title};
                }).end().PikaChoose({
                    autoPlay: false,
                    showCaption: true,
                    carousel: true,
                    carouselVertical: false,
                    // bind fancybox to big images element after pikachoose is built
                    buildFinished: fancy
                }); // PikaChoose
            });

            $("#zoom-in").click(function (e) {
                $("div.pika-stage a").trigger('click');
                return false;
            });

            // ----------- SCROLL TO TOP LINK --------------------
            $("#scroll-top").find("a").click(function () {

                var offset = $("#main-content").offset();
                var scrollTarget = $("#main-content");
                while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent()) {
                    scrollTarget = $(scrollTarget).parent();
                }
                // Only scroll upward
                if (offset.top - 10 < $(scrollTarget).scrollTop()) {
                    $(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
                }

                return false;

            });

            $("#item-video-img").unbind('click').click(function () {
                $.fancybox.open($('div.embedded-video'), {
                    width: 800,
                    height: 600,
                    closeBtn: true,
                    closeClick: false,
                    mouseWheel: true,
                });
            });

            $("#bottomBannerImg").once(function () {
                $(this).click(function () {
                    showPromoModal();
                    return false;
                });
            });

            // Add hover effect to category list thumb images
            $("img.brand-category-img", $("#categoryList")).once(function () {
                $(this).hover(
                    function () {
                        var src = $(this).attr('src');
                        var srcParts = src.split(".");
                        var new_src = srcParts[0] + "_hover." + srcParts[1];
                        $(this).data('origSrc', src);
                        $(this).attr('src', new_src);
                    },
                    function () {
                        var origSrc = $(this).data('origSrc');
                        if (origSrc) {
                            $(this).attr('src', origSrc);
                        }
                    }
                );
            });

            // Hover effect for brands page
            $("ul.brands-list li div.views-field-field-brand-page-image img").once(function () {
                $(this).hover(
                    function () {
                        var origSrc = $(this).attr('src');
                        var newSrc = $(this).attr('data-hover-img');
                        if (newSrc) {
                            $(this).data('origSrc', origSrc);
                            $(this).attr('src', newSrc);
                        }
                    },
                    function () {
                        var origSrc = $(this).data('origSrc');
                        if (origSrc) {
                            $(this).attr('src', origSrc);
                        }
                    }
                );
            });

            if (typeof $.ui !== "undefined" && typeof $.ui.tooltip !== "undefined") {
                $("a.promo-link").tooltip({
                    content: function () {
                        return "Due to custom options available for this product, the price varies upon your preferences. <br/><br/>Please contact us using the form below or call us at: <span style=\"font-weight:bold\">916-332-9000 </span><br/>We will happily assist you.";
                    }
                }).click(function (e) {
                    e.stopPropagation();
                    /*$(this).tooltip({
                     position: { my: "left+15 center", at: "center+20 center" }
                     });
                     $(this).tooltip("open");*/
                    $("#request-quote").trigger('click');
                    return false;
                });
            }

            // Add Tab effect to content with tabs markup
            if (typeof $.ui !== "undefined" && typeof $.ui.tabs !== "undefined") {
                $( "#tabs" ).tabs();
            }

            /*$("article a.promo-link").on("click", function(e) {
             var msg = '<div style="width:400px; height:300px"><p style="font-size:1.3em;line-height:135%" class="furn-ucase furn-red">We move, you save!</p><p>Furnitalia is going through exciting renovations and we are launching MOVING SALE starting <span class="furn-red">January 23, 2014</span>. Now is the time to shop and save!</p> <p style="font-size:1.2em" class="furn-ucase furn-red">SALE 20% - 70% OFF IN STORE ONLY.</p> <p>Click <a href="/moving-sale" style="text-decoration:underline;color:blue;">here</a> for more info</p></div>'

             $.fancybox.open(msg,
             {
             closeBtn:true,
             closeClick:false,
             autoDimensions:false,
             width:300,
             height:200
             });

             e.stopPropagation();
             e.preventDefault();

             });*/

            $("form.webform-client-form input.email").focus(function () {
                if (!EMAIL_REGEX.test($(this).val().trim())) {
                    $(this).val("");
                }
            }).blur(function () {
                if ($(this).val().trim() === "") {
                    $(this).val("Email Address");
                }
            });


            if (typeof $.fancybox !== "undefined") {
                $(".fancybox").fancybox({
                    // Options will go here
                });
            }
        }
	};

	var fancy = function (self) {
	    // bind click event to big image
	    self.anchor.on("click", function(e){
			// find index of corresponding thumbnail
			var pikaindex = $("#pikame").find("li.active").index();
	        // open fancybox gallery starting from corresponding index
	        $.fancybox(fancyGallery,{
                // fancybox options
                "cyclic": true, // optional for fancybox v1.3.4 ONLY, use "loop" for v2.x
                "index": pikaindex // start with the corresponding thumb index
            });
	        return false; // prevent default and stop propagation
	     }); // on click

	     /*if ($('#pikame li').length > 4) {
		     $("#pikame").wrap($('<div id="pikawrapper"></div>'));
		     $("#pikawrapper").jScrollPane({
			     verticalDragMinHeight: 25,
				 verticalDragMaxHeight: 25,
				 horizontalDragMinWidth: 25,
				 horizontalDragMaxWidth: 25
		     });
	     }*/
	 };


	function InitNavMenuAnimation() {

		 if (!jQuery().accordion) {
		 	return;
		 }

	 	//main navigation menu accordeon
		$(".accordion").once(function() {

			$(".accordion-inner").accordion({
				icons:false,
				collapsible: true,
				active: false,
				heightStyle:'content',
				animate:300
			});

			$("div.ui-accordion-content div.item-list div.accordion-inner").each(function(index, val) {
				if ($(this).find('a.active-menu').length) {
					$(this).accordion("option", "active", 0);
				}
			});

		});

	}

	function InitFeaturedSlideShow(container, callback) {

	    if (container === "") {
	      container = '#category-image-pane';
	    }

		//Featured Slide Show images list
		//var imgPathPrefix = Drupal.settings.basePath + "sites/default/files/promo/fall2014/";
		var imgPathPrefix = Drupal.settings.basePath + "sites/default/files/promo/year_end14/";
		var link = Drupal.settings.basePath + "brand/natuzzi-editions?category=1&sort_by=title&items_per_page=All";
		//var couponLink = Drupal.settings.basePath + "coupon";
		var featuredImgs = [
			{'image':imgPathPrefix + "725x530-front-promo-showroom.jpg", link:link},
			{'image':imgPathPrefix + "725x530-july4-sale.jpg", link:link}
			//{'image': Drupal.settings.basePath + "sites/default/files/promo/coupons/10percent_coupon.jpg", link:couponLink}
			//{'image':imgPathPrefix + "500x350_dining.jpg", link:link}
		];

		//!Promo
		$(container).html('').PikaChoose({
			transition:[5],
			showCaption:false,
			showTooltips:false,
			data:featuredImgs,
			autoPlay:true,
      		carousel:false,
			speed:3000,
			buildFinished: function() {

        		callback();
				/*$('#category-image-pane div.pika-stage').click(function(){
					openPromoImg();
					return false;
				});*/
			}
		});
	}

	function openPromoImg() {
		var promoImgPath = Drupal.settings.basePath + "sites/default/files/promo/moving-sale/";
		var img = new Image();

		$.fancybox.showLoading();

		// wrap our new image in jQuery, then:
		$(img)
			// once the image has loaded, execute this code
			.load(function () {

				$.fancybox.hideLoading();
				$.fancybox.open('<img src="'+ promoImgPath +'" alt="Holiday sale"/>',
				{
					closeBtn:true,
					closeClick:false,
					mouseWheel:true,
					openEffect	: 'none',
					closeEffect	: 'none'
				});


			})
			// *finally*, set the src attribute of the new image to our image
			.attr('src', promoImgPath);
	}

	function showPromoModal() {
		var promoImgPath = Drupal.settings.basePath + "sites/default/files/promo/moving-sale/";

		//$.fancybox.showLoading();

	    var promoContent = $('<div id="promoModal>promo</div>');

	    /*$.fancybox.open(

	    {
	    	'href' : Drupal.settings.basePath + 'promo/ajax/',
	    	'type': 'ajax',
	        'afterShow': function(current, previous) {
	        	var slider = $('.bxslider').bxSlider({
				  mode: 'fade',
				  auto:true,
				  autoStart:true,
				  pause: 2000,
				  speed: 500,
				  controls: false,
				  infiniteLoop: true,
				  slideWidth:725
				});
	        }

	    });*/

	    InitFeaturedSlideShow("#promoModal", function() {
	      $.fancybox.update();
	    });
	    //$.fancybox.hideLoading();

	}

    function HamburgerMenu() {
        // Change menu style to hamburger menu
        var $body = $('body');
        if ($body.hasClass('hasHamburger')) {
            $body.once(function () {
                var $left = $("#left-section")
                $left.css({
                    "z-index": "1000",
                    "position": "absolute",
                    "top": "125px",
                    "left": "17px",
                    "display": "none",
                    "background": "white",
                    "padding": "20px 0 20px 20px",
                    "border": "2px solid lightgray"
                });

                var $cross = $(".cross");
                var $hamburger = $(".hamburger");
                $cross.hide();
                $("#content").addClass("full-width");
                $hamburger.click(function () {
                    $left.slideToggle("fast", function () {
                        $hamburger.hide();
                        $cross.show();
                    });
                });

                $cross.click(function () {
                    $left.slideToggle("fast", function () {
                        $cross.hide();
                        $hamburger.show();
                    });
                });
            });
        }
    }

    function initCustomDropDown() {
        if (jQuery().dropkick) {

            var applyDropKick = function(index) {
                var sel = $(this);
                $(this).attr('tabindex', index).dropkick({
                    change: function (value, label) {
                        sel.trigger('change');
                    }
                });
            };
            $('select:not(.no-dropkick)', '#content').each(
                applyDropKick
            );

            $('article.node select:not(.no-dropkick)').each(
                applyDropKick
            );

        }
    }

})(jQuery);

