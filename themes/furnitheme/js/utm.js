(function ($) {

    window.addEventListener("load", function(){

        if (typeof  window.UtmForm !== "undefined" && typeof window.UtmForm.utmCookie !== "undefined") {

                var initialReferrer = "Referrer=" + window.UtmForm.utmCookie.initialReferrer();
                var initialLandingPage = "Landing_page=" + window.UtmForm.utmCookie.initialLandingPageUrl();
                var lastReferrer = "Last_Referrer=" + window.UtmForm.utmCookie.lastReferrer();
                var visits = "Visits=" + window.UtmForm.utmCookie.visits();
                var utmSource = "utm_source=" + window.UtmForm.utmCookie.readCookie("utm_source");
                var utmMedium = "utm_medium=" + window.UtmForm.utmCookie.readCookie("utm_medium");
                var utmCampaign = "utm_campaign=" + window.UtmForm.utmCookie.readCookie("utm_campaign");

                var leadInfo = [initialReferrer, initialLandingPage, lastReferrer, visits, utmSource, utmMedium, utmCampaign];

                var leadInfoStr = "";
                for (prop in leadInfo) {
                    leadInfoStr += leadInfo[prop] + "|";
                }
                // console.log(leadInfoStr);

                $('form.webform-client-form').once(function(){
                    var utmField = $(this).find('input[name*=utm][type=hidden]');
                    // console.log(utmField);
                    if (utmField.length) {
                        utmField.val(leadInfoStr);
                    }
                });

            } else {
                console.log("UtmForm is undefined...");
            }
    });


})(jQuery);