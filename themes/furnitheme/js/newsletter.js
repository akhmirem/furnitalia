(function ($) {

    /**
     * Add an extra function to the Drupal ajax object
     * which allows us to trigger an ajax response without
     * an element that triggers it.
     */
    Drupal.ajax.prototype.processWebformCallback = function () {
        var ajax = this;

        // Do not perform another ajax command if one is already in progress.
        if (ajax.ajaxing) {
            return false;
        }

        try {
            $.ajax(ajax.options);
        }
        catch (err) {
            console.log('An error occurred while attempting to process ' + ajax.options.url);
            return false;
        }

        return false;
    };

    /**
     * Define a custom ajax action not associated with an element.
     */
    // var custom_settings = {};
    // custom_settings.url = '/webform/ajax/33';
    // custom_settings.event = 'onload';
    // custom_settings.keypress = false;
    // custom_settings.prevent = false;
    // Drupal.ajax['custom_ajax_action'] = new Drupal.ajax(null, $(document.body), custom_settings);

    $(document).ready(function(){
        var nid, matches = document.body.className.match(/(^|\s)page-node-(\d+)(\s|$)/);
        if (matches) {
            nid = matches[2];
            console.log('node id =' + nid);

            var custom_settings = {};
            custom_settings.url = '/request-form/' + nid + '/ajax';
            custom_settings.event = 'onload';
            custom_settings.keypress = false;
            custom_settings.prevent = false;
            custom_settings.progress = {
                type: 'throbber',
                message: ''
            };
            Drupal.ajax['request_form'] = new Drupal.ajax(null, $(document.body), custom_settings);
            Drupal.ajax['request_form'].processWebformCallback();

        }
    });

    //
    //
    // $(document).ready(function () {
    //     Drupal.ajax['request_form'].customResponse();
    // });

})(jQuery);