<?php

const NATUZZI_EDITIONS_URLS = array("natuzzi-editions", "natuzzi-editions/*", "natuzzi-editions/*/*", "taxonomy/term/22");

/**
 * Implementation of hook_js_alter
 */
function furnitheme_js_alter(&$javascript)
{

  if (isset($javascript['misc/jquery.form.js'])) {
    $jquery_path = drupal_get_path('theme', 'furnitheme') . '/js/jquery.form.min.js';

    //We duplicate the important information from the Drupal one
    $javascript[$jquery_path] = $javascript['misc/jquery.form.js'];
    //..and we update the information that we care about
    $javascript[$jquery_path]['version'] = '3.35';
    $javascript[$jquery_path]['data'] = $jquery_path;

    unset($javascript['misc/jquery.form.js']);

  }

  if ((arg(0) == 'promo' && arg(1) == 'natuzzi-harmony-friday-sale')) {

    $js_to_delete = array(
      'sites/all/themes/furnitheme/js/jquery.pikachoose.min.js',
      'sites/all/themes/furnitheme/js/jquery.jcarousel.min.js',
      'sites/all/themes/furnitheme/lib/bxslider/jquery.bxslider.min.js',
      'sites/all/themes/furnitheme/lib/slick/slick.min.js',
      'sites/all/themes/furnitheme/js/jquery.dropkick-1.0.0.js',
      'misc/textarea.js',
      'sites/all/modules/contrib/jquery_update/replace/misc/jquery.form.min.js',
      'sites/all/themes/furnitheme/js/popupAjaxCallback.js',
      '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
      'sites/all/themes/furnitheme/lib/fancybox/jquery.fancybox.pack.js',
      'sites/all/themes/furnitheme/lib/fancybox/jquery.mousewheel-3.0.6.pack.js',
    );
    foreach ($js_to_delete as $js) {
      unset($javascript[$js]);
    }
  }
}

/**
 * Implementation of hook_css_alter
 */
function furnitheme_css_alter(&$css)
{
  if ((arg(0) == 'promo' && arg(1) == 'natuzzi-harmony-friday-sale')) {

    $css_to_delete = array(
      'sites/all/modules/contrib/mollom/mollom.css ',
      'sites/all/modules/contrib/ubercart/uc_order/uc_order.css ',
      'sites/all/modules/contrib/ubercart/uc_product/uc_product.css ',
      'sites/all/modules/contrib/ubercart/uc_store/uc_store.css ',
      'sites/all/modules/contrib/views/css/views.css ',
      'sites/all/themes/furnitheme/css/ui/base/jquery.ui.all.css ',
      'sites/all/themes/furnitheme/css/pikachoose.css ',
      'sites/all/themes/furnitheme/lib/bxslider/jquery.bxslider.css ',
      'sites/all/themes/furnitheme/lib/slick/slick.css ',
      'sites/all/themes/furnitheme/lib/slick/slick-theme.css ',
      'sites/all/themes/furnitheme/css/dropkick.css ',
    );
    foreach ($css_to_delete as $del) {
      unset($css[$del]);
    }
  }
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
//!PreprocessPage
function furnitheme_preprocess_page(&$vars)
{
  global $theme_path;
  //    $files_dir = base_path() . variable_get('file_public_path', conf_path() . '/files');

  drupal_add_css($theme_path . '/css/ui/base/jquery.ui.all.css', array('group' => -100, 'every_page' => TRUE));

  drupal_add_js('misc/jquery.form.js');
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_library('system', 'ui.accordion');
  drupal_add_library('system', 'ui.tooltip');
  //drupal_add_library('system', 'jquery.bbq'); //for processing url in javacsript

  //jquery ui
  drupal_add_js(drupal_get_path('module', 'webform_ajax') . '/js/webform_ajax.js', 'file');

  // set up specific template based on page
  $vip_nid = variable_get('furn_preferred_customer_nid', 1237);
  if (isset($vars['node']) && $vars['node']->nid == $vip_nid || arg(0) == 'vip') {

    drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.tools.min.js");
    drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.watermark.min.js");
    drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.maskedinput.min.js");
    drupal_add_js($theme_path . "/templates/vip_program/media/js/lpg/_init.js");
    drupal_add_js($theme_path . "/templates/vip_program/media/js/lpg/extensions/array.js");
    drupal_add_js($theme_path . "/templates/vip_program/media/js/lpg/utils/elements.js");
    drupal_add_js($theme_path . "/templates/vip_program/media/js/jquery.tools.min.js");


    drupal_add_css("//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css", 'external');
    drupal_add_css($theme_path . "/templates/vip_program/media/css/global/screen.css", array(
      //'media' => 'screen,projection',
      'group' => 100,
      'weight' => 8,
      'preprocess' => FALSE,
    ));
    drupal_add_css($theme_path . "/templates/vip_program/media/css/print.css", array(
      'media' => 'print',
      'weight' => 9,
      'preprocess' => FALSE,
    ));
    drupal_add_css($theme_path . "/templates/vip_program/media/css/public.css", array(
      //'media' => 'screen,projection',
      'group' => 100,
      'weight' => 10,
      'preprocess' => FALSE,
    ));
    drupal_add_css("https://fonts.googleapis.com/css?family=Neucha&subset=latin", array(
      'type' => 'external',
      //'media' => 'screen,projection',
    ));
    drupal_add_css("https://fonts.googleapis.com/css?family=Montserrat:400,700", array("type" => "external"));

    $vars['theme_hook_suggestions'][] = "page__vip";
    return;
  }

  //item description page thumbnail gallery
  drupal_add_js($theme_path . '/js/jquery.pikachoose.min.js');
  drupal_add_js($theme_path . '/js/jquery.jcarousel.min.js');
  drupal_add_css($theme_path . '/css/pikachoose.css');

  //Slick
  drupal_add_js($theme_path . '/lib/slick/slick.min.js');
  drupal_add_css($theme_path . '/lib/slick/slick.css');
  drupal_add_css($theme_path . '/lib/slick/slick-theme.css');

  //dropdown custom controls
  drupal_add_js($theme_path . '/js/jquery.dropkick-1.0.0.js');
  drupal_add_css($theme_path . '/css/dropkick.css');

  // gallery page styles
  drupal_add_css($theme_path . '/css/product-gallery.css');
  drupal_add_css($theme_path . '/fonts/natuzzi-landing-fonts.css');

  $italia_editions_gallery_paths = array("natuzzi-italia", "natuzzi-italia/*", "natuzzi-italia/*/*", "natuzzi-editions", "natuzzi-editions/*", "natuzzi-editions/*/*");
  $paths_no_title = array_merge(array("<front>", "home", "node/*", "sale", "collections", "taxonomy/term/*", "moving-sale",
    "bauformat-kitchen-and-bath", "modern-european-kitchen-cabinets", "modern-european-bathroom-cabinets",
    "miele-appliances", "ekornes-stressless-recliners-sofas",
    "stressless-ekornes-promotion", "bdi-furniture-media-event", 'promo/*'), $italia_editions_gallery_paths);
  $paths_no_tabs = array_merge(array("taxonomy/term/*", "natuzzi-italia", "natuzzi-editions"), $italia_editions_gallery_paths);

  //determine whether to show/hide titles and promo area depending on path
  $paths_no_promo = array("stressless-ekornes-promotion",
    "bauformat-kitchen-and-bath",
    "modern-european-kitchen-cabinets",
    "modern-european-bathroom-cabinets",
    "coupon",
    "ekornes-stressless-recliners-sofas",
    "collections/rugs-and-furnishings-by-kalora",
    'promo',
    '<front>',
    'home',
    'promo/*',
    'virtual-showroom',
  );
  $vars['show_promo'] = !drupal_match_path(current_path(), implode("\n", $paths_no_promo));
  $vars['show_title'] = !drupal_match_path(current_path(), implode("\n", $paths_no_title));

  if (drupal_match_path(current_path(), "node/*")) {
    drupal_add_js($theme_path . '/js/newsletter.js');
  }

  if (arg(0) === 'miele-appliances') {
    drupal_add_library('system', 'ui.tabs');
  }

  // disable taxonomy tabs
  if (drupal_match_path(current_path(), implode("\n", $paths_no_tabs))) {
    unset($vars['tabs']);
  }

  //set footer menu links
  furnitheme_set_up_footer_menu($vars);

  //set up top menu
  furnitheme_set_up_top_menu($vars);

  if (isset($vars['node']) && arg(2) != 'edit') {
    // node page
    $node = $vars['node'];
    if ($node->type == 'item') {
      $content = $vars['page']['content']['system_main']['nodes'][$node->nid];

      furn_global_preprocess_node_common_fields($content, 'page', $node);

      $vars['page']['content']['system_main']['nodes'][$node->nid] = $content;

    } else if ($node->type == 'blog') {
      $vars['show_title'] = TRUE;
    }
  }

  // Show Hamburger Menu
  $vars['hamburger_menu'] = FALSE;
  //$vars['hamburger_menu'] = is_hamburger_menu_displayed() || $node_fullpage;

  // get header promo text
  $header_promo = furn_global_get_header_promo();
  if (is_array($header_promo) && count($header_promo) > 0) {
    $vars['header_promo'] = $header_promo;
  }

  // Top banner
  if ($vars['show_promo']) {
    process_promo_block($vars);
  }

  setup_session_variables();
}

function process_promo_block(&$vars) {
  // get promo banners
  $promos = furn_global_get_promos();
  $vars['promos'] = $promos;

  $generic_banner = TRUE;

  $item_brand = 0;
  $brand_italia = 21;
  $brand_editions = 22;
  $brand_calligaris = 31;
  if (isset($vars['node']) && $vars['node']->type == 'item') {
    $item_brand = field_get_items('node', $vars['node'], 'field_brand');
    if ($item_brand) {
      $item_brand = $item_brand['0']['tid'];
    }
  }
  if ($item_brand == $brand_editions || drupal_match_path(current_path(), implode("\n", NATUZZI_EDITIONS_URLS))) {


    $vars['page']['description_section'] = 'Natuzzi Editions is a modern/contemporary furniture line, designed in 
      Italy. All modern collections are upholstered with genuine Italian leather and fabric. Natuzzi Editions sofas, 
      sectionals, and armchairs are well-known for their complete comfort, style, and versatile configurations &mdash; a 
      perfect fit for American lifestyles. <br/> Furnitalia is the largest Natuzzi retailer in N California. We offer 
      large selection of Natuzzi furniture in our 35,000 sq ft showroom. Exceptional service and best prices are 
      guaranteed.';
    $vars['show_title'] = TRUE;

    if (isset($promos['natuzzi-editions'])) {
      $vars['page']['content_top'] = theme("natuzzi-editions-promo-block");
      $generic_banner = FALSE;
    }

  } elseif ($item_brand == $brand_italia || drupal_match_path(current_path(), implode("\n", array("natuzzi-italia", "natuzzi-italia/*", "natuzzi-italia/*/*", "taxonomy/term/21")))) {
    if (isset($promos['natuzzi-italia'])) {
      $vars['page']['content_top'] = theme("natuzzi-italia-promo-block");
      $generic_banner = FALSE;
    }
  } elseif ($item_brand == $brand_calligaris || drupal_match_path(current_path(), implode("\n", array("taxonomy/term/$brand_calligaris")))) {
    if (isset($promos['calligaris'])) {
      $vars['page']['content_top'] = theme("calligaris-promo-block");
      $generic_banner = FALSE;
    }
  }
  if ($generic_banner) {

    $top_banner = '';
    $i = 0;
    foreach ($promos as $name => $promo) {
      foreach ($promo['banners'] as $key => $banner) {
        $top_banner .= '<li >';
        $img_style = '';
        if ($i > 0) {
          $img_style = '';
        }
        /*$banner_img = theme("image", array(
                "path" => $img['image'],
                "title" => $img['alt'],
                "attributes" => array("ref" => "", "")
            )
        );*/
        $banner_img = '<img data-lazy="' . $banner['image'] . '" alt="' . $banner['alt'] . '" style="' . $img_style . '" >';
        if (isset($banner['link'])) {
          $top_banner .= '<a href="' . $banner['link'] . '" title="' . $banner['alt'] . '">' . $banner_img . '</a>';
        } else {
          $top_banner .= $banner_img;
        }
        $top_banner .= '</li>';

      }
      $i++;
    }
    $top_banner = '<ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none" data-slick-lazy="true">' . $top_banner . '</ul>';
    $vars['page']['content_top'] = array(
      '#markup' => $top_banner,
    );
  }
}

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @internal param $hook The name of the template being rendered ("html" in this case.)*   The name of the template being rendered ("html" in this case.)
 */
function furnitheme_preprocess_html(&$variables) {
  $node_fullpage = FALSE;
  try {
    //$node = $variables['page']['content']['system_main']['nodes'][1890]['#node'];
    if (isset($variables['page']['content']) &&
        isset($variables['page']['content']['system_main']) &&
        isset($variables['page']['content']['system_main']['nodes'])) {
      foreach ($variables['page']['content']['system_main']['nodes'] as $nid => $node_data) {
        $node = $node_data['#node'];
        if ($node && $node->type == 'landing_page') {
          if ($node->field_fullscreen_flag['und'][0]['value'] == '1') {
            $node_fullpage = TRUE;
          }
        }
      }
    }
  } catch (Exception $e) {}

  if (is_page_fullwidth() || $node_fullpage) {
    $variables['classes_array'][] = 'wide';
  }

  if (is_hamburger_menu_displayed() || $node_fullpage) {
    $variables['classes_array'][] = 'hasHamburger';
  }

  if (arg(0) == 'promo' || $node_fullpage) {
    $variables['classes_array'][] = 'landingpage';
  }

}

/**
 * Implements hook_html_head_later(...)
 */
function furnitheme_html_head_alter(&$head_elements)
{
  if (variable_get('site_frontpage') == current_path()) {
    $alternate_url = 'https://m.furnitalia.com';
  } else {
    $alternate_url = 'https://m.furnitalia.com' . url(current_path());
  }
  $metatag_alternate = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#theme' => 'metatag_link_rel',
    '#attributes' => array(
      'name' => 'alternate',
      'href' => $alternate_url,
      'media' => 'only screen and (max-width: 640px)',
    ),
  );
  $head_elements['metatag_alternate'] = $metatag_alternate;

  $metatag_description = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'description',
      'content' => "",
    ),
  );

  // custom description headers
  if (arg(0) == 'miele-appliances') {

    $metatag_description['#attributes']['content'] =
      "Furnitalia is an authorized direct reseller of Miele appliances in California. 
                Miele - the world's most inspiring kitchen, at an inspiring price. 
                Built to last. Engineered to amaze.";
    $head_elements['metatag_description'] = $metatag_description;

  } else if (arg(0) == 'modern-european-kitchen-cabinets') {

    $metatag_description['#attributes']['content'] =
      "Bauformat offers a large selection of modern kitchen cabinets, made in Germany for 
                every budget and any room size. An individual solution from factory direct.";
    $head_elements['metatag_description'] = $metatag_description;

  } else if (arg(0) == 'modern-european-bathroom-cabinets') {

    $metatag_description['#attributes']['content'] =
      "Badea by Bauformat is a collection of top-quality modern bathroom cabinets, designed 
                in Germany for every budget and any room size. An individual solution from factory direct.";
    $head_elements['metatag_description'] = $metatag_description;
  } else if (arg(0) == 're-vive') {

    $metatag_description['#attributes']['content'] =
      "Re-Vive by Natuzzi is an iconic responsive recliner that adapts to any seating position. Re-Vive is comfortable, infinitely customizable, with premium leather or fabric selection.";
    $head_elements['metatag_description'] = $metatag_description;
  }

  // set up canonical URL for Natuzzi Editions pages
  /*if (drupal_match_path(current_path(), implode("\n", NATUZZI_EDITIONS_URLS))) {
    $metatag_canonical = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#theme' => 'metatag_link_rel',
      '#attributes' => array(
        'name' => 'canonical',
        'href' => 'https://www.furnitalia.com/',
      ),
    );
    $head_elements['metatag_canonical'] = $metatag_canonical;
  }*/

  // set up canonical URL for Natuzzi Italia pages

}

/**
 * Returns HTML for a select form element.
 *
 * It is possible to group options together; to do this, change the format of
 * $options to an associative array in which the keys are group labels, and the
 * values are associative arrays in the normal $options format.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #options, #description, #extra,
 *     #multiple, #required, #name, #attributes, #size.
 *
 * @ingroup themeable
 */
function furnitheme_select($variables)
{
  $element = $variables['element'];

  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select'));

  return '<select' . drupal_attributes($element['#attributes']) . '>' . furnitheme_form_select_options($element) . '</select>';
}

/**
 * Converts a select form element's options array into HTML.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 * @param $choices
 *   Mixed: Either an associative array of items to list as choices, or an
 *   object with an 'option' member that is an associative array. This
 *   parameter is only used internally and should not be passed.
 *
 * @return
 *   An HTML string of options for the select form element.
 */
function furnitheme_form_select_options($element, $choices = NULL)
{
  if (!isset($choices)) {
    $choices = $element['#options'];
  }
  // array_key_exists() accommodates the rare event where $element['#value'] is NULL.
  // isset() fails in this situation.
  $value_valid = isset($element['#value']) || array_key_exists('#value', $element);
  $value_is_array = $value_valid && is_array($element['#value']);
  $options = '';
  $i = 0;
  foreach ($choices as $key => $choice) {
    if (is_array($choice)) {
      $options .= '<optgroup label="' . $key . '">';
      $options .= form_select_options($element, $choice);
      $options .= '</optgroup>';
    } elseif (is_object($choice)) {
      $options .= form_select_options($element, $choice->option);
    } else {
      $key = (string)$key;
      if ($value_valid && (!$value_is_array && (string)$element['#value'] === $key || ($value_is_array && in_array($key, $element['#value'])))) {
        $selected = ' selected="selected"';
      } else {
        $selected = '';
      }
      $extra_attrs = isset($element['#extra_option_attributes']) && isset($element['#extra_option_attributes'][$i]) ? $element['#extra_option_attributes'][$i] : '';
      $options .= '<option value="' . check_plain($key) . '" ' . $selected . ' ' . $extra_attrs . '>' . check_plain($choice) . '</option>';
    }

    ++$i;
  }
  return $options;
}

/**
 * Implements hook_form_alter().
 *
 * Alter search form
 */
function furnitheme_form_search_form_alter(&$form, &$form_state, $form_id)
{
  unset($form['advanced']);
}

/**
 * Implements hook_preprocess_region().
 *
 * Alter search form
 */
function furnitheme_preprocess_region(&$variables, $hook)
{
  if ($variables['region'] == "content_top") {
    $variables['classes_array'][] = 'clearfix';
  }
}


/**
 * Formats a product's length, width, and height.
 *
 * @param $variables
 *   An associative array containing:
 *   - length: A numerical length value.
 *   - width: A numerical width value.
 *   - height: A numerical height value.
 *   - units: String abbreviation representing the units of measure.
 *   - attributes: (optional) Array of attributes to apply to enclosing DIV.
 *
 * @see uc_length_format()
 * @ingroup themeable
 */
function furnitheme_uc_product_dimensions($variables)
{
  $length = $variables['length'];
  $width = $variables['width'];
  $height = $variables['height'];
  $units = $variables['units'];
  $attributes = $variables['attributes'];
  $attributes['class'][] = "product-info";
  $attributes['class'][] = "dimensions";
  $attributes['class'][] = "field";

  $output = '';
  if ($length || $width || $height) {
    $output = '<div ' . drupal_attributes($attributes) . '>';
    $output .= '<div class="field-label product-info-label">' . t('Dimensions') . ':</div> ';
    $output .= '<span class="product-info-value">';
    $output .= uc_length_format($length, $units) . ' × ';
    $output .= uc_length_format($width, $units) . ' × ';
    $output .= uc_length_format($height, $units) . '</span>';
    $output .= '</div>';
  }

  return $output;
}

/**
 * Formats a product's model number.
 *
 * @param $variables
 *   An associative array containing:
 *   - model: Product model number, also known as SKU.
 *   - attributes: (optional) Array of attributes to apply to enclosing DIV.
 *
 * @ingroup themeable
 */
function furnitheme_uc_product_model($variables)
{
  $model = $variables['model'];
  $attributes = $variables['attributes'];
  $attributes['class'][] = "product-info";
  $attributes['class'][] = "model";

  $output = '<div ' . drupal_attributes($attributes) . '>';
  $output .= '<span class="product-info-label">' . t('SKU') . ':</span> ';
  $output .= '<span class="product-info-value" property="schema:model">' . check_plain($model) . '</span>';
  $output .= '</div>';

  return $output;
}

function furnitheme_views_mini_pager($vars)
{
  global $pager_page_array, $pager_total;

  $element = $vars['element'];
  $parameters = $vars['parameters'];

  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  if ($pager_total[$element] > 1) {

    $li_previous = theme('pager_previous',
      array(
        'text' => 'previous', //(isset($tags[1]) ? $tags[1] : t('‹‹')),
        'element' => $element,
        'interval' => 1,
        'parameters' => $parameters,
      )
    );
    if (empty($li_previous)) {
      $li_previous = "&nbsp;";
    }

    $li_next = theme('pager_next',
      array(
        'text' => 'next', //(isset($tags[3]) ? $tags[3] : t('››')),
        'element' => $element,
        'interval' => 1,
        'parameters' => $parameters,
      )
    );

    if (empty($li_next)) {
      $li_next = "&nbsp;";
    }

    $items[] = array(
      'class' => array('pager-previous'),
      'data' => $li_previous,
    );

    $items[] = array(
      'class' => array('pager-current'),
      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
    );


    // Set up link to view all results in one page - taken from theme_pager_link
    $query = array();
    if (count($parameters)) {
      $query = drupal_get_query_parameters($parameters, array());
    }
    if ($query_pager = pager_get_query_parameters()) {
      $query = array_merge($query, $query_pager);
    }
    $query['items_per_page'] = 'All';
    $attributes['href'] = url($_GET['q'], array('query' => $query));
    $li_view_all = '<a' . drupal_attributes($attributes) . '>' . 'view all' . '</a>';

    $items[] = $li_view_all;

    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => array('pager')),
      )
    );
  } else if ($pager_max > 0) {

    // Set up link to view results by pages
    $query = array();
    if (count($parameters)) {
      $query = drupal_get_query_parameters($parameters, array());
    }
    if ($query_pager = pager_get_query_parameters()) {
      $query = array_merge($query, $query_pager);
    }
    $query['items_per_page'] = '9';
    $attributes['href'] = url($_GET['q'], array('query' => $query));
    $li_view_paged = '<a' . drupal_attributes($attributes) . '>' . 'View by pages' . '</a>';

    $items[] = $li_view_paged;

    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => array('pager')),
      )
    );
  }
  return '';
}

/**
 * Custom theme for pager links
 */
function furnitheme_pager($variables)
{

  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }

  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $first_item_text = '1';
  $first_item_class = "";
  if ($i < 2) {
    $first_item_class = "with-separator";  //show border separator if current page is 1
  }

  $li_first = theme('pager_first', array('text' => $first_item_text, 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next ›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => $pager_max, 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {

    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first', $first_item_class),
        'data' => $li_first,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {

        $cur_text = $i;
        $sep_class = 'with-separator';
        if ($i == $pager_last && $pager_last + 1 != $pager_max || $i == $pager_max) {
          //unset separator if ... follows current
          $cur_text = $i;
          $sep_class = '';
        }

        //if last item in the range, unset the explicit 'last' pager
        if ($i == $pager_max) {
          $li_last = "";
        }

        if ($i < $pager_current && $i > 1) {
          $items[] = array(
            'class' => array('pager-item', $sep_class),
            'data' => theme('pager_previous', array('text' => $cur_text, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current', $sep_class),
            'data' => $cur_text,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item', $sep_class),
            'data' => theme('pager_next', array('text' => $cur_text, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }

      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.

    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }

    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
        'items' => $items,
        'attributes' => array('class' => array('pager')),
      ));
  }
  return '';
}

function furnitheme_webform_mail_headers($vars)
{

  /*
  For MIME-attachment emails use these headers:
  ---------------------------------
  global $drupal_hash_salt;

  if (513 == $vars['node']->nid) {
      $hash = md5($drupal_hash_salt);
      $mime_boundary = "==Multipart_Boundary_x{$hash}x";
      $headers = array(
        "Content-Type" => "multipart/mixed; boundary=\"".$mime_boundary."\"",
        "MIME-Version"  => "1.0",
        "X-Mailer" => 'Drupal Webform (PHP/'.phpversion().')',
      );
      return $headers;
  }*/

  $headers = array(
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'X-Mailer' => 'Drupal Webform (PHP/' . phpversion() . ')',
  );
  return $headers;
}

function furnitheme_set_up_footer_menu(&$vars)
{
  $info_menu = array(
    l("About", 'about'),
    l("FAQ", 'faq'),
    l("Shipping and Delivery", "shipping-deliveries"),
    l("Catalogs", "catalogs"),
    l("Terms of Service", "service-terms"),
    l("Privacy Policy", "privacy-policy"),
    l("Login", 'user/login'),
  );
  $vars['footer_menu'] = array(
    '#theme' => 'item_list',
    '#items' => array_values($info_menu),
    '#type' => 'ul',
    '#attributes' => array('class' => 'links'),
  );

}

function furnitheme_set_up_top_menu(&$vars)
{
  $top_menu = array();
  $top_menu [] = l("Store Locations", 'store-locations', array('attributes' => array('style' => 'color:black !important; font-weight:bold !important')));
  $top_menu [] = l("Virtual Showroom", 'virtual-showroom');
  $top_menu [] = l("Contact Us", 'contact-us');
  $vars['page']['header']['top_menu'] = array(
    '#theme' => 'item_list',
    '#items' => array_values($top_menu),
    '#type' => 'ul',
    '#attributes' => array('class' => 'menu'),
  );
}

/**
 * Implements hook_preprocess_node
 * @param $vars
 */
function furnitheme_preprocess_node(&$vars, $hook)
{
  //if ($vars['page'] == TRUE && $vars['nid'] == 1237) {
  //$vars['theme_hook_suggestions'] = array('node__vip');
  //}
}

function furnitheme_form_webform_client_form_alter(&$form, &$form_state, $form_id)
{
  // see if webform_client_form_ is in the form_id
  $vip_nid = variable_get('furn_preferred_customer_nid', 1237);
  if (strstr($form_id, 'webform_client_form_' . $vip_nid)) {

    $form['#attributes']['class'][] = 'our_form';
    $form['submitted']['name']['#prefix'] = '<div class="cont" data-name="submitted[name]" data-first_name="1" data-ordering="0" data-required="1" data-label="First name" data-type="input">';
    $form['submitted']['name']['#title_display'] = 'invisible';
    $form['submitted']['name']['#attributes'] = array(
      'placeholder' => 'Name*',
      'class' => array('input'),
    );
    $form['submitted']['name']['#suffix'] = '</div>';

    $form['submitted']['phone']['#prefix'] = '<div class="cont " data-name="submitted[phone]" data-ordering="1" data-pattern="\(\d\d\d\) \d\d\d-\d\d\d\d" data-required="1" data-mask="(999) 999-9999" data-phone="1" data-label="Phone number" data-type="tel">';
//        <input type="tel" pattern="\d \(\d\d\d\) \d\d\d-\d\d-\d\d" data-mask="9 (999) 999-99-99" class="input" name="widget_1" value="" required placeholder="Phone*" />
//        $form['submitted']['phone']['#type'] = 'tel';
    $form['submitted']['phone']['#title_display'] = 'invisible';
    $form['submitted']['phone']['#attributes'] = array(
      'placeholder' => 'Phone*',
      'class' => array('input'),
      'type' => 'tel',
      'pattern' => "\(\d\d\d\) \d\d\d-\d\d\d\d",
      'data-mask' => "(999) 999-9999",
    );
    $form['submitted']['phone']['#suffix'] = '</div>';

    $form['submitted']['email']['#prefix'] = '<div class="cont " data-name="submitted[email]" data-ordering="2" data-required="1" data-label="Your e-mail" data-type="email" data-email="1">';
//            <input type="email" class="input" name="widget_2" value="" required placeholder="E-mail*" />
    $form['submitted']['email']['#title_display'] = 'invisible';
    $form['submitted']['email']['#attributes'] = array(
      'placeholder' => 'E-mail*',
      'class' => array('input'),
    );
    $form['submitted']['email']['#suffix'] = '</div>';

    $form['submitted']['birth_date']['#prefix'] = '<div class="cont " data-name="submitted[birth_date]" data-ordering="4" data-label="Birth Date" data-type="bday">';
//            <input type="text" class="input" name="widget_4" value="" required placeholder="Birth date" pattern="\d\d-\d\d-\d\d\d\d" data-mask="99-99-9999"/>
    $form['submitted']['birth_date']['#title_display'] = 'invisible';
    $form['submitted']['birth_date']['#attributes'] = array(
      'placeholder' => 'Birth date',
      'class' => array('input'),
      'pattern' => "\d\d-\d\d-\d\d\d\d",
      'data-mask' => "99-99-9999",
    );
    $form['submitted']['birth_date']['#suffix'] = '</div>';

    $form['submitted']['note']['#prefix'] = '<div class="cont ">';
//            <input type="text" class="input" name="widget_5" value="" required placeholder="Note"   />
    $form['submitted']['note']['#title_display'] = 'invisible';
    $form['submitted']['note']['#attributes'] = array(
      'placeholder' => 'Note',
      'class' => array('input'),
    );
    $form['submitted']['note']['#suffix'] = '</div>';

    $form['actions']['submit']['#prefix'] = '<div class="block block-button is-submit" id="block-new26" >';
    $form['actions']['submit']['#suffix'] = '</div>';

    $form['actions']['submit']['#ajax'] = array(
      'callback' => 'furn_global_webform_js_submit',
      'wrapper' => $form['#id'],
      'method' => 'replace',
      'effect' => 'fade',
      'event' => 'click',
    );

  }
}

function furnitheme_button($variables)
{

  $override = FALSE;
  if (FALSE && isset($variables['element']['#ajax']) && strstr($variables['element']['#ajax']['wrapper'], 'webform-client-form-32')) {
    $override = TRUE;
  }

  $element = $variables['element'];
  if (!$override) {
    $element['#attributes']['type'] = 'submit';
  }
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  if ($override) {
    $val = $element['#attributes']['value'];
    $element['#attributes']['value'] = 'submit';

    return '<button ' . drupal_attributes($element['#attributes']) . ' >' . $val . '</button>';

  } else {
    return '<input' . drupal_attributes($element['#attributes']) . ' />';
  }
}


/**
 * Implements template_preprocess_THEME_HOOK().
 * taken from webform.module
 */
function furnitheme_preprocess_webform_element(&$variables)
{
  $element = &$variables['element'];


  $override = FALSE;
  if (isset($variables['element']['#webform_component']) && $variables['element']['#webform_component']['nid'] === 32) {
    $is_natuzzi_promo_page = $variables['element']['#title_display'] === 'invisible'; //stristr(current_path(), 'natuzzi-summer-sale');
    if ($is_natuzzi_promo_page) {
      $override = TRUE;
    }
  }

  if (stristr(current_path(), 'renovation-private-sale')) {
    if (isset($variables['element']['#webform_component']) && $variables['element']['#webform_component']['nid'] === 513) {
      $override = true;
    }
  }

  // Ensure defaults.
  $element += array(
    '#title_display' => 'before',
    '#wrapper_attributes' => array(),
  );
  $element['#wrapper_attributes'] += array(
    'class' => array(),
  );

  // All elements using this for display only are given the "display" type.
  if (isset($element['#format']) && $element['#format'] == 'html') {
    $type = 'display';
  } else {
    $type = ($element['#webform_component']['type'] == 'select' && isset($element['#type'])) ? $element['#type'] : $element['#webform_component']['type'];
  }

  // Convert the parents array into a string, excluding the "submitted" wrapper.
  $nested_level = $element['#parents'][0] == 'submitted' ? 1 : 0;
  $parents = str_replace('_', '-', implode('--', array_slice($element['#parents'], $nested_level)));

  // Build up a list of classes to apply on the element wrapper.

  if ($override) {
    $wrapper_classes = array(
      'form-group',
      'form-component-' . $parents,
    );
  } else {
    $wrapper_classes = array(
      'form-item',
      'webform-component',
      'webform-component-' . str_replace('_', '-', $type),
      'webform-component--' . $parents,
    );
  }


  if (isset($element['#title_display']) && strcmp($element['#title_display'], 'inline') === 0) {
    $wrapper_classes[] = 'webform-container-inline';
  }
  $element['#wrapper_attributes']['class'] = array_merge($element['#wrapper_attributes']['class'], $wrapper_classes);

  // If #title_display is none, set it to invisible instead - none only used if
  // we have no title at all to use.
  if ($element['#title_display'] == 'none') {
    $element['#title_display'] = 'invisible';
    if (empty($element['#attributes']['title']) && !empty($element['#title'])) {
      $element['#attributes']['title'] = $element['#title'];
    }
  }

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }

  // If an internal title is being used, generate no external title.
  if ($element['#title_display'] == 'internal') {
    $element['#title_display'] = 'none';
  }
}

function is_hamburger_menu_displayed()
{
  return arg(0) == 'bauformat-kitchen-and-bath' ||
    arg(0) == 'virtual-showroom' ||
    arg(0) == 'modern-european-bathroom-cabinets' ||
    arg(0) == 'modern-european-kitchen-cabinets' ||
    arg(0) == 'ekornes-stressless-recliners-sofas' ||
    arg(0) == 'bdi-furniture-media-event' ||
    (arg(0) == 'promo' && arg(1) == 'natuzzi-harmony-friday-sale') ||
    (arg(0) == 'promo' && arg(1) == 'black-friday-sale') ||
    (arg(0) == 'promo' && arg(1) == 'december-clearance') ||
    (arg(0) == 'promo' && strstr(arg(1), 'mid-winter-mega-sale') !== FALSE) ||
    (arg(0) == 'promo') ||
    (arg(0) == 'collections' && arg(1) == 'rugs-and-furnishings-by-kalora') ||
    arg(0) == 'stressless-ekornes-promotion';

}

function is_page_fullwidth()
{
  $vip_nid = variable_get('furn_preferred_customer_nid', 1237);
  if (arg(0) == 'node' && arg(1) == $vip_nid ||
    arg(0) == 'vip' ||
    arg(0) == 'virtual-showroom' ||
    (arg(0) == 'collections' && arg(1) == 'rugs-and-furnishings-by-kalora') ||
    (arg(0) == 'promo' && arg(1) == 'natuzzi-harmony-friday-sale') ||
    (arg(0) == 'promo' && arg(1) == 'black-friday-sale') ||
    (arg(0) == 'promo' && strstr(arg(1), 'mid-winter-mega-sale') !== FALSE) ||
    (arg(0) == 'promo' && arg(1) == 'december-clearance') ||
    (arg(0) == 'promo') ||
    (arg(0) == 'home')
  ) {
    return true;
  }
  return false;
}

function furnitheme_preprocess_webform_form(&$vars)
{
//  if (in_array($vars['nid'], array(1891))) {
//    $vars['theme_hook_suggestions'][] = 'webform_form_1890';
//    unset($vars['theme_hook_original']);
//  }
}

function furnitheme_preprocess_webform_mail_message(&$vars)
{
  if (in_array($vars['node']->nid, array(1891, 1875))) {
    $vars['theme_hook_suggestions'][] = 'webform-mail-1891';
    unset($vars['theme_hook_original']);
  }
}

/**
 * Process variables for paragraphs-items.tpl.php
 */
function furnitheme_preprocess_paragraphs_items(&$variables, $hook)
{
//  if ($variables['element']['#bundle'] == 'content_strap') {
//  }
  $variables['theme_hook_suggestions'][] = 'paragraphs_items__' . $variables['element']['#field_name'] . '__' . $variables['element']['#bundle'];
  $variables['theme_hook_suggestions'][] = 'paragraphs_items__' . $variables['element']['#field_name'] . '__' . $variables['element']['#bundle'] . '__' . $variables['view_mode'];
}

/**
 * Process variables for paragraphs-items.tpl.php
 */
function furnitheme_preprocess_entity(&$variables, $hook)
{
  if ($variables['entity_type'] == 'paragraphs_item') {
    if (in_array($variables['elements']['#bundle'], ['image_content', 'webform_content', 'content_strap'])) {
//      dpm($variables);
      $variables['paragraph'] = $variables['elements']['#entity'];
    }

    if ($variables['elements']['#bundle'] == 'content_strap') {
//      $variables['test'] = field_view_field('paragraphs_item', $variables['paragraphs_item'], 'field_paragraphs_reference');
      $strap_column_fields = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_paragraphs_reference');
      $strap_columns = [];
      foreach ($strap_column_fields as $col) {
        $strap_columns [] = field_view_value('paragraphs_item', $variables['paragraphs_item'], 'field_paragraphs_reference', $col);
      }
      $variables['columns'] = $strap_columns;
//      dpm($variables);
    }
  }
}

function furnitheme_preprocess_field(&$variables)
{
  if ($variables['element']['#field_name'] == 'field_paragraphs_reference') {
    if ($variables['element']['#bundle'] == 'content_strap') {
//      dpm("content strap field preprocess");
//      dpm($variables);
//      krumo($variables);
    }
  }

  // add responsive image class to image strap
  if ($variables['element']['#field_name'] == 'field_image') {
    if ($variables['element']['#bundle'] == 'image_strap') {
      foreach ($variables['items'] as $key => $item) {
        $variables['items'][$key]['#item']['attributes']['class'][] = 'img-responsive';
      }
    }
  }
}

/**
 * Returns HTML for a list or nested list of items.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An array of items to be displayed in the list. If an item is a
 *     string, then it is used as is. If an item is an array, then the "data"
 *     element of the array is used as the contents of the list item. If an item
 *     is an array with a "children" element, those children are displayed in a
 *     nested list. All other elements are treated as attributes of the list
 *     item element.
 *   - title: The title of the list.
 *   - type: The type of list to return (e.g. "ul", "ol").
 *   - attributes: The attributes applied to the list element.
 */
function furnitheme_item_list_no_container($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $no_container = FALSE;
  if (isset($variables['attributes']['no_container']) && $variables['attributes']['no_container']) {
    $no_container = TRUE;
  }
  $attributes = $variables['attributes'];
//  dpm($variables);

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
//  $output = '<div class="item-list">';

    $output = '';

//  if (isset($title) && $title !== '') {
//    $output .= '<h3>' . $title . '</h3>';
//  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= furnitheme_item_list_no_container(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }

  return $output;
}

function furnitheme_menu_tree__main_menu($variables) {
  return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

function setup_session_variables()
{
  if (isset($_GET["utm_source"])) {
    $_SESSION["utm_source"] = $_GET["utm_source"];
  }
  if (isset($_GET["utm_medium"])) {
    $_SESSION["utm_medium"] = $_GET["utm_medium"];
  }
  if (isset($_GET["utm_campaign"])) {
    $_SESSION["utm_campaign"] = $_GET["utm_campaign"];
  }
}
