<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$theme = drupal_get_path('theme', 'furnitheme');

?>

<?php if ($is_desktop): ?>

    <style>
        #toggle_fullscreen {
            font-size: 25px;
            cursor: pointer;
            margin: 10px auto;
            padding: 10px;
            width: 7em;
            text-transform: lowercase;
            color: #fff;
            background-color: #cd9712;
            border: 2px solid #b68610;
            text-align: center;
        }
        #toggle_fullscreen:hover {
            background-color: #9e740e;
            border-color: #7d5c0b;
        }
        /* Responsive css */
        @media screen and (min-width: 1400px) {
            #main {
                width: 70%;
            }
        }
        /* \Responsive css */
    </style>

    <div id="virt-store-container" style="position: relative; padding-bottom: 56.25%;height: 0; overflow: hidden;">
        <iframe id="virtStoreIframe" width="853" height="480"
                src="https://my.matterport.com/show/?m=cTBtiLzsgfA&brand=0&help=1&play=0&wh=0&title=1"
                frameborder="0" allowfullscreen
                style="position: absolute; top: 0; left: 0;width: 100%; height: 100%;">
        </iframe>
    </div>
    <div id="toggle_fullscreen">open fullscreen</div>

    <div style="margin: 2em 5em;
    font-size: 2em;
    line-height: 130%;
    font-family: ProximaNovaThRegular,sans-serif;
    letter-spacing: 1px;">
        3D virtual showroom is a new way to experience our store. Browse and immerse into our large collection of
        contemporary Italian furniture: Natuzzi Italia, Natuzzi Editions, Catellan Italia, Alf Italia, Bontempi, ...
    </div>

    <script type="text/javascript">
        /* Load Fonts CSS File */
        jQuery(document).ready(function() {
            var link_elem = document.createElement('link');
            link_elem.rel = 'stylesheet';
            link_elem.href = '<?php print base_path() . $theme; ?>/css/ntz_fonts.css';
            link_elem.type = 'text/css';
            var godefer = document.getElementsByTagName('link')[0];
            godefer.parentNode.insertBefore(link_elem, godefer);

            jQuery('#toggle_fullscreen').on('click', function () {
                var elem = document.getElementById("virtStoreIframe");
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.msRequestFullscreen) {
                    elem.msRequestFullscreen();
                } else if (elem.mozRequestFullScreen) {
                    elem.mozRequestFullScreen();
                } else if (elem.webkitRequestFullscreen) {
                    elem.webkitRequestFullscreen();
                }
            });
        });

        /*$(document).on('mozfullscreenchange webkitfullscreenchange fullscreenchange',function(){
            var fullScreenMode = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
            if (!fullScreenMode) {
                console.log("exit fullscreen");
            } else {
                console.log("enter fullscreen");
            }
        });*/

    </script>


<?php else: //mobile ?>

    <div id="virt-store-container" style="position: relative; padding-bottom: 56.25%;height: 0; overflow: hidden;">
        <iframe id="virtStoreIframe" src="https://my.matterport.com/show/?m=cTBtiLzsgfA&brand=0&help=1&play=0&wh=0&title=1"
            frameborder="0"
            allowfullscreen
            style="position: absolute; top: 0; left: 0;width: 100%; height: 100%;"
        ></iframe>
    </div>

    <div style="    margin: 1em auto;
    font-size: 2em;
    line-height: 130%;
    letter-spacing: 1px;">
        3D virtual showroom is a new way to experience our store. Tap on "Play" button to immerse into our large collection
        of contemporary Italian furniture.
    </div>
<?php endif; ?>



