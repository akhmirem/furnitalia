<?php
/**
 * @file views-view-grid.tpl.php
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php $kalora_shown = FALSE; ?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<ul class="brands-list <?php print $class; ?>"<?php print $attributes; ?>>
    <?php foreach ($rows as $row_number => $columns): ?>
      <span class="row <?php print $row_classes[$row_number]; ?> clearfix">
        <?php foreach ($columns as $column_number => $item): ?>
          <li class="gallery-item <?php print $column_classes[$row_number][$column_number]; ?>">
            <?php print $item; ?>

            <?php //This is a temporary placeholder for Kalora Collection. It now leads to landing page with IFrame ?>
            <?php //======================================================================================== ?>
            <?php if (empty($item)): ?>
              <?php $kalora_shown = TRUE; ?>
              <div class="field-content brand-name Kalora">
                <a href="/collections/rugs-and-furnishings-by-kalora" title="Rugs and Furnishings Collection by Kalora">Kalora</a>
              </div>
              <div class="views-field views-field-field-brand-page-image">
                <a href="/collections/rugs-and-furnishings-by-kalora">
                  <img data-hover-img="/sites/default/files/promo/kalora/Kalora-logo-brands-page-color.jpg" typeof="foaf:Image" src="/sites/default/files/promo/kalora/Kalora-logo-brands-page-grey.jpg" width="239" height="329" alt="Kalora Rugs and Furnishings Collection" title="Kalora Rugs and Furnishings Collection" class="">
                </a>
              </div>
            <?php endif; ?>
            <?php //======================================================================================== ?>

          </li>
        <?php endforeach; ?>
      </span>
    <?php endforeach; ?>

  <?php if (!$kalora_shown): ?>
    <?php //This is a temporary placeholder for Kalora Collection. It now leads to landing page with IFrame ?>
    <?php //======================================================================================== ?>
    <span class="row clearfix">
      <li class="gallery-item">
      <div class="field-content brand-name Kalora">
        <a href="/collections/rugs-and-furnishings-by-kalora"
           title="Rugs and Furnishings Collection by Kalora">Kalora</a>
      </div>
      <div class="views-field views-field-field-brand-page-image">
        <a href="/collections/rugs-and-furnishings-by-kalora">
          <img data-hover-img="/sites/default/files/promo/kalora/Kalora-logo-brands-page-color.jpg" typeof="foaf:Image"
               src="/sites/default/files/promo/kalora/Kalora-logo-brands-page-grey.jpg" width="239" height="329"
               alt="Kalora Rugs and Furnishings Collection" title="Kalora Rugs and Furnishings Collection" class="">
        </a>
      </div>
      </li>
      </span>
  <?php endif; ?>
  <?php //======================================================================================== ?>

</ul>
