<?php
	$theme_path = base_path() . drupal_get_path('theme', 'furnitheme');
?>
<article class="about-us">
<img src="<?php print $theme_path; ?>/images/new_about_us_top.jpg"/>
<p>Furnitalia came about from one couple’s deep appreciation of Italian contemporary furniture. Dmitriy Aks, an engineer, admired the beauty, craftsmanship and precision with which Italians build their furniture, especially leather furniture. Ella Aks, an interior designer, delighted in the artistry of the lines and materials.</p>

<p>Moving to California, they discovered Italian contemporary furniture was not available
in the Sacramento area. Only after a great amount of time and effort spent researching were they able to import their own furnishings. Visitors loved their vision and desired it for their own homes. As Dmitriy and Ella realized there was a real and growing desire for quality Italian contemporary furniture in their community, Furnitalia was born.</p>

<p>Today, Furnitalia has grown into a respected name in high-end Italian contemporary furniture. The elegant showroom is a world of beautiful design, carrying the finest names in Italian furniture, including Natuzzi Italia, Natuzzi Editions, BDI, Bontempi, Rossetto, Elite Modern, and DreamWeavers. In fact, Furnitalia is the largest Natuzzi gallery on the West Coast, with more than 60 Natuzzi living room groups on display.</p>

<p>We invite you to enjoy the Furnitalia experience. As you stroll around the floor, examine the exquisite workmanship – seams, joints, decorative details. Imagine the sheer pleasure and pride you will feel, living with the most beautifully crafted furniture in the world. </p>

</article>

<div class="content">
  <h3 class="cta"><a href="/brands" style="margin-left:2em">Check out our brands!</a></h3>
  <div class="brands-slideshow">
    <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
        data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
    >
      <li>
        <a href="/brand/natuzzi-italia"><img src="/sites/default/files/brands/brand-slider-logos/Natuzzi-Logo.png"
                                             alt="Natuzzi Italia" style=""/></a>
      </li>
      <li>
        <a href="/brand/alf"><img src="/sites/default/files/brands/brand-slider-logos/Alf-Italia-Logo.png"
                                  alt="Alf Italia" style=""/></a>
      </li>
      <li>
        <a href="/modern-european-kitchen-cabinets"><img src="/sites/default/files/brands/brand-slider-logos/Bauformat-German-Kitchen-Logo.png"
                                                         alt="Bauformat German Kitchen Cabinets" style=""/></a>
      </li>
      <li>
        <a href="/brand/bontempi"><img src="/sites/default/files/brands/brand-slider-logos/Bontempi-Logo.png"
                                       alt="Bontempi" style=""/></a>
      </li>
      <li>
        <a href="/brand/calligaris"><img src="/sites/default/files/brands/brand-slider-logos/Calligaris-Logo.png"
                                         alt="Calligaris" style=""/></a>
      </li>
      <li>
        <a href="/brand/incanto"><img src="/sites/default/files/brands/brand-slider-logos/Incanto-Logo.png"
                                      alt="Incanto" style=""/></a>
      </li>
      <li>
        <a href="/miele-appliances"><img src="/sites/default/files/brands/brand-slider-logos//Miele-Logo.png"
                                         alt="Miele" style=""/></a>
      </li>
      <li>
        <a href="/ekornes-stressless-recliners-sofas"><img src="/sites/default/files/brands/brand-slider-logos/Stressless-Logo.png"
                                                           alt="Stressless" style=""/></a>
      </li>
    </ul>
  </div>
</div>
<br/>

<div class="row">
  <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
    <?php include_once DRUPAL_ROOT . "/static/map.html";?>
  </div>
</div>
<br/>
<hr class="gradient" style="width:100%"/>
<br/>
<section id="expertise" style="clear:both; text-align:center">

    <table style="width: 200px;
    display: inline;
    margin: 0px 10px;
    position: relative;
    top: 5px;" cellpadding="0" cellspacing="0"><tr><td><a href="https://www.houzz.com/pro/furnitalia/furnitalia"><img src="https://st.hzcdn.com/static/badge_32_9@2x.png" alt="furnitalia in Sacramento, CA on Houzz" width="96" height="96" border="0" style="width: auto;height: 160px;"/></a></td></tr></table>

</section>

<script>
  (function ($) {
    $(".slider").slick({
      dots: false,
      infinite: true,
      speed: 2000,
      fade: false,
      slide: 'li',
      cssEase: 'linear',
      centerMode: false,
      centerPadding: "0px",
      slidesToShow: 7,
      variableWidth: true,
      autoplay: true,
      autoplaySpeed: 5,
      responsive: [{
        breakpoint: 1200,
        settings: {
          arrows: true,
          centerMode: false,
          //centerMode: false,
          //centerPadding: '40px',
          centerPadding: '0px',
          variableWidth: true,
          slidesToShow: 7,
          dots: false,
          // autoplaySpeed:300,
          // speed:300

        }
      }, {
        breakpoint: 800,
        settings: {
          arrows: true,
          //centerMode: false,
          //centerPadding: '40px',
          variableWidth: true,
          slidesToShow: 3,
          dots: false,
          centerMode: false,
          centerPadding: '0px'
        }
      }],
      customPaging: function (slider, i) {
        return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
      }
    });

  })(jQuery);
</script>
