<?php
/**
 * @file
 * Zen theme's implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
?>

<?php

global $base_path, $base_url, $theme_path;

$promo_dir = base_path() . "sites/default/files/promo";

$brand_title = $content['field_brand'][0]['#title'];

// setup images
$image = $node->field_image['und'][0];
$style = "gallery_thumb";
$derivative_uri = image_style_path($style, $image['uri']);
if (!file_exists($derivative_uri)) {
  $display_style = image_style_load($style);
  image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
}
$img_placeholder = base_path() . 'sites/default/files/lazyLoadImage.gif';
$img_url = file_create_url($derivative_uri);

// setup price
$formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
$sell_price = $content['sell_price_value'] ? $formatter->formatCurrency($content['sell_price_value'], 'USD') : '';
$sale_price = $content['sale_price_value'] ?  $formatter->formatCurrency($content['sale_price_value'], 'USD') : '';
$sell_price_label = '';
$discount = '';
unset($content['sell_price']['#title']);

if ($teaser) : // GALLERY PAGE ?>

  <div class="cardContainer col-lg-4 col-md-4 col-sm-6 col-sx-12">
    <div class="slide-card" id="slide-card-<?php print $node->nid; ?>">
      <div class="inside">
        <a class="fancybox" href="<?php print $node_url; ?>"
           data-rel="lightcase:mycollection:slideshow<?php print $node->nid; ?>"
           property="schema:url" >

          <img data-src-2="<?php print $img_placeholder; ?>"
               src="<?php print $img_url; ?>"
               data-src="<?php print $img_url; ?>"
               alt="<?php print $image['alt']; ?>"
               class="card-img lazyy"
               property="schema:image"
          />
        </a>
        <div class="card-info">
          <div class="slide-title">
            <div class="item-brand"><?php print $brand_title; ?></div>
            <span property="schema:name"><?php print $title; ?></span>
          </div>
          <div class="slide-pricing">
            <div class="row">
              <?php if ($sell_price): ?>
                <div class="msrp col-lg-5 col-xs-4">
                  <?php print $sell_price_label; ?> <span class="price"><?php print $sell_price; ?></span>
                </div>
              <?php endif; ?>
              <?php if ($sale_price): ?>
                <div class="col-lg-6 col-xs-8">
                  <div class="promo-price">
                    SALE: <span class="price price-promo"><?php print $sale_price; ?></span>
                  </div>
                </div>
              <?php endif; ?>
              <?php if (isset($discount) && $discount) : ?>
                <div class="discount col-lg-3 col-xs-3">
                  <div class="discount-label red"> <span>-<?php print $discount; ?></span></div>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <?php if ('asd' === 'Sold Out'): ?>
            <button disabled>Sold Out!</button>
          <?php endif; ?>
          <div class="desc">
            <?php //print $item['markup']; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php else: // FULL PAGE ?>

    <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?> >

        <?php if ($unpublished): ?>
            <header>
                <?php if ($unpublished): ?>
                    <p class="unpublished"><?php print t('Unpublished'); ?></p>
                <?php endif; ?>
            </header>
        <?php endif; ?>

        <!-- Product images (slideshow) -->
        <div id="item-images">

            <ul id="pikame">
                <?php //dsm($content['field_image']['#items']); ?>

                <?php foreach ($content['field_image']['#items'] as $i => $image) : ?>

                    <?php
                    /*$style = "large";
                    $derivative_uri = image_style_path($style, $image['uri']);
                    if (!file_exists($derivative_uri)) {
                        $display_style = image_style_load($style);
                        image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
                    }
                    $img_url = file_create_url($derivative_uri);*/
                    $img_url = file_create_url($image['uri']);

                    $style = "thumbnail";
                    $derivative_uri = image_style_path($style, $image['uri']);
                    if (!file_exists($derivative_uri)) {
                        $display_style = image_style_load($style);
                        image_style_create_derivative($display_style, $image['uri'], $derivative_uri);
                    }
                    $thumb_url = file_create_url($derivative_uri);

                    $full_img_url = file_create_url($image['uri']);

                    ?>

                    <li>
                        <a href="<?php print $full_img_url; ?>">
                            <?php print theme("image", array(
                                    "path" => $thumb_url,
                                    "attributes" => array(
                                        "ref" => $img_url,
                                        "property" => 'schema:image',
                                    )
                            )); ?>
                            <?php if (isset($image['alt'])): ?>
                                <span><?php print $image['alt']; ?></span>
                            <?php endif; ?>
                        </a>
                    </li>

                <?php endforeach; ?>

                <?php if ($content['has_video']): ?>
                    <li><img src="<?php print base_path() . path_to_theme(); ?>/images/play-button.gif"
                             id="item-video-img"/></li>
                <?php endif; ?>

            </ul>

        </div>
        <!-- \End slideshow -->

        <!-- Product main info -->
        <div id="item-info">

            <h1 class="title" id="page-title" property="schema:name"><?php print $brand_title; ?> <?php print $node->title; ?></h1>

            <?php $content['field_alu']['#title'] = 'Model'; ?>
            <?php print render($content['field_alu']); ?>
            <?php print render($content['model']); ?>

            <?php if (is_array($content['field_brand']['#object']->field_brand['und'][0]['taxonomy_term']->field_brand_image)) {

                    $brand_image = $content['field_brand']['#object']->field_brand['und'][0]['taxonomy_term']->field_brand_image['und'][0];
                    $brand_display = theme("image",
                        array(
                            "path" => $brand_image['uri'],
                            'attributes' => array(
                                "alt" => $brand_title,
                                "title" => $brand_title,
                                "class" => array("brand-img"),
                                "property" => "schema:logo"
                            )
                        )
                    );
                    $brand_display .= "<div class='brand-title'>(<span property='schema:name'>". $content['field_brand'][0]['#title'] ."</span>)</div>";

                    print "<div typeof='schema:Brand' property='schema:brand' class='product-brand'><div style='text-transform:uppercase;font-size:1em;color: #767272;text-align: center;letter-spacing: 3px;margin-bottom: 1em'>Official Dealer<hr class='gradient article-sep'></div>" .
                                l($brand_display,
                                    $content['field_brand'][0]['#href'],
                                    array(
                                        "html" => TRUE,
                                        'attributes' => array(
                                            "property" => "schema:url",
                                        ),
                                    )
                                ) .
                        "</div>";
            } ?>

            <?php print render($content['body']); ?>

            <?php if (isset($content['field_details']) && is_array($content['field_details'])): ?>
                <p class="item-info-p">
                    <?php $content['field_details'][0]['#markup'] = nl2br($content['field_details']['#items'][0]['value']); ?>
                    <?php print render($content['field_details']); ?>
                </p>
            <?php endif; ?>

            <?php print render($content['dimensions']); ?>
            <?php if (isset($content['field_product_pdf']) && count($content['field_product_pdf']['#items']) > 0) : ?>
                &mdash;<span class="schematics"><a
                            href="<?php print file_create_url($content['field_product_pdf']['#items'][0]['uri']); ?>"
                            class="furn-red">Download Schematics</a> (PDF)</span>
            <?php endif; ?>

            <?php if (isset($content['field_availability'])) : ?>
                <?php print render($content['field_availability']); ?>
            <?php endif; ?>

            <?php if (false && $content['bdi_media_furniture']): ?>
              <div class="price-explanation furn-red" style="position: relative;top: 1.5em;">
                *Save additional 15% OFF, available through Feb. 5, 2020
                <img src="<?php print $promo_dir; ?>/bdi/entertainment-sale/BDi-entertainment-sale-banner-2020.jpg"
                     alt="BDI Entertainment Furniture Sale!">
              </div>
            <?php endif; ?>

          <div id="pricing" rel="schema:offers">
            <span typeof="schema:Offer">
              <?php
              print render($content['sell_price']);
              print render($content['sale_price']);
              ?>
            </span>
          </div>


        </div>
        <!-- \End product info -->


        <?php if (isset($display_collection_items) && $display_collection_items) : ?>
            <hr class="gradient title-sep"/>
            <h3 class="title">More from <span style="font-style: italic;"><?php print $collection_title; ?></span>
                collection</h3>
            <div id="collection-items-gallery">

                <?php
                $display_id = 'block_collection_items';
                $view_name = 'taxonomy_term';
                $args = array($collection_nid);

                //print views_embed_view($view_name, $display_id, $args);
                $view = views_get_view($view_name);
                $view->set_arguments($args);
                $view->set_display($display_id);
                $view->is_cacheable = FALSE;

                //exclude current zitem from the list of related items
                $view->add_item($view->current_display, 'filter', 'node', 'nid', array('operator' => '!=', 'value' => array('value' => $node->nid), 'group' => 1));

                $view->pre_execute();
                $view->execute();
                $view->post_execute();
                print $view->preview($display_id);
                $view->destroy();

                ?>
            </div>
        <?php endif; ?>

        <?php if ($content['has_video']): ?>
            <div id="hidden-video">
                <!-- This is a hidden container for item video, its contents will appear in popup box -->
                <?php print render($content['field_video']); ?>
            </div>
        <?php endif; ?>

        <div id="requestFormContainer">
        </div>

        <br/>

    </article><!-- /.node -->

<?php endif; ?>
