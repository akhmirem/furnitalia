<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$stressless_promo_root_dir = base_path() . "sites/default/files/promo/stressless";
$promo_dir = base_path() . "sites/default/files/promo/stressless/charity/2020";

$img_path = 'Stressless-P3-charity-promo-landing.png';
$alt = "Save more than ever before on Stressless®";

?>


<style>

  article {
    color: #444;
    padding-left: 4em;
  }

  .sale-caption-section {
    color: #6f2e3e;
  }

  .sale-caption-section h2 {
    line-height: 1;
    float: left;
  }

  .sale-caption-section h2 .sale-title {
    font-size: 1.25em;
    color: #000;
  }

  .sale-caption-section img {
    margin-left: 50px;
    float: left;
    margin-top: 1em;
  }

  .sale-caption-section .subcaption {
    clear: both;
    margin-bottom: 2em;
    font-size: 24px;
    color: #212d4b;
    line-height: 125%;
    padding: 10px 0;
  }
  .sale-caption-section .promo-duration {
    font-size: 16px;
  }


  .promo-description-container {
    text-align: center;
  }

  .promo-description-text {
    max-width: 750px;
    margin: 0 auto;
    padding-top: 2em;
  }

  .promo-description-container a.promo-cta {
    padding: 17px 20px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    margin: 10px auto;
    margin-right: 1em;
    border-radius: 7px;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
  }

  p {
    font-size: 1.5em;
    line-height: 150%
  }

  .bold {
    font-weight: bold;
  }

  .promo-hero-img {
    width: 1200px;
    position: relative;
  }

  .promo-hero-img img {
  }

  .promo-hero-img .promo-img-caption {
    position: absolute;
    top: .5em;
    right: 0;
    background: rgba(87, 86, 84, 0.61);
    width: 23em;
    padding: 1em 1em 1em 2em;
    font-size: 1.75em;
    line-height: 130%;
    color: white;
  }

  .stressless-charity-sofa-promo {
    max-width: 1200px;
    display: flex;
  }
  .stressless-sofa-promo-description {
    padding: 20px;
  }
  .stressless-sofa-promo-description h3 {
    font-size: 30px;
    font-weight: bold;
    line-height: 120%;
  }

  @media only screen and (max-width: 720px) {
    .stressless-charity-sofa-promo {
      max-width: 720px;
      flex-wrap: wrap;
      align-items:center;
    }
    .stressless-charity-sofa-promo img {
      flex-basis: 100%;
    }
  }


  <?php if(!$is_desktop): ?>

  article {
    padding: 0 1em;
  }

  .sale-caption-section {
    display: grid;
    grid-template-rows: auto auto;
    grid-template-columns: auto auto;
    background: white;
  }

  .sale-caption-section h2 {
    grid-row: 2;
    grid-column: 1/3;
    padding-left: 0.5em;
  }

  .sale-caption-section img {
    height: 7em;
    margin: 0;
    padding: 1em 0;
    grid-column: 1/3;
    align-self: center;
    justify-self: center;
    grid-row: 1;
  }

  .sale-caption-section .subcaption {
    margin: 0;
    grid-column: 1;
    padding: 0 0 1em .5em;
  }

  .promo-hero-img {
    width: auto;
  }

  #requestFormContainer {
    margin-left: 0;
    padding-right: 0;
  }

  .promo-hero-img img,
  .promo-offer-details img {
    width: auto;
    max-width: 100%;
  }

  .promo-hero-img .promo-img-caption {
    position: static;
    width: auto;
    padding: 1em;
    font-style: italic;
  }

  .promo-description-text {
    margin-bottom: 1em;
    flex-wrap: wrap;
  }

  div.promo-description-container a.request {
    width: 100%;
  }

  .promo-description-text p {
    border: none;
    padding: 0;
  }


  #requestFormContainer h3.furn-red {
    margin-top: 1em !important;
  }

  #requestFormContainer form .form-control {
    width: 90%;
    padding: .7em;
    font-size: 1.2em;
  }

  #requestFormContainer form input.form-submit {
    background-color: #ed8223;
    color: #fff;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 18px;
    line-height: 30px;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border: 0;
    text-shadow: #C17C3A 0 -1px 0;
    padding: .3em 1em;
  }

  <?php endif;?>


</style>

<article>

  <div class="sale-caption-section clearfix">
    <h2 class="stressless-orange">
          <span style=" font-size: 101px; <?php print (!$is_desktop ? 'font-size:50px;' : ''); ?>;font-style: italic">
             Kindness Makes
          <br/>
          <span class="sale-title" style="font-size: 70px;">A World of Difference.</span> </span>
    </h2>
    <img src="<?php print $stressless_promo_root_dir . '/stressless-logo.png'; ?>" alt="Stressless logo" class="stressless-logo"/>
    <div class="subcaption"><span class="promo-duration">June 5 - August 3, 2020</span><br/>
      In times like these, kindness really counts. And here’s a way you can help the ones who need it the most. If you give a
      kind donation of $50 or more to charity, we’ll give you hundreds off Stressless® recliners, sofas and office chairs.
    </div>

  </div>

  <div class="promo-hero-img">
    <img src="<?php print $promo_dir . '/' . $img_path; ?>" alt="<?php print $alt; ?>"/>
  </div>

  <div class="promo-description-container">
    <div class="promo-description-text">
      <p>
        Donate $50 or more to charity and
        we’ll give you $500 off Stressless® Wing,
        $400 off Stressless® Mayfair and $300 off
        most other Stressless® recliners and
        ottomans, including Stressless® recliners
        with Power™ and Stressless® office chairs
        as appreciation for your donation.*
      </p>

    </div>

    <a href="<?php print base_path(); ?>ekornes-stressless-recliners-sofas" class="request promo-cta"
       title="Browse Stressless" style="">
      Browse Stressless®
    </a>

  </div>

  <br/><br/>

  <div class="stressless-charity-sofa-promo" style="">
      <div class="stressless-sofa-promo-description" style="">
        <h3>Your Donation of $50 or
          More Saves You $200 on
          Every Stressless® Sofa Seat*.</h3>
        <p><span style="font-style: italic">Now’s a great time to fill your home with
comfort and beauty that will last and last!</span></p>
      </div>
      <img src="<?php print $promo_dir . '/Stressless-charity-promo-2020-emily-sofa.jpg'; ?>"
         alt="Stressless® Emily Sofa."/>


  </div>
  <div class="stressless-charity-sofa-promo" style="">
    <img src="<?php print $promo_dir . '/Stressless-charity-promo-2020-mary-sofa.jpg'; ?>"
         alt="Stressless® Mary Sofa."/>
    <div class="stressless-sofa-promo-description" style="">
      <p>
        2-seater
        = <strong>$400 of savings</strong><br/>
        3-seater
        = <strong>$600 of savings</strong><br/>
        2-seater + 3-seater
        = <strong>$1,000 of savings</strong><br/>
        3-seater + 3-seater
        = <strong>$1,200 of savings</strong>
      </p>
      <p><span style="font-size: 14px;line-height: 75%">*With a qualifying
          purchase. Stressless®
          Oslo and Stressless®
          Manhattan sofas
            not included.</span></p>
    </div>



  </div>


  <br/><br/>

  <div class="promo-create-your-perfect-stressles">
    <?php if ($is_desktop): ?>
      <img src="<?php print $promo_dir . '/Stressless-charity-promo-2020-what-makes-stressless-special.jpg'; ?>"
         alt="What Makes Stressless® Seating Comfortably Yours?"/>
    <?php else: ?>
      <img src="<?php print $promo_dir . '/Stressless-P3-charity-promo-what-makes.png'; ?>"
           alt="What Makes Stressless® Seating Comfortably Yours?"/>
    <?php endif;?>
  </div>

  <?php print render($content); ?>

</article>

