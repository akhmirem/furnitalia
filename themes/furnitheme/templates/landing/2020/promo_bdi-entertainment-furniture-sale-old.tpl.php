<?php
$bdi_landing = base_path() . "sites/default/files/promo/bdi/media_sale";
$theme_path = base_path() . drupal_get_path('theme', 'furnitheme');
$img_path = $theme_path . '/images/promo/bdi/entertainment_sale';

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/bdi/entertainment-sale";

?>

<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=PT+Sans|PT+Sans+Narrow|Raleway:400,200,300,500,600,100"
      xmlns="http://www.w3.org/1999/html"/>
<style type="text/css">
    p.promo-copy {
        font-family: 'PT Sans', sans-serif;
        color: black;
        font-size: 1.2em;
    }
  @supports (di splay: grid) {
    article.promo-article {
      display: grid;
      grid-template-columns: 1fr 720px 1fr;
    }
    article > * {
      grid-column: 2;
    }

    @media (max-width: 767px){
      article.promo-article {
        grid: auto-flow / minmax(10px, 1fr) auto minmax(10px, 1fr);
      }
      article h3 span {
        font-size: 2em!important;
      }

    }

  }

  article h1 {
    font-family: Raleway, sans-serif;
    letter-spacing: .05em;
    font-size: 26px;
    margin: 0;
  }
  article h1 span.img {
    float:left;
    font-weight: bold;
  }
  article h1 span.sale-title {
    display: inline-block;
    background: #F15D2F;
    color: white;
    padding: .79em .5em;
    margin-left: .5em;
  }

  #requestFormContainer {
    display: flex;
    justify-content: center;
  }

  @media (max-width: 767px){
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ed8223;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #C17C3A 0 -1px 0;
      padding: .3em 1em;
    }
  }


</style>


<article class="promo-article">

  <h1><img src="<?php print $img_path; ?>/BDI-Entertainment-Furniture-Sale.png" alt="BDI Entertainment Furniture Sale"
           title="BDI Entertainment Furniture Sale"></h1>

  <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;"
      data-slick-lazy="true">
    <?php $promo_images = array(
      array(
        "src" => $img_path ."/items/BDI-corridor-cabinet.jpg",
        "title" => "BDI Corridor Entertainment Unit"
      ),
      array(
        "src" => $img_path ."/items/BDI-corridor-bar.jpg",
        "title" => "BDI Corridor Bar"
      ),
      array(
        "src" => $img_path ."/items/BDI-corridor-cabinet-sv.jpg",
        "title" => "BDI Corridor Entertainment Unit"
      ),
      array(
        "src" => $img_path ."/items/BDI-cosmo-bar.jpg",
        "title" => "BDI Cosmo Bar"
      ),
      array(
        "src" => $img_path ."/items/BDI-elements-cabinet.jpg",
        "title" => "BDI Elements Entertainment Unit"
      ),
      array(
        "src" => $img_path ."/items/BDI-elements-cabinet-tv.jpg",
        "title" => "BDI Elements TV/Entertainment Unit"
      ),
      array(
        "src" => $img_path ."/items/BDI-sector-cabinet.jpg",
        "title" => "BDI Sector Entertainment Unit"
      ),
      array(
        "src" => $img_path ."/items/BDI-tanami-cabinet.jpg",
        "title" => "BDI Tanami Entertainment Unit"
      ),
      array(
        "src" => $img_path ."/items/BDI-tanami-cabinet-white.jpg",
        "title" => "BDI Tanami Entertainment Unit White"
      ),
    );
    foreach($promo_images as $key => $image): ?>
      <li>
        <img src="<?php print $image['src'];?>"
             alt="<?php print $image['title']; ?>" class="img-responsiv e" style=""/>
      </li>
    <?php endforeach; ?>
  </ul>


<h3 style="text-align: center;margin: 0;font-size: 1.8em;color: black;padding:0.5em 0;line-height:130%">
    <span style="font-size: 2.5em;display: block;margin: .5em 0;">Save 15% OFF*</span> on BDI Entertainment Furniture
</h3>
<h4 style="    text-align: center;
    margin: 15px 0 30px 0;
    font-style: italic;
    font-size: 1.5em;">January 16 &mdash; February 5, 2020</h4>


<!--<aside class="bdi-box">
    <span class="orange">Innovative Designs</span><br/> For Modern Living
</aside>-->

<?php if (FALSE): ?>
    <!--Your media center is the entertainment hub of your home, and BDI offers an affordable and innovative home theatre and
    media furniture. BDI is an industry leader in media furniture and office systems. Always more than meets the eye, BDI
    merges innovative engineering and original design to seamlessly integrate technology into the home.-->

    <!--BDI’s furniture combines great design with innovative-->
    <!--functionality to seamlessly integrate technology into the home and office environments.-->
    <!--Whether you are looking to house a TV and soundbar or-->
    <!--a full blown multi-channel system, make sure the furniture that you-->
    <!--select meets your needs.-->

<?php endif;?>

<p class="promo-copy" style="font-size:1.7em;line-height:1.6em">
  Discover innovative designs of BDI entertainment furniture &mdash; media units, home bars, and BDI Semblance
  Modular Systems. Come in for 15% OFF on BDI home entertainment furniture. Now is the perfect time to get
  the best entertainment set up for your home!
</p>


<br/><br/>

<div style="text-align: center;">
    <img src="<?php print $promo_dir . '/' . $img_path; ?>" alt="BDI - Entertainment Furniture Sales Event" />
</div>
<br/>

<div style="text-align: center">`
    <?php if ($is_desktop): ?>
        <iframe width="720" height="405" src="//www.youtube.com/embed/yyossyAF2Cs" frameborder="0" allowfullscreen></iframe>
    <?php else: ?>
        <iframe width="320" height="236" src="//www.youtube.com/embed/yyossyAF2Cs" frameborder="0" allowfullscreen></iframe>
    <?php endif; ?>
</div>

<br/>

<hr class="grey-sep center"/>

<p style="color: black;text-align: center">
<!--    CALL TODAY FOR MORE INFORMATION ABOUT THIS EVENT! <br/>
    <a href="tel:+19163329000" style="border-bottom: 1px dashed #981b1e; color;#981b1e !important; text-decoration: none">916-332-9000</a><br/>
    <a href="<?php /*print base_path();*/?>request/ajax/bdi" class="request" id="request-info" title="Request BDI Product Information"
       style="padding: 17px 10px;
        background: #981b1e;
        display: inline-block;
        color: white;
        text-decoration: none;
        text-rendering: optimizeLegibility;
        font-size: 22px;
        width: 170px;
        margin: 10px auto;">
        Request Info
    </a> &nbsp;-->

    <a href="<?php print base_path();?>occasional/tv-media-storage?brand=29" class="request"  title="Shop BDI"
       style="padding: 17px 10px;
        background: #981b1e;
        display: inline-block;
        color: white;
        text-decoration: none;
        text-rendering: optimizeLegibility;
        font-size: 22px;
        width: 170px;
        margin: 10px auto;">
        Shop BDI
    </a>
</p>
<p style="    text-align: center;
    font-size: 0.7em;
    text-transform: uppercase;color:black">
* Website prices are as marked and already include 15% promo discount.
</p>

</article>

<?php print render($content); ?>

<!--<video class="wp-video-shortcode" id="video-10923-1" width="640" height="360" preload="metadata" src="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4?_=1" style="width: 100%; height: 100%;"><source type="video/mp4" src="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4?_=1"><a href="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4">http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4</a></video>-->

<section id="requestFormSection"></section>

