<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/inventory-closeout-2019";

if ($is_desktop) {
  $img_path = 'natuzzi-editions-and-alf-italia-closeout-desktop.jpg';
}
else {
  $img_path = 'natuzzi-editions-and-alf-italia-closeout-desktop.jpg';
}
$alt = "Annual Warehouse Closeout Sale - 30% OFF Natuzzi Editions and Alf Italia!";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Lato|Abril+Fatface" rel="stylesheet">
<style>

  article {
    color:#444;
    font-family: "Lato", Helvetica, Arial, sans-serif;
    padding-left: 4em;
  }

  h2 {
    font-family: 'Source Sans Pro', sans-serif;
    letter-spacing: 2px;
    line-height: 1;
    font-size: 2.5em;
    margin: 0 0 .7em 0;
    color: white;
    background-color: #ca2d30;
    margin-left: -1.6em;
    padding: .3em 0 .3em 1.6em;
  }

  h2 .sale-title {
    font-family: 'Abril Fatface', cursive;
    font-weight: 400;
    font-size: 1.5em;
  }

  .promo-description-text {
    width:50%;
    float: left;
  }
  #side-banner {

  }

  p {
    font-size:1.5em;
    line-height:150%
  }

  .bold {
    font-weight: bold;
  }

  .promo-hero-img {
    text-align: left;
  }
  .promo-hero-img img {
    width: 50%;
  }

  <?php if(!$is_desktop): ?>
  article {
    padding:0;
  }
  .promo-description-text {
    width: 100%;
  }
  #requestFormContainer {
    margin-left: 0;
    padding-right: 0;
  }
  .promo-hero-img img {
    width: 100%;
  }

  .promo-description-text {
    margin-bottom: 1em;
  }

  div.promo-description-container a.request {
    width:100%;
  }


  #requestFormContainer h3.furn-red {
    margin-top: 1em !important;
  }
  #requestFormContainer form .form-control {
    width: 90%;
    padding: .7em;
    font-size: 1.2em;
  }
  #requestFormContainer form input.form-submit {
    background-color: #ed8223;
    color: #fff;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 18px;
    line-height: 30px;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border: 0;
    text-shadow: #C17C3A 0 -1px 0;
    padding: .3em 1em;
  }
  <?php endif;?>

</style>

<article>

  <h2 class="stressless-orange">
        <span style="font-size: 1.8em; <?php print (!$is_desktop ? 'font-size:1em;' : ''); ?>">
            mid-winter
        <br/>
        <span class="sale-title" style="">MEGA Sale</span> </span>
  </h2>

  <div class="promo-hero-img">
    <img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>" />
  </div>

  <div style="font-size: 6.7em;letter-spacing: 9px;color: #ca2d30;font-family: 'Source Sans Pro', sans-serif;margin: .5em 0 .5em;font-weight: bold;
  <?php print (!$is_desktop ? 'line-height: 1;font-size: 4.6em;' : ''); ?>
    ">
    SAVE 30%
  </div>

  <div style="
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 2.5em;
    padding-bottom: 1em;
    line-height: 130%;
    ">OFF ON <br/>Natuzzi Editions and Alf Italia</div>

  <div class="promo-description-container">
    <div class="promo-description-text">
      <p>
        Looking for a great deal on contemporary furniture? <br/>
        Take advantage of our Mid-Winter MEGA Sales event.
        Save <strong>30% OFF MSRP</strong> on modern sofas, sectionals, and armchairs from Natuzzi Editions
        collection. There is a rich variety of Natuzzi Editions furniture available in our 40,000 sq ft
        showroom in Sacramento.
      </p>

      <p>
        Save <strong>30% OFF MSRP</strong> on contemporary dining, bedroom, living, and occasional case goods
        furniture from ALF Italia. Stop by for a wide selection of furniture for any room and for any budget!
      </p>

      <br/>
      <p>Furnitalia is your ultimate destination for modern and contemporary leather furniture in Sacramento
        and Northern California. We offer a large selection of modern furniture collections from Natuzzi
        Italia, Natuzzi Editions, Alf Italia, Bontempi, Stressless, Cattelan Italia, Calligaris,
        and many others.
      </p>

      <a href="<?php print base_path();?>natuzzi-editions/living" class="request"  title="Browse Natuzzi Editions"
         style="padding: 17px 10px;
                background: #981b1e;
                display: inline-block;
                color: white;
                text-decoration: none;
                text-rendering: optimizeLegibility;
                font-size: 22px;
                margin: 10px auto;
                margin-right:1em;
                border-radius: 7px;
                -webkit-border-radius: 7px;
                -moz-border-radius: 7px;">
        Browse Natuzzi Editions
      </a>
      <a href="<?php print base_path();?>brand/alf" class="request"  title="Browse ALF Italia"
         style="padding: 17px 10px;
                background: #981b1e;
                display: inline-block;
                color: white;
                text-decoration: none;
                text-rendering: optimizeLegibility;
                font-size: 22px;
                margin: 10px auto;
                border-radius: 7px;
                -webkit-border-radius: 7px;
                -moz-border-radius: 7px;">
        Browse Alf Italia
      </a>


    </div>
    <aside id="side-banner">
      <img src="<?php print $promo_dir; ?>/inventory-closeout-sample-items.jpg" />
    </aside>
  </div>





  <p class="" style='    font-family: "Source Sans Pro", sans-serif;
    font-size: 2.2em;
    line-height: 1.42858;
    color: #857874;
    font-weight: bold;
    letter-spacing: 3.5px;
    position: relative;
    top: 1.5em;
  <?php print (!$is_desktop ? 'top: 1em; font-size: 1.4em;' : ''); ?>
    '>Fill out the form for discount</p>

  <!--<div class="renovation-sale-form-container">
        <img src="<?php print $promo_dir; ?>/webform-cover-img.jpg" />
        <?php print render($content); ?>
    </div> -->

  <?php print render($content); ?>

  <!--<div id="pants">
        <div id="pocket">
            <img src="<?php /*print $promo_dir; */?>/webform-scissors-icon.jpg" class="scissors-icon" />
            <div class="stitch-top bordered"></div>
            <div class="stitch-left bordered"></div>
            <div class="stitch-right bordered"></div>
            <div class="stitch-bottom bordered"></div>
            <div class="content clearfix">
                <img src="<?php /*print $promo_dir; */?>/webform-caption-image.png" class="webform-caption-img"/>
                <?php /*print render($content); */?>
            </div>
        </div>
    </div>-->




  <br/>



  <!--<p style="text-align: center">
        <a href="<?php /*print base_path();*/?>collections" class="request"  title="Browse collection"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Browse collection
        </a>
    </p>-->



</article>


<script>

  (function($) {


    $(function () {
      //handleVideoJSPlugin();
    });

  })(jQuery);
</script>


