<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/summer_sale";

if ($is_desktop) {
    $img_path = 'stressless_summer_sale_landing.png';
    $img_path2 = 'stressless_summer_sale_landing2.png';
}
else {
    $img_path = 'stressless_summer_sale_mobile.png';
    $img_path2 = 'stressless_summer_sale_landing_mobile2.png';
}
$alt = "Spring into Savings: March 31 - May 15, 2017";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400" rel="stylesheet">

<h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 2em;
    font-size: 1.9em;"><span style="
    letter-spacing: 1px;
    color:#cd431f;
    ">Save $300.</span>Plus get an FREE accessory when you purchase Stressless seating..</h2>


<!--<img src="--><?php //print $promo_dir . '/'. $img_path; ?><!--" alt="Spring into Savings: March 31 - May 15, 2017"/>-->


<ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none" data-slick-lazy="true">
    <li>
        <img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>"/>

    </li>
    <li>
        <img src="<?php print $promo_dir . '/'. $img_path2; ?>" alt="<?php print $alt;?>"/>

    </li>
</ul>



<p class="" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;color:#cd431f;"><strong>Comfort Plus Promotion:</strong> June 16 - July 31, 2017.</p>

<p style="font-size: 1.3em;
    line-height: 130%;">
    Want to fill your summer with comfort and savings? Stressless® has two offers you simply can’t refuse. Right now, with the purchase of any Stressless seating, choose an accompanying accessory for free. PLUS, as an additional offer, save $300 on recliners from our Stressless Live family in select colors. Don’t let these offers pass you by.
</p>

<p style="font-size: 1.3em;
    line-height: 130%;">
    Innovatively designed from the inside out for ultimate comfort, Stressless® line is the only furniture endorsed by the American Chiropractic Association (ACA) for proper head and lumbar support. Available in a vast array of colors and leather grades, Stressless seating can be customized any way you want it.
</p>

<p style="font-size: 1.3em;
    line-height: 130%;">Plus, enjoy $300 OFF Stressless Live recliner and ottoman in our Classic or Signature Base, Stressless Live with our NEW LegComfort system or Stressless Live Office in the following leather colors: Paloma Copper, Funghi, Metal Grey or Oxford Blue.
</p>

<p style="font-size: 1.3em;
    line-height: 130%;">See sales associate for complete details.</p>

<p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
    Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
</p>

*Complete conditions in store. This offer runs until July 31st, 2017.

<p style="text-align: center">
    <a href="<?php print base_path();?>brand/stressless-ekornes" class="request"  title="Shop Stressless"
       style="padding: 17px 10px;
            background: #ea2328;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
        Shop Stressless
    </a>
</p>
