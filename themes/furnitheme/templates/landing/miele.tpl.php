<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$theme_path = base_path() . drupal_get_path('theme', 'furnitheme');
$promoPathPrefix = base_path() . variable_get('file_public_path', conf_path() . '/files'). '/promo/miele_promo';

/*//$miele_oven_banner = $promoPathPrefix . "/dishwasher_100off/2018_miele_steam_promo_desktop.png";
$miele_kitchen_package_disctount_banner = $promoPathPrefix . '/kitchen/miele-kitchen-10-percent-off-top-banner.png';
if (!$is_desktop) {
    $miele_oven_banner = $promoPathPrefix . "/dishwasher_100off/2018_miele_steam_promo_mobile.png";
    $miele_kitchen_package_disctount_banner = $promoPathPrefix . '/kitchen/miele-kitchen-10-percent-off-top-banner-mobile.png';
}*/

$miele_banners = array(
    /*array(
        'src' => $promoPathPrefix . '/kitchen/miele-kitchen-10-percent-off-top-banner.png',
        'alt' => 'Receive a 10% discount off the purchase price of qualifying appliances!',
    ),
    array(
        'src' => $promoPathPrefix . '/dishwasher_100off/miele_steam_promo_top_banner.png',
        'alt' => 'Receive a complimentary accessory set with the purchase of a DGC XL or XXL combi-steam oven. For a limited time only!',
    ),
    */
    array(
        'src' => $theme_path. '/images/miele/Miele-slideshow-main-image.jpg',
        'alt' => 'Miele Applianaces - an exceptional kitchen with a price to match!',
    ),
    array(
        'src' => $theme_path . '/images/miele/landing-page-image.jpg',
        'alt' => 'Miele - The world\'s most inspiring kitchen, at an inspiring price.',
    ),
    array(
        'src' => $theme_path . '/images/miele/Miele-slideshow-dishwasher.jpg',
        'alt' => 'Miele Dishwashers - Built to last. Engineered to amaze.'
    ),
    array(
        'src' => $theme_path . '/images/miele/miele-fridge-banner.jpg',
        'alt' => 'Miele Refrigerators - Tasting is believing.'
    ),
    array(
        'src' => $theme_path . '/images/miele/miele-home-care-banner.jpg',
        'alt' => 'HomeCare by Miele. Your custom performance machine.'
    ),
    array(
        'src' => $theme_path . '/images/miele/miele-umbrella-banner.jpg',
        'alt' => 'There are things in life which people just love. Miele. For everything you really love.'
    ),
);
?>

<style>
    @import "<?php print $theme_path ?>/css/font-awesome.min.css";

    .miele-logo-container {
      display: flex;
      justify-content: center;
    }

    .page-miele-appliances .page-title{
      font-weight: 300;
      font-size: 26px;
      color: #666;
      text-align: center;
    }

    .landing-page-image{
        height: auto;
        overflow: hidden;
        position: relative;
        margin-bottom: 20px;
        text-align: center;
        color: #212020;
    }
    .landing-page-overlay{
        background: rgb(0,0,0);
        background: rgba(0,0,0, .85);
        bottom: 0;
        left: 0;
        right: 0;
        min-height: 143px;
        padding: 20px;
    }

    .landing-page-overlay{
        color: #fff;
        z-index: 10;
        position: relative;
    }

    .landing-page-overlay .miele-concierge-logo{
        opacity: .75;
        text-align: center;
        margin-left: 250px;
    }

    .landing-page-logo-image{
        position: absolute;
        background-color: #fff;
        width: 205px;
        color: #333;
        padding: 27px 10px;
        text-align: center;
        left: 120px;
        <?php print ($is_desktop) ? "left: 150px;" : ""; ?>
        top: 20px;
    }

    .landing-page-logo-image img{
        max-width: 100%;
    }

    .landing-page-description{
        font-size: 1.2em;
        <?php print ($is_desktop) ? "padding: 0 60px;" : ""; ?>
    }

    .landing-page-description .sub-heading{
        color: #999;
    }

    .container .shop-online{
        background-color: #e60000;
        border-radius: 5px;
        color: #fff;
        display: inline-block;
        padding: 10px 20px 10px 20px;
        text-decoration: none;
        font-weight: 700;
        font-size: 1.25em;
        margin:10px 0 10px 0;
    }
    .container .shop-online.slider-link {
        position: relative;
    }

    .landing-page-description p:last-child{
        margin-bottom: 0;
    }

    .landing-page-footer-tiles .footer-tile{
        font-size: .85em;
        position: relative;
    }

    .landing-page-footer-tiles .icon{
        position: absolute;
        left: 0;
        width: 40px;
        min-height: 1em;
        font-size: 36px;
        color: #999;
        text-shadow: 2px 2px #fff;
    }

    .landing-page-footer-tiles .tile-content{
        margin-left: 50px;
    }

    .landing-page-footer-tiles .tile-content a {
        font-weight: 700;
        font-size: 1.25em;
        color: #333;
        text-decoration: underline;
    }

    @media only screen and (max-width: 720px) {
        .landing-page-logo-image {
            position: relative;
            margin: 0 auto 4em auto;
            left: 0;
        }
        .landing-page-overlay .miele-concierge-logo {
            display:none;
        }
    }
</style>

<!-- Tabs -->
<style>
  /* TABS */
  /* FROM https://kyleschaeffer.com/development/simple-jquery-tabs-template/ */
    .ui-tabs {
        zoom: 1;
    }
    .ui-tabs .ui-tabs-nav {
        list-style: none;
        position: relative;
        padding: 0;
        margin: 0;
    }
    .ui-tabs .ui-tabs-nav li {
        position: relative;
        float: left;
        margin: 0 3px -2px 0;
        padding: 0;
    }
    .ui-tabs .ui-tabs-nav li a {
        display: block;
        padding: 10px 20px;
        background: #f0f0f0;
        border: 2px #ccc solid;
        border-bottom-color: #ccc;
        outline: none;
    }
    .ui-tabs .ui-tabs-nav li.ui-tabs-selected a {
        padding: 10px 20px 12px 20px;
        background: #fff;
        border-bottom-style: none;
    }
    .ui-tabs .ui-tabs-nav li.ui-tabs-selected a,
    .ui-tabs .ui-tabs-nav li.ui-state-disabled a,
    .ui-tabs .ui-tabs-nav li.ui-state-processing a {
        cursor: default;
    }
    .ui-tabs .ui-tabs-nav li a,
    .ui-tabs.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-selected a {
        cursor: pointer;
    }
    .ui-tabs .ui-tabs-panel {
        display: block;
        clear: both;
        border: 2px #ccc solid;
        padding: 10px;
    }
    .ui-tabs .ui-tabs-hide {
        display: none;
    }
    .ui-tabs .ui-widget-header {
        background: none;
        font-weight: 400;
        border: none;
    }
    .ui-tabs .ui-tabs-nav li.ui-tabs-active {
        margin-bottom: -2px;
        padding-bottom: 0;
    }
    .ui-tabs .ui-tabs-nav li.ui-tabs-active a {
        color:#a12a2e;
        font-weight: 700;
    }

    <?php if (!$is_desktop) : ?>
    .ui-tabs .ui-tabs-nav li {
        width:100%;
    }
    .ui-tabs .ui-tabs-nav li a {
        float: none;
    }
    <?php endif; ?>
</style>

<!-- Promo Teaser Boxes -->
<style>
  .tsr-text-container {
    display: flex;
    flex-wrap: wrap;
  }
  .tsr-text__text-wrapper, .tsr-text__visual {
    max-width: 100%;
    -webkit-box-flex: 0;
    -webkit-flex: 0 0 100%;
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
  }
  .tsr-text__text-wrapper {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: start;
    -webkit-align-items: start;
    -ms-flex-align: start;
    align-items: start;
  }
  .tsr-text__text {
    padding: 24px 32px;
    width: 100%;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: baseline;
    -webkit-align-items: baseline;
    -ms-flex-align: baseline;
    align-items: baseline;
    height: 100%;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    text-align: left;
  }
  .tsr-text-container .tsr-text {
    flex-basis: 100%;
  }
  .tsr-text--text-left .tsr-text__visual {
    -webkit-box-ordinal-group: 3;
    -webkit-order: 2;
    -ms-flex-order: 2;
    order: 2;
    overflow: hidden;
  }
  .tsr-text__visual {
    display: block;
    background-position: 50%;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
  }
  .tsr-text {
    position: relative;
    background: white;
  }
  .tsr-text img {
    padding: 30px 60px;
  }
  .imagefixer {
    position: relative;
  }
  .imagefixer img {
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    padding: 0;
  }
  .tsr-text .cpy {
    font-size: 16px;
    line-height: 24px;
  }
  .tsr-text__description__highlight {
    font-size: 22px;
    line-height: 32px;
  }
  @media (min-width: 768px){
    .tsr-text {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-wrap: wrap;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
    }
    .tsr-text__text-wrapper, .tsr-text__visual {
      position: relative;
      width: 100%;
      -webkit-box-flex: 0;
      -webkit-flex: 0 0 50%;
      -ms-flex: 0 0 50%;
      flex: 0 0 50%;
      max-width: 50%;
      padding: 0;
    }
    .tsr-text__text {
      padding: 40px 80px;
    }
    .tsr-text-container .tsr-text__text {
      padding: 0;
    }
    .tsr-text-container .tsr-text {
      flex-basis: 50%;
    }
  }
</style>

<div class="container">

    <div class="miele-logo-container">
        <div class="miele-logo"><img width="261" height="100" src="<?php print $theme_path;?>/images/miele/miele-logo-new-2020.jpg" class="attachment-thumbnail" alt="Miele Logo"></div>
<!--        <div class="redbar"></div>-->
    </div>


    <h1 class="page-title">Shop Best Miele Home Appliances at Furnitalia</h1>

    <!-- landing page main content -->
    <div class="landing-page-image">

        <?php if (count($miele_banners) > 1) : ?>
            <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none" data-slick-lazy="true">
                <?php foreach($miele_banners as $banner): ?>
                    <li>
                        <img data-lazy="<?php print $banner['src']; ?>" alt="<?php print $banner['alt']; ?>" style="" />
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <img width="720" src="<?php print $miele_banners[0]['src']; ?>" class="attachment-post-thumbnail wp-post-image" alt="<?php print $miele_banners[0]['alt']; ?>">
        <?php endif; ?>

        <div class="landing-page-overlay-bg"></div>
        <div class="landing-page-overlay">

            <div class="landing-page-logo-image">
                <img width="225" height="84" src="<?php print $theme_path;?>/images/miele/miele-authorized-direct-seller.jpg" class="attachment-full" alt="miele-authorized-direct-seller">
            </div>
            <div class="f-right miele-concierge-logo">
                <img width="150" height="145" src="<?php print $theme_path;?>/images/miele/mieleconcierge.png" class="attachment-thumbnail" alt="mieleconcierge">
            </div>
            <div class="landing-page-description">
                <p>Miele - the world's most inspiring kitchen appliances, at an inspiring price! Furnitalia and Miele have partnered together in business for many years. We both agree that customer service is our primary focus and we ensure that every customer is completely satisfied with not only the appliances, but with their overall purchasing experience.  We invite you to enjoy your experience.</p>

                <p>
                    <a href="/contact-us" class="shop-online"
                       target="_blank" title="Shop Miele appliances with Furnitalia">Contact Us →</a>
                </p>

            </div>

        </div>

        <h3 style="font-size: 2em;line-height: 2;margin-top: 3em;">Current Miele Promotions</h3>

        <div id="tabs" class="ui-tabs">
            <ul class="ui-tabs-nav">
                <li><a href="#tabs-0">Miele Kitchen Package</a></li>
                <li><a href="#tabs-1">$200 Off Crystal Dishwasher</a></li>
                <li><a href="#tabs-2">$100 Off Washer or Dryer</a></li>
                <?php if (false): ?><li><a href="#tabs-3">$200 Off any Countertop Coffee System</a></li><?php endif;?>
                <!--<li><a href="#tabs-2">
                    $200 off Crystal Dishwasher</a></li>-->
            </ul>

            <div id="tabs-0" class="clearfix ui-tabs-panel">

                <span style="font-size: 1.5em;margin: 1em;display: block;">Up to $1,400 in savings on a Miele appliance package.</span>

                <img src="<?php print $promoPathPrefix; ?>/kitchen2020/2020_Miele-KitchenPackage.png"
                     alt="Up to $1,400 in savings on a Miele appliance package!"
                     style="width:100%;height:auto"
                />
                <br/>
                <p style="line-height: 130%;font-size: 1.5em;text-align: left;">Out of your dreams and into your kitchen.
                  Select the qualifying built-in kitchen appliance package that is right for you. <br/>
                  <span style="font-size: .75em;margin-top: .5em;">Valid April 1st - November 30th, 2020</span></p>

                <h3>Qualifying Packages</h3>
                <div class="tsr-text tsr-text--text-left">
                  <div class="tsr-text__visual imagefixer">
                    <picture><source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=2000&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp; 1x, https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=4000&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp; 2x"><source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=1200&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp; 1x, https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=2400&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp; 2x"><source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=992&amp;x=1501&amp;y=818&amp;w=2100&amp;h=2100&amp; 1x, https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=1984&amp;x=1501&amp;y=818&amp;w=2100&amp;h=2100&amp; 2x"><source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=768&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp; 1x, https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=1536&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp; 2x"><img src="https://media.miele.com/images/2000016/200001693/20000169338.png?impolicy=teaser&amp;imwidth=2000&amp;x=1079&amp;y=953&amp;w=3103&amp;h=1746&amp;" alt=""></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        Package A
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p>Purchase a<strong> range </strong>and full-size <strong>refrigeration</strong> to <strong>receive $1,000</strong> in savings<sup>*</sup>. &nbsp;Plus,<strong> receive $100</strong> off any additional qualifying appliance<sup>†</sup>.</p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tsr-text  ">
                  <div class="tsr-text__visual imagefixer"><picture><source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=2000&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp; 1x, https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=4000&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp; 2x"><source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=1200&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp; 1x, https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=2400&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp; 2x"><source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=992&amp;x=1330&amp;y=547&amp;w=2440&amp;h=2440&amp; 1x, https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=1984&amp;x=1330&amp;y=547&amp;w=2440&amp;h=2440&amp; 2x"><source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=768&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp; 1x, https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=1536&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp; 2x"><img src="https://media.miele.com/images/2000016/200001694/20000169491.png?impolicy=teaser&amp;imwidth=2000&amp;x=773&amp;y=828&amp;w=3584&amp;h=2017&amp;" alt=""></picture></div>
                  <div class="tsr-text__text-wrapper" style="">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        Package B
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p>Purchase an <strong>oven</strong>, <strong>cooktop/rangetop</strong> and full-size<strong> refrigeration</strong> to <strong>receive $1,000</strong> in savings<sup>*</sup>. &nbsp;Plus,<strong> receive $100</strong> off any additional qualifying appliance<sup>†</sup>.</p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>

              <img src="<?php print $promoPathPrefix; ?>/kitchen2020/kitchen-promo-rules.png"
                   style="width:100%;height:auto"
                   alt="Kitchen package promo rules"/>

                <p style="text-align: left;">
                  *Redeem a $1,000 rebate on qualifying packages after purchase.<br/>
                  †Additional Miele appliances must have UMRP value of $1,499 or more. Limit of four additional appliances per customer.
                </p>
            </div>

            <div id="tabs-1" class="clearfix ui-tabs-panel">


                <span style="font-size: 1.5em;margin: 1em;display: block;">A Quieter Clean at $200 Off</span>
                <p style="text-align: left">
                  Save on any Miele Crystal Dishwasher.
                </p>

                <img src="<?php print $promoPathPrefix; ?>/dw2020/Miele_2020_CrystalDishwasher_promo-hero.png"
                     alt="Save on any Miele Crystal Dishwasher"
                    style="width:100%;height:auto"
                />

                <h3>For a limited time, save on any Miele Crystal Dishwasher.</h3>
                <p>From April 1st - August 14th, 2020, receive a $200 rebate on any Miele Crystal Dishwasher.
                </p>


                <br/>

                <div class="tsr-text-container">
                  <div class="tsr-text ">
                    <div class="tsr-text__visual ">
                      <picture>
                        <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000013/200001304/20000130433.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130433.png?d=500&amp;impolicy=boxed 2x">
                        <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000013/200001304/20000130433.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130433.png?d=900&amp;impolicy=boxed 2x">
                        <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000013/200001304/20000130433.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130433.png?d=660&amp;impolicy=boxed 2x">
                        <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000013/200001304/20000130433.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130433.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000013/200001304/20000130433.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000013/200001304/20000130433.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130433.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                    </div>
                    <div class="tsr-text__text-wrapper">
                      <div class="tsr-text__text">
                        <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                          G 6625 SCU AM
                        </h3>
                        <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                          <p><span class="tsr-text__description__highlight">Pre-​finished, full-​size dishwasher</span><br/>
                            with visible control panel, 3D+ cutlery tray, water softener and 6 Programs.
                          </p>
                        </div>
                        <div class="tsr__button-box">
                          <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                             onclick="jivo_api.open();return false;">Contact Us</a>
                        </div>
                      </div>
                    </div>
                  </div>
                    <div class="tsr-text ">
                      <div class="tsr-text__visual ">
                        <picture>
                          <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000013/200001304/20000130437.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130437.png?d=500&amp;impolicy=boxed 2x">
                          <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000013/200001304/20000130437.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130437.png?d=900&amp;impolicy=boxed 2x">
                          <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000013/200001304/20000130437.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130437.png?d=660&amp;impolicy=boxed 2x">
                          <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000013/200001304/20000130437.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130437.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000013/200001304/20000130437.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000013/200001304/20000130437.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130437.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                      </div>
                      <div class="tsr-text__text-wrapper">
                        <div class="tsr-text__text">
                          <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                            G 6665 SCVi AM
                          </h3>
                          <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                            <p><span class="tsr-text__description__highlight">Fully-​integrated, full-​size dishwasher</span><br/>
                              with hidden control panel, 3D+ cutlery tray, custom panel and handle ready
                            </p>
                          </div>
                          <div class="tsr__button-box">
                            <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                               onclick="jivo_api.open();return false;">Contact Us</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tsr-text ">
                      <div class="tsr-text__visual ">
                        <picture>
                          <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000013/200001304/20000130439.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130439.png?d=500&amp;impolicy=boxed 2x">
                          <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000013/200001304/20000130439.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130439.png?d=900&amp;impolicy=boxed 2x">
                          <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000013/200001304/20000130439.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130439.png?d=660&amp;impolicy=boxed 2x">
                          <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000013/200001304/20000130439.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130439.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000013/200001304/20000130439.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000013/200001304/20000130439.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000013/200001304/20000130439.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                      </div>
                      <div class="tsr-text__text-wrapper">
                        <div class="tsr-text__text">
                          <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                            G 6665 SCVi SF AM
                          </h3>
                          <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                            <p><span class="tsr-text__description__highlight">Fully-​integrated, full-​size dishwasher</span><br/>
                              with hidden control panel, 3D+ cutlery tray and CleanTouch Steel panel
                            </p>
                          </div>
                          <div class="tsr__button-box">
                            <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                               onclick="jivo_api.open();return false;">Contact Us</a>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                <p style="text-align: left;">
                  *For full promotion terms and conditions, please visit Miele USA <a style="text-decoration: underline;"
                                                                                      href="https://www.mieleusa.com/c/promotions-158.htm">here</a>
                </p>

            </div>

            <div id="tabs-2" class="clearfix ui-tabs-panel">


              <span style="font-size: 1.5em;margin: 1em;display: block;">$100 off any Washer & Dryer</span>
              <p style="text-align: left">
                Savings on unrivaled performance.
              </p>

              <img src="<?php print $promoPathPrefix; ?>/washer&dryer2020/2020_Miele-washer-promo_hero.png"
                   alt="Save on any Miele Washer & Dryer"
                   style="width:100%;height:auto"
              />

              <h3>For a limited time, save $100 on any washer or dryer</h3>
              <p>Experience the power and efficiency of German engineering. Valid April 1st - August 14th, 2020.</p>


              <br/>

              <h3>Washing Machines</h3>
              <div class="tsr-text-container">
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000015/200001514/20000151490.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151490.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000015/200001514/20000151490.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151490.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000015/200001514/20000151490.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151490.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000015/200001514/20000151490.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151490.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000015/200001514/20000151490.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000015/200001514/20000151490.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151490.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        WWB 020 WCS
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">W1 Classic front-​loading washing machine</span><br/>
                          With CapDosing for intelligent laundry care.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000012/200001257/20000125792.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125792.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000012/200001257/20000125792.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125792.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000012/200001257/20000125792.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125792.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000012/200001257/20000125792.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125792.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000012/200001257/20000125792.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000012/200001257/20000125792.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125792.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        WWF 060 WCS WiFiConn@ct
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">W1 Front-​loading washing machine</span><br/>
                          with CapDosing and WiFiConn@ct.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000012/200001257/20000125788.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125788.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000012/200001257/20000125788.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125788.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000012/200001257/20000125788.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125788.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000012/200001257/20000125788.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125788.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000012/200001257/20000125788.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000012/200001257/20000125788.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125788.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        WWH 660 WCS TwinDos & WiFiConn@ct
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">W1 Front-​loading washing machine</span><br/>
                          with TwinDos, CapDosing, and WiFiConn@ct.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000012/200001257/20000125790.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125790.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000012/200001257/20000125790.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125790.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000012/200001257/20000125790.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125790.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000012/200001257/20000125790.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125790.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000012/200001257/20000125790.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000012/200001257/20000125790.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001257/20000125790.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        WWH 860 WCS TwinDos & IntenseWash WiFi
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">W1 Front-​loading washing machine</span><br/>
                          with QuickIntenseWash, TwinDos, CapDosing, and WiFiConn@ct.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <br/>

              <h3>Tumble Dryers</h3>
              <div class="tsr-text-container">
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000015/200001514/20000151489.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151489.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000015/200001514/20000151489.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151489.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000015/200001514/20000151489.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151489.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000015/200001514/20000151489.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151489.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000015/200001514/20000151489.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000015/200001514/20000151489.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000015/200001514/20000151489.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        TWB120WP
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">T1 Classic heat-​pump tumble dryer</span><br/>
                          With FragranceDos for laundry that smells great.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000012/200001258/20000125886.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001258/20000125886.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000012/200001258/20000125886.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001258/20000125886.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000012/200001258/20000125886.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001258/20000125886.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000012/200001258/20000125886.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001258/20000125886.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000012/200001258/20000125886.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000012/200001258/20000125886.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001258/20000125886.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        T1 Heat-​pump tumble dryer
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">T1 Classic heat-​pump tumble dryer</span><br/>
                          with WiFiConn@ct and FragranceDos.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-text ">
                  <div class="tsr-text__visual ">
                    <picture>
                      <source media="(min-width: 1200px)" srcset="https://media.miele.com/images/2000012/200001259/20000125974.png?d=250&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001259/20000125974.png?d=500&amp;impolicy=boxed 2x">
                      <source media="(min-width: 992px)" srcset="https://media.miele.com/images/2000012/200001259/20000125974.png?d=450&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001259/20000125974.png?d=900&amp;impolicy=boxed 2x">
                      <source media="(min-width: 768px)" srcset="https://media.miele.com/images/2000012/200001259/20000125974.png?d=330&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001259/20000125974.png?d=660&amp;impolicy=boxed 2x">
                      <source media="(min-width: 576px)" srcset="https://media.miele.com/images/2000012/200001259/20000125974.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001259/20000125974.png?d=1080&amp;impolicy=boxed 2x"><img alt="" src="https://media.miele.com/images/2000012/200001259/20000125974.png?d=540&amp;impolicy=boxed" srcset="https://media.miele.com/images/2000012/200001259/20000125974.png?d=540&amp;impolicy=boxed 1x, https://media.miele.com/images/2000012/200001259/20000125974.png?d=1080&amp;impolicy=boxed 2x" width="100%"></picture>
                  </div>
                  <div class="tsr-text__text-wrapper">
                    <div class="tsr-text__text">
                      <h3 class="tsr-text__title hdl hdl--headline hyphenate">
                        TWI180 WP Eco&Steam WiFiConn@ct
                      </h3>
                      <div class="tsr-text__description cpy cpy--body cpy--primary txt-textparagraph">
                        <p><span class="tsr-text__description__highlight">T1 Classic heat-​pump tumble dryer</span><br/>
                          with WiFiConn@ct, FragranceDos, and SteamFinish.
                        </p>
                      </div>
                      <div class="tsr__button-box">
                        <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                           onclick="jivo_api.open();return false;">Contact Us</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <p style="text-align: left;">
                *For full promotion terms and conditions, please visit Miele USA <a style="text-decoration: underline;"
                  href="https://www.mieleusa.com/c/100-dollar-rebate-on-washers-dryers-2159.htm">here</a>
              </p>

            </div>

            <?php if (false): ?>
            <!-- Countertop Coffee Systems -->
            <div id="tabs-3" class="clearfix ui-tabs-panel">


              <span style="font-size: 1.5em;margin: 1em;display: block;">Receive $200 Off any Miele Countertop Coffee System.</span>
              <p style="text-align: left">
                Pure coffee enjoyment at the touch of a button.
              </p>

              <img src="<?php print $promoPathPrefix; ?>/coffee2020/miele_2020_coffee-promo_hero.jpg"
                   alt="Save $200 on any Miele countertop Coffee System"
                   style="width:100%;height:auto"
              />

              <h3>For a limited time, save $200 on any Miele Countertop Coffee System</h3>
              <p>Receive an instant $200 off any Miele Countertop Coffee System and experience pure coffee enjoyment at the touch of a button.
                <br/>Valid April 1st - May 31st, 2020.</p>


              <br/>


              <p style="text-align: left;">
                *For full promotion terms and conditions, please visit Miele USA <a style="text-decoration: underline;"
                                                                                    href="https://www.mieleusa.com/c/promotions-158.htm">here</a>
              </p>

              <a href="/contact-us" class="btn btn-primary btn-contact shop-online"
                 onclick="jivo_api.open();return false;">Contact Us</a>

            </div>
            <?php endif;?>

          <!--<div id="tabs-2" class="clearfix ui-tabs-panel">

                <span style="font-size: 1.5em;margin: 1em;display: block;">Innovative dishwasher features, $100 in savings.</span>
                <span style="font-size: 1.5em;margin: 1em;display: block;">only through Feb 22, 2020</span>

              <img src="<?php /*print $promoPathPrefix; */?>/dishwasher/miele-dishwasher-100off-promo.jpg"
                   alt="Save $100 on select dishwashers." style="width:100%;height:auto"
                />
              <br/>
              <h4>Perfect Results</h4>
              <p>
                Our dishwashers are whisper quiet with performance that exceeds expectations.
                Our dishwashers are equipped with innovative features and guarantee perfect results every time.
              </p>

              <h4>Energy Star Qualified</h4>
              <p>Our hot water connection capabilities allow for a more efficient clean, with up to 50% of
                electricity savings.
              </p>


            </div>-->
        </div>


    <!--    <p style="text-align: center">
          <a href="/contact-us" class="btn btn-primary btn-contact shop-online">Contact Us</a>
        </p>-->

    </div>


    <?php if (false): ?>
    <!-- footer tiles -->
    <div class="row landing-page-footer-tiles">
<!--        <div class="span4 footer-tile">-->
<!--            <div class="icon">-->
<!--                <i class="fa fa-truck"></i>-->
<!--            </div>-->
<!--            <div class="tile-content">-->
<!--                <a href="http://www.mieleconcierge.com/">Mieleconcierge.com</a>-->
<!--                <p>Learn more about what you can expect when you purchase a Miele appliance!</p>-->
<!--            </div>-->
<!--        </div>-->


        <div class="span4 footer-tile">
            <div class="icon">
                <i class="fa fa-envelope"></i>
            </div>
            <div class="tile-content">
                <a href="<?php print base_path();?>request/ajax/miele" class="request" id="request-info" title="Contact Us for more information on Miele Appliances">Contact us</a>
                <p>If you have any questions regarding purchasing Miele appliances, please do not hesitate to contact us directly.</p>
            </div>
        </div>
      <?php endif; ?>

        <div class="span4 footer-tile">         &nbsp;
		<?php if (!$is_desktop) : ?>
		    <section id="requestFormSection"></section>
		<?php endif; ?>
        </div>
    </div>

</div>

