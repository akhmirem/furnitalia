<?php
    global $conf;
    if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
        $is_desktop = FALSE;
    } else {
        $is_desktop = TRUE;
    }
    $promo_dir = base_path() . "sites/default/files/promo/presidents_day";

    if ($is_desktop) $img_path = 'natuzzi_presidents_day_desktop_landing.jpg';
    else $img_path = 'natuzzi_presidents_day_mobile_landing.jpg';
?>

<h2 class="furn-red" style="text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;line-height: 2em;"><span style="font-size:1.6em ">President's Day Sale. </span><br/>Living, dining and bed with great savings</h2>


<img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="President's Day Sale - Great Savings - Until February 26th"/>

<h4 style="text-align: center"><!--Presidents' Day Sale on sofas, furnishings and accessories<br/>-->GREAT SAVINGS
    UNTIL FEBRUARY 26TH</h4>

<!--Exclusive discounts on complete Natuzzi Italia living solutions.-->



<p class="furn-red" style="    text-align: center;
    font-size: 1.8em;">Enjoy 20% OFF and Free Local Delivery*</p>

<p style="font-size: 1.3em;
    line-height: 130%;">
Take advantage of our Presidents' Day sale. We are offering our 100% “Made in Italy” collection with exclusive deals for a limited time only.
Furnish your home with all the creativity of Italian style. Living, dining and beds in a perfectly elegant way.
We look forward to seeing you soon in Furnitalia!
</p>

<p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
</p>

*Complete conditions in store. This offer runs until 26th February 2017.

<p style="text-align: center">
    <a href="<?php print base_path();?>natuzzi-italia/living" class="request"  title="Shop Natuzzi Italia"
       style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
        Shop Natuzzi Italia
    </a>
</p>
