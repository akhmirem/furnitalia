<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/leather-upgrade/2019";

if ($is_desktop) {
  $img_path = 'Stressless-P1-promo-img.jpg';
} else {
  $img_path = 'Stressless-P1-promo-img.jpg';
}
$alt = "Stressless Free Leather Upgrade Promotion";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Lato|Abril+Fatface" rel="stylesheet">
<style>

  article {
    color: #444;
    font-family: "Lato", Helvetica, Arial, sans-serif;
    padding-left: 4em;
  }

  .sale-caption-section {
    max-width: 1024px;
    color: #6f2e3e;
  }

  .sale-caption-section h2 {
    line-height: 1;
    float: left;
  }

  .sale-caption-section h2 .sale-title {
    font-size: 1.25em;
    color: #000;
  }

  .sale-caption-section img {
    height: 110px;
    margin-left: 50px;
    float: right;
    padding-top: 1em;
  }

  .sale-caption-section .subcation {
    clear: both;
    margin-bottom: 2em;
    font-size: 1.45em;
  }

  .promo-description-text {
    display: flex;
    max-width: 1024px;
  }

  .promo-description-text p {
    border-left: 1px solid #ccc;
    padding-left: 1em;
    padding-right: 1em;
  }
  .promo-description-text p:first-child {
    border: none;
    padding-left: 0;
  }

  .promo-description-container a.promo-cta {
    padding: 17px 20px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    margin: 10px auto;
    margin-right: 1em;
    border-radius: 7px;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
  }

  p {
    font-size: 1.5em;
    line-height: 150%
  }

  .bold {
    font-weight: bold;
  }

  .promo-hero-img {
    width: 1024px;
    position: relative;
  }

  .promo-hero-img img {
  }

  .promo-hero-img .promo-img-caption {
    position: absolute;
    top: .5em;
    right: 0;
    background: rgba(245, 247, 236, 0.9);
    width: 23em;
    padding: 1em 0 1em 2em;
    font-size: 1.5em;
    line-height: 130%;
    font-style:italic;
  }

  .promo-offer-details img {
    max-width:1024px;
  }

  <?php if(!$is_desktop): ?>

  article {
    padding: 0 1em;
  }

  .sale-caption-section {
    display: grid;
    grid-template-rows: auto auto;
    grid-template-columns: auto auto;
    background: white;
  }

  .sale-caption-section h2 {
    grid-row: 1/2;
    grid-column: 1/3;
    padding-left: 0.5em;
  }

  .sale-caption-section img {
    height: 5em;
    margin: 0;
    padding: 0;
    grid-column: 2/3;
    align-self: center;
  }

  .sale-caption-section .subcation {
    margin: 0;
    grid-column: 1;
    grid-row: 2;
    padding: 0 0 1em .5em;
  }

  .promo-hero-img {
    width: auto;
  }

  #requestFormContainer {
    margin-left: 0;
    padding-right: 0;
  }

  .promo-hero-img img, 
  .promo-offer-details img {
    width: auto;
    max-width: 100%;
  }

  .promo-hero-img .promo-img-caption {
    position: static;
    width: auto;
    padding: 1em;
    font-style: italic;
  }

  .promo-description-text {
    margin-bottom: 1em;
    flex-wrap: wrap;
  }

  div.promo-description-container a.request {
    width: 100%;
  }

  .promo-description-text p {
    border: none;
    padding: 0;
  }


  #requestFormContainer h3.furn-red {
    margin-top: 1em !important;
  }

  #requestFormContainer form .form-control {
    width: 90%;
    padding: .7em;
    font-size: 1.2em;
  }

  #requestFormContainer form input.form-submit {
    background-color: #ed8223;
    color: #fff;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 18px;
    line-height: 30px;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border: 0;
    text-shadow: #C17C3A 0 -1px 0;
    padding: .3em 1em;
  }

  <?php endif;?>

</style>

<article>

  <div class="sale-caption-section clearfix">
    <h2 class="stressless-orange">
          <span style="font-size: 1.8em; <?php print (!$is_desktop ? 'font-size:1em;' : ''); ?>">
             STEP UP TO A
          <br/>
          <span class="sale-title" style="">FREE LEATHER UPGRADE</span> </span>
    </h2>
    <img src="<?php print $promo_dir . '/Stressless-icon.jpg'; ?>" alt="Stressless logo" class="stressless-logo"/>
    <div class="subcation">Go from fabric to leather or to a higher grade of leather and save big on any Stressless®
      seating.
    </div>

  </div>

  <div class="promo-hero-img">
    <img src="<?php print $promo_dir . '/' . $img_path; ?>" alt="<?php print $alt; ?>"/>
    <div class="promo-img-caption">
      The moment you sit in a Stressless, you feel
      why people are drawn to its comfort.
      The difference is our patented Plus™,
      Glide™, LegComfort™ and BalanceAdapt™
      comfort technologies. The result is
      Norwegian-crafted recliners, office chairs,
      sofas and dining chairs that possess the
      special gift of bringing people together.
    </div>
  </div>


  <div class="promo-offer-details">
    <?php if ($is_desktop): ?>
      <img src="<?php print $promo_dir . '/Promo-Offer-Details.png'; ?>"
         alt="MULTIPLE OFFERS. Valid until March 11 only."/>
    <?php else: ?>
      <img src="<?php print $promo_dir . '/Promo-Offer-Details-Mobile.jpg'; ?>"
         alt="MULTIPLE OFFERS. Valid until March 11 only."/>
    <?php endif; ?>
    
  </div>

  <div class="promo-create-your-perfect-stressles">
    <?php if ($is_desktop): ?>
      <img src="<?php print $promo_dir . '/create-your-perfect-stressless.png'; ?>"
         alt="Create Your Perfect Stressless."/>
    <?php else: ?>
      <img src="<?php print $promo_dir . '/create-your-perfect-stressless-mobile.jpg'; ?>"
         alt="Create Your Perfect Stressless."/>
    <?php endif; ?>
  </div>


  <div class="promo-description-container">
    <div class="promo-description-text">
      <p>
        If you’ve been waiting for the perfect opportunity to grace your home with the most comfortable,
        well-crafted furniture in the world, now’s the perfect time to act. Because right now, you can get a
        <em class="furn-red bold">FREE leather upgrade</em>, going from fabric to leather or from one grade of
        leather to a higher grade at no cost.
      </p>

      <p>
        Or, you can <em class="furn-red bold">save $300</em> on a Stressless Reno recliner and ottoman, LegComfort™
        recliner or office chair in all Paloma leather colors. Plus, you can extend a Stressless dining table
        for FREE! Buy a table with six Stressless dining chairs and upgrade to an integrated leaf table or two
        leaf inserts for FREE. These offers end March 11. Step up now!
      </p>

    </div>

    <a href="<?php print base_path(); ?>ekornes-stressless-recliners-sofas" class="request promo-cta"
       title="Browse Stressless" style="">
      Browse Stressless
    </a>

  </div>

  <?php print render($content); ?>

</article>


<script>

  (function ($) {


    $(function () {
      //handleVideoJSPlugin();
    });

  })(jQuery);
</script>


