<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/renovation-sale-summer2018";

if ($is_desktop) {
    $img_path = 'renovation-sale-desktop-header.jpg';
}
else {
    $img_path = 'renovation-sale-mobile-header.jpg';
}
$alt = "Renovation Sale! Up to 60% OFF entire inventory!";
$video_media_base = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<link href="https://vjs.zencdn.net/5.19.1/video-js.css" rel="stylesheet">
<style>
    .video-js .vjs-big-play-button {
        left: 10.5em;
        top: 6em;
    }
    <?php if(!$is_desktop): ?>
    .video-js .vjs-big-play-button {
        left: 4em;
        top: 2em;
    }
    <?php endif;?>

    .video-js.vjs-fluid {
        margin-top:.7rem;
    }

    .bold {
        font-weight: bold;
    }

    ul.category-list {
        display: flex;
        flex-wrap: wrap;
        list-style-type: none;
        align-items: center;
        justify-content: flex-start;
        clear: both;
    }
    ul.category-list li.category-item {
        width: 30%;
        padding: .7em 0;
    }

    <?php if(!$is_desktop): ?>
    ul.category-list li.category-item {
        width: 50%;
    }
    article a {
        color:#251f23;
        border-bottom: 1px dotted;
    }
    article a:visited { color: #656063}
    <?php endif;?>

    ul.category-list li.category-item a.category-name {
        font-size: 1.5em;
        text-decoration: none;
        border-bottom: 1px dotted;
        font-family: 'Source Sans Pro', sans-serif;
        font-weight: 400;
    }

    section.store-locations {
        clear: both;
    }
    section.store-locations div.location {
        text-align: center;
        margin: 1em 0;
        font-size: 1.5em;
        font-family: 'Source Sans Pro', sans-serif;
        font-weight: 400;
        line-height: 1.3;
    }
    section.store-locations div.location h3 {
        margin-bottom: .5em;
        font-size: 1.5em;
    }
    section.store-locations div.location .store-address {
        font-size: 1.3em;
    }

</style>

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; '>

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 1.3;
    font-size: 5em;
    color: #cd431f;
    margin: 0 0 .5em 0;
    padding: 0;
    <?php print (!$is_desktop ? 'font-size:3em;line-height:130%;' : ''); ?>"><span style="">RENOVATION</span> SALE!</h2>

    <!--<div style="    font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 2.5em;
    padding-bottom: 1em;
    <?php /*print (!$is_desktop ? 'line-height: 100%;margin-top: .5em;' : ''); */?>">20% - 60% OFF SELECT FLOOR MODELS.</div>-->

    <img src="<?php print $promo_dir . '/discount/'. $img_path; ?>" alt="<?php print $alt;?>" />


    <video id="video_2" class="video-js vjs-fluid mid-video with-audio with-play control-audio"
           preload="auto" muted
           loop playsinline data-setup='{"fluid": true, "autoplay": false, "controls": true}'
           poster="<?php print $promo_dir; ?>/renovation-private-sale-mobile-banner.jpg">
        <source src="<?php print $promo_dir . '/Furnitalia_Renovation_Break_YT.mp4'; ?>"
                type='video/mp4'>
    </video>

    <ul class="category-list">
        <li class="category-item"><a href="/living" class="category-name">Living</a></li>
        <li class="category-item"><a href="/dining" class="category-name">Dining</a></li>
        <li class="category-item"><a href="/occasional" class="category-name">Occasional</a></li>
        <li class="category-item"><a href="/bedroom" class="category-name">Bedroom</a></li>
        <li class="category-item"><a href="/office" class="category-name">Office</a></li>
        <li class="category-item"><a href="/rugs-pillows" class="category-name">Rugs/Pillows</a></li>
        <li class="category-item"><a href="/accessories" class="category-name">Accessories</a></li>
        <li class="category-item"><a href="/lighting" class="category-name">Lighting</a></li>
    </ul>

    <!--<p class="furn-red" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;">For a limited time,</p>-->

    <p class="" style='font-family: "Source Sans Pro", sans-serif;
    font-size: 2em;
    line-height: 1.42858;
    color: #857874;
    font-weight: bold;
    letter-spacing: 1.2px;
    margin-bottom: .3em;
    margin-top:1.5em
    '>Register to get your <strong class="furn-red">exclusive additional discount:</strong></p>

    <!--<div class="renovation-sale-form-container">
        <img src="<?php print $promo_dir; ?>/webform-cover-img.jpg" />
        <?php print render($content); ?>
    </div> -->

    <div id="pants">
        <div id="pocket">
            <img src="<?php print $promo_dir; ?>/webform-scissors-icon.jpg" class="scissors-icon" />
            <div class="stitch-top bordered"></div>
            <div class="stitch-left bordered"></div>
            <div class="stitch-right bordered"></div>
            <div class="stitch-bottom bordered"></div>
            <div class="content clearfix">
                <?php print render($content); ?>
            </div>
        </div>
    </div>

    <p style="font-size:1.3em; line-height:150%">Furnitalia is your ultimate destination store for contemporary leather furniture and modern
        kitchen and bath cabinets in Sacramento and N. California. We offer a large selection of modern furniture
        collections from <a href="/brand/natuzzi-italia" class="brand-name">Natuzzi Italia</a>,
        <a href="/brand/alf" class="brand-name">Alf Italia</a>,
        <a href="/brand/bontempi" class="brand-name">Bontempi</a>, <a href="/brand/stressless-ekornes" class="brand-name">Stressless</a>,
        <a href="/brand/cattelan-italia" class="brand-name">Cattelan Italia</a>, and others.</p>

    <p style="font-size: 1.3em;
    line-height: 150%;">
        <span class="bold">FURNITALIA</span>, the original trendsetter for high-end modern Italian leather furniture
        and contemporary kitchen and bath design, has grown thanks to your exquisite taste and loyal patronage.
        The success we've enjoyed is pushing us to expand our showroom again, and this time, we'll be
        <span class="bold">RENOVATING</span> the entire building inside and out to create a more convenient and
        enjoyable shopping experience for our customers.
        <!--Furnitalia - Your Source for Contemporary European Design.-->
    </p>

    <h3 class="furn-red" style="text-align: center;
    text-transform: uppercase;
    font-size: 2.5em;
    padding: 0;
    line-height: 130%;">$2,000,000 Furniture Elimination!</h3>


    <section id="sale-details">
        <p style="font-size: 1.1em;
    line-height: 150%;">
            In order to accomplish our goal, nearly <span class="bold">$2,000,000</span> worth of the best name brand
            contemporary home furnishings <span class="bold">MUST BE SOLD</span> regardless of cost or loss! our
            <span class="bold">ENTIRE DESIGNER FURNITURE INVENTORY</span> is up for <span class="bold">IMMEDIATE
                SALE</span> to make room for the construction
            which is about to begin! This is your only chance to buy Modern Leather Sofas, Sectionals, Recliners,
            Dining Sets, and Bedroom Furniture floor samples at up to 60% OFF!
        </p>

        <p style="font-size: 1.1em;
    line-height: 150%;">
            If you need just one piece or a whole house full of furniture...
        </p>

        <h3 class="furn-red" style="text-align: center;
    text-transform: uppercase;
    font-size: 1.8em;
    margin: 0;
    line-height: 130%;
    padding: 0 1.5em;">now is the time to buy!</h3>

        <img src="<?php print $promo_dir; ?>/furnitalia-brands.jpg" alt="Modern Furniture Brands on sale" style="margin:1em auto; display:block"/>

        <p style="font-size: 1.1em;
    line-height: 150%;">
            We encourage you to join us, and <span class="bold">BRING A FRIEND</span> to take advantage of the
            <span class="bold">BIGGEST SALE</span> in our 14-year history!
        </p>
    </section>

    <aside id="side-banner">
        <img src="<?php print $promo_dir; ?>/renovation-sale-landing-sidebar.jpg" />
    </aside>

    <br/>

    <section class="store-locations">
        <div class="location">
            <h3>Store Location</h3>
            <span class="bold store-address furn-red">5252 Auburn Boulevard <br/>
                Sacramento, California 95841</span> <br/>
            – close to the corner of Madison Ave & Auburn Blvd <br/>
            (next to the U-Haul Rentals on Auburn Blvd) <br/> <br/>

            Phone: <a href="tel:+19163329000" class="furn-red">(916) 332–9000</a> <br/>
            Hours: Monday – Sunday 10:00 am – 6:00 pm
        </div>

        <div id="map-sac" class="">&nbsp;</div>

    </section>

    <ul class="category-list">
        <li class="category-item"><a href="/living" class="category-name">Living</a></li>
        <li class="category-item"><a href="/dining" class="category-name">Dining</a></li>
        <li class="category-item"><a href="/occasional" class="category-name">Occasional</a></li>
        <li class="category-item"><a href="/bedroom" class="category-name">Bedroom</a></li>
        <li class="category-item"><a href="/office" class="category-name">Office</a></li>
        <li class="category-item"><a href="/rugs-pillows" class="category-name">Rugs/Pillows</a></li>
        <li class="category-item"><a href="/accessories" class="category-name">Accessories</a></li>
        <li class="category-item"><a href="/lighting" class="category-name">Lighting</a></li>
    </ul>



    <!--<p style="text-align: center">
        <a href="<?php /*print base_path();*/?>collections" class="request"  title="Browse collection"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Browse collection
        </a>
    </p>-->



</article>

<script src="<?php print $video_media_base; ?>/js/videojs.min.js"></script>
<script>

    (function($) {


        $(function () {
            //handleVideoJSPlugin();

            $.each($(".video-js"), function(o, t) {
                var i = videojs(this.id);
                i.ready(function() {
                    setTimeout(function(){
                        i.play();
                    }, 5000);
                });
            });
        });

    })(jQuery);
</script>


<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyB1-So6UFlqJuy0Byg833vwrPgPX1Qc9gc&sensor=false&extension=.js'></script>
<script>
    google.maps.event.addDomListener(window, 'load', InitMaps());
    var map;

    function InitMaps() {
        var sac_loc = [
            ['Furnitalia - Sacramento', 'undefined', '916-332-9000', 'info@furnitalia.com', 'https://www.furnitalia.com', 38.6645294, -121.3420888, 'https://mapkit.io/images/legacy/default.png']
        ];
        var map_center_sac = [38.660448,-121.345683];
        showMap(sac_loc, map_center_sac, 'map-sac', 13);

    }

    function showMap(locations, center, containerId, zoom) {
        var mapOptions = {
            center: new google.maps.LatLng(center[0], center[1]),
            zoom: zoom,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
            },
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            panControl: true,
            streetViewControl: false,
            draggable : true,
            overviewMapControl: false,
            overviewMapControlOptions: {
                opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        var mapElement = document.getElementById(containerId);
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker;
        for (i = 0; i < locations.length; i++) {
            if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
            if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
            if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
            if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
            if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
            if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
            marker = new google.maps.Marker({
                icon: markericon,
                position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                map: map,
                title: locations[i][0],
                desc: description,
                tel: telephone,
                email: email,
                web: web
            });
            link = '';
        }
    }
</script>

<style>
    #map-sac  {
        height:20em;
        width:100%;
    }
    .gm-style-iw * {
        display: block;
        width: 100%;
    }
    .gm-style-iw h4, .gm-style-iw p {
        margin: 0;
        padding: 0;
    }
    .gm-style-iw a {
        color: #4272db;
    }
</style>
