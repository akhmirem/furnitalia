<?php 

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/renovation-sale-summer2018";

if ($is_desktop) {
    $img_path = 'renovation-sale-landing-page-desktop.jpg';
}
else {
    $img_path = 'renovation-sale-landing-page-mobile.jpg';
}
$alt = "Renovation Sale! 20% - 60% OFF select floor models!";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<style>

    <?php if(!$is_desktop): ?>

    <?php endif;?>

    .bold {
        font-weight: bold;
    }

</style>

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; '>

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 1.3;
    font-size: 4em;
    color: #cd431f;
    margin: 0 0 .7em 0;
    padding: 0;
    <?php print (!$is_desktop ? 'font-size:3em;line-height:130%;' : ''); ?>"><span style="font-size: 1.8em; <?php print (!$is_desktop ? 'font-size:1.4em;' : ''); ?>">RENOVATION</span> <br/> PRIVATE SALE!</h2>

    <!--<div style="    font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 2.5em;
    padding-bottom: 1em;
    <?php /*print (!$is_desktop ? 'line-height: 100%;margin-top: .5em;' : ''); */?>">20% - 60% OFF SELECT FLOOR MODELS.</div>-->

    <img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>" />



    <!--<p class="furn-red" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;">For a limited time,</p>-->

    <p class="" style='    font-family: "Source Sans Pro", sans-serif;
    font-size: 2.2em;
    line-height: 1.42858;
    color: #857874;
    text-align: center;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: 1.5px;
    '>Register to win up to <strong class="furn-red">$2,500</strong> worth of  <strong class="furn-red">free furniture</strong></p>

    <!--<div class="renovation-sale-form-container">
        <img src="<?php print $promo_dir; ?>/webform-cover-img.jpg" />
        <?php print render($content); ?>
    </div> -->

    <div id="pants">
        <div id="pocket">
            <img src="<?php print $promo_dir; ?>/webform-scissors-icon.jpg" class="scissors-icon" />
            <div class="stitch-top bordered"></div>
            <div class="stitch-left bordered"></div>
            <div class="stitch-right bordered"></div>
            <div class="stitch-bottom bordered"></div>
            <div class="content clearfix">
                <img src="<?php print $promo_dir; ?>/webform-caption-image.png" class="webform-caption-img"/>
                <?php print render($content); ?>
            </div>
        </div>
    </div>

    <p style="font-size:1.3em; line-height:150%">Furnitalia showroom is your ultimate destination for modern and contemporary leather furniture in Sacramento and Northern California. We offer a large selection of modern furniture collections from Natuzzi Italia, Alf Italia, Bontempi, Stressless, Cattelan, and others.</p>

    <p style="font-size: 1.3em;
    line-height: 150%;">
        <span class="bold">FURNITALIA</span>, the original trendsetter for high-end contemporary Italian leather
        furniture, has grown thanks to your exquisite taste and loyal patronage. The success we've enjoyed is pushing
        us to expand our showroom again, and this time, we'll be <span class="bold">RENOVATING</span> the entire
        building inside and out to create a more convenient and enjoyable shopping experience for our customers.
        <!--Furnitalia - Your Source for Contemporary European Design.-->
    </p>

    <h3 class="furn-red" style="text-align: center;
    text-transform: uppercase;
    font-size: 2em;
    padding: 0;
    line-height: 130%;">$2,000,000 Furniture Elimination!</h3>


    <section id="sale-details">
        <p style="font-size: 1.1em;
    line-height: 150%;">
            In order to accomplish our goal, over <span class="bold">$2,000,000</span> worth of top-name brand home
            furnishings <span class="bold">MUST BE SOLD</span> regardless of cost or loss! our <span class="bold">ENTIRE
        INVENTORY</span> is up for <span class="bold">IMMEDIATE ELIMINATION</span> to make room for the construction
            which is about to begin!
        </p>

        <h3 class="furn-red" style="text-align: center;
    text-transform: uppercase;
    font-size: 1.7em;
    padding: 0;
    margin: 0;
    line-height: 130%;">private sale 4 days only!</h3>

        <p style="font-size: 1.1em;
    line-height: 150%;">
            As a way of saying thank you, we are extending you this <span class="bold">PERSONAL INVITATION</span> to a
            <span class="bold">4-DAY SALE</span> for <span class="bold">PREFERRED CUSTOMERS ONLY:</span>
        </p>

        <p class="furn-red" style="    text-align: center;
    font-size: 1.3em; line-height: 1.3">
            FRI: JUNE 22 - 10AM-6PM <br/>
            SAT: JUNE 23 - 10AM-6PM <br/>
            SUN: JUNE 24 - 10AM-6PM <br/>
            MON: JUNE 25 - 10AM-6PM
        </p>

        <p style="font-size: 1.1em;
    line-height: 150%;">
            During these four days, you will be entitled to <span class="bold">SPECIAL PREFERRED PRICING</span> on the
            <span class="bold">FIRST</span> and <span class="bold">BIGGEST SELECTION</span> available. <br/>
            If you need just one piece or a whole house full of furniture, <span class="bold">NOW IS THE TIME TO BUY!</span>
            <br/>
            We encourage you to join us, and <span class="bold">BRING A FRIEND</span> to take advantage of the
            <span class="bold">BIGGEST SALE</span> in our 14-year history!
        </p>
    </section>

    <aside id="side-banner">
        <img src="<?php print $promo_dir; ?>/renovation-sale-landing-sidebar.jpg" />
    </aside>

    <br/>



    <!--<p style="text-align: center">
        <a href="<?php /*print base_path();*/?>collections" class="request"  title="Browse collection"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Browse collection
        </a>
    </p>-->



</article>


<script>

    (function($) {


        $(function () {
            //handleVideoJSPlugin();
        });

    })(jQuery);
</script>


