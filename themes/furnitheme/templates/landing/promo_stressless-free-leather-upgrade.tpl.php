<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/leather-upgrade";

if ($is_desktop) {
    $img_path = 'stressless-leather-upgarde-desktop-landing.png';
}
else {
    $img_path = 'stressless-leather-upgarde-mobile-landing.png';
}

$video_path = 'stressless-leather-upgrade-sale-video.mp4';

$alt = "Find Multiple Ways to Save on Stressless Now.";
$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<link href="https://vjs.zencdn.net/5.19.1/video-js.css" rel="stylesheet">

<style>
    #promo-article .promo-p {
        font-size: 1.3em;
        line-height: 150%;
    }
</style>
<!--<link href="https://fonts.googleapis.com/css?" rel="stylesheet">-->

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; ' id="promo-article">

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 1.2em;
    font-size: 2.7em;color:#cd431f;     margin: 0;
    padding: 0 0 0.5em 0;"><span style="color:black">GET MORE LUXURY.</span> <span>PAY MUCH LESS.</span></h2>

    <div style="font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 2em;
    line-height: 130%;
    padding-bottom: 1em;">Receive up to <span style="color:#cd431f;">$600 off</span> Stressless&copy; during our <br/>
        FREE Leather Upgrade Event.
    </div>



<!--    <img src="--><?php //print $promo_dir . '/'. $img_path; ?><!--" alt="--><?php //print $alt;?><!--"/>-->

    <video id="video_2" class="video-js vjs-fluid mid-video with-audio with-play control-audio"
           preload="auto" muted controls
           loop playsinline data-setup='{"fluid": true,"autoplay": false}'
           poster="<?php print $promo_dir . '/' . $img_path; ?>">
        <source src="<?php print $promo_dir . '/' . $video_path; ?>"
                type='video/mp4'>
    </video>


    <p class="" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;color:#cd431f;">January 26 - March 12, 2018.</p>

    <!-- <p class="" style='font-family: "Signica","Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 16px;
    line-height: 1.42858;
    color: #857874;
    font-weight: bold;
    text-align: center;
    margin-top: -1em;
    margin-bottom: 2em;
    '>we've cut the cost, not the comfort</p> -->

    <p style="" class="promo-p">
        Until March 12 only, save up to $600 with a free leather upgrade on any Stressless or Ekornes seating.
        PLUS, receive $100 off fabric pricing and Paloma special pricing.
    </p>

    <p class="promo-p">
        Stressless comes in 4 different leather grades and over 50 different leather colors.
        Each grade is of the finest quality, purchased from selected tanneries all over the world.
    </p>

    <p style='font-family: "Signica","Helvetica Neue", Helvetica, Arial, sans-serif;
        font-family: "Signica","Helvetica Neue", Helvetica, Arial, sans-serif;
    color: #857874;
    font-weight: bold;
    text-align: center;
    font-size: 1.5em;
    '>
        SAVINGS UP TO $600!
    </p>
    <br/>

    <hr class="article-sep" />
    <div class="or" style="text-align: center;
    font-size: 2em;
    margin: 0.5em 0;
    color: #cd431f;
    letter-spacing: 3px;">OR</div>
    <hr class="article-sep" />

    <br/>

    <p class="promo-p">
        Save $300 on a luxurious Stressless Peace recliner and ottoman in all Paloma colors.
    </p>

    <p class="promo-p">
        Stressless Peace combines the classic look of a Stressless with some modern details.
        The uniquely shaped back perfectly cradles your spine, giving you perfect support in every position.
        Add to that our revolutionary Plus™-system and available LegComfort™-system for comfort beyond compare.
    </p>

    <?php if ($is_desktop) : ?>
        <img src="<?php print $promo_dir; ?>/stressless-peace-recliner-desktop.png" alt="Stressless Peace Recliners" />
    <?php else: ?>
        <img src="<?php print $promo_dir; ?>/stressless-peace-recliner-mobile.png" alt="Stressless Peace Recliners"/>
    <?php endif; ?>



    <p style="font-size: 1em;">Exclusions apply. See sales associate for complete details.</p>

    <br/>

    <p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
        Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
    </p>

    <p style="text-align: center">
        <a href="<?php print base_path();?>brand/stressless-ekornes" class="request"  title="Shop Stressless"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Shop Stressless
        </a>
    </p>

    <?php print render($content); ?>

</article>

<script src="<?php print $base_folder; ?>/js/videojs.min.js"></script>

<script>

    (function($) {

        var isMobile = false;
        <?php if(!$is_desktop): ?>
        isMobile = true;
        <?php endif; ?>

        handleVideoJSPlugin = function() {
            var o = $.Deferred()
                , e = function() {
                setTimeout(function() {
                    o.resolve()
                }, 1e3)
            };
            $.each($(".video-js"), function(o, t) {
                var i = videojs(this.id);
                i.ready(function() {
                    var s = $(this.el_);
                    if (s.data("bvPlayer", this),
                            s.hasClass("with-audio")) {
                        var a = $(".change_color_label").hasClass("text_white") ? "control-audio-white" : "";
                        s.append('<div class="control-audio ' + a + ' off"></div>')
                    }
                    s.hasClass("with-play") && (s.append('<div class="goplay"></div>'),
                        this.on("play", function() {
                            s.find(".goplay").fadeOut()
                        }),
                        this.on("pause", function() {
                            s.find(".goplay").fadeIn()
                        })),
                        s.on("click tap", function(o) {
                            var e = $(o.target);
                            e.hasClass("control-audio") ? (e.toggleClass("on off"),
                                i.muted(e.hasClass("off"))) : i.paused() ? i.play() : i.pause()
                        }),
                        $(window).on("scroll", function() {
                            var o = $(t).first().offset().top
                                , e = $(t).first().height();
                            isMobile || ($(window).scrollTop() > o - e / 2 && $(window).scrollTop() < o + e / 2 ? i.paused() && !$(i.el_).hasClass("video-mobile") && i.play() : i.pause())
                        }),
                    "undefined" != typeof _controlVideo && _controlVideo(s),
                    o || e()
                }, !1),
                    i.on("error", function() {
                        container.remove(),
                            e()
                    })
            })
        };

        $(function () {
            handleVideoJSPlugin();
        });

    })(jQuery);
</script>



