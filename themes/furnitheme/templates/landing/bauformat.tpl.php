
<?php 
    $images_dir = base_path() . "sites/all/themes/furnitheme/images/landing/bauformat";
    $bauformat_landing = base_path() . "sites/default/files/landing/bauformat";
    $files_dir = base_path() . "sites/default/files";
    $landing_dir_path = base_path() . "sites/all/themes/furnitheme/templates/landing";
?>
<style>
.bauformat-body-text {
    font-size: 20px;
    line-height: 26px;
    color: #000;
    font-family: 'ProximaNovaCondLight';
    text-align: center;
    width: 803px;
    margin: 0 auto;
}
</style>

<h4 style="font-size: 30px;
    font-family: 'ProximaNovaCondLight';
    line-height: 40px;
    position: relative;
    letter-spacing: 0.03em;
    text-transform: uppercase;
    margin: 0 0 0 30px;
    text-align:center"><img src="<?php print $bauformat_landing;?>/bauformat-logo.svg" alt="Bauformat" style="margin-left:17px"/> <span>Creating style and comfort</span></h4>
<?php //print render($category_preview); ?>

<p style="font-size: 22px;
    color: #000;
    font-family: 'ProximaNovaCondLight';
    margin: 20px 0 0 0;
    line-height: 30px;
    letter-spacing: 0.03em;
    text-align: center;
    text-transform: uppercase;
    font-weight: bold;">Modern AND STYLISH KITCHEN CABINETS MADE IN GERMANY FOR EVERY BUDGET</p>

<img src="<?php print $bauformat_landing;?>/4.jpg" alt="Bauformat - Modern Kitchens from Germany" style="margin-top: 15px; width:1200px"/>

<p class="bauformat-body-text"> The Bauformat range offers an individual solution to suit your budget for every floor plan and any room size. Modern European kitchen cabinets from factory direct.</p>

<p class="bauformat-body-text">
    Call us at <a href="tel:+19163329000">916-332-9000</a> or
    <a href="<?php print base_path();?>request/ajax/bauformat" class="request" id="request-info" title="Request Bauformat Product Information"
    style="padding: 17px 10px;
    background: #981b1e;
    display: block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: 370px;
    margin: 10px auto;">
        Request Bauformat Product Information
    </a>
</p>
<p class="bauformat-body-text">We can design and install the kitchen of your dreams.</p>

<br/>
<!-- GALLERY
============================================ -->
<h4 style="font-size: 36px;
    font-weight: bold;
    font-family: 'ProximaNovaCondLight';
    line-height: 40px;
    position: relative;
    letter-spacing: 0.03em;
    text-transform: uppercase;
    margin: 40px 0 15px 0;
    text-align:center"><span>Bauformat Kitchen Cabinet Collections</span></h4>


<?php //include_once $landing_dir_path . "/bauformat_slider/bauformat_slider.tpl.php" ?>
<?php include_once dirname(__FILE__) . "/../featured_pages/bauformat_slider/bauformat_slider.tpl.php" ?>
<!-- \GALLERY -->

<br/>
<!-- CATALOG
========================================== -->
<section class="catalogs">
    <h4 style="font-size: 36px;
        font-weight: bold;
        font-family: 'ProximaNovaCondLight';
        line-height: 40px;
        position: relative;
        letter-spacing: 0.03em;
        text-transform: uppercase;
        margin: 40px 0 15px 0;
        text-align:center"><span>Bauformat Kitchen Cabinets Catalog</span></h4>

    <div class="post">
        <img src="<?php print $files_dir; ?>/catalogs/Bauformat-Catalog-image.png" alt="View Bauformat Kitchen Catalog 2018-2019" />
        <div class="text clearfix">
            <p style="width: 12em;line-height: 130%;">Bauformat Kitchen Catalog 2018-2019</p>
            <a class="" href="https://view.publitas.com/p222-10363/bauformat-kitchen-catalogue-2018-2019"
               title="View Bauformat European Kitchen Catalog online"
               target="_blank" style="
                ">View online</a> <br/>
            <a href="<?php print $files_dir; ?>/catalogs/Bauformat-European-Kitchen-Cabinets-Catalogue-2018-2019.pdf" title="Download Bauformat European Kitchen Catalog">Download</a>
        </div>
    </div>
    <div class="post">
        <img src="<?php print $images_dir; ?>/bauformat-kitchen-brochure-2017.png" alt="Bauformat Europen Kitchen Brochure" />
        <div class="text clearfix">
            <p>Bauformat Kitchen Brochure</p>
            <a class="" href="https://view.publitas.com/p222-10363/bauformat-european-kitchen-brochure/"
               title="View Bauformat European Kitchen Brochure online"
               target="_blank" style="
                ">View online</a> <br/>
            <a href="<?php print $files_dir; ?>/catalogs/bauformat_brochure.pdf" title="Download Bauformat European Kitchen Brochure">Download</a>
        </div>
    </div>
</section>



<!--<a href="http://demo.conceptstudio.am/bauformat/wp-content/uploads/2015/09/Burger-Ku%CC%88chenimpressionen-2015.pdf" name="catalog" id="catalog" title="View Bauformat Catalog Online"
    style="display: block; text-align: center" target="_blank">
    <img src="<?php print $bauformat_landing;?>/bauformat-catalog-img.png" alt="Bauformat Catalog Online"/>
</a>-->
<!-- \CATALOG -->
