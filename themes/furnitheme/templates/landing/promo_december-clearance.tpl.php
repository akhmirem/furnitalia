<?php
$assets_path = base_path() . "sites/all/themes/furnitheme/lib/mightyslider";
$img_path = base_path() . 'sites/default/files/black-friday-2018';
$img_placeholder = base_path() . 'sites/default/files/lazyLoadImage.gif';

$promo_path = base_path().'sites/default/files/promo/christmas';

$items = array();
include_once(dirname($_SERVER["SCRIPT_FILENAME"]) . "/sites/default/files/black-friday-2018/items.php");

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}

?>

<!-- Styles -->
<link href="<?php print $assets_path; ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<!--<link href="<?php print $assets_path; ?>/assets/css/style.css" rel="stylesheet"> -->

<!--
<link href="<?php print $assets_path; ?>/assets/css/m-styles.min.css" rel="stylesheet">
<link href="<?php print $assets_path; ?>/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php print $assets_path; ?>/src/css/mightyslider.css" rel="stylesheet">
-->

<!--<div id="preloader">
    <div id="status">
        <div class="timer">Loading...</div>
    </div>
</div> -->


<style>
    /*#example {
        position: relative;
        overflow: hidden;
        margin-top: 61px;
        background: transparent;
    }

    #example .frame {
        position: relative;
        width: 100%;
        padding: 20px 0;
        padding-bottom: 50px;

        -webkit-perspective: 1000px;
        perspective: 1000px;
        -webkit-perspective-origin: 50% 50%;
        perspective-origin: 50% 50%;
    }

    #example .frame .slide_element {
        height: 100%;

        -webkit-transform-style: preserve-3d;
        transform-style: preserve-3d;
    }

    #example .frame .slide_element .slide {
        float: left;
        height: 100%;
        margin-right: 20px;
        opacity: 0.6;
        background: #000;
        z-index: 9;
        border-radius: 10px;

        -webkit-box-reflect: below 0 -webkit-gradient(linear, 0% 0%, 0% 100%, from(transparent), color-stop(0.9, transparent), to(rgba(255, 255, 255, 0.498039)));

        -webkit-transform: rotateY(45deg);
        transform: rotateY(45deg);
        -ms-transform: perspective(1000px) rotateY(25deg);

        -webkit-transition: -webkit-transform 1s cubic-bezier(0.190, 1.000, 0.220, 1.000);
        -webkit-transition-property: opacity, -webkit-transform;
        transition: transform 1s cubic-bezier(0.190, 1.000, 0.220, 1.000);
        transition-property: opacity, transform;
    }

    #example .frame .slide_element .slide.active {
        opacity: 1;
        z-index: 10;

        -webkit-transform: rotateY(0deg);
        transform: rotateY(0deg);
    }

    #example .frame .slide_element .slide.active ~ .slide {
        -webkit-transform: rotateY(-45deg);
        transform: rotateY(-45deg);
        -ms-transform: perspective(1000px) rotateY(-25deg);
    }

    #example .details {
        display: block;
        text-align: center;
        padding: 20px;
        padding-top: 0;
        opacity: 0;
        min-height: 114px;
        margin-top: -20px;
    }

    #example .details h3 {
        text-transform: uppercase;
        font-size: 32px;
        font-weight: 300;
        margin: 0;
    }

    #example .details h4#photographer {
        text-transform: uppercase;
        color: #7A7A7A;
    }

    #example .details #description {
        color: #3D3D3D;
    }

    #example .mSButtons.mSPrev {
        left: 25%;
        right: auto;
        bottom: 30px;
    }

    #example .mSButtons.mSNext {
        right: 25%;
        left: auto;
        bottom: 30px;
    }

    #example.isTouch .mSButtons {
        display: none;
    }

    #example .mSCaption {
        left: 20px;
        bottom: 40px;
        font-size: 18px;
        line-height: normal;
        font-weight: 300;
        color: #000;
        letter-spacing: -1px;
        white-space: nowrap;
        text-transform: uppercase;
        z-index: 1002;

        -webkit-transition: all 0.3s;
        transition: all 0.3s;
    }

    #example .mSCaption.showed {
        bottom: 20px;
        opacity: 1;
    } */

    /* Responsive */
    /*@media (max-width: 1200px) {
        #example .frame .slide_element .slide {
            margin-right: 0;
        }
    }

    @media (max-width: 979px) {
        #example .frame .slide_element .slide {
            margin-right: -10px;
        }
    }

    @media (max-width: 767px) {
        #example .frame .slide_element .slide {
            margin-right: -40px;
            -webkit-transform: scale(0.8) rotateY(45deg);
            transform: scale(0.8) rotateY(45deg);
            -ms-transform: perspective(1000px) scale(0.8) rotateY(25deg);
        }

        #example .frame .slide_element .slide.active {
            -webkit-transform: rotateY(0deg);
            transform: rotateY(0deg);
        }

        #example .frame .slide_element .slide.active ~ .slide {
            -webkit-transform: scale(0.8) rotateY(-45deg);
            transform: scale(0.8) rotateY(-45deg);
            -ms-transform: perspective(1000px) scale(0.8) rotateY(-25deg);
        }
    }

    @media (max-width: 480px) {
        #example .frame .slide_element .slide {
            margin-right: -40px;
            -webkit-transform: scale(0.7) rotateY(45deg);
            transform: scale(0.7) rotateY(45deg);
            -ms-transform: perspective(1000px) scale(0.7) rotateY(25deg);
        }

        #example .frame .slide_element .slide.active {
            -webkit-transform: rotateY(0deg);
            transform: rotateY(0deg);
        }

        #example .frame .slide_element .slide.active ~ .slide {
            -webkit-transform: scale(0.7) rotateY(-45deg);
            transform: scale(0.7) rotateY(-45deg);
            -ms-transform: perspective(1000px) scale(0.7) rotateY(-25deg);
        }
    }

    @media (max-width: 320px) {
        #example .frame .slide_element .slide {
            margin-right: -30px;
        }
    }*/
</style>

<link href="https://fonts.googleapis.com/css?family=Oswald:500,600|Lato:100,300|Lobster|Open+Sans:300,400,600,700" rel="stylesheet">
<style>
    .white-font {
        color: #ffffff;
    }
    .black-font { 
        color: #000;
    }
    .pink-font {
        color: #ef5063;
    }
    .italic-font {
        font-style:italic;
    }
    .sale-name {
        font-size:6.5em;
        font-family: Lobster,cursive;
    }
    .display-1 {
        font-size: 96px;
        font-size: 6rem;
        font-weight: 300;
    }
    .btn-important, .btn-critical, .btn-success {
        color: #FFFFFF;
        border: 2px solid #e56a54;
        background: #e56a54;
        background: #ef5063;
        border-color: #ef5063;
    }
    .display-4 {
        font-size: 48px;
        font-size: 3rem;
        font-weight: 300;
    }
    
    /*@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');*/
    /*@import url('https://fonts.googleapis.com/css?family=Lato:100,300');*/
    @-webkit-keyframes spinner {
        0%{-webkit-transform: rotate(0deg);}
        100%{-webkit-transform: rotate(360deg);}
    }
    @keyframes spinner {
        0%{transform: rotate(0deg);}
        100%{transform: rotate(360deg);}
    }
    body.page-promo-black-friday-sale {
        padding: 0;
        font-family: 'Open Sans', sans-serif;
        color: #25272B;
        text-rendering: optimizespeed;
        -webkit-font-smoothing: antialiased;
    }
/* ==== Preloader ==== */

#preloader {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background-color: #fff;
	z-index: 9999;
}

#status {
	width: 64px;
	height: 64px;
	position: absolute;
	left: 50%;
	top: 50%;
	background-repeat: no-repeat;
	background-position: center;
	margin: -32px 0 0 -32px;
}

#preloader .timer {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-ms-box-sizing: border-box;
	box-sizing: border-box;
	display: block;
	width: 64px;
	height: 64px;
	margin: auto;
	border-width: 5px;
	border-style: solid;
	border-color: #000000 transparent transparent transparent;
	border-radius: 32px;
	text-indent: -9999px;
	-webkit-animation: spinner 1s linear infinite;
	animation: spinner 1s linear infinite;
}
.featurette-divider2 {
	margin: 80px 0 0;
}

div.slideshow-row {
	padding: 2em;
}
#slideshow {
    background-color: #dadce2;
    background-image: linear-gradient(140deg, white, #dadce2);
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
}
div.cardContainer {
    position: relative;
    /*min-width: 300px;*/
    width:430px;
    /*width: 300px;
    height: 400px;
    min-height: 550px;*/
    /*margin: 4px;*/
    -webkit-perspective: 1000px;
            perspective: 1000px;
}
div.slide-card {
	display: inline-block;
	width: 100%;
	height: 100%;
    /*cursor: pointer;*/
	-moz-backface-visibility: hidden;
	-webkit-transform-style: preserve-3d;
		  transform-style: preserve-3d;
	-webkit-transform: translateZ(-100px);
		  transform: translateZ(-100px);
	transition: all 0.4s cubic-bezier(0.165, 0.84, 0.44, 1);
}
/*.slide-card:after {
  content: "";
  position: absolute;
  z-index: -1;
  width: 100%;
  height: 100%;
  border-radius: 5px;
  box-shadow: 0 14px 50px -4px rgba(0, 0, 0, 0.15);
  opacity: 0;
  transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1.4);
}*/
.slide-card:hover {
  -webkit-transform: translateZ(0px);
          transform: translateZ(0px);
}
.slide-card:hover:after {
  opacity: 1;
}
.slide-card .inside {
	 -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  /*position: absolute;
  min-height: 650px;*/
  width: 100%;
  height: 100%;
  border-radius: 5px;
  background-color: white;
}
.slide-card .card-info {
	padding: 16px;
}
.slide-card img {
  background-color: #dadce2;
  background-position: center;
  background-size: cover;
  border-radius: 5px 5px 0 0;
  width: 100%;
  /*height: 250px;*/
}
#slideshow .slide-title {
    font-family: "Oswald", sans-serif;
    text-transform: uppercase;
    color: #333333;
    font-size: 27px;
    font-weight: 500;
    letter-spacing: -0.2px;
    margin-bottom: 10px;
}
#slideshow .desc {
    font-weight: 400;
    color: #555;
    line-height: 22px;
    font-size: 1.25em;
}
#slideshow .slide-pricing {
    padding: 0.7em 0;
}
.price {
    font-size:1.5em;
    font-weight:bold;
    color: #333;
}
.price-crossed {
    font-weight:300;
    text-decoration:line-through;
    text-decoration-color: #787474;
}
.black-friday-price {
    text-transform: uppercase;
    color: #d32222;
}
.price-promo {
    font-weight:bold;
    color: #d32222;
}

.discount-label {
	padding:6px; 
	position:relative;
	float:left;
	-webkit-border-radius:0 4px 0 4px;
	-moz-border-radius:0 4px 0 4px;
	border-radius:0 4px 4px 0;

}
.discount-label:after { 
    right: 100%; 
    border: solid transparent; content: " "; 
    height: 0; 
    width: 0; 
    position: absolute;
    border-color: rgba(136, 183, 213, 0);
     
    border-width: 20px; 
    top: 50%; 
    margin-top: -20px;
} 
.discount-label:before {
  content: '';
  z-index: 2;
  position: absolute;
  top: 42%;
  right: 100%;
  width: 7px;
  height: 7px;
  opacity: .95;
  background: #ffffff;
  border-radius: 7px;
  -webkit-box-shadow: inset .5px 0 rgba(0, 0, 0, 0.6);
  box-shadow: inset .5px 0 rgba(0, 0, 0, 0.6);

}
.discount-label span {
	color:#ffffff;
	font-size:20px;
	text-align:center;
	font-family:"Raleway",Helvetica;
}

.red{ 
	background-color:#E80707; 
}
.red:after{ 
	border-right-color: #E80707;
  
}
<?php if ($is_desktop): ?>
.coupon-form {
    border: 5px dotted #9e9ea0;
    border-radius: 15px;
    padding: 1em 2em;
    display:block;
    margin: 0 auto;
    width: 790px;
}
<?php else: ?>
#requestFormContainer {
    margin:0;
}
<?php endif; ?>

#slideshow button {
    text-transform: uppercase;
    background: none;
    border: 1px solid #000;
    font-weight: 600;
    font-size: 1.5rem;
    font-family: 'Open Sans';
    -webkit-transition: all 0.2s;
    -o-transition: all 0.2s;
    transition: all 0.2s;
    position: relative;
    z-index: 2;
}
#slideshow button[disabled], #slideshow input[type=submit][disabled] {
    color: #d12028;
    background: #fff;
    border-color: #d12028;
    -webkit-transform: rotate(-15deg) scale(2) translateX(120%) translateY(-650%);
        -ms-transform: rotate(-15deg) scale(2) translateX(120%) translateY(-650%);
            transform: rotate(-15deg) scale(2) translateX(120%) translateY(-650%);
}

/* Responsive */
@media (max-width: 1310px) {
    div.cardContainer {
        width:350px;
    }
}
@media (max-width: 1100px) {
    div.cardContainer {
        width:300px;
    }
	#slideshow button[disabled], #slideshow input[type=submit][disabled] {
		-webkit-transform: rotate(-15deg) scale(2) translateX(80%) translateY(-650%);
		    -ms-transform: rotate(-15deg) scale(2) translateX(80%) translateY(-650%);
                transform: rotate(-15deg) scale(2) translateX(80%) translateY(-650%);
	}
    .sale-name {
        font-size:5em;
    }
}
@media (max-width: 760px) {
    div.cardContainer {
        width:350px;
    }
	#slideshow button[disabled], #slideshow input[type=submit][disabled] {
		-webkit-transform: rotate(-15deg) scale(2) translateX(100%) translateY(-650%);
		    -ms-transform: rotate(-15deg) scale(2) translateX(100%) translateY(-650%);
                transform: rotate(-15deg) scale(2) translateX(100%) translateY(-650%);
	}
    .sale-name {
        font-size:5em;
    }
}
@media (max-width: 480px) {
    div.cardContainer {
        width:400px;
        margin-top:1.5em;
    }
    div.slide-card {
        transform: none;
    }
}
</style>



    <div style="padding:35px 10px; max-height:600px; background-color: #000000; background:url(<?php print $promo_path;?>/sale-promo-background.jpg); background-repeat:repeat-y;background-size:cover" align="center">
    <!--    <h4 class="text-uppercase white-font">VIP EARLY ACCESS</h4>-->
    <h3 class="text-uppercase pink-font sale-name">December Clearance Sale</h3>
    <h1 class="display-1 black-font">UP TO 70% OFF*</h1>

    <div style="margin: 30px 0" align="center"><a href="#requestFormContainer" class="btn btn-important btn-lg">&nbsp;&nbsp;REQUEST QUOTE&nbsp;&nbsp;</a></div>
    <h4 class="text-uppercase black-font italic-font">Limited stock, get the best deals NOW!</h4>
</div>

<div class="row" style="padding-bottom: 0px">
    <h4 style="font-weight: bold; font-size: 3em; text-align:center; margin-top:2em">EXPLORE OUR CLEARANCE ITEMS</h4>
</div>

<?php if (false): ?>
<!--<div id="example" class="mightyslider_modern_skin black">
    <div class="frame" data-mightyslider="width: 1585, height: 500">
        <div class="slide_element">

            <?php foreach ($items as $item) : ?>
                <?php
                        $pricing = '<div class="msrp">MSRP: <span class="price-crossed">' . $item['msrp'] . '</span></div>
                                <div class="our-price">Our price: <span class="price-crossed">' . $item['regular'] . '</span></div>
                                <div class="black-friday-price">CLEARANCE: ' . $item['sale'] . '</div>
                                <div class="discount">' . $item['discount'] . ' OFF</div>';
                ?>
                <div class="slide"
                     data-mightyslider="cover: '<?php print $img_path . '/' . $item['image']; ?>',
                     title: '<?php print urlencode($item['title']); ?>',
                     photographer: '<?php print urlencode($pricing);?>',
                     description: ''">
                    <div class="desc">
                        <?php print $item['markup']; ?>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
        <a class="mSButtons mSPrev"></a>
        <a class="mSButtons mSNext"></a>
    </div>
    <div class="details">
        <h3 id="title"></h3>
        <h4 id="photographer"></h4>
        <p id="description"></p>
    </div>
</div> -->
<?php endif; ?>

<div id="slideshow">
			<!-- <div> -->
            <?php foreach ($items as $key => $item) : ?>
                <?php
                    $pricing = '
                        <div class="row">
                            <div class="msrp col-lg-3 col-xs-4">MSRP: <span class="price">' . $item['msrp'] . '</span></div>
                            <div class="col-lg-6 col-xs-8">
                                <div class="our-price">Sale: <span class="price price-crossed">' . $item['regular'] . '</span></div>
                                <div class="black-friday-price">CLEARANCE: <span class="price price-promo">' . $item['sale'] . '</span></div>
                            </div>
                            <div class="discount col-lg-3 col-xs-3"><div class="discount-label red"> <span>-' . $item['discount'] . ' </span></div></div>
                        </div>';
                        $item_image = $img_path . '/' . $item['image']; 
                ?>
				<?php //if ($key % 3 == 0) { print '</div><div class="row slideshow-row">'; } ?>
                <div class="cardContainer col-lg-4 col-md-4 col-sm-6 col-sx-12">
                    <div class="slide-card" id="slide-card-<?php print $key; ?>">
						<div class="inside">
                            <a class="fancybox" href="<?php print $item_image;?>" data-rel="lightcase:mycollection:slideshow<?php print $key;?>">
                                <!--href="#slide-card-descr-<?php print $key;?>"-->
                                <img src="<?php print $img_placeholder;?>" 
                                    data-src="<?php print $item_image;?>" 
                                    alt="<?php print $item['title']; ?>" 
                                    class="card-img lazy" />
                            </a>
							<div class="card-info">
								<div class="slide-title"><?php print $item['title']; ?></div>
								<div class="slide-pricing"><?php print $pricing; ?></div>
								<?php if ($item['status'] === 'Sold Out'): ?>
									<button disabled>Sold Out!</button>
								<?php endif; ?>
								<div class="desc">
									<?php print $item['markup']; ?>
								</div>
							</div>
                            <!--<div id="slide-card-descr-<?php print $key;?>" style="display:none">
                                <img src="<?php print $item_image;?>" 
                                    alt="<?php print $item['title']; ?>" class="card-img " style="width:100%; text-align:center;" />
                                <br/>
                                <div class="desc-text" style="font-weight: 400;
    color: #555;
    line-height: 22px;
    font-size: 1.25em;">
                                    <?php print $item['markup']; ?>
                                </div>
                            </div> -->
						</div>
					</div>
                </div>


            <?php endforeach; ?>
			<!-- </div> -->
</div>

<div class="featurette-divider2"></div>


<!-- /END THE FEATURETTES -->

<?php print render($content); ?>

<section id="requestFormSection"></section>

<!--<div class="row" style="padding: 30px 0; background-color: #d7d7d7;margin-top: 45px">
    <div class="col-sm-10 col-sm-offset-1">
        <h3 align="center" class="display-4">NOT READY TO BUY? EXTEND THIS OFFER</h3>
        <h3 align="center">Order free samples or schedule a free appointment by<br>
            Midnight 11/6 to lock in the offer through 11/26</h3>
        <div class="row" style="margin-top: 25px">
            <div class="col-sm-4" align="center" style="padding:0 10px 15px 10px">
                <a href="//www.smithandnoble.com/inhomestylist"><img class="img-responsive" src="<?php print $img_path;?>/free-design.jpg"></a>
                <p style="margin-top: 30px;"><a href="/interior-design" class="extension-cta" style="color: #000000;">FREE DESIGN &amp; MEASURE ➤</a></p>
            </div>
            <div class="col-sm-4" align="center" style="padding:0 10px 15px 10px">
                <a href="//www.smithandnoble.com/guaranteedfit"><img class="img-responsive" src="<?php print $img_path;?>/free-measure.jpg"></a>
                <p style="margin-top: 30px"><a href="/interior-design" class="extension-cta" style="color: #000000;">FREE MEASURE ➤</a></p>
            </div>
            <div class="col-sm-4" align="center" style="padding:0 10px 15px 10px">
                <a href="//www.smithandnoble.com/sample"><img class="img-responsive" src="<?php print $img_path;?>/free-samples.jpg"></a>
                <p style="margin-top: 30px"><a href="/contact" class="extension-cta" style="color: #000000;">FREE SAMPLES ➤</a></p>
            </div>
        </div>
    </div>
</div> -->


<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->


<!--<script src="<?php print $assets_path; ?>/assets/js/jquery.requestAnimationFrame.js"></script>
<script src="<?php print $assets_path; ?>/assets/js/jquery.mobile.just-touch.js"></script>
<script src="<?php print $assets_path; ?>/assets/js/jquery.migrate.min.js"></script>
<script src="<?php print $assets_path; ?>/assets/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="<?php print $assets_path; ?>/assets/js/jquery.scrollto-min.js"></script>
<script src="<?php print $assets_path; ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
-->

<!-- <script src="<?php print $assets_path; ?>/assets/js/retina.js" type="text/javascript"></script> --> 

<!--
<script src="<?php print $assets_path; ?>/assets/js/jquery.easing-1.3.pack.js" type="text/javascript"></script>
<script src="<?php print $assets_path; ?>/assets/js/jquery.simplr.smoothscroll.js" type="text/javascript"></script>
<script src="<?php print $assets_path; ?>/src/js/tweenlite.js" type="text/javascript"></script>
<script src="<?php print $assets_path; ?>/src/js/mightyslider.min.js" type="text/javascript"></script>
-->

<!-- <script src="<?php print $assets_path; ?>/assets/js/google-code-prettify/prettify.js"></script>
<script src="<?php print $assets_path; ?>/assets/js/google-code-prettify/lang-css.js"></script> -->
<!-- <script src="<?php print $assets_path; ?>/assets/js/custom.js" type="text/javascript"></script> -->
<script>
    /**
     * Get viewport/window size (width and height).
     *
     * @return {Object}
     */
    /*function getViewport() {
        var e = window,
            a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        }
    }*/


    /*jQuery(document).ready(function ($) {
        var $win = $(window),
            isTouch = !!('ontouchstart' in window),
            clickEvent = isTouch ? 'tap' : 'click',
            $preloader = $("#preloader"),
            $intro = $('#intro');

        $win.load(function () {
            $("#status", $preloader).fadeOut();
            $preloader.delay(350).fadeOut("slow", function () {
                $preloader.remove();
            });
        });

        // override
        clickEvent = 'click';

        (function () {
            function calculator(width) {
                var percent = '25%';
                if (width <= 480) {
                    percent = '60%';
                }
                else if (width <= 767) {
                    percent = '55%';
                }
                else {
                    percent = '50%';
                }
                return percent;
            };

            var $example = $('#example'),
                $frame = $('.frame', $example),
                $details = $('div.details', $example),
                $title = $('#title', $details),
                $photographer = $('#photographer', $details),
                $description = $('#description', $details),
                lastIndex = -1;

            $frame.mightySlider({
                speed: 1000,
                startAt: 1,
                autoScale: 1,
                easing: 'easeOutExpo',

                // Navigation options
                navigation: {
                    slideSize: calculator(getViewport().width),
                    keyboardNavBy: 'slides',
                    activateOn: clickEvent
                },

                // Dragging
                dragging: {
                    swingSpeed: 0.12,
                    onePage: 1
                },

                // Buttons
                buttons: !isTouch ? {
                    prev: $('a.mSPrev', $frame),
                    next: $('a.mSNext', $frame)
                } : {},

                // Cycling
                cycling: {
                    cycleBy: 'slides',
                    pauseOnHover: 1
                }
            }, {
                active: function (name, index) {
                    var slideOptions = this.slides[index].options;
                    var slideElement = this.slides[index].element;

                    console.log(slideOptions);
                    // console.log(layers);

                    if (lastIndex !== index)
                        $details.stop().animate({opacity: 0}, 500, function () {
                            $title.html(urldecode(slideOptions.title));
                            $photographer.html(urldecode(slideOptions.photographer));
                            $description.html(slideElement.innerHTML);
                            $details.animate({opacity: 1}, 500);
                        });

                    lastIndex = index;
                }
            });

            var API = $frame.data().mightySlider;

            $win.resize(function () {
                API.set({
                    navigation: {
                        slideSize: calculator(getViewport().width)
                    }
                });
            });

            function urldecode(str) {
                return decodeURIComponent((str+'').replace(/\+/g, '%20'));
            }
        })();
    }); */

document.addEventListener("DOMContentLoaded", function() {
  let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
  let active = false;

  const lazyLoad = function() {
    if (active === false) {
      active = true;

      setTimeout(function() {
        lazyImages.forEach(function(lazyImage) {
          if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
            lazyImage.src = lazyImage.dataset.src;
            //lazyImage.srcset = lazyImage.dataset.srcset;
            lazyImage.classList.remove("lazy");

            lazyImages = lazyImages.filter(function(image) {
              return image !== lazyImage;
            });

            if (lazyImages.length === 0) {
              document.removeEventListener("scroll", lazyLoad);
              window.removeEventListener("resize", lazyLoad);
              window.removeEventListener("orientationchange", lazyLoad);
            }
          }
        });

        active = false;
      }, 200);
    }
  };

  lazyLoad();

  document.addEventListener("scroll", lazyLoad);
  window.addEventListener("resize", lazyLoad);
  window.addEventListener("orientationchange", lazyLoad);
});
</script>

<?php if (!$is_desktop): ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
                $('a[data-rel^=lightcase]').lightcase();
    });
</script>
<?php endif; ?>
