<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$files_dir = base_path() . "sites/default/files";
$promo_dir = base_path() . "sites/default/files/promo/calligaris/taxes-on-us";

if ($is_desktop) {
  $img_path = 'US_taxes_on_us-HghRes.jpg';
} else {
  $img_path = 'US_taxes_on_us-HghRes.jpg';
}
$alt = "Taxes On Us + save 25% OFF on Calligaris";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Lato|Abril+Fatface" rel="stylesheet">
<style>

  article {
    color: #444;
    font-family: "Lato", Helvetica, Arial, sans-serif;
    padding-left: 4em;
  }

  article p {
    font-size: 1.75em;
  }
  p.p-smaller {
    font-size: 1.25em;
    font-style: italic;
  }

  .sale-caption-section {
    display: flex;
    color: #080c15;
    max-width: 1024px;
  }

  .sale-caption-section h2 {
    line-height: 1;
    float: left;
    flex: 1;
  }

  .sale-caption-section h2 .sale-title {
    font-size: 1.25em;
    color: #000;
  }
  .sale-caption-section span:last-of-type {
    align-self: center;
    margin-top: 1em;
  }

  .promo-description-text {
    max-width: 65ch;
  }

  .promo-description-container a.promo-cta {
    padding: 17px 20px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    margin: 10px auto;
    margin-right: 1em;
    border-radius: 7px;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
  }

  p {
    font-size: 1.5em;
    line-height: 150%
  }

  .bold {
    font-weight: bold;
  }

  .promo-hero-img {
    max-width: 1024px;
    position: relative;
  }

  .promo-hero-img img {
    width: 100%;
  }

  .promo-hero-img .promo-img-caption {
    position: absolute;
    top: .5em;
    right: 0;
    background: rgba(245, 247, 236, 0.9);
    width: 23em;
    padding: 1em 0 1em 2em;
    font-size: 1.5em;
    line-height: 130%;
    font-style:italic;
  }

  .promo-offer-details {
    font-size: 3em;
    line-height: 1.5;
    margin-top: 1em;
    margin-bottom: 1em;
    padding: 0 1em;
    max-width:22em;
  }
  .promo-description-container {
    margin-left:3em;
  }
  .promo-offer-details img {
    max-width:1024px;
  }

  #requestFormContainer {
    display: flex;
    justify-content: center;
  }

  <?php if(!$is_desktop): ?>

  article {
    padding: 0 1em;
  }
  article p {
    font-size: 1.5em;
  }

  .sale-caption-section {
    display: grid;
    grid-template-rows: auto auto;
    grid-template-columns: auto auto;
  }

  .sale-caption-section h2 {
    grid-row: 1/2;
    grid-column: 1/4;
    padding-left: 0.5em;
  }

  .sale-caption-section span:last-of-type {
    margin: 0;
    grid-column: 3;
  }

  .sale-caption-section img {

  }

  .promo-hero-img {
    width: auto;
  }

  #requestFormContainer {
    margin-left: 0;
    padding-right: 0;
  }

  .promo-offer-details {
    padding: 0;
    margin: .5em 0;
    font-size: 2em;
    max-width:100%;
  }

  .promo-hero-img img, 
  .promo-offer-details img {
    width: auto;
    max-width: 100%;
  }
  .promo-description-container {
    margin-left:0;
  }

  .promo-description-text {
    margin-bottom: 1em;
    flex-wrap: wrap;
  }

  .slide-wrapper {
    flex-wrap: wrap;
  }
  .slide-wrapper .catalog-text {
    flex-basis: initial!important;
    height: 380px!important;
  }

  div.promo-description-container a.request {
    width: 100%;
  }

  .promo-description-text p {
    border: none;
    padding: 0;
  }


  #requestFormContainer h3.furn-red {
    margin-top: 1em !important;
  }

  #requestFormContainer form .form-control {
    width: 90%;
    padding: .7em;
    font-size: 1.2em;
  }

  #requestFormContainer form input.form-submit {
    background-color: #ed8223;
    color: #fff;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 18px;
    line-height: 30px;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border: 0;
    text-shadow: #C17C3A 0 -1px 0;
    padding: .3em 1em;
  }

  <?php endif;?>

</style>

<article>

  <div class="sale-caption-section clearfix">
    <h2 class="stressless-orange">
          <span style="font-size: 1.8em; <?php print (!$is_desktop ? 'font-size:1em;' : ''); ?>">
             Take a Break this Tax Season!
          </span>
    </h2>
    <span><img src="<?php print $files_dir . '/brands/calligaris/calligaris-logo.svg'; ?>" alt="Calligaris" class="stressless-logo" style="width: 10em;"/></span>
  </div>

  <div class="promo-hero-img">
    <img src="<?php print $promo_dir . '/' . $img_path; ?>" alt="<?php print $alt; ?>"/>
  </div>


  <div class="promo-offer-details">
    We are offering you a <span class="furn-red bold">FREE tax</span> on any Calligaris purchase, through April 1, 2019.
    <br/>
    Plus, save up to
    <span class="furn-red bold">25%* OFF</span>
    on entire Calligaris collection.

  </div>



  <div class="promo-description-container">
    <div class="promo-description-text">
      <p>
        We invite you to come in and explore our 2019 Collection and save on new, Italian design for your home.

        We offer an extensive QuickShip program, with hundreds of options available at incredibly fast lead times - speak with a Design Consultant for more details on selection and availability. 
      </p>

      <p>
        Visit our Sacramento store to experience Calligaris for your dining room, living room, bedroom and more. Our Design Consultants are here to help you create the perfect space.
      </p>

    </div>

    <a href="<?php print base_path(); ?>brand/calligaris" class="request promo-cta"
       title="Shop Calligaris" style="">
      Shop Calligaris
    </a>

  </div>

  <p class="p-smaller">
    *Offer is valid through April 1st, 2019. Save up to 25% off MSRP. Tax will be offset by an additional discount. California State law requires tax to be included on every sale, so you will still see tax on your final invoice. This discount may not be combined with any other offers or incentives. Discount will not be applied to previous purchases. Please speak with a Design Consultant for more information.
  </p>



  <div class="slide-wrapper row imgleft halfimgleft text-left text-left btn-left btn-style-transparent-black slide-500" style="background-color: #ebc194;display: flex;">

    <div class="col-xs-12 col-sm-6 eq-height" style="height: 300px;flex: 1;">
      <div class="eq-height slide-image-bg" style="background-image: url(&quot;https://calligarisnyc.com/files/styles/slideshow-large/public/slideshow/cg_myhome_2019-b-5.jpg&quot;);height: 300px;background-position: center;background-size: auto 350px;background-repeat: no-repeat;">


      </div>
    </div>

    <div class="col-xs-12 col-sm-6 eq-height catalog-text" style="height: 300px;flex: 1;">
      <div class="container col-sm-11 slide-text text-left" style="
    padding: 20px;
">


        <div class="h2" style="
    font-family: 'sofia-pro',Helvetica,Arial,sans-serif;
    font-weight: 700;
    line-height: 1.1;
    margin-top: 22px;
    margin-bottom: 11px;
    font-size: 40px;
    color: #000;
">Browse Our Catalog</div>
        <p class="m-bot-15">
        </p><div class="lead" style="font-size: 20px;
    font-weight: 300;
    line-height: 1.4;
    color: #000;">Explore the My Home 2019 magazine, full of inspiration and ideas to furnish the beautiful, contemporary home of your dreams.</div>


        <!--        https://view.publitas.com/calligaris-nyc/calligaris-myhome-2019-->
        <p class="m-bot-15"><a href="https://view.publitas.com/p222-10363/calligaris-my-home-2019-magazine/" class="btn btn-default" target="_blank" style="
    text-transform: uppercase;
    background-color: #53565a !important;
    border-color: #53565a !important;
    color: #fff;
    letter-spacing: 2px;
    text-decoration: none;
    font-size: 13px !important;
    font-weight: bold !important;
    padding: 14px 30px;
    font-size: 13px;
    line-height: 1em;
    border-radius: 4px;
">BROWSE ONLINE</a></p>

      </div>
    </div>


  </div>


  <?php print render($content); ?>

</article>


<script>

  (function ($) {


    $(function () {
      //handleVideoJSPlugin();
    });

  })(jQuery);
</script>


