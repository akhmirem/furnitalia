<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/natuzzi-editions/may2018";

if ($is_desktop) {
    $img_path = 'furnitalia-renovation-sale-mar2018.mp4';
} else {
    $img_path = 'furnitalia-renovation-sale-mar2018.mp4';
}
$alt = "Renovation Sale! 20% - 60% OFF select floor models!";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";
$files_path = base_path() . 'sites/default/files/landing/bauformat/slider';
$logo_path = base_path() . 'sites/default/files/brands';

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<!--<link rel="stylesheet" href="--><?php //print $files_path; ?><!--/bootstrap.css" type="text/css">-->

<?php
/*drupal_add_css($files_path . "/bootstrap.css", array(
    'weight' => -10,
    'every_page' => FALSE,
));*/
?>

<style>
    .promo-item-title {
        font-size: 1.5em
    }

    table.pricing-table tbody tr td:nth-of-type(3) {
        color: #cd431f;
        font-weight: bold;
        font-size: 1.2em;
        padding-left: 1em;
    }

    .promo-img img {
        width: 250px;
        margin-top: 3em
    }

    .slick-dots {
        bottom: -10px
    }
</style>

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; '>

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 2em;
    font-size: 3em;color:#cd431f;
    margin: 0;
    padding: 0;
    <?php print (!$is_desktop ? 'font-size:3em;line-height:130%;' : ''); ?>">Natuzzi Editions Spring Promo!</h2>

    <div style="    font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 2.5em;
    padding-bottom: 1em;
    <?php print (!$is_desktop ? 'line-height: 100%;margin-top: .5em;' : ''); ?>">JUST IN. Promo Pricing, while supplies
        last.
    </div>

    <img src="<?= $logo_path ?>/natuzzi_editions_logo_165x54.png" class="center" alt="Natuzzi Editions"
         style="display:block; margin:1em auto"/>

    <p style="    font-size: 1.8em;
    font-family: 'Source Sans Pro', sans-serif;
    line-height: 130%">Looking for a great deal on quality leather sofa or set?
        Now is the perfect time to save on Natuzzi Editions! While supplies last, Furnitalia is offering promo
        prices on select Natuzzi Editions new arrivals. Visit Furnitalia before our spring offers end!</p>

    <h2 style="text-align: center;
    font-size: 2.5em;
    letter-spacing: 3px;
    font-family: sans-serif;">Showroom Specials</h2>

    <h3 style="" class="promo-item-title furn-red">B619 Saggezza - 15GG Phoenix White - Full Leather</h3>
    <div class="clearfix row">
        <div class="col-md-7">
            <table class="table table-striped pricing-table">
                <thead>
                <tr>
                    <th>Models</th>
                    <th>MSRP</th>
                    <th>Promo Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Sofa/Loveseat/Chair</td>
                    <td>$7,910</td>
                    <td>$4,099</td>
                </tr>
                </tbody>
            </table>
            <h3 style="" class="promo-item-title furn-red">B619 Saggezza - 15WGQ
                Oregon Dark Grey - Full Leather</h3>
                    <table class="table table-striped pricing-table">
                        <thead>
                        <tr>
                            <th>Models</th>
                            <th>MSRP</th>
                            <th>Promo Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Sofa/Loveseat/Chair</td>
                            <td>$7,910</td>
                            <td>$4,099</td>
                        </tr>
                        <tr>
                            <td>Sofa/Loveseat</td>
                            <td>$6,080</td>
                            <td>$3,099</td>
                        </tr>
                        <tr>
                            <td>Sofa/Chair</td>
                            <td>$4,910</td>
                            <td>$2,599</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="promo-img col-md-5">
                    <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none"
                        data-slick-lazy="true">
                        <li>
                            <img src="<?php print $promo_dir ?>/B619-side-phoenix-white.jpg"
                                 alt="B619 Saggezza Sofa by Natuzzi Editions" style="width:250px"/>
                        </li>
                        <li>
                            <img src="<?php print $promo_dir ?>/B619-front-phoenix-white.jpg"
                                 alt="B619 Saggezza Sofa by Natuzzi Editions" style="width:250px"/>
                        </li>
                    </ul>
                </div>
            </div>

            <h3 style="" class="promo-item-title furn-red">B757 Brivido - 15GN Phoenix Rose Beige - Split Leather</h3>
            <div class="clearfix row">
                <div class="col-md-7">
                    <table class="table table-striped pricing-table">
                        <thead>
                        <tr>
                            <th>Models</th>
                            <th>MSRP</th>
                            <th>Promo Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Power Sofa/Power Chair</td>
                            <td>$5,490</td>
                            <td>$3,099</td>
                        </tr>
                        <tr>
                            <td>Power Sofa/Stationary Loveseat</td>
                            <td>$5,740</td>
                            <td>$3,199</td>
                        </tr>
                        <tr>
                            <td>Power Sofa/Stationary Chair</td>
                            <td>$5,160</td>
                            <td>$2,999</td>
                        </tr>
                        <tr>
                            <td>Stationary Sofa/Stationary Loveseat</td>
                            <td>$4,910</td>
                            <td>$2,499</td>
                        </tr>
                        <tr>
                            <td>Stationary Sofa/Stationary Chair</td>
                            <td>$4,330</td>
                            <td>$2,299</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="promo-img col-md-5">
                    <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none"
                        data-slick-lazy="true">
                        <li><img src="<?php print $promo_dir ?>/B757-Brivido-scene.jpg"
                                 alt="B757 Brivido Sofa by Natuzzi Editions"/></li>
                        <li><img src="<?php print $promo_dir ?>/Natuzzi-Editions-B757-Sofa-800x600.jpg"
                                 alt="B757 Brivido Sofa by Natuzzi Editions"/></li>
                    </ul>
                </div>
            </div>


            <h3 style="" class="promo-item-title furn-red">B845 Sollievo - 15WQ Oregon Dark Grey - Split Leather</h3>
            <div class="clearfix row">
                <div class="col-md-7">
                    <table class="table table-striped pricing-table">
                        <thead>
                        <tr>
                            <th>Models</th>
                            <th>MSRP</th>
                            <th>Promo Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Sofa/Loveseat/Chair</td>
                            <td>$5,070</td>
                            <td>$2,699</td>
                        </tr>
                        <tr>
                            <td>Sofa/Loveseat</td>
                            <td>$3,740</td>
                            <td>$1,899</td>
                        </tr>
                        <tr>
                            <td>Sofa/Chair</td>
                            <td>$3,240</td>
                            <td>$1,799</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="promo-img col-md-5">
                    <img src="<?php print $promo_dir ?>/B845-Sollievo-front.jpg"
                         alt="B845 Sollievo Sofa by Natuzzi Editions"/>
                </div>
            </div>

            <br/><br/>

            <p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
                Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
            </p>

            <p style="text-align: center">
                <a href="<?php print base_path(); ?>natuzzi-editions" class="request"
                   title="More Natuzzi Editions Collections"
                   style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
                    More Natuzzi Editions Collections
                </a>
            </p>

            <?php print render($content); ?>

</article>


<script>

    (function ($) {

        var isMobile = false;
        <?php if(!$is_desktop): ?>
        isMobile = true;
        <?php endif; ?>

    })(jQuery);
</script>


