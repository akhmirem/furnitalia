<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}

$theme_path = drupal_get_path('theme', 'furnitheme');
$images_dir = $theme_path . "/images/landing/badea";
$bauformat_landing = base_path() . "sites/default/files/landing/bauformat";
$files_dir = base_path() . "sites/default/files";
$landing_dir_path = $theme_path . "/templates/landing";

?>
<style>
    .bauformat-body-text {
        font-size: 1.2em;
        line-height: 130%;
        color: #000;
        /*font-family: 'ProximaNovaCondLight';*/
        font-family: 'ProximaNovaCondRegular';
        /*text-align: center;*/
        /*width: 803px;*/
        margin: 0 auto;
    }
    .bauformat-body-text.hero {
        font-size: 1.4em;
        line-height: 130%;
        font-family: 'ProximaNovaCondRegular';
    }
    .bauformat-body-text.center {
        text-align: center;
    }

    .mobile .bauformat-body-text {
        width:100%;
    }
</style>

<link rel="stylesheet" href="<?php print $theme_path; ?>/css/bauformat.css"/>
<link rel="stylesheet" href="<?php print $theme_path; ?>/css/bauformat-badea.css"/>
<link rel="stylesheet" href="<?php print $theme_path; ?>/css/bauformat-badea-responsive.css"/>


<!-- GALLERY
============================================ -->


<?php //include_once dirname(__FILE__) . "/bauformat_slider/bauformat_slider.tpl.php" ?>
<div class="home-page">
    <div class="content">
        <div class="particular-pages <?php print $is_desktop ? '' : 'mobile'; ?>">
            <div class="particular-pages-cont">
                <h1 class="title" style="
    text-align: center;
    font-family: 'ProximaNovaCondLight';
    font-size: 2.1em;line-height: 130%;
">Badea - European Modern Bathroom Cabinets</h1>


                <div class="single-post-slider">
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-8_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-7_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-6_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-5_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-3_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-2_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-1_re-sized.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php print $images_dir; ?>/slider-9_re-sized.jpg" alt="">
                    </div>
                </div>

                <p class="bauformat-body-text hero">
                    Badea by Bauformat presents the top-quality modern bathroom cabinets, designed in Germany.
                    The Badea range includes items for all tastes: from furniture to washstands. The bathroom deserves
                    your special attention and should not simply be purpose-built. Badea bathroom cabinets bring
                    character and a certain special touch for you to feel comfortable and forget the hectic of
                    everyday life. With Badea, your bathroom truly becomes an oasis of pure relaxation. </p>
                <br/>
                <p class="bauformat-body-text">
                    The bathroom &mdash; whether elegant or modern &mdash; reveals something about its users. You can choose from
                    a large selection of shapes and colors to create your own very personal style. Professional advice
                    is available from Furnitalia's bathroom furniture experts, located in Sacramento, CA. </p>

                <br/>
                <p class="bauformat-body-text center">
                    Call us at <a href="tel:+19163329000">916-332-9000</a> or
                    <a href="<?php print base_path(); ?>request/ajax/badea" class="request" id="request-info"
                       title="Request Information about Modern European Bathroom Cabinets"
                       style="padding: 17px 10px;
    background: #981b1e;
    display: block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: 370px;
    margin: 10px auto;">
                        Request Info
                    </a>
                </p>
                <p class="bauformat-body-text center">We can design and install the best modern bathroom cabinets into your home.</p>

                <h2 style="    text-align: center;
    font-family: 'ProximaNovaCondLight';
    font-size: 2em;line-height: 130%">See Badea Modern Bathroom Cabinets</h2>

                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-080-Avola-Pinie-1_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 080 Avola Pinie</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-080-Avola-Pinie-1_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-080-Avola-Pinie-2_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 080 Avola Pinie</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-080-Avola-Pinie-2_re-sized.jpg" alt="">
                    </div>
                </div>


                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-088-Ash-Molina-grey-1_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 088 Ash Molina Grey</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-088-Ash-Molina-grey-1_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-088-Ash-Molina-grey-2_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 088 Ash Molina Grey</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-088-Ash-Molina-grey-2_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-088-Ash-Molina-grey-3_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 088 Ash Molina Grey</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-088-Ash-Molina-grey-3_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-Forum-1_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic Forum</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-Forum-1_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classic-Forum-2_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic Forum</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classic-Forum-2_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classis-092-Hacienda-black_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 092 Hacienda Black</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classis-092-Hacienda-black_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Classis-092-Hacienda-black-1_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 092 Hacienda Black</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Classis-092-Hacienda-black-1_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Fallada-035-Rainboy-ash-1_re-sized-780x380.jpg" alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 035 Rainbow Ash</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Fallada-035-Rainboy-ash-1_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Fallada-036-Yukon-Lime-tree-natural-1_re-sized-780x380.jpg"
                         alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 036 Yukon Lime Tree Natural</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Fallada-036-Yukon-Lime-tree-natural-1_re-sized.jpg"
                             alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Traditinal-087-Yukon-Lime-tree-natural-1_re-sized-780x380.jpg"
                         alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Traditional 087 Yukon Lime Tree Natural</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Traditinal-087-Yukon-Lime-tree-natural-1_re-sized.jpg"
                             alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Traditinal-087-Yukon-Lime-tree-natural-2_re-sized-780x380.jpg"
                         alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Traditional 087 Yukon Lime Tree Natural</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Traditinal-087-Yukon-Lime-tree-natural-2_re-sized.jpg"
                             alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Traditional-Gladstone-Oak-tobacco-1_re-sized-780x380.jpg"
                         alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Traditional Gladstone Oak Tobacco</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Traditional-Gladstone-Oak-tobacco-1_re-sized.jpg" alt="">
                    </div>
                </div>
                <div class="row-2">
                    <img src="<?php print $images_dir; ?>/Traditional-Gladstone-Oak-tobacco-2-1_re-sized-780x380.jpg"
                         alt="">
                    <div class="text-block">
						<span class="middle">
							<h2>Traditional Gladstone Oak Tobacco</h2>
							<p></p>
						</span>
                    </div>
                    <div class="fancyk">
                        <img src="<?php print $images_dir; ?>/Traditional-Gladstone-Oak-tobacco-2-1_re-sized.jpg"
                             alt="">
                    </div>
                </div>

                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-1_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 010 Lyon Walnut</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-2_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-2_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-3_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-3_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Classic 010 Lyon Walnut</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-1_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Classic-010-Lyon-Walnut-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Classic-Glass-068-Black-1_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Classic-Glass-068-Black-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Classic Glass 068 Black</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Classic-Glass-068-Black-2_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Classic-Glass-068-Black-2_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-1_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 050 High Gloss White</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-2_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-2_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-3_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-3_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 050 High Gloss White</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-5_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-5_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-5_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-5_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 050 High Gloss White</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-6_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-050-High-gloss-white-6_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-1_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 059 Brilliant Anthracite</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-2_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-2_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-3_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-3_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Fallada 059 Brilliant Anthracite</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-1_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Fallada-059-Brilliant-anthracite-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>
                </div>
                <div class="row-3">
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Traditional-Sky-062-Graphit-Brown-2_re-sized-380x380.jpg" alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Traditional-Sky-062-Graphit-Brown-2_re-sized-580x580.jpg" alt="">
                        </div>
                    </div>
                    <div class="text-block">
						<span class="middle">
							<h2>Traditional Sky 062 Graphit Brown</h2>
							<p></p>
						</span>
                    </div>
                    <div class="image">
                        <img src="<?php print $images_dir; ?>/Traditional-Sky-062-Graphit-Brown-1_re-sized-380x380.jpg"
                             alt="">
                        <div class="fancyk">
                            <img src="<?php print $images_dir; ?>/Traditional-Sky-062-Graphit-Brown-1_re-sized-580x580.jpg"
                                 alt="">
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


<!-- \GALLERY -->

<br/>
<!-- CATALOG
========================================== -->

<section class="catalogs">
    <h4 style="font-size: 36px;
        font-weight: bold;
        font-family: 'ProximaNovaCondLight';
        line-height: 40px;
        position: relative;
        letter-spacing: 0.03em;
        text-transform: uppercase;
        margin: 40px 0 15px 0;
        text-align:center"><span>Badea Catalog</span></h4>

    <div class="post">
        <img src="<?php print $files_dir; ?>/catalogs/Catalog-image-Badea.png"
             alt="Badea Modern Europen Bathroom Cabinets Catalog" style="width: 100%"/>
        <div class="text clearfix">
            <p>Badea European Bathroom Cabinets Catalog 2019</p>
            <a class="" href="https://view.publitas.com/p222-10363/badea-bathroom-cabinets-catalogue-2018-2019/"
               title="View Badea Modern European Bathroom Cabinets Catalog online"
               target="_blank" style="
                ">View online</a> <br/>
            <a href="<?php print $files_dir; ?>/catalogs/Badea-European-Bathroom-Cabinets-Catalog-2019.pdf" title="Download Badea Modern European Bathroom Cabinets Catalog">Download</a>
        </div>
    </div>
</section>

<script>
    (function ($) {

        $(window).load(function() {
            function imageRatio(img, width, height) {
                $(img).each(function() {
                    var curWidth = $(this).width();
                    var curHeight = $(this).height();
                    var curParentWidth = $(width).width();
                    var curParentHeight = $(height).height();

                    var boxRatio = curParentWidth / curParentHeight;
                    var imageRatio = curWidth / curHeight;

                    if (boxRatio >= imageRatio) {
                        $(this).width('100%');
                        $(this).height('auto');
                        curHeight = $(this).height();
                        var pos = Math.abs((curParentHeight - curHeight) / 2);
                        $(this).css({
                            left: 0,
                            top: -pos
                        });
                    } else {
                        $(this).width('auto');
                        $(this).height('100%');
                        curWidth = $(this).width();
                        var pos = Math.abs((curParentWidth - curWidth) / 2);
                        $(this).css({
                            left: -pos,
                            top: 0
                        });
                    }
                });
            }

            imageRatio(".single-post-slider img", ".single-post-slider", ".single-post-slider");
        });

        $(document).ready(function() {

            $('.single-post-slider:not(.slick-initialized)').each(function (i, elem) {
                $(elem).once(function () {
                    $(this).slick({
                        dots: true,
                        speed: 600,
                        autoplaySpeed: 2000,
                        arrows: false,
                        autoplay: true
                    });
                });
            });

            $(".row-4 img, .row-2 img, .row-3 img").on("click", function(){

                <?php if ($is_desktop): ?>

                $.fancybox({
                        content: $(this).parent().find(".fancyk").html()
                    });

                <?php else: ?>

                    lightcase.start({
                        href: $(this).parent().find(".fancyk img").attr('src').toString()
                    });

                <?php endif; ?>
            });



        });


    })(jQuery);
</script>

<style>

</style>