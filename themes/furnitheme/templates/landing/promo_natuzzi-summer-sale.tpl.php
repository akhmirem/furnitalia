
<?php
    if (arg(0) == 'promo' && arg(1) == 'natuzzi-summer-sale') {
        if (!is_null(arg(2)) && arg(2) == 'sofas') {
            require_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-sofas.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'sofa-beds') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-sofa-beds.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'armchairs') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-armchairs.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'beds') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-beds.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'chairs') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-chairs.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'tables') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-tables.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'tables') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-tables.tpl.php';
        } else if (!is_null(arg(2)) && arg(2) == 'furnishings-and-accessories') {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-furnishings-and-accessories.tpl.php';
        }
        else {
            include_once dirname(__FILE__) . '/../featured_pages/natuzzi_summer_sale/summer-sale-index.tpl.php';
        }
    }
?>
