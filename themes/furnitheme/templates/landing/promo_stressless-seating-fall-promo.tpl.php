<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/fall-seating-promo";

if ($is_desktop) {
    $promo_images = array(
        array('url' => 'Stressless-P4-Fall-seating-promo-landing-desktop2.png', 'alt' => 'Stressless Promo - Buy More, Earn up to $1,500 Credit. Or Save $500.'),
        array('url' => 'Stressless-P4-Fall-seating-promo-landing-desktop.png', 'alt' => 'Stressless Promo - Buy More, Earn up to $1,500 Credit. Or Save $500.'),
    );
}
else {
    $promo_images = array(
        array('url' => 'Stressless-P4-Fall-seating-promo-landing-mobile2.png', 'alt' => 'Stressless Promo - Buy More, Earn up to $1,500 Credit. Or Save $500.'),
        array('url' => 'Stressless-P4-Fall-seating-promo-landing-mobile.png', 'alt' => 'Stressless Promo - Buy More, Earn up to $1,500 Credit. Or Save $500.'),
    );
}

$video_path = 'furnitalia-stressless-p2-comfort-plus.mp4';

$alt = "Stressless Promo - Buy More, Earn up to $1,500 Credit. Or Save $500.";
$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<!--<link href="https://vjs.zencdn.net/5.19.1/video-js.css" rel="stylesheet">-->

<style>
    #promo-article .promo-p {
        font-size: 1.3em;
        line-height: 150%;
    }
    <?php if (false) :?>
    .video-js .vjs-big-play-button {
         left: 10.5em;
         top: 6em;
    }
    <?php if(!$is_desktop): ?>
        .video-js .vjs-big-play-button {
            left: 4em;
            top: 2em;
        }
    <?php endif;?>
    <?php endif; ?>

    span.or {
        display: block;
    }

    .slick-initialized .slick-slide {
        float: none;
        display: inline-block;
        vertical-align: middle;
    }

</style>

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; ' id="promo-article">

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 1.2em;
    font-size: 2.7em;color:#cd431f;     margin: 0;
    padding: 0 0 0.5em 0;">
        <span style="color:black">Buy More, Earn up to $1,500 Credit. </span> <span class="or"> - OR - </span><span>Save $500.</span>
    </h2>

    <div style="font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 2em;
    line-height: 130%;
    padding-bottom: 1em;">
        until October 22, 2018.
    </div>


<!--    <img src="--><?php //print $promo_dir . '/'. $img_path; ?><!--" alt="--><?php //print $alt;?><!--"/>-->


    <div style="text-align: center;">

        <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none" data-slick-lazy="true">
            <?php
            foreach($promo_images as $img) {
                print '<li><img data-lazy="' . $promo_dir . '/' . $img['url'] . '" alt="' . $img['alt'] . '" ></li>';
            }

            ?>
        </ul>
    </div>
    <br/>

    <?php if (false) :?>
    <!--<video id="video_2" class="video-js vjs-fluid mid-video with-audio with-play control-audio"
           preload="auto" muted controls
           loop playsinline data-setup='{"fluid": true,"autoplay": false,"controls": true}'
           poster="<?php /*print $promo_dir . '/' . $img_path; */?>">
        <source src="<?php /*print $promo_dir . '/' . $video_path; */?>"
                type='video/mp4'>
    </video>
    -->

    <!--<p class="" style="    text-align: center;
        font-size: 1.8em;line-height: 1.5em;color:#cd431f;">January 26 - March 12, 2018.</p> -->

    <?php endif; ?>



    <p style="" class="promo-p">
        Now is the perfect time to fill your home with the most comfortable seating in the world. Until October 22,
        receive credit up to $1,500 toward the purchase of additional Stressless or Ekornes seating with your
        purchase. Or, pay $500 less on all Signature base recliners and ottomans and LegComfort™
        recliners with the automatically integrated footrest system.
    </p>

    <p class="promo-p">
        No other seating is as comfortable as Stressless because ours is the only seating with Plus™-system,
        BalanceAdapt™-system and available LegComfort™-system comfort technologies.
    </p>

    <p class="promo-p">
        Our Plus™-system provides perfect neck and lumbar support at any sitting angle. Our BalanceAdapt™-system
        gives you a soft, gentle rocking motion as the seat angle automatically moves with the movements of
        your body. And our LegComfort™-system is an elegantly integrated footrest that automatically lifts and
        tucks away with the touch of a button.
    </p>

    <p style="font-size: 1em;">Exclusions apply. See sales associate for complete details.</p>

    <br/>

    <p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
        Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
    </p>

    <p style="text-align: center">
        <a href="<?php print base_path();?>brand/stressless-ekornes" class="request"  title="Shop Stressless"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Shop Stressless
        </a>
    </p>

    <?php print render($content); ?>

</article>

<?php if (false) :?>
    <script src="<?php print $base_folder; ?>/js/videojs.min.js"></script>
<?php endif; ?>




