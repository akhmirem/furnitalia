<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/comfort_plus";

if ($is_desktop) {
    $img_path =  'stressless-comfort-plus-desktop-landing.jpg';
}
else {
    $img_path =  'stressless-comfort-plus-mobile-landing.jpg';
}

$video_path = 'furnitalia-stressless-p2-comfort-plus.mp4';

$alt = "Select a Free accessory! Plus save $300.";
$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<link href="https://vjs.zencdn.net/5.19.1/video-js.css" rel="stylesheet">

<style>
    #promo-article .promo-p {
        font-size: 1.3em;
        line-height: 150%;
    }
    .video-js .vjs-big-play-button {
         left: 10.5em;
         top: 6em;
    }
    <?php if(!$is_desktop): ?>
        .video-js .vjs-big-play-button {
            left: 4em;
            top: 2em;
        }
    <?php endif;?>
</style>

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; ' id="promo-article">

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 1.2em;
    font-size: 2.7em;color:#cd431f;     margin: 0;
    padding: 0 0 0.5em 0;"><span style="color:black">SELECT A FREE ACCESSORY.</span> <span>PLUS SAVE $300 on Stressless Sunrise.</span></h2>

    <div style="font-family: 'Source Sans Pro', sans-serif;
    text-align: left;
    margin-top: -.2em;
    font-size: 2em;
    line-height: 130%;
    padding-bottom: 1em;">
        Until May 28 only, choose a <span style="font-weight: bold">FREE</span> accessory with your purchase of any Stressless recliner and ottoman, Stressless office chair, Stressless sofa, Stressless LegComfort™ recliner or Ekornes Collection seating.
        <br/><br/>
        <span style="font-weight: bold">PLUS</span>, get an additional $300 off a Stressless Sunrise recliner and ottoman (Classic or Signature base), a Stressless Sunrise recliner with LegComfort™ or a Stressless Sunrise office chair in all Paloma colors.
    </div>


    <!--    <img src="--><?php //print $promo_dir . '/'. $img_path; ?><!--" alt="--><?php //print $alt;?><!--"/>-->

    <video id="video_2" class="video-js vjs-fluid mid-video with-audio with-play control-audio"
           preload="auto" muted controls
           loop playsinline data-setup='{"fluid": true,"autoplay": false,"controls": true}'
           poster="<?php print $promo_dir . '/' . $img_path; ?>">
        <source src="<?php print $promo_dir . '/' . $video_path; ?>"
                type='video/mp4'>
    </video>


    <!--<p class="" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;color:#cd431f;">January 26 - March 12, 2018.</p> -->

    <p style="" class="promo-p">
        Stressless Sunrise is the perfect choice if you’re looking for that classic, Stressless look.
        All Sunrise models come with our Plus™ and Glide™-system comfort technologies. The Plus™-system automatically
        articulates with the contour of your body, giving your lower back and head perfect support as you recline
        or sit upright.The Glide™-system lets you adjusts to the most comfortable sitting position by using your
        body weight.
    </p>

    <p class="promo-p">
        The Stressless Sunrise with our Signature base has our BalanceAdapt™-system which allows the chair's back
        and seat to automatically adjust your seating position according to the movements of your body.
    </p>

    <p style="font-size: 1em;">Exclusions apply. See sales associate for complete details.</p>

    <br/>

    <p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
        Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
    </p>

    <p style="text-align: center">
        <a href="<?php print base_path();?>brand/stressless-ekornes" class="request"  title="Shop Stressless"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Shop Stressless
        </a>
    </p>

    <?php print render($content); ?>

</article>

<script src="<?php print $base_folder; ?>/js/videojs.min.js"></script>




