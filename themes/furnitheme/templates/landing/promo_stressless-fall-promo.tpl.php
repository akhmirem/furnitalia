<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/fall_promo";

if ($is_desktop) {
    $img_path = 'stressless_fall_promo_landing.jpg';
}
else {
    $img_path = 'stressless_fall_promo_mobile.jpg';
}
$alt = "Receive up to $1,500 credit toward Ekornes Stressless purchase!";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400" rel="stylesheet">

<article style="color:#444">

<h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 2em;
    font-size: 1.9em;">Two Ways to SAVE on Stressless.</h2>


        <img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>"/>


<p class="" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;color:#cd431f;">September 1 - October 16, 2017.</p>

<p style="font-size: 1.1em;
    line-height: 150%;">
    Looking for a great deal on Stressless? We’ll give you two. Right now, buy Stressless or Ekornes products and receive up to <strong>$1,500 credit</strong> toward more Stressless purchases. OR, <strong>save $500</strong> on all our Signature Base and LegComfort™ recliners. Our Signature Base has the soft, gentle, rocking motion of our BalanceAdapt™-system integrated into it. And our LegComfort™-system, an electronically activated footstool, is height-adjustable and remains hidden when not in use. Visit us by October 16 before these two offers end!
</p>
<br/>
<p style="font-size: 1em;
    font-style: italic">
    Here’s how it works:
</p>

<ul>
    <li>Purchase 2-3 seats and receive <strong>$750 credit</strong> to use towards additional Stressless or Ekornes purchases.</li>
    <li>Purchase 4-5 seats and receive <strong>$1,100 credit</strong> to use towards additional Stressless or Ekornes purchases.</li>
    <li>Purchase 6+ seats and receive <strong>$1,500 credit</strong> to use towards additional Stressless or Ekornes purchases.</li>
</ul>
<p style="font-size: 1em;">
    Credit can be used to purchase a selection of highly functional tables, ottomans and accessories or additional seats for extra family members. The choice is yours. It’s the perfect opportunity to create the ultimate home seating environment that you’ve always dreamed of.<br/>
</p>
<br/>
<p style="font-size: 1em;">Not in the market for multiple seats? You can still receive comfort for less by purchasing any Stressless Signature Base recliner and ottoman or Stressless LegComfort recliner and <strong>SAVE $500.</strong>
</p>

<img src="<?php print $promo_dir; ?>/Live_LC_575x465_path.jpg"/>
<img src="<?php print $promo_dir; ?>/LiveSignatur575x465.jpg"/>

<p style="font-size: 1em;">Innovatively designed from the inside out for ultimate comfort, Ekornes’ Stressless® line is the only furniture endorsed by the American Chiropractic Association (ACA) for proper head and lumbar support. Available in a vast array of colors and leather grades, Stressless home seating can be customized any way you want it.
</p>


<p style="font-size: 1em;">Exclusions apply. See sales associate for complete details.</p>

    <br/>

<p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
    Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
</p>

<p style="text-align: center">
    <a href="<?php print base_path();?>brand/stressless-ekornes" class="request"  title="Shop Stressless"
       style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
        Shop Stressless
    </a>
</p>

<?php print render($content); ?>

</article>
