<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$base = base_path() . "sites/all/themes/furnitheme/templates/featured_pages";
$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_presidents_day";
$promo_dir = base_path() . "sites/default/files/promo/natuzzi/qt_sale_2019";

?>


<!-- styles begin -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      crossorigin="anonymous">
<link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
      crossorigin="anonymous">
<link href="<?php print $base_folder; ?>/css/landingpage.css?v=1" rel="stylesheet" type="text/css" media="all"/>
<style>

  <?php //LANDING PAGE CSS OVERRIDES ARE HERE   ?>
  .columm-product-right h1,
  .figure-caption h2,
  .img-detail h3,
  .col-next-gif h2,
  .content-product-mobile h1,
  .img-detail-mobile h3,
  .figure-caption.figure-caption-mobile h2,
  .col-next-gif-mobile h2 {
    color: #5b6c59;
  }

  .columm-product-right h1,
  .img-detail h3,
  .separator-mobile,
  .separator-mobile-small,
  .img-detail-mobile h3 {
    border-color: #5b6c59;
  }

  .buttons-mobile {
    border-bottom: 3px solid #5b6c59;
  }

  .button-item-mobile-with-bg {
    background-color: #5b6c59;
  }

  .button-item-mobile-r-border {
    border-right: 3px solid #5b6c59;
  }

  .div-gif-absolute,
  .div-gif-relative {
    background-color: #5b6c59;
  }


  .img-detail-mobile,
  .img-detail {
    background-color: rgba(255, 255, 255, 0.8);
  }


  .gif-right {
    background-color: rgb(255, 255, 255);
  }

  .lnd-sl-title, .lnd-sl-title span.size1, span.size1-blu-mobile {
    color: #fff
  }
  .separator-blu {
    border-top: 5px solid #ffffff;
  }
  .separator-small-blu {
    padding-bottom: 50px;
    border-top: 5px solid #ffffff;
  }


  .store-address-box {
    padding-bottom: 50px;
    color: #fff;
  }
  .store-address-box a.store-phone-link {
    color: #fff;
  }
  .store-address-box .btn-item-store {
    background-color: #4e4e50;
  }


  #m-form {
    top:45px;
  }

  @media (max-width: 767px) {


    /* search form */
    input#edit-search-block-form--2 {
      float: left;
      padding: 0.15em;
      bottom: 0.3em;
      left: .6em;
    }


    .desktop-version, main {
      background-color: #fff;
    }
    .gif-right col:first-of-type {
      order:2;
    }
    .row .col-next-gif {
      order:-1;
    }

    .buttons {
      position: relative;
      left: auto;
      transform: none;
      display: flex;
    }
    .button-item {
      border-bottom: none;
    }
    .button-item:first-of-type {
      border-right: 3px solid #5b6c59;
      padding-right: 3px;
    }
    img.button-icon {
      max-width: 40px!important;
      margin-right: 10px;
    }
    span.icon-txt {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
    .button-item {
      border-bottom: none;
    }
    .button-item .icon-txt span {
      font-size: 14px;
      line-height: 14px;
      color: #69686e;
      font-weight: 400;
    }

    .img-detail {
      width: 100%;
      bottom: -3em;
      padding: 0;
    }
    .img-detail h3 {
      text-align: right;
      display: inline-block;
      float: right;
      font-size: 1.2em;
      width: auto;
      padding: 0;
    }
    
    .letter-n {
      text-align: center;
    }

  }


</style>

<!-- \styles end -->


<?php if (isset($messages)) : ?>
  <?php print $messages; ?>
<?php endif; ?>

<div class="natuzzi-promo">
  <div class="desktop-version">
    <div class="container-fluid container-promo">
      <div class="row">
        <div class="col-12 col-md-2 text-center col-logo"><img src="<?php print $base_folder; ?>/images/logo_left.png"
                                                               class="logo-left img-fluid">
          <div class="buttons">
            <div class="button-item"><a href="javascript:document.querySelector('#m-form .form-input:first-of-type').focus()"><img
                    class="button-icon" src="<?php print $base_folder; ?>/images/icons/01-btn-icon.png"><span
                    class="icon-txt"><span
                      class="icon-title">WRITE US</span><span class="icon-subtitle">For more info</span></span></a></div>
            <div class="button-item"><a class="scrollto" data-to="desktop-map" href="javascript:;"><img
                    class="button-icon" src="<?php print $base_folder; ?>/images/icons/02-btn-icon.png"><span
                    class="icon-txt"><span
                      class="icon-title">VISIT US</span><span class="icon-subtitle">Our store</span></span></a></div>
          </div>
        </div>
        <div class="col-12 col-md-9">

          <!--<video id="video_hero" class="w-100" preload="auto" autoplay muted playsinline>
            <source
                src="<?php /*print $base_folder; */?>/video/1221287773.mp4"
                type='video/mp4'>
            <p class="vjs-no-js">
              To view this video please enable JavaScript, and consider upgrading to a web browser that
              <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>-->
          <img src="<?php print $promo_dir;?>/Avana.png" alt="Natuzzi Floor Sample SALE - save up to 40% OFF" class="w-100">

        </div>
        <div class="col-12 col-md-1 letter-n"><img src="<?php print $base_folder; ?>/images/N.png" class="logo-n"></div>
      </div>
      <div class="row">
        <div class="col-12 col-md-7 offset-md-2 columm-product-right">
          <div class="w-100 content-product"><h1>Up To 40% OFF</h1>
            <p>Discover Natuzzi floor samples and overstock buys at a <strong>special price</strong>. <br/>Made in Italy. Limited stock.</p>

           <!-- <figure class="w-100 figure figure-product-rigth">
              <div class="img-container"><img
                    src="<?php /*print $base_folder; */?>/images/cms/uploads/compressed/02-17-387-1200x746.jpg"
                    class="w-100 figure-img img-fluid" alt="">
                <div class="img-detail">
                  <div class="img-detail-container"><h3>SENSORIAL COMFORT</h3>
                    <p><span>The mix of padding in polyurethane and Climalight (special latest-generation silicone microfibre) guarantees a look, comfort and support that remain unchanged over time.</span>
                    </p></div>
                </div>
              </div>
              <figcaption class="figure-caption"></figcaption>
            </figure>-->

          </div>
        </div>
      </div>
    </div>

    <?php // AVANA ?>
    <div class="container-fluid container-fluid-img-big">
      <div class="img-big-container">
        <img
            src="<?php print $promo_dir; ?>/Avana_V570_studio-view.jpg"
            class="w-100 visual-bottom img-fluid" alt="Avana V570 QT studio view" />
        <div class="img-detail">
          <div class="img-detail-container"><h3>AVANA SOFA</h3></div>
        </div>
      </div>
      <div class="gif-container">
        <div class="row">
          <div class="col col-12 col-md-5 offset-md-1 text-center align-middle gif-left"><img
                src="<?php print $promo_dir; ?>/Avana_V570_front-view.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
          <div class="col col-12 col-md-6 col-next-gif align-middle"><h2>DESIGN</h2>
            <p><span>Avana is characterized by its modern edge and finer details, such as the tufting on the seat,
                the rounded metal framing around the armrest and metal insert in the wooden foot. </span></p>
          </div>
        </div>
        <div class="row align-items-center gif-right">
          <div class="col col-12 col-md-6  text-center align-middle gif-left">
            <div class="columm-product-left-containerr columm-product-left-container-celll col-next-gif"><h2>UNMATCHED COMFORT & RELAXATION</h2>
              <p>Avana guarantees great comfort thanks to its comfortable seat and adjustable headrests.</p></div>
          </div>
          <div class="col col-12 col-md-5 col-md-gif-right text-center"><img
                src="<?php print $promo_dir; ?>/Avana_V570_side-view.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
        </div>
        <div class="row">
          <div class="col col-12 col-md-7 col-md-gif columm-product-right-bottom"><img
                src="<?php print $promo_dir; ?>/Avana_V570_studio-view2.jpg"
                class="w-100 figure-img img-fluid img-gif" alt="" width="">

          </div>
          <div class="col col-12 col-md-5 col-next-gif align-middle"><h2>$3,999</h2> <div class="msrp">MSRP: $5,640</div>
            <p>
              <span class="savings">30% OFF</span>
            </p>
            <p>
              Available in MonteCarlo Violet Brown leather
            </p>
          </div>
        </div>
      </div>
    </div>



    <?php // PIUMA ?>
    <div class="container-fluid container-fluid-img-big">
      <div class="img-big-container">
        <img
            src="<?php print $promo_dir; ?>/Piuma_bed.jpg"
            class="w-100 visual-bottom img-fluid" alt="Piuma Bed QT studio view" />
        <div class="img-detail">
          <div class="img-detail-container"><h3>PIUMA BED</h3></div>
        </div>
      </div>
      <div class="gif-container">
        <div class="row">
          <div class="col col-12 col-md-5 offset-md-1 text-center align-middle gif-left"><img
                src="<?php print $promo_dir; ?>/Piuma_bed_front-view.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
          <div class="col col-12 col-md-6 col-next-gif align-middle"><h2>DESIGN</h2>
            <p><span>An extreme softness that you can sense at first glance.
  When you look at the headboard of the
  Piuma bed it’s easy to imagine two pillows side by side
  offering an absolute comfort. A refined profile
  is distinguished by an original detail: the ruche. </span></p>
          </div>
        </div>
        <div class="row align-items-center gif-right">
          <div class="col col-12 col-md-6  text-center align-middle gif-left">
            <div class="columm-product-left-containerr columm-product-left-container-celll col-next-gif"><h2>UNMATCHED COMFORT & RELAXATION</h2>
              <p><strong>Bed with slatted base</strong><br/>
                Size Queen L97 x W75 x H37 inch<br/>
                King L97 x W91 x H37 inch</p></div>
          </div>
          <div class="col col-12 col-md-5 col-md-gif-right text-center"><img
                src="<?php print $promo_dir; ?>/Piuma_bed_side-view.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
        </div>
        <div class="row">
          <div class="col col-12 col-md-7 col-md-gif columm-product-right-bottom"><img
                src="<?php print $promo_dir; ?>/Piuma_bed_studio-view.png"
                class="w-100 figure-img img-fluid img-gif" alt="" width="">

          </div>
          <div class="col col-12 col-md-5 col-next-gif align-middle ">
            <h2>$3,999</h2> <div class="msrp">MSRP: $6,700</div>
            <p>
              <span class="savings">40% OFF</span>
            </p>
            <p>
              King Bed in Status Ice White or Ostrica leather
            </p>
            <div class="separator-green-box"></div>
            <h2>$3,499</h2> <div class="msrp">MSRP: $6,220</div>
            <p>
              <span class="savings">45% OFF</span>
            </p>
            <p>
              Queen Bed in Status Ice White leather
            </p>
          </div>
        </div>
      </div>
    </div>




    <?php // RELEVE ?>
    <div class="container-fluid container-fluid-img-big">
      <div class="img-big-container">
        <img
            src="<?php print $promo_dir; ?>/Releve_studio-view1.jpg"
            class="w-100 visual-bottom img-fluid" alt="Releve QT sectional" />
        <div class="img-detail">
          <div class="img-detail-container"><h3>RELEVE SECTIONAL</h3></div>
        </div>
      </div>
      <div class="gif-container">
        <div class="row">
          <div class="col col-12 col-md-5 offset-md-1 text-center align-middle gif-left"><img
                src="<?php print $promo_dir; ?>/Releve_front-view.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
          <div class="col col-12 col-md-6 col-next-gif align-middle"><h2>DESIGN</h2>
            <p><span>Releve is distinguished by its simple design and contemporary lines, which
                are clear and precise. Rich details are found in the original armrest design,
                a continuous invitation to settle in.</span></p>
          </div>
        </div>
        <div class="row align-items-center gif-right">
          <div class="col col-12 col-md-6  text-center align-middle gif-left">
            <div class="columm-product-left-containerr columm-product-left-container-celll col-next-gif"><h2>UNMATCHED COMFORT & RELAXATION</h2>
              <p>The triangular satin nickel feet add a sculptural element to the design of this contemporary sofa, enhancing the overall look.</p></div>
          </div>
          <div class="col col-12 col-md-5 col-md-gif-right text-center"><img
                src="<?php print $promo_dir; ?>/Releve_side-view.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
        </div>
        <div class="row">
          <div class="col col-12 col-md-7 col-md-gif columm-product-right-bottom"><img
                src="<?php print $promo_dir; ?>/Releve_studio-view3.jpg"
                class="w-100 figure-img img-fluid img-gif" alt="" width="">

          </div>
          <div class="col col-12 col-md-5 col-next-gif align-middle ">
            <h2>$4,499</h2> <div class="msrp">MSRP: $6,530</div>
            <p>
              <span class="savings">30% OFF</span>
            </p>
            <p>
              Available in left and right-arm facing configurations <br/>
              Phoenix paper-white leather covering<br/>
            </p>
          </div>
        </div>
      </div>
    </div>



    <?php // TEMPO ?>
    <div class="container-fluid container-fluid-img-big">
      <div class="img-big-container">
        <img
            src="<?php print $promo_dir; ?>/Tempo_studio-view.jpg"
            class="w-100 visual-bottom img-fluid" alt="Tempo QT Sectional" />
        <div class="img-detail">
          <div class="img-detail-container"><h3>TEMPO SECTIONAL</h3></div>
        </div>
      </div>
      <div class="gif-container">
        <div class="row">
          <div class="col col-12 col-md-5 offset-md-1 text-center align-middle gif-left"><img
                src="<?php print $promo_dir; ?>/Tempo_sectional-white-bg.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
          <div class="col col-12 col-md-6 col-next-gif align-middle"><h2>DESIGN</h2>
            <p><span>Tempo sofa is distinguishing by its sophisticated and timeless design.
                The feet are in a special dark grey powder coated metal finish. Tempo's
                versatility makes it perfect for any environment. </span></p>
          </div>
        </div>
        <div class="row align-items-center gif-right">
          <div class="col col-12 col-md-6  text-center align-middle gif-left">
            <div class="columm-product-left-containerr columm-product-left-container-celll col-next-gif"><h2>COMFORT</h2>
              <p>Seat cushion includes a layer of memory foam, a pressure-sensitive soft polyurethane,
                that recovers its form completely for ultimate style and comfort. The padding also incorporate
                a layer of 100% goose feathers to ensure luxurious comfort.</p></div>
          </div>
          <div class="col col-12 col-md-5 col-md-gif-right text-center"><img
                src="<?php print $promo_dir; ?>/Tempo_studio-view3.jpg"
                class="figure-img img-fluid img-gif" alt="" width=""></div>
        </div>
        <div class="row">
          <div class="col col-12 col-md-7 col-md-gif columm-product-right-bottom"><img
                src="<?php print $promo_dir; ?>/Tempo-sectional-studio-view2.png"
                class="w-100 figure-img img-fluid img-gif" alt="" width="">

          </div>
          <div class="col col-12 col-md-5 col-next-gif align-middle ">
            <h2>$8,999</h2> <div class="msrp">MSRP: $12,870</div>
            <p>
              <span class="savings">30% OFF</span>
            </p>
            <p>
              5-piece sectional</br>
              Available in Palio Bronze Fabric<br/>
            </p>
          </div>
        </div>
      </div>
    </div>



      <!--<div class="row">
        <div class="col-12 col-md-6 offset-md-5 columm-product-right-bottom">
          <figure class="w-100 figure figure-product-rigth">
            <div class="img-container"><img src="<?php print $base_folder; ?>/images/cms/uploads/compressed/02-b-2-387-900x559.jpg"
                                            class="w-100 figure-img img-fluid" alt="" width=""></div>
            <figcaption class="figure-caption"></figcaption>
          </figure>
        </div>
        <div class="columm-product-left-container"><h3>VOLUMINOUS DESIGN</h3>
          <p><span>A modular sofa, available in in-line and corner versions, with or without a chaise-longue, to satisfy every space and style need.</span>
          </p></div>
      </div>-->


    <div class="container-fluid container-activity">
      <!--
      <div class="container">
        <div class="top">
          <div class="separator"></div>
          <span class="size1">natuzzi quality</span><br/><span class="size3">is also reflected in the service</span></div>
        <div class="separator-small"></div>
        <div class="plus row">
          <div class="col-5-div"><img
                src="<?php print $base_folder; ?>/images/icons/01_360_colours_S.svg"><br><br><span
                class="icon-txt"><span class="icon-title">360 COLOURS</span><span
                  class="icon-subtitle">In leather or fabric</span></span></div>
          <div class="col-5-div"><img
                src="<?php print $base_folder; ?>/images/icons/02_madeItaly_S.svg"><br><br><span
                class="icon-txt"><span class="icon-title">MADE IN ITALY</span><span class="icon-subtitle">Design and technology</span></span>
          </div>
          <div class="col-5-div"><img
                src="<?php print $base_folder; ?>/images/icons/03_Warranty_S.svg"><br><br><span
                class="icon-txt"><span class="icon-title">NATUZZI</span><span class="icon-subtitle">Warranty</span></span>
          </div>
          <div class="col-5-div"><img src="<?php print $base_folder; ?>/images/icons/04_Visit_S.svg"/><br><br><span
                class="icon-txt"><span class="icon-title">VISIT US</span><span
                  class="icon-subtitle">Our Stores</span></span></div>
          <div class="col-5-div hidden-sm hidden-xs"><img
                src="<?php print $base_folder; ?>/images/icons/05_Info_S.svg"><br><br><span
                class="icon-txt"><span class="icon-title">MORE INFO</span><span
                  class="icon-subtitle">Call us</span></span>
          </div>
        </div>
      </div>
      -->
      <div class="container">
        <div class="top">
          <div class="separator-blu"></div>
          <p class="lnd-sl-title"><span class="size1">COME AND TEST</span><br/>NATUZZI QUALITY</p></div>
        <div class="separator-small-blu"></div>

        <div class="row justify-content-center text-center store-address-box">
          <div class="col col-12 col-md-4"><h3>Furnitalia Sacramento</h3>
            <p>5252 Auburn Blvd.<br/>95841 - Sacramento, CA<br/>corner of Madison & Hemlock <br/><a class='store-phone-link' href='tel:916-332-9000'>916-332-9000</a><br>
            </p><a href="http://www.google.com/maps/place/38.6643559,-121.3421965" target="_blank"
                   class="btn btn-item-store btn-store btn-go-to-store">BRING ME HERE&nbsp;&nbsp;<i
                  class="fa fa-angle-right" aria-hidden="true"></i></a></div>
        </div>

      </div>
    </div>

    <?php if (FALSE): ?>
    <div class="container-fluid container-fluid-img-big">
      <div id="desktop-map" class="map"><!-- js load --></div>
    </div>
    <?php endif; ?>

    <div class="container-fluid container-carousel">
  <!--    <div class="container">
        <div class="top">
          <div class="separator-blu"></div>
          <p class="lnd-sl-title"><span class="size1">COME AND TEST</span><br/>NATUZZI QUALITY</p></div>
        <div class="separator-small-blu"></div>
      </div>-->

      <a id="back2Top" title="Back to top" href="#">.</a>

    </div>

    <div class="container-fluid container-fluid-footer">
      <div class="row">
        <div class="col col-12 col-md-3">
          <nav class="navbar"><a class="navbar-brand" href="https://www.furnitalia.com"><img
                  src="<?php print $base_folder; ?>/images/logo_white.png"></a></nav>
        </div>
      </div>
    </div>
  </div>


  <?php if (false): ?>
  <div class="container-fluid" id="m-top">
    <div class="promo">
      <div class="detail-product"><i class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>


      <div id="template-harmony-friday">
        <!-- video -->
  <section class="video visible-lg visible-md start-stop">
    <video id="video_dk" class="video-js vjs-fluid vjs-4-3 mid-video with-play control-audio" preload="auto"
           loop playsinline data-setup='{"fluid": true,"autoplay": true}'
           poster="<?php print $base_folder; ?>/images/video_poster.jpg">
      <source
          src="https://player.vimeo.com/external/242556287.sd.mp4?s=a42ba88e49e0c5776ffb9bca0e8ed717deac3fa7&profile_id=165"
          type='video/mp4'>
    </video>
  </section>
  <section class="video visible-sm visible-xs start-stop">
    <video id="video_mb" class="video-js vjs-fluid mid-video with-play control-audio video-mobile"
           preload="auto" loop playsinline data-setup='{"fluid": true,"autoplay": false}'
           poster="<?php print $base_folder; ?>/images/video_poster.jpg">
      <source
          src="https://player.vimeo.com/external/242556287.sd.mp4?s=a42ba88e49e0c5776ffb9bca0e8ed717deac3fa7&profile_id=165"
          type='video/mp4'>
    </video>
  </section>


  <section class="text-uppercase font-big text-center boxed harmony-friday-title">
    HARMONY <span class='font-didot'>friday</span></section>
  <section class="" id="m-gif">
    <div class="text-center gif-text font-big boxed">
      The amazing versatility of our floor sample collection at <strong>20% off</strong> and <strong>FREE local
        delivery</strong>.
    </div>
    <img class="img-responsive" src="<?php print $base_folder; ?>/images/herman.gif"
         alt="DRESS YOUR home LIKE YOURSELF."/>

    <div class="single-product">
      <a href="javascript:;"
         data-id="14564" data-id-landing="121"
         class="btn btn-discover"><strong>DISCOVER</strong> Herman&nbsp;&nbsp;<i
            class="btn-loading fa fa-chevron-right" aria-hidden="true"></i>

      </a>
    </div>
  </section>
  <section class="show-sticky-footer slide-text-image" id="m-services">
    <div class="title">services included</div>
    <a href="javascript:;" class="prev"><img
          src="<?php print $base_folder; ?>/images/icons/arrow-left.png"/></a><a
        href="javascript:;" class="next"><img
          src="<?php print $base_folder; ?>/images/icons/arrow-right.png"/></a>
    <div class="owl-carousel owl-theme">
      <div class="item dark">
        <div class="preview"><h3>FREE LOCAL<br/>DELIVERY</h3></div>
      </div>
      <div class="item dark">
        <div class="preview"><h3>TAILORED<br/>PROJECTS</h3></div>
      </div>
      <div class="item dark">
        <div class="preview"><h3>3D INTERIOR<br/> DESIGN</h3></div>
      </div>
      <div class="item dark">
        <div class="preview"><h3>LIFETIME<br/>WARRANTY*</h3></div>
      </div>
    </div>
    <a href="javascript:;" data-scroll-to="#m-col" data-scroll-to-mobile="#m-col" class="btn btn-discover"><span
          style="font-style: normal;"><b>LIVING, DINING, BED</b></span>&nbsp;&nbsp;<i
          class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a>
  </section>
  <section class="slide-video-image" id="m-slide-video">
    <a href="javascript:;" class="prev"><img src="<?php print $base_folder; ?>/images/icons/arrow-left-white.png"/></a>
    <a href="javascript:;" class="next"><img src="<?php print $base_folder; ?>/images/icons/arrow-right-white.png"/></a>
    <div class="owl-carousel owl-theme">
      <div class="item start-stop"><img class="img-responsive"
                                        src="<?php print $base_folder; ?>/images/herman_ambient.jpg"
                                        alt="Zero wall system"/>
        <div class="bg-dark boxed"><span class="text-uppercase font-medium"><div
                style='margin-top: 50px;'>ONLY UNTIL DECEMBER 3<sup>rd</sup></div></span>
          <!-- <br/><br/> --><span class="font-didot font-small subtitle">Exclusive purchasing terms on floor sample sofas, beds and furnishing, entirely made in Italy.</span>
        </div>
      </div>
      <div class="item start-stop">
        <video id="video_2" class="video-js vjs-fluid mid-video with-play control-audio" preload="auto"
               loop playsinline data-setup='{"fluid": true,"autoplay": false}'
               poster="<?php print $base_folder; ?>/images/poster-slide-1.jpg">
          <source
              src="https://player.vimeo.com/external/206573382.hd.mp4?s=657581259daf5d79cac8ea33991ea0364474e0d1&profile_id=119"
              type='video/mp4'>
        </video>
        <!-- <video width="100%" loop poster="images/_tmp/harmony/poster-slide-1.jpg"><source src="https://player.vimeo.com/external/206573382.hd.mp4?s=657581259daf5d79cac8ea33991ea0364474e0d1&profile_id=119" type="video/mp4"></video> -->
        <div class="bg-dark boxed"><span class="text-uppercase font-medium">LATEST INNOVATIONS</span>
          <!-- <br/><br/> --><span class="font-didot font-small subtitle">Means adapting relax to your spaces thanks to successful innovations that have changed the world of furniture forever</span><a
              data-toggle="slide-modal" data-target="#pop-slide-2"
              class="btn btn-discover"><strong>DISCOVER</strong> CRAFTSMANSHIP AND TECHNOLOGY&nbsp;&nbsp;<i
                class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a></div>
      </div>
      <div class="item start-stop"><img class="img-responsive"
                                        src="<?php print $base_folder; ?>/images/poster-slide-2.jpg"
                                        alt="Zero wall system"/>
        <div class="bg-dark boxed"><span class="text-uppercase font-medium">TOTAL COMFORT</span>
          <!-- <br/><br/> --><span class="font-didot font-small subtitle">for your dreams</span><a
              data-toggle="slide-modal" data-target="#pop-slide-3"
              class="btn btn-discover"><strong>DISCOVER</strong> OUR COMMITMENT TO QUALITY&nbsp;&nbsp;<i
                class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a></div>
      </div>
      <div class="item start-stop"><img class="img-responsive"
                                        src="<?php print $base_folder; ?>/images/poster-slide-3.jpg"
                                        alt="Zero wall system"/>
        <div class="bg-dark boxed"><span class="text-uppercase font-medium">LIFESTYLE OFFER</span>
          <!-- <br/><br/> --><span
              class="font-didot font-small subtitle">a way to accompany your day, step by step</span><a
              data-toggle="slide-modal" data-target="#pop-slide-4"
              class="btn btn-discover"><strong>DISCOVER</strong> FURNISHINGS AND ACCESSORIES&nbsp;&nbsp;<i
                class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="slide-modal animated" id="pop-slide-2">
      <div class="slide-modal-body">
        <button type="button" class="close" data-dismiss="slide-modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
        <div class="container">
          <div class="text-center"><span
                class="text-uppercase font-medium">LATEST INNOVATIONS</span><br/>
            <div class="font-didot font-small">Means adapting relax to your spaces.</div>
            <br/><br/></div>
          <div class="text-justify"><p><em>The Zerowall space-saving system eliminates the need to
                move the sofa away from the wall when the Soft Touch mechanism simultaneously
                opens
                the
                seat, headrest and footrest.</em></p>
            <p>The Natuzzi history has been characterised by successful innovations that have
              changed the world of furniture forever. Some examples are: the coloured leathers of
              the 1980s, the recliner mechanisms of the 1990s, or the <strong>latest
                generation</strong> motion sofas of the new millennium. </p>
            <hr/>
            <p><strong>Relax mechanism</strong> for seat, headrest, and footrest at your complete
              discretion so you can reach your ideal resting position. Features that enhance the
              comfort of model. Eco-friendly non-toxic polyurethane padding ensures lasting
              comfort and respect for human health and the environment.  </p><img
                src="<?php print $base_folder; ?>/images/sofa-relax.jpg"
                class="img-responsive visible-md visible-lg" alt="Sofa Relax"/><img
                src="<?php print $base_folder; ?>/images/sofa-relax-mobile.jpg"
                class="img-responsive visible-sm visible-xs" alt="Sofa Relax"/>
            <p>Choose the model that best suits your comfort needs, visit one of our Natuzzi stores
              or galleries and discover special models with a mix of feathers, memory foam
              and polyurethane padding to guarantee extraordinary comfort, offering support and
              lasting good looks. </p>
            <hr/>
            <p><strong>Sofa-beds</strong> are ideal for those who are looking for an elegant sofa
              during the day and a convenient bed at night, preserving a seat of excellent
              comfort. The <strong>easy-to-open version</strong>, where the bed can be opened in
              few seconds, guarantees elegance while giving a high quality rest, combining
              aesthetic with comfort and functionality. </p><img
                src="<?php print $base_folder; ?>/images/sofa-beds.jpg"
                class="img-responsive visible-md visible-lg" alt="Sofa Beds"/><img
                src="<?php print $base_folder; ?>/images/sofa-beds-mobile.jpg"
                class="img-responsive visible-sm visible-xs" alt="Sofa Beds"/>
            <hr/>
            <p>Sofas and armchairs now play music.<br/><strong>With integrated audio system</strong>,
              sofas like Surround and armchairs like Sound are characterized by great design,
              multi-sensorial comfort and a technological soul.<br/>Music notes are like pillows
              where to accommodate the silence of loneliness, funny chats and secrets. </p><img
                src="<?php print $base_folder; ?>/images/sofa-audio.jpg"
                class="img-responsive visible-md visible-lg" alt="Sofa Audio"/><img
                src="<?php print $base_folder; ?>/images/sofa-audio-mobile.jpg"
                class="img-responsive visible-sm visible-xs" alt="Sofa Audio"/>
            <p>The desire to push ourselves and imagine things that have never been done before has
              always been an important characteristic of our history. We combine research,
              creativity, skilled craftsmanship and technology in a process that has been
              perfected through our years of dedicated work in furniture. </p>
            <p>Only through this, can harmony take shape in our hands.</p></div>
          <a href="javascript:;" class="btn btn-discover" data-scroll-to-mobile="#footer-addresses"
             data-scroll-to="#footer-addresses"
             onclick="trackEvent('store', 'click-visit-from-popup', 'zero wall system')"><strong>COME
              AND TEST SOFAS QUALITY</strong>&nbsp;&nbsp;<img
                src="<?php print $base_folder; ?>/images/trova-negozio-white.svg" height="19"/></a>
        </div>
      </div>
    </div>
    <div class="slide-modal animated" id="pop-slide-3">
      <div class="slide-modal-body">
        <button type="button" class="close" data-dismiss="slide-modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
        <div class="container">
          <div class="text-center"><span class="text-uppercase font-medium">TOTAL COMFORT</span><br/>
            <div class="font-didot font-small">for your dreams</div>
            <br/><br/></div>
          <div class="text-justify"><p><em>We have always been committed to quality. When it comes to
                sleep, quality is the most important thing of all.</em></p>
            <p>This is why we decided to enter an exclusive partnership with TRECA, leading
              manufacturer of highend mattresses. Since 1935, TRECA has controlled the entire
              production process, from the choice of raw materials - all strictly of the highest
              quality - to the smallest details of the finished mattresses, which are still
              hand-sewn today. Their expertise is unique, allowing them to combine natural
              materials and cutting-edge technology to ensure the ultimate in comfort, rest and
              wellbeing. We could not have made a better choice.</p>
            <hr/>
            <p><strong>Beds</strong> - The Natuzzi Bed Collection consists of beds in various sizes
              and styles. All of our beds have a European slatted bed suspension and mattresses,
              to meet the needs of all markets. We offer the same vast choice of leather and
              fabric coverings as the Sofa Collection. The range of functions is also extensive:
              beds are available with space-saving storage bases with precision movements; one
              model also has an adjustable headboard.</p>
            <img src="<?php print $base_folder; ?>/images/beds.jpg"
                 class="img-responsive visible-md visible-lg"
                 alt="Beds"/><img
                src="<?php print $base_folder; ?>/images/beds-mobile.jpg"
                class="img-responsive visible-sm visible-xs" alt="Beds"/>
            <p>We have completed our Beds and Accessories Collection with exclusive bedlinen ranges.
              As tactile as <strong>linen</strong> or as sensual as <strong>satin</strong>. From
              cold, rational tones to warmer, brighter hues. All embellished with stylish details.
            </p>
            <p>To transform the dream into a personal world, we have supplemented the Bed Collection
              with accessories for the sleeping area. </p>
            <hr/>
            <p>A versatile collections which reflect the idea of quality and customization at the
              heart of the Natuzzi Italia world. Dressers and nightstands designed <strong>in
                collaboration with major names from the world of design</strong>. These pieces
              are designed with flexibility in mind, allowing them also to be used in a living
              area. </p></div>
          <a href="javascript:;" class="btn btn-discover" data-scroll-to-mobile="#footer-addresses"
             data-scroll-to="#footer-addresses"
          ><strong>COME
              AND TEST BEDS QUALITY</strong>&nbsp;&nbsp;<img
                src="<?php print $base_folder; ?>/images/trova-negozio-white.svg" height="19"/></a>
        </div>
      </div>
    </div>
    <div class="slide-modal animated" id="pop-slide-4">
      <div class="slide-modal-body">
        <button type="button" class="close" data-dismiss="slide-modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
        <div class="container">
          <div class="text-center"><span
                class="text-uppercase font-medium">LIFESTYLE OFFER</span><br/>
            <div class="font-didot font-small"> a way to accompany your day, step by step</div>
            <br/><br/></div>
          <div class="text-justify"><p>The Natuzzi Italia collection stands out for high quality in
              the choice of materials and finishes, as well as for the creativity and details of
              its
              design.</p>
            <p>Furniture (wall units, coffee tables, tables, chairs, lamps and
              carpets) and accessories (vases, mirrors, clocks, magazines racks, trays, room
              fragrances and decorative objects) are designed by Natuzzi Style Center and in
              collaboration with major names from the world of design, to offer complete
              furnishings and make your Total Living unique and personal.</p><img
                src="<?php print $base_folder; ?>/images/lifestyle-1.jpg"
                class="img-responsive visible-md visible-lg" alt="lifestyle"/><img
                src="<?php print $base_folder; ?>/images/lifestyle-1-mobile.jpg"
                class="img-responsive visible-sm visible-xs" alt="lifestyle"/>
            <p>Always in harmony with you and the things around you, these objects stand out for
              their innovative shapes and materials <strong>such as Carrara
                marble</strong> and <strong>Murano glass</strong>. Furthermore, all <strong>wood-based
                materials</strong> are produced in accordance with the laws governing
              formaldehyde content, to ensure our products are completely non-toxic. We only use
              wood sourced from renewable forests. </p><img
                src="<?php print $base_folder; ?>/images/lifestyle-2.jpg"
                class="img-responsive visible-md visible-lg" alt="lifestyle"/><img
                src="<?php print $base_folder; ?>/images/lifestyle-2-mobile.jpg"
                class="img-responsive visible-sm visible-xs" alt="lifestyle"/></div>
          <a href="javascript:;" class="btn btn-discover" data-scroll-to-mobile="#footer-addresses"
             data-scroll-to="#footer-addresses"
          ><strong>COME
              AND TEST LIFESTYLE QUALITY</strong>&nbsp;&nbsp;<img
                src="<?php print $base_folder; ?>/images/trova-negozio-white.svg" height="19"/></a>
        </div>
      </div>
    </div>
  </section>

  </div>


  <!-- other promo -->
  <?php
  $url = base_path() . 'promo/natuzzi-harmony-friday-sale';
  ?>

  <section class="other-promo" id="m-col">
    <div class="content">
      <div class="top">
        <div><br/><br/>DO YOU WANT TO COMPLETE THE LOOK?<br/><span
              class="size2">BROWSE THE COLLECTION</span></div>
      </div>
    </div><!-- DESKTOP -->
    <div class="grid visible-lg visible-md">
      <div class="row">
        <div class="col-md-4"><a href="<?php print $url; ?>/sofas"><img
                src="<?php print $base_folder; ?>/images/500x641-0007-divani-v-6-500x641-5.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Sofas</span></div>
          </a></div>
        <div class="col-md-4"><a href="<?php print $url; ?>/sofa-beds"><img
                src="<?php print $base_folder; ?>/images/500x641-0008-divani-letto-v-7-500x641-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Sofa beds</span></div>
          </a></div>
        <div class="col-md-4"><a href="<?php print $url; ?>/armchairs"><img
                src="<?php print $base_folder; ?>/images/500x313-poltrone-h-8-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Armchairs</span></div>
          </a><a href="<?php print $url; ?>/beds"><img
                src="<?php print $base_folder; ?>/images/500x313-letti-h-25-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Beds</span></div>
          </a></div>
      </div>
      <div class="row">
        <div class="col-md-4"><a href="<?php print $url; ?>/tables"><img
                src="<?php print $base_folder; ?>/images/500x313-tavoli-h-24-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Tables</span></div>
          </a></div>
        <div class="col-md-4"><a href="<?php print $url; ?>/chairs"><img
                src="<?php print $base_folder; ?>/images/500x313-sedie-h-26-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Chairs</span></div>
          </a></div>
        <div class="col-md-4"><a href="<?php print $url; ?>/furnishings-and-accessories"><img
                src="<?php print $base_folder; ?>/images/500x313-librerie-h-27-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Furnishings and accessories</span></div>
          </a></div>
      </div>
    </div><!-- TABLET & MOBILE -->
    <div class="grid visible-sm visible-xs">
      <div class="row">
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/sofas"><img
                src="<?php print $base_folder; ?>/images/500x313-divani-h-6-500x313-5.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Sofas</span></div>
          </a></div>
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/sofa-beds"><img
                src="<?php print $base_folder; ?>/images/500x313-letti-h-7-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Sofa beds</span></div>
          </a></div>
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/armchairs"><img
                src="<?php print $base_folder; ?>/images/500x313-poltrone-h-8-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Armchairs</span></div>
          </a></div>
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/beds"><img
                src="<?php print $base_folder; ?>/images/500x313-letti-h-25-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Beds</span></div>
          </a></div>
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/tables"><img
                src="<?php print $base_folder; ?>/images/500x313-tavoli-h-24-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Tables</span></div>
          </a></div>
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/chairs"><img
                src="<?php print $base_folder; ?>/images/500x313-sedie-h-26-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Chairs</span></div>
          </a></div>
        <div class="col-sm-6 col-xs-6"><a href="<?php print $url; ?>/furnishings-and-accessories"><img
                src="<?php print $base_folder; ?>/images/500x313-librerie-h-27-500x313-4.jpg"
                width="100%" class="img-responsive center-block"/>
            <div class="overlay"><span>Furnishings and accessories</span></div>
          </a></div>
      </div>
    </div>
  </section>

  <section class="legal-notes visible-lg visible-md"><p>
      *All Natuzzi sofas and armchairs have a lifetime warranty on the frame and two years on the
      upholstery,
      mechanical parts and coverings. All furnishing accessories carry a two year warranty.
    </p>
  </section>


  <!-- why -->
  <!--<section class="why" id="m-why">
      <div class="content">
          <div class="top">
              <p><span
                          class="size1">COME AND TEST</span><br/>NATUZZI
                  QUALITY
              </p>
          </div>
          <div class="plus row">
              <div class="col-5-div no-bt"><img
                          src="https://www.natuzzi.us/promotions/images/icons/colori.svg"/><br/><br/>360
                  COLOURS IN
                  LEATHER OR FABRIC
              </div>
              <div class="col-5-div no-bt no-br"><img
                          src="https://www.natuzzi.us/promotions/images/icons/design.svg"/><br/><br/>MADE
                  IN ITALY
                  DESIGN AND TECHNOLOGY
              </div>
              <div class="col-5-div"><img
                          src="https://www.natuzzi.us/promotions/images/icons/quality.svg"/><br/><br/>NATUZZI
                  WARRANTY
              </div>
              <div class="col-5-div no-br"><img
                          src="https://www.natuzzi.us/promotions/images/icons/pointer.svg"/><br/><br/>COME
                  AND VISIT US
              </div>
              <div class="col-5-div hidden-sm hidden-xs"><img
                          src="https://www.natuzzi.us/promotions/images/icons/chiama.svg"/><br/><br/>CALL
                  US FOR MORE
                  INFO
              </div>
          </div>
      </div>
  </section> -->

  </div>
  </div>
  <?php endif; ?>

  <!-- Webform -->
  <div class="form" id="m-form"><p><span class="size2">FLOOR SAMPLE SALE</span><br/>
      UP TO 40% off &mdash; exclusive offers on Natuzzi.
    </p>
    <?php print render($content); ?>
  </div>
  <!-- \Webform -->

</div>


<!-- vendor cdn -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"
        integrity="sha256-NjbogQqosWgor0UBdCURR5dzcvAgHnfUZMcZ8RCwkk8=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"
        integrity="sha256-YrHP9EpeNLlYetSffKlRFg8VWcXFRbz5nhNXTMqlQlo=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"
        integrity="sha256-4F7e4JsAJyLUdpP7Q8Sah866jCOhv72zU5E8lIRER4w=" crossorigin="anonymous"></script>
<script src="//unpkg.com/sweetalert%402.1.2/dist/sweetalert.min.js"></script>-->
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"
        integrity="sha256-4O4pS1SH31ZqrSO2A/2QJTVjTPqVe+jnYgOWUVr7EEc=" crossorigin="anonymous"></script> -->
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"
        integrity="sha256-jDnOKIOq2KNsQZTcBTEnsp76FnfMEttF6AV2DF2fFNE=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"
        integrity="sha256-iMTCex8BQ+iVxpZO83MoRkKBaoh9Dz9h3tEVrM5Rxqo=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.center.min.js"
        integrity="sha256-puNNvBbwmmw0WIyDXJ5cTd0W/EAiCArmnjo5gUfDxC4=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.swipe.min.js"
        integrity="sha256-Prq5k7iqQbFCbpoBYTw3cqMbUZyKh3Y0BBGPBfcxs20=" crossorigin="anonymous"></script>
<script src="//unpkg.com/imagesloaded%404.1.4/imagesloaded.pkgd.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"
        integrity="sha256-ffw+9zwShMev88XNrDgS0hLIuJkDfXhgyLogod77mn8=" crossorigin="anonymous"></script>
<script src="//hammerjs.github.io/dist/hammer.js"></script>-->

<!--<script type="text/javascript" src="../../lib/gmap3/dist/gmap3.min.js"></script>
<script type="text/javascript" src="../../js/formlocatorb9a2.js?v=1521017327"></script>-->


<!--
<script src="<?php print $base_folder; ?>/js/app_script_landingpage.js?v=1521017327"></script>
-->

<script src="<?php print $base_folder; ?>/js/landingpage.js?v=1521017327"></script><!-- js -->
<!--<script
    src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;client=gme-natuzzi&amp;language=en-US&amp;region=en-US"></script>
-->




<!--<script src="<?php /*print $base_folder; */?>/js/app_script.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script src="<?php /*print $base_folder; */?>/js/videojs.min.js"></script>-->


<script>

  (function ($) {


    lang = 'en-US';
    labels = {
      "FORM_ERROR_TITLE": "Attention",
      "FORM_ERROR_TEXT": "Check the required fields",
      "FORM_ERROR_TEXT_GENERAL": "A general error occurred. Please try again later.",
      "NL_STORE_ERROR_GPS": "Your device doesn't support localization",
      "NL_STORE_ERROR_LOCATION": "We couldn't get your position"
    };
    url = '//www.natuzzi.us/promotions';
    landingSlug = 'sale';
    userPosition = 0;
    idLanding = 158;

    time = {"from": "900", "to": "1800"};


    /*
    $(function () {
      // preload images
      $('.banner').imagesLoaded(function () {
        $('.img-loading').addClass('hidden');
        $('.main-slideshow').removeClass('hidden');
      });

      bindMainSliderControls();

      openDetail();
      getMoreProducts();

      // mobile
      bindFormStoreMobile();
      handleVideoJSPlugin();

      //////////////////////////////// SLICK CAROUSEL
      $('.slide-video-image .owl-carousel').slick({
        infinite: true,
        slidesToShow: 1,
        prevArrow: $('.slide-video-image .prev'),
        nextArrow: $('.slide-video-image .next')
      });
      $('.slide-text-image .owl-carousel').slick({
        infinite: true,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: $('.slide-text-image .prev'),
        nextArrow: $('.slide-text-image .next')
      });


      handleSlideModals();

    });
*/

   /* WebFont.load({
      google: {
        families: ['Lato:100,300,400,700']
      }
    });
*/

  })(jQuery);

</script>
