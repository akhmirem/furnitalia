
<?php global $theme_path;?>

<style>
    div.stressless-items-link {
        padding: 15px 0 30px 0;
    }
    div.stressless-items-link a.more-info {
        font-size: 1.5em;
        border-bottom: 1px dotted;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>

<h1 style="color:#981b1e;text-align:center;">Stressless&reg; Comfort Plus Event</h1>

<img src="<?php print $theme_path;?>/images/landing/stressless/stressless_P2_Comfort_Plus_Event_725x517.jpg"
     alt="Discover $300 OFF Stressless Crown plus a Free Accessory during Comfort Plus Event"
     style="margin-left:150px"
/>
<p style="font-size: 1.2em;
    line-height: 1.5em;
    font-weight: 500;">
    Offer ends June 20!
</p>
<p style="font-family:Arial">
During our Comfort Plus event, you can receive a FREE accessory with your purchase of any Stressless® Recliner and Ottoman, Stressless Office Chair, Stressless Sofa or Ekornes Collection Sofa. Plus, receive $300 off our new Stressless Crown recliner. 
</p>
<p style="font-family:Arial">
Nothing helps you relax and unwind like the unmatched comfort and luxury of Stressless. Stressless seating is the only furniture capable of providing such comfort. You’ll feel the difference in our chairs and couches equipped with innovative comfort technologies like our patented Plus™-system, which provides exceptional lumbar support by interpreting your body’s movement. Once you sit in a Stressless, you’ll wonder how you’ve been able to relax without it. 
</p>

<aside class="contact" style="text-align:center;">
    Call us at <a href="tel:+19163329000">916-332-9000</a> or
    <a href="<?php print base_path();?>request/ajax/stressless" class="request"
       id="request-info"
       title="Request Stressless Product Information"
        style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            width: 200px;
            margin: 10px auto 0 20px;
            border-radius: 7px;">
        CONTACT US
    </a>    
    
    <?php print $signup_form; ?>

    <?php if (isset($view_items_link)) : ?>
        <?php print render($view_items_link); ?>
    <?php endif; ?>

    
</aside>

<script language="javascript"> 
function scrollToTop() { 
    
    //scroll(0,0); 

    //scroll to newly inserted content
    var offset = jQuery("#ekornesIframe").offset();
    var scrollTarget = jQuery("#ekornesIframe");
    while (scrollTarget.scrollTop() == 0 && scrollTarget.parent()) {
        scrollTarget = scrollTarget.parent();
    }
    // Only scroll upward
    scrollTarget.animate({scrollTop: (offset.top - 5)}, 500);
} 
</script>

<iframe id="ekornesIframe" src="http://www.ekornes.com/us/?d=1" frameborder="0" width="100%" height="3500" scrolling="no" onload="scrollToTop();"></iframe>

<!--<hr/>
<a href="http://www.ekornes.com/us/e-catalog" target="_blank"><img src="http://www.stresslessdigital.com/media/1378/onlinecatus.png"/></a>-->
<?php if (isset($view_items_link)) : ?>
    <?php print render($view_items_link); ?>
<?php endif; ?>
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: EKOR - Dealer - Furnitalia
URL of the webpage where the tag is expected to be placed: http://www.furnitalia.com/stressless-recliners-sofas-leather-upgrade-promotion
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 02/09/2016
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4682854.fls.doubleclick.net/activityi;src=4682854;type=landi0;cat=ekord00b;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4682854.fls.doubleclick.net/activityi;src=4682854;type=landi0;cat=ekord00b;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<?php //include_once(path_to_theme() . '/templates/amocrm_form.tpl.php'); ?>
