<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/norway-days-event-2019";

?>
<style>

  .row-padded {
    padding-top: 4em;
  }

  .brand-img {
    display: block;
  }
  .separator-small {
    height: 5px;
    border-top: 3px solid #000;
    width: 250px;
    margin: 30px auto;
  }

  #content article, #content article a {
    color: #000;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
    margin-top: -1em;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page a.underline {
    text-decoration: underline;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
  }
  article.promo-landing-page .sale-intro {
    margin: 1em auto 0;
    max-width: 57ch;
    font-size: 1.2em;
  }
  article.promo-landing-page ul.sale-intro {
    text-align: left;
    line-height: 150%;
    margin-top:-1em;
  }
  article.promo-landing-page ul li {
    margin: .5em 0;
  }
  article.promo-landing-page .image-cta {
    position: relative;
  }
  @keyframes slideInFromBottom {
    0% {
      transform: translateY(100%);
    }
    100% {
      transform: translateY(0);
    }
  }
  @keyframes pulse{
    25%  {transform: scale(0.9);}
    75%  {transform: scale(1.1);}
  }

  article.promo-landing-page .image-cta .btn {
    /*animation: 1s ease-out 0s 1 slideInFromBottom;*/
    animation: pulse 0.5s ease-in;
  }
  article.promo-landing-page .image-cta .btn:hover{
    animation: pulse 0.5s ease-in infinite;
  }
  article.promo-landing-page h1 {
    font-weight: bold;
    text-transform: uppercase;
    color: #000;
    font-size: 2em;
    letter-spacing: 1.5px;
  }
  article.promo-landing-page .subtitle-emphasis {
    font-size: 1.5em;
    color: #ea2328;
    text-transform: capitalize;
    line-height: 150%;
  }
  article.promo-landing-page .sale-subtitle {
    font-size: 2em;
    line-height: 200%;
    text-transform: uppercase;
    letter-spacing: 2px;
  }
  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
    line-height: 150%;
  }
  article.promo-landing-page .promo-block p {
    font-size: 1.2em;
  }
  #content article.promo-landing-page .btn-contact,
  #container article.promo-landing-page .btn-contact {
    background: #ea2328;
    border-color: #290606;
    color: #fff;
    padding: .75em 2em;
    margin: 1em 0;
    font-size: 1.2em;
    text-transform: uppercase;
  }

  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  .webform-section {
    float: none;
  }
  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }

  .bottom-cta p {
    max-width: 50ch;
    margin: auto;
  }

  .store-location-container {
    display: flex;
    align-items: center;
    justify-content: center;
    background: #10232A;
    margin-top: 2em;
    height: 450px;
  }
  .store-location-block {
    max-width: 50%;
    padding: 15px;
  }

  @media (max-width: 767px){
    body {
      /*overflow-y:scroll;*/
      /*-webkit-overflow-scrolling: touch;*/
      /*position: absolute;*/
      /*top: 0;*/
    }
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    #container article.promo-landing-page .btn-contact {
      position: static!important;
      margin-top: 1em;
      margin-bottom: 2em;
      cursor: pointer;
      padding: 0.5em 1em;
      font-size: 1em;
    }
    article.promo-landing-page .btn-contact:hover{
      animation: none;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
    }
    article.promo-landing-page .promo-block {
      padding-left:1em;
      padding-right:1em;
    }
    article.promo-landing-page h1 {
      font-size: 1.35em;
    }
    article.promo-landing-page h2.cta {
      font-size: 1.5em;
    }
    article.promo-landing-page .sale-subtitle {
      font-size: 1.35em;
      line-height: 150%;
      margin: 1em 0;
    }
    article.promo-landing-page .sale-intro {
      margin-right: 0.5em;
      font-size: 1em;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }
    .bottom-cta p {
      width: 100%;
    }
    .store-location-container {
      height: 500px;
    }
    .store-location-block {
      max-width: 100%;
      padding: 15px;
    }
  }


</style>

<article class="promo-landing-page clearfix">

  <div class="image-cta text-center">

    <img class="img-responsive center-block"
         src="<?php print $promo_dir;?>/Furnitalia-Norway-Days-Event-Hero-Image.png"
         alt="Norway Days Event at Furnitalia | JOIN US!">
  </div>

  <div class="content text-center">

    <h1>Norway Days Event at Furnitalia</h1>

    <div class="subtitle-emphasis">September 7, 2019<br/>11am&ndash;5pm</div>

    <!--<p class="sale-intro">Come in and receive up to $1,500 credit toward additional Stressless® seating. Or take $500 off a Signature base recliner
and ottoman or LegComfort™ recliner. Or get $50 off each dining chair when you buy four or more.*<br>
    </p>-->
    <p class="sale-intro">Come in and take advantage of multiple Stressless® offers!</p>

    <h3 class="cta">Save on Stressless&reg; and MORE:</h3>
    <ul class="sale-intro">
      <li>Get a personal fitting to find the perfect Stressless® for you!</li>
      <li>Enjoy delicious Norwegian food and beverages!</li>
      <li>Save up to 20% OFF on floor models or receive up to $1,500 credit* toward Stressless® seating!</li>
    </ul>

    <a href="#contact-form" id="rsvpButton" class="btn btn-primary btn-contact scroll-to-contact"  style="
    color: #fff;
    padding: .75em 2em;
    margin: 1em 0;
    "><span style="font-size:18px;">RSVP For Event »</span></a>


    <p class="sale-intro"><span>Visit our newly-renovated
        37,000 sq. ft. contemporary furnishings showroom
        in Sacramento, the largest Natuzzi and Stressless furniture
        gallery in Northern California. Our designers are here to showcase our large selection of sofas,
        sectionals, recliners, dining tables, kitchen cabinets, and bedroom furniture on sale. </span></p>

  </div>


  <div class="row">
    <div class="col-md-6 center-block webform-section">
      <a id="contact-form"></a>

      <noscript><a href="https://norway-days-event-at-furnitalia.eventbrite.com" rel="noopener noreferrer" target="_blank"></noscript>
      <noscript></a>Reserve Tickets on Eventbrite</noscript>

      <script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>

      <script type="text/javascript">
        var exampleCallback = function() {
          console.log('Reservation complete!');
        };

        var w = window.EBWidgets.createWidget({
          widgetType: 'checkout',
          eventId: '71016360847',
          modal: true,
          modalTriggerElementId: 'rsvpButton',
          onOrderComplete: exampleCallback
        });

      </script>
      <?php //print render($content); ?>
    </div>
  </div>

  <br/><br/>

  <div class="row text-center">
    <img class="img-responsive center-block"
         src="<?php print $promo_dir;?>/Norway-Days-Event--landing-Stressless-bottom-image.jpg"
         alt="Norway Days Event at Furnitalia | About Stressless">
  </div>

  <div class="store-location-container">
    <!--<div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content" style="font-size: 1.35em;line-height: 1.5;">

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>-->
    <div class="store-location-block" >
      <div class="content event-location" style="color:white;padding:45px"><p><strong class="standout">Visit us at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>

  <br/>

  <?php if (false): ?>
  <div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="/sites/default/files/brands/brand-slider-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="/sites/default/files/brands/brand-slider-logos/Alf-Italia-Logo.png"
                                    alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="/sites/default/files/brands/brand-slider-logos/Bauformat-German-Kitchen-Logo.png"
                                                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="/sites/default/files/brands/brand-slider-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="/sites/default/files/brands/brand-slider-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="/sites/default/files/brands/brand-slider-logos/Incanto-Logo.png"
                                        alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="/sites/default/files/brands/brand-slider-logos//Miele-Logo.png"
                                           alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="/sites/default/files/brands/brand-slider-logos/Stressless-Logo.png"
                                                             alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>
  <br/>
  <?php endif; ?>

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

  <div class="row">
    <span>*With a qualifying purchase. See store for details.</span>
  </div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);

  });
</script>

<script>
  (function ($) {
    $(".slider").slick({
      dots: false,
      infinite: true,
      speed: 2000,
      fade: false,
      slide: 'li',
      cssEase: 'linear',
      centerMode: false,
      centerPadding: "0px",
      slidesToShow: 7,
      variableWidth: true,
      autoplay: true,
      autoplaySpeed: 5,
      responsive: [{
        breakpoint: 1200,
        settings: {
          arrows: true,
          centerMode: false,
          //centerMode: false,
          //centerPadding: '40px',
          centerPadding: '0px',
          variableWidth: true,
          slidesToShow: 7,
          dots: false,
          // autoplaySpeed:300,
          // speed:300

        }
      }, {
        breakpoint: 800,
        settings: {
          arrows: true,
          //centerMode: false,
          //centerPadding: '40px',
          variableWidth: true,
          slidesToShow: 3,
          dots: false,
          centerMode: false,
          centerPadding: '0px'
        }
      }],
      customPaging: function (slider, i) {
        return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
      }
    });

    /*$(".btn-contact").on('click', function () {

      $([document.documentElement, document.body]).animate({
        scrollTop: $("#contact-form").offset().top
      }, 1000);

      return false;

    });*/
  })(jQuery);
</script>