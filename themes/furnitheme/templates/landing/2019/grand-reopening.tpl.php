<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/grand_reopening";

?>
<style>

  #content article, #content article a {
    color: #000;
  }
  #content article {
    overflow-x: hidden;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page .content.intro-content {
    max-width: 900px;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
  }
  article.promo-landing-page p a {
    text-decoration: underline;
  }
  article.promo-landing-page h1 {
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 22px;
    color: #b2cfd4;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
    margin-left: 0!important;
    margin-top: 2em;
  }
  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
  }
  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section {
    float: none;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }
  .lh200 {
    line-height: 200%;
  }

  @media (max-width: 767px){
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
    }

    article.promo-landing-page .mobile-no-center {
      text-align:left!important;
    }

    article.promo-landing-page ul.activities {
      font-size: 16px!important;
    }
    article.promo-landing-page .coupon-cta-block {
      padding: 0 20px !important;
      margin: 0!important;
    }

    article.promo-landing-page h1 span span {
      font-size: 26px!important;
    }
    article.promo-landing-page h2.cta {
      font-size: 2em;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }

    .hidden-sm {
      display: none;
    }
    .lh150-mobile {
      line-height: 150%!important;
    }
    .fineprint {
      padding: 0 40px;
    }
  }

</style>

<article class="promo-landing-page clearfix">

  <header>
    <span property="dc:title" content="Furnitalia Sacramento - Grand Re-Opening" class="rdf-meta element-hidden"></span>
  </header>

  <img class="img-responsive center-block" typeof="foaf:Image"
       src="<?php print $promo_dir; ?>/furnitalia-grand-reopening-hero-image-v7.png"
       alt="Furnitalia Sacramento - Sacramento Grand Re-Opening!">

  <div class="content center-block intro-content">

    <!--<h1><span style="font-size:28px"><span style="color:#ff0000"><strong>PLEASE JOIN US FOR OUR GRAND RE-OPENING!</strong>&nbsp;</span></span></h1>-->
    <br/>
    <h1 class="mobile-no-center" style="text-align: center;display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica,sans-serif;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;"><span
      style="font-family:helvetica neue,helvetica,arial,verdana,sans-serif"><span
        style="font-size:38px;line-height:120%;color:#a49484"><strong>FURNITALIA PRESENTS<br>
SACRAMENTO STORE GRAND RE-OPENING!</strong></span></span></h1>

    <div style="max-width: 60ch;margin:0 auto; line-height: 200%;text-align: center;" class="mobile-no-center"><br>
      <span style="font-family:helvetica neue,helvetica,arial,verdana,sans-serif"><span

          style="font-size:20px">Furnitalia — Northern California's largest source of premier home décor — celebrates their
          expansion into the new 32,000 square-foot Sacramento showroom.</span></span>

    </div>
    <br/>

    <p class="mobile-no-center" style="max-width: 60ch;text-align: center;line-height: 200%;margin: 10px auto;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica,sans-serif;font-size: 16px;"><span
        style="font-family:'helvetica neue',helvetica,arial,verdana,sans-serif"><span
          style="font-size:20px">Come experience a premier showroom of contemporary Italian furniture from Natuzzi,
          Alf, Cattelan, Bontempi, Incanto, Calligaris, and more. We also feature a full showroom of Ekornes
          Stressless&copy; living and dining furniture from Norway. </span><br/>
        <span style="font-size:20px">Thinking of a kitchen remodelling? We offer a large selection of
          innovative, high-quality, and stylish German kitchen cabinets by
          <a href="/modern-european-kitchen-cabinets" title="Kitchen Cabinets by Bauformat">Bauformat</a>.
          Home appliances from <a href="/miele-appliances" title="Home Appliances by Miele">Miele</a>
          will perfectly complement your contemporary kitchen. We also offer a great selection of
          German bathroom cabinets by
          <a href="/modern-european-bathroom-cabinets" title="Bathroom cabinets by Badea">Badea</a>.
          </span>
      </span><br>
                                <br>
                            <span style="font-family:'helvetica neue',helvetica,arial,verdana,sans-serif"><span
                                style="font-size:22px"><strong>Have a Project in Mind?</strong></span><br>
Contact Us for FREE professional interior design! Our full-service interior designers are here to assist
                              with your remodelling project from start to finish.<br>
</span>
    </p>

<!--    <br/>
    <div class="mobile-no-center" style="margin: 0 auto;
    font-size: 20px;
    text-align: center;
    line-height: 200%;">
      <span>Grand Re-Opening Activities include:</span><br/>
      <ul class="activities" style="color:#a49484;list-style-type: none;text-transform: uppercase;margin: 0;padding:0">
        <li><strong><span style="">Daily design tips, how to’s &amp; more</span></strong>
        </li>
        <li><strong><span
            style="">Mimosas, Italian coffee, tea &amp; desserts</span></strong>
        </li>
        <li><strong><span style="">Maserati &amp; Alfa Romeo's on display</span></strong>
        </li>
      </ul>
      <p><span style="">Our full-service design team will be available to answer any questions.</span>
      </p>

      <br class="hidden-sm"/>
    </div>-->



    <hr class="gradient"/>
    <br class="hidden-sm"/>



  </div>

  <div class="row">
    <div class="col-md-6 center-block  webform-section"> <!-- col-md-offset-3-->
      <?php print render($content); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content" style="font-size: 1.35em;line-height: 1.5;">

        <div style="" class="lh200 lh150-mobile"><br class="hidden-sm">
          <span style="font-family:'helvetica neue',helvetica,arial,verdana,sans-serif"><span
              style="font-size:20px">Tour our newly-renovated design center, featuring a vast selection of fantastic home and
          kitchen design solutions from Italy, Germany, and Norway.<br><br>
          Experience the unrivaled style, comfort, and performance of the finest in contemporary design that's
          suitable for any budget.<br><br></span></span>
        </div>

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>
    <div class="col-xs-12 col-md-5 col-lg-5" >
      <div class="content event-location" style="background-color: #10232A;color:white;padding:45px"><p><strong class="standout">Grand ReOpening takes
            place at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>

  <br/>

  <div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="<?php print $promo_dir ?>/brand-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="<?php print $promo_dir ?>/brand-logos/Alf-Italia-Logo.png"
                                           alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="<?php print $promo_dir ?>/brand-logos/Bauformat-German-Kitchen-Logo.png"
                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="<?php print $promo_dir ?>/brand-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="<?php print $promo_dir ?>/brand-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="<?php print $promo_dir ?>/brand-logos/Incanto-Logo.png"
                                       alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="<?php print $promo_dir ?>/brand-logos/Miele-Logo.png"
                  alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="<?php print $promo_dir ?>/brand-logos/Stressless-Logo.png"
                           alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>

  <div class="row">
    <div class="fineprint text-center">* For furniture purchases only. Not for accessories.</span>
  </div>
  <br/>

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);

  });
</script>

<script>
  (function ($) {
      $(".slider").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: false,
        slide: 'li',
        cssEase: 'linear',
        centerMode: false,
        centerPadding: "0px",
        slidesToShow: 3,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
          breakpoint: 1200,
          settings: {
            arrows: true,
            centerMode: false,
            //centerMode: false,
            //centerPadding: '40px',
            centerPadding: '0px',
            variableWidth: true,
            slidesToShow: 5,
            dots: false

          }
        }, {
          breakpoint: 800,
          settings: {
            arrows: true,
            //centerMode: false,
            //centerPadding: '40px',
            variableWidth: true,
            slidesToShow: 3,
            dots: false,
            centerMode: false,
            centerPadding: '0px'
          }
        }],
        customPaging: function (slider, i) {
          return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
        }
      });
  })(jQuery);
</script>

