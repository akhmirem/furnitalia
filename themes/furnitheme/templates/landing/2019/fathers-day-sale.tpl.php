<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/fathers_day/2019";

?>
<style>

  .row-padded {
    padding-top: 4em;
  }

  .brand-img {
    display: block;
  }
  .separator-small {
    height: 5px;
    border-top: 3px solid #000;
    width: 250px;
    margin: 30px auto;
  }

  #content article, #content article a {
    color: #000;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page a.underline {
    text-decoration: underline;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
  }
  article.promo-landing-page h1 {
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 4em;
    color: #b2cfd4;
    text-transform: uppercase;
    text-align: left;
    line-height: 150%;
    margin-bottom: 20px;
    margin-left: 0!important;
    margin-top: .2em;
  }
  article.promo-landing-page .sale-subtitle {
    font-size: 2em;
    line-height: 200%;
    text-transform: uppercase;
    letter-spacing: 2px;
  }
  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
    line-height: 150%;
  }
  article.promo-landing-page .promo-block p {
    font-size: 1.5em;
  }

  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  .webform-section {
    float: none;
  }
  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }

  @media (max-width: 767px){
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
    }
    article.promo-landing-page .promo-block {
      padding-left:1em;
      padding-right:1em;
    }
    article.promo-landing-page h1 {
      font-size: 1.7em;
    }
    article.promo-landing-page h2.cta {
      font-size: 1.5em;
    }
    article.promo-landing-page .sale-subtitle {
      font-size: 1.35em;
      line-height: 150%;
      margin: 1em 0;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }
  }

</style>

<article class="promo-landing-page clearfix">

  <img class="img-responsive center-block"
       src="<?php print $promo_dir;?>/Fathers-Day-Sale--top-image.jpg"
       alt="Father's Day Sale is on at Furnitalia!">

  <div class="content">

    <h1><span style="color:#ff0000"><strong>Father's Day Sale</strong>&nbsp;</span></h1>
    <div><em><span style="font-size:18px;font-family: 'EB Garamond 12',serif;">Until June 19, 2019</span></em></div>

    <div class="sale-subtitle">Give Dad a Gift of Comfort!</div>

    <p><span style="font-size:24px">Save 10% OFF on Natuzzi Re-Vive and Stressless&copy; recliners, just in time for
        Father's Day! <br/> Shop our rich selection of comfort recliners in our renovated contemporary
        showroom in Sacramento&mdash;the largest Natuzzi and Stressless furniture
        gallery in Northern California.&nbsp;</span></p>

    <h3 class="cta">Come in today for best selection! </h3>

  </div>

  <div class="row row-padded promo-block jumbotron">
    <div class="col-md-6"><img src="<?php print $promo_dir;?>/Fathers-Day-Sale--Natuzzi-Re-Vive-room-image.jpg"
                               alt="Natuzzi Re-Vive - Find your personal disconnection"
                               class="img-responsive"/></div>
    <div class="col-md-6">
      <h2 class="cta">NATUZZI RE-VIVE</h2>

      <p>
        PAUSE. DISCONNECT FROM THE OUTSIDE WORLD AND RECONNECT WITH YOUR INNER SELF.
        <br/>
        A unique, 100% Natuzzi experience brought to you by Re-Vive recliner innovation. <br/>
        Natuzzi Re-vive is the world’s first performance recliner, mimicking your every movement.<br/>
        It flexes as you change position, supporting your body and mind in a fluid dynamic form.
      </p>
      <a href="/re-vive" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Explore Collection</span></a>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 ">
      <h3 class="cta text-align-center">Re-vive is the only recliner in the world that adjusts to the way you like to sit.</h3>
      <img src="<?php print $promo_dir;?>/Fathers-Day-Sale--Natuzzi-Re-Vive-slides.jpg"
           alt="Natuzzi Re-Vive leading edge, ergonomic technology produces comfort that will enable you to disconnect
           from everything else but yourself."
           class="img-responsive center-block"/>
    </div>
  </div>


  <hr class="gradient"/>



  <div class="row  row-padded promo-block jumbotron">
    <div class="col-md-6">
      <img class="img-responsive"
           src="<?php print $promo_dir;?>/Fathers-Day-Sale--Stressless-room-image2.jpg"
           alt="Ekornes Stressless recliners - some feelings cannot be explained, only experienced">
    </div>
    <div class="col-md-6 center-block">
      <div class="">
        <h2 class="cta">Ekornes Stressless&copy;</h2>
        <p>Some moments cannot be explained&mdash;only experienced. Stressless&copy; is renowned for its unique level of
        comfort.<br/>
        Everyone has different needs, so we encourage our customers to try a Stressless&copy; so that you can find a
        recliner of the right size that suits your own personal tastes. Stressless&copy; recliner is an investment that
        you'll enjoy for many years to come.</p>

        <a href="/ekornes-stressless-recliners-sofas" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Explore Collection</span></a> |
        <a href="http://stressless.ekornes.com/US/totalus/" target="_blank" class="btn btn-default" style=""><span style="font-size:18px">View PDF Catalog</span></a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 ">
      <h3 class="cta text-align-center">Discover the Pleasure of Comfort with Stressless&copy;</h3>
      <img src="<?php print $promo_dir;?>/Fathers-Day-Sale--Stressless-outdoor-image-wide.jpg"
           alt="Ekornes Stressless - discover the secrets of personal comfort"
           class="img-responsive center-block"/>
    </div>
  </div>


  <!--<div class="row row-padded promo-block jumbotron">
    <div class="row">
      <img src="/sites/default/files/brands/natuzzi_italia_logo_165x54.png" alt="Natuzzi Italia" class="brand-img center-block">
      <div class="separator-small"></div>
    </div>
    <div class="col-md-6">
      <img class="img-responsive"
           src="<?php /*print $promo_dir;*/?>/Natuzzi-Italia-memorial-day-promo.jpg"
           alt="Save up to 20% OFF on Natuzzi Italia!">
    </div>
    <div class="col-md-6">
      <div class="">
        <h2 class="cta">BUY MORE, SAVE MORE ON NATUZZI ITALIA</h2>
        <p>Until May 29th savings up to 20% off <br/>

          10% off up to $6,000 <br/>
          15% off from $6,000 to $10,000 <br/>
          20% off over $10,000 <br/></p>

        <a href="/brand/natuzzi-italia" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Explore Collection</span></a> |
        <a href="https://www.natuzzi.com/italia/cataloghi/catalogo_2017_eng" target="_blank" class="btn btn-default" style=""><span style="font-size:18px">PDF Catalog</span></a>

      </div>
    </div>

  </div>-->

  <div class="row row-padded promo-block">
    <div class="col-md-12 text-align-center">
      <p>Our Design Consultants are here to help you find the perfect, best-fit recliners.<br/>
        Visit Us Today!</p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 center-block webform-section">
      <?php print render($content); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content" style="font-size: 1.35em;line-height: 1.5;">

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>
    <div class="col-xs-12 col-md-5 col-lg-5" >
      <div class="content event-location" style="background-color: #10232A;color:white;padding:45px"><p><strong class="standout">Visit us at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>

  <br/>

  <div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="/sites/default/files/brands/brand-slider-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="/sites/default/files/brands/brand-slider-logos/Alf-Italia-Logo.png"
                                           alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="/sites/default/files/brands/brand-slider-logos/Bauformat-German-Kitchen-Logo.png"
                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="/sites/default/files/brands/brand-slider-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="/sites/default/files/brands/brand-slider-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="/sites/default/files/brands/brand-slider-logos/Incanto-Logo.png"
                                       alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="/sites/default/files/brands/brand-slider-logos//Miele-Logo.png"
                  alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="/sites/default/files/brands/brand-slider-logos/Stressless-Logo.png"
                           alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>
  <br/>

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);

  });
</script>

<script>
  (function ($) {
      $(".slider").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: false,
        slide: 'li',
        cssEase: 'linear',
        centerMode: false,
        centerPadding: "0px",
        slidesToShow: 3,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
          breakpoint: 1200,
          settings: {
            arrows: true,
            centerMode: false,
            //centerMode: false,
            //centerPadding: '40px',
            centerPadding: '0px',
            variableWidth: true,
            slidesToShow: 5,
            dots: false

          }
        }, {
          breakpoint: 800,
          settings: {
            arrows: true,
            //centerMode: false,
            //centerPadding: '40px',
            variableWidth: true,
            slidesToShow: 3,
            dots: false,
            centerMode: false,
            centerPadding: '0px'
          }
        }],
        customPaging: function (slider, i) {
          return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
        }
      });
  })(jQuery);
</script>

