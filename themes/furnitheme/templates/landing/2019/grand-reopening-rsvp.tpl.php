<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/grand_reopening";

?>
<style>

  #content article, #content article a {
    color: #000;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page .content.intro-content {
    max-width: 900px;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
    font-size: 20px;
  }
  article.promo-landing-page h1 {
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 22px;
    color: #b2cfd4;
    text-transform: uppercase;
    text-align: center;
    line-height: 1.5em;
    margin-bottom: 20px;
    margin-left: 0!important;
    margin-top: 2em;
  }

  article.promo-landing-page img.hero-image {
    max-width: 800px;
  }

  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
  }
  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }

  .lh200 {
    line-height: 200%;
  }

  @media (max-width: 767px){
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
      padding-right: 1rem;
    }

    article.promo-landing-page .mobile-no-center {
      text-align:left!important;
    }

    article.promo-landing-page img.hero-image {
      max-width: 100%;
    }

    article.promo-landing-page .event-time {
      font-size: 18px;
      padding: 1rem 3rem;
    }

    article.promo-landing-page h1 span span {
      font-size: 26px!important;
    }

    article.promo-landing-page h2.cta {
      font-size: 2em;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }

    .hidden-sm {
      display: none;
    }
    .lh150-mobile {
      line-height: 150%!important;
    }
    .fineprint {
      padding: 0 40px;
    }
  }

</style>

<article class="promo-landing-page clearfix">

  <header>
    <span property="dc:title" content="Furnitalia Sacramento - Grand Re-Opening Event" class="rdf-meta element-hidden"></span>
  </header>

  <img class="img-responsive center-block hero-image" alt="Join us for our Grand Re-Opening!"
       src="<?php print $promo_dir; ?>/furnitalia-grand-reopening-join-us-v2.png">
       <!--src="<?php /*print $promo_dir; */?>/furnitalia-grand-reopening-join-us.png">-->


  <div class="content center-block intro-content">

    <br/>
    <h1 class="mobile-no-center" style="text-align: center;display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica,sans-serif;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;"><span
        style="font-family:helvetica neue,helvetica,arial,verdana,sans-serif"><span
          style="font-size:38px;line-height:120%;color:#a49484"><strong>PLEASE JOIN US FOR<br>
OUR GRAND RE-OPENING!</strong></span></span></h1>


    <div style="max-width: 60ch;margin:0 auto; line-height: 200%;text-align: center;" class="mobile-no-center"><br>
      <span style="font-family:helvetica neue,helvetica,arial,verdana,sans-serif"><span

          style="font-size:20px">Furnitalia — Northern California's largest source of premier home décor — celebrates their
          expansion into the new 32,000 square-foot Sacramento showroom.</span></span>

    </div>
    <br/>

    <p class="mobile-no-center" style="max-width: 60ch;text-align: center;line-height: 200%;margin: 10px auto;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica,sans-serif;font-size: 16px;"><span
        style="font-family:'helvetica neue',helvetica,arial,verdana,sans-serif"><span
          style="font-size:20px">Come experience a special evening featuring fine Italian furniture, hors d’oeuvres,
          libations, and fun including:</span><br>
          <strong><span
              style="color:#A49483;">FARM-TO-FORK CATERING <br/> WINE DEMONSTRATIONS  <br/> LIVE ACOUSTIC MUSIC</span></strong></span><br>
      <br>
      <span style="font-family:'helvetica neue',helvetica,arial,verdana,sans-serif"><span
          style="font-size:22px"><strong>THURSDAY, 9 MAY 2019 • 5–10 PM</strong></span><br>
<br>
5252 Auburn Blvd. Sacramento, CA 95841<br>
(916) 332-9000</span>
    </p>


    <div class="text-center mobile-no-center">
      <p><strong><span style="font-size:20px">You are also welcome to bid in a<br>
  silent benefit auction for prizes from:</span></strong><br>
        <span style="color:#a49484"><span style="font-size:20px;color:#A49483;">NATUZZI • BDI • CALLIGARIS<br>
  STRESSLESS • TOTAL WINE • &amp; MORE! *</span></span></span><br>
        <br>
        <span style="font-size:14px;max-width:50ch;display: inline-block;">
          <span>Proceeds will go to&nbsp;<a href="https://www.orovillehopecenter.org"
                                                                       style="text-decoration: underline;">The
              Oroville Hope Center</a> for Camp Fire Emergency Response Program for communities affected by
            the California fires.</span></span><br>
      </p>
    </div>


    <div class="text-center mobile-no-center">
      <p class=""><span style="">We appreciate the patronage of our valued clients,
referrals, and business partners.</span></p>
    </div>


    <!-- font-family: 'EB Garamond 12',serif; -->

    <!-- RSVP Form -->
    <div class="row">
      <div class="webform-section">
        <?php //print render($content); ?>

        <br/>
        <h2 class="text-center"><strong>RSVP FOR EVENT</strong></h2>
        <!-- Eventbrite RSVP -->
        <div id="eventbrite-widget-container-60101064903"></div>

        <script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>

        <script type="text/javascript">
          var exampleCallback = function() {
            console.log('Order complete!');
          };

          window.EBWidgets.createWidget({
            // Required
            widgetType: 'checkout',
            eventId: '60101064903',
            iframeContainerId: 'eventbrite-widget-container-60101064903',

            // Optional
            iframeContainerHeight: 425,  // Widget height in pixels. Defaults to a minimum of 425px if not provided
            onOrderComplete: exampleCallback  // Method called when an order has successfully completed
          });
        </script>
        <!-- End Eventbrite RSVP -->

        <br/>

      </div>
    </div>


    <div style="font-size:16px;background: #fff" class="col-md-6 col-md-push-3 text-center"><br>
      <span style=""><strong><span>Presenting Media Sponsor</span></strong></span><br>
      <span><span style="color:#a49484">SACTOWN Magazine</span></span><br>
      <br>
      <span style=""><strong><span>Promotional consideration provided by</span></strong></span><br>
      <span><span style="color:#a49484">MICHELE MOORE FROM THE CONCOURS • THE NIELLO COMPANY • THE PIANO STORE • TOTAL WINE
EURO STYLE • GINGER ELIZABETH • THE HIDDEN TABLE
MACKIE BROOKSHIRE FINE ART / SCULPTURES&nbsp;•
NICK SADEK SOTHEBY'S INTERNATIONAL REALTY</span><br>
<br>
<span style="font-size:13px"><strong>and</strong></span></span></div>

    <img align="center" class="center-block"
         alt="Special Promotional Consideration provided by Furnitalia Vendors"
         src="https://gallery.mailchimp.com/66b55ad437d9bfa2366d0d5e2/images/072f9038-24a3-43ac-91d2-f8bf47fd5f82.png"
         width="564" style="" class="mcnImage">


    <div style="text-align: center;"><span style="font-size:10px"><span>* Subject to changes and conditions.
          Please contact Furnitalia for details.</span></span></div>



  </div>

  <br/>

  <div class="row">
    <div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content lh200 lh150-mobile" style="font-size: 1.35em;">
        <div style="text-align: center;">
          <p style="text-align: left;"><span><span style="font-size:18px">Tour our newly-renovated design center, featuring a
                vast selection of fantastic home and kitchen design solutions from Italy,
                Germany, and Norway.</span></span></p>

          <p style="text-align: left;"><span><span style="font-size:18px">Experience the unrivaled style, comfort, and
          performance of the finest in contemporary design
          that's suitable for any budget.</span></span></p>

        </div>

        <br class="hidden-sm"/>

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>
    <div class="col-xs-12 col-md-5 col-lg-5" >
      <div class="content event-location" style="background-color: #10232A;color:white;padding:45px"><p><strong class="standout">Grand ReOpening takes
            place at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br class="hidden-sm"/>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>

  <br/>

  <div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="<?php print $promo_dir ?>/brand-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="<?php print $promo_dir ?>/brand-logos/Alf-Italia-Logo.png"
                                           alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="<?php print $promo_dir ?>/brand-logos/Bauformat-German-Kitchen-Logo.png"
                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="<?php print $promo_dir ?>/brand-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="<?php print $promo_dir ?>/brand-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="<?php print $promo_dir ?>/brand-logos/Incanto-Logo.png"
                                       alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="<?php print $promo_dir ?>/brand-logos/Miele-Logo.png"
                  alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="<?php print $promo_dir ?>/brand-logos/Stressless-Logo.png"
                           alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>

  <br/>

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);

  });
</script>

<script>
  (function ($) {
      $(".slider").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: false,
        slide: 'li',
        cssEase: 'linear',
        centerMode: false,
        centerPadding: "0px",
        slidesToShow: 3,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
          breakpoint: 1200,
          settings: {
            arrows: true,
            centerMode: false,
            //centerMode: false,
            //centerPadding: '40px',
            centerPadding: '0px',
            variableWidth: true,
            slidesToShow: 5,
            dots: false

          }
        }, {
          breakpoint: 800,
          settings: {
            arrows: true,
            //centerMode: false,
            //centerPadding: '40px',
            variableWidth: true,
            slidesToShow: 3,
            dots: false,
            centerMode: false,
            centerPadding: '0px'
          }
        }],
        customPaging: function (slider, i) {
          return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
        }
      });
  })(jQuery);
</script>

