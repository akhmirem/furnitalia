<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/pre-holiday-sale-2019";

?>
<style>

  .row-padded {
    padding-top: 4em;
  }

  .brand-img {
    display: block;
  }
  .separator-small {
    height: 5px;
    border-top: 3px solid #000;
    width: 250px;
    margin: 30px auto;
  }

  #content article, #content article a {
    color: #000;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page a.underline {
    text-decoration: underline;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
  }
  article.promo-landing-page p.sale-intro {
    margin: 1em auto 0;
    max-width: 57ch;
    font-size: 1.2em;
  }
  article.promo-landing-page .image-cta {
    position: relative;
  }
  @keyframes slideInFromBottom {
    0% {
      transform: translateY(100%);
    }
    100% {
      transform: translateY(0);
    }
  }
  @keyframes pulse{
    25%  {transform: scale(0.9);}
    75%  {transform: scale(1.1);}
  }

  article.promo-landing-page .image-cta .btn {
    /*animation: 1s ease-out 0s 1 slideInFromBottom;*/
    animation: pulse 0.5s ease-in;
  }
  article.promo-landing-page .image-cta .btn:hover{
    animation: pulse 0.5s ease-in infinite;
  }
  article.promo-landing-page h1 {
    font-weight: bold;
    text-transform: uppercase;
    color: #000;
    font-size: 2em;
    letter-spacing: 1.5px;
  }
  article.promo-landing-page .subtitle-emphasis {
    font-size: 1em;
    color:#ea2328;
    text-transform: capitalize;
  }
  article.promo-landing-page .sale-subtitle {
    font-size: 2em;
    line-height: 200%;
    text-transform: uppercase;
    letter-spacing: 2px;
  }
  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
    line-height: 150%;
  }
  article.promo-landing-page .promo-block p {
    font-size: 1.2em;
  }
  #content article.promo-landing-page .btn-contact,
  #container article.promo-landing-page .btn-contact {
    background: #ea2328;
    border-color: #290606;
    color: #fff;
    padding: .75em 2em;
    margin: 1em 0;
    font-size: 1.2em;
    text-transform: uppercase;
  }

  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  .webform-section {
    float: none;
  }
  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }

  .bottom-cta p {
    max-width: 50ch;
    margin: auto;
  }
  span.new-price {
    display: block;
    color: red;
    font-weight: bold;
    margin-top: .75em;
    font-size: 1.5em;
  }

  .slick-track {
    display: flex: !important;
  }
  .slick-slide {
    display: flex!important;
    align-items: center;
    justify-content: center;
  }
  .slick-slide img {
    display: flex !important;
    width: 100%;
    height: auto !important;
  }

  @media (max-width: 767px){
    body {
      /*overflow-y:scroll;*/
      /*-webkit-overflow-scrolling: touch;*/
      /*position: absolute;*/
      /*top: 0;*/
    }
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    #container article.promo-landing-page .btn-contact {
      position: static!important;
      margin-top: 1em;
      margin-bottom: 2em;
      cursor: pointer;
      padding: 0.5em 1em;
      font-size: 1em;
    }
    article.promo-landing-page .btn-contact:hover{
      animation: none;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
    }
    article.promo-landing-page .promo-block {
      padding-left:1em;
      padding-right:1em;
    }
    article.promo-landing-page h1 {
      font-size: 1.35em;
    }
    article.promo-landing-page h2.cta {
      font-size: 1.5em;
    }
    article.promo-landing-page .sale-subtitle {
      font-size: 1.35em;
      line-height: 150%;
      margin: 1em 0;
    }
    article.promo-landing-page p.sale-intro {
      margin-right: 0.5em;
      font-size: 1em;
    }
    .reg-price {
      font-size: .75em;
    }
    span.new-price {
      font-size: 1em;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }
    .bottom-cta p {
      width: 100%;
    }
  }
</style>

<article class="promo-landing-page clearfix">

  <div class="image-cta text-center">
    <a href="#contact-form" class="scroll-to-contact">
      <img class="img-responsive center-block"
           src="<?php print $promo_dir;?>/Pre-Holiday-Sale-Promo-landing-v5.jpg"
           alt="Pre-Holiday Sale: Save up to 40%">
    </a>
  </div>

  <div class="content text-center">

    <h1>PRE-HOLIDAY SALE - Save up to 40% OFF</h1>

    <div class="subtitle-emphasis">extended to Nov 4, 2019</div>

    <p class="sale-intro">
      Enhance your home style with our best selection of contemporary furnishings before the holidays -<br/>
      now through Nov 4<sup>th</sup> <br></p>

    <p class="caps">PLUS, get extra discount on floor samples!</p>

    <a href="#contact-form" class="btn btn-primary btn-contact scroll-to-contact">Contact Us!</a>

  </div>



  <?php
  $featured_items = array(
    array(
      'images' => array (
        $promo_dir . "/promo-products/calligaris-vortex-table-cropped.jpg",
        $promo_dir . "/promo-products/calligaris-omnia-wood-table2.jpg",
        $promo_dir . "/promo-products/calligaris-cartesio-table2.jpg",
      ),
      'title' => 'Calligaris',
      'subtitle' => 'Dining and Living Furniture',
      'old_price' => '',
      'new_price' => '40% OFF',
      'description' => 'floor models on sale!',
    ),
    array(
      'images' => array (
        $promo_dir . "/promo-products/calligaris-vortex-table-cropped.jpg",
        $promo_dir . "/promo-products/calligaris-omnia-wood-table2.jpg",
        $promo_dir . "/promo-products/calligaris-cartesio-table2.jpg",
      ),
      'title' => 'Bontempi',
      'subtitle' => '',
      'old_price' => '',
      'new_price' => '30% OFF',
      'description' => 'floor models on sale!',
    ),

  );
  ?>

  <?php if (false): //featured items section ?>
    <h3 class="cta">Featured</h3>
    <div class="row row-padded promo-block jumbotron">
      <div class="row">
        <img src="/sites/default/files/brands/calligaris/calligaris-logo.svg" alt="Calligaris"
             class="brand-img center-block"
             style="width: 165px;">
        <div class="separator-small" style="border-top: 3px solid #ef3340;position: relative;right: .75em;"></div>
      </div>
    </div>
    <?php foreach($featured_items as $key => $item): ?>
    <div class="row row-padded promo-block jumbotron">
      <div class="col-md-6">
        <?php if (count($item['images']) > 1): ?>
          <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none"
              data-slick-lazy="true" data-slick-adaptive="true">
            <?php foreach($item['images'] as $key => $image): ?>
              <li>
                <img src="<?php print $image;?>"
                     alt="<?php print $item['title']; ?>" class="img-responsiv e" style=""/>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php else: ?>
          <img src="<?php print $item['images'][0];?>"
               alt="<?php print $item['title']; ?>" class="img-responsive" style="max-height: 300px"/>
        <?php endif;?>
      </div>
      <div class="col-md-6">

        <h2 class="cta"><?php print $item['title']; ?></h2>
        <h3 class="cta"><?php print $item['subtitle'];?></h3>

        <p>
          <span class="reg-price">Regular: <?php print $item['old_price']; ?></span>
          <span class="new-price">SALE: <?php print $item['new_price']; ?></span>
        </p>

        <a href="https://view.publitas.com/p222-10363/calligaris-my-home-2019-magazine/" target="_blank"
           class="underline" style=""><span style="font-size:18px">View Catalog »</span></a>
        <!--<a href="/ekornes-stressless-recliners-sofas" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Explore Collection</span></a> |
        <a href="http://stressless.ekornes.com/US/totalus/" target="_blank" class="btn btn-default" style=""><span style="font-size:18px">PDF Catalog</span></a>-->

      </div>
    </div>
  <?php endforeach; ?>

  <?php endif; ?>

  <div class="row row-padded promo-block">
    <div class="col-md-12 text-center bottom-cta">
      <h2 class="cta">Have a Project in Mind?</h2>
      <p>
        Our Design Consultants are here to help you find the perfect pieces to complete your project
        or update your space with stylish modern Italian and Scandinavian Design.<br/>
        </p>
      <p>Contact us for a complementary professional interior design services! Visit our newly-renovated
        37,000 sq. ft. contemporary furnishings showroom
        in Sacramento, the largest modern Italian furniture
        gallery in Northern California.</p>
    </div>
  </div>


  <div class="row">
    <div class="col-md-6 center-block webform-section">
      <a id="contact-form"></a>
      <?php print render($content); ?>
    </div>
  </div>

  <div class="row stressless-microsite" style=""></div>

  <div class="row">
    <div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content" style="font-size: 1.35em;line-height: 1.5;">

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>
    <div class="col-xs-12 col-md-5 col-lg-5" >
      <div class="content event-location" style="background-color: #10232A;color:white;padding:45px"><p><strong class="standout">Visit us at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>


  <br/>

  <!--<div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="/sites/default/files/brands/brand-slider-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="/sites/default/files/brands/brand-slider-logos/Alf-Italia-Logo.png"
                                    alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="/sites/default/files/brands/brand-slider-logos/Bauformat-German-Kitchen-Logo.png"
                                                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="/sites/default/files/brands/brand-slider-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="/sites/default/files/brands/brand-slider-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="/sites/default/files/brands/brand-slider-logos/Incanto-Logo.png"
                                        alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="/sites/default/files/brands/brand-slider-logos//Miele-Logo.png"
                                           alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="/sites/default/files/brands/brand-slider-logos/Stressless-Logo.png"
                                                             alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>
  <br/>-->

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

  <div class="row">*Contact store for details</div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);
  });
</script>

<script>
  (function ($) {
    $(".slider").slick({
      dots: false,
      infinite: true,
      speed: 2000,
      fade: false,
      slide: 'li',
      cssEase: 'linear',
      centerMode: false,
      centerPadding: "0px",
      slidesToShow: 7,
      variableWidth: true,
      autoplay: true,
      autoplaySpeed: 5,
      responsive: [{
        breakpoint: 1200,
        settings: {
          arrows: true,
          centerMode: false,
          //centerMode: false,
          //centerPadding: '40px',
          centerPadding: '0px',
          variableWidth: true,
          slidesToShow: 7,
          dots: false,
          // autoplaySpeed:300,
          // speed:300

        }
      }, {
        breakpoint: 800,
        settings: {
          arrows: true,
          //centerMode: false,
          //centerPadding: '40px',
          variableWidth: true,
          slidesToShow: 3,
          dots: false,
          centerMode: false,
          centerPadding: '0px'
        }
      }],
      customPaging: function (slider, i) {
        return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
      }
    });

    $(".scroll-to-contact").on('click', function () {

      $([document.documentElement, document.body]).animate({
        scrollTop: $("#contact-form").offset().top
      }, 1000);

      return false;

    });


  })(jQuery);
</script>
