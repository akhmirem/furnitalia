<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/grand_reopening";

?>
<style>

  #content article, #content article a {
    color: #000;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
  }
  article.promo-landing-page h1 {
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 22px;
    color: #b2cfd4;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
    margin-left: 0!important;
    margin-top: 2em;
  }
  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
  }
  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }

  @media (max-width: 767px){
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
    }
    article.promo-landing-page h2.cta {
      font-size: 2em;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }
  }

</style>

<article class="promo-landing-page clearfix">

  <header>
    <span property="dc:title" content="Spring Sale at Furnitalia" class="rdf-meta element-hidden"></span>
  </header>

  <img class="img-responsive" typeof="foaf:Image"
       src="https://www.furnitalia.com/sites/default/files/image-straps/FURNITALIA-Landing_Page-Spring_Sale%20Graphics-1200x314-v2.png"
       alt="Spring Sale is going on now at Furnitalia!">

  <div class="content">

    <h1><span style="font-size:28px"><span style="color:#ff0000"><strong>Furnitalia - Spring Savings Happening NOW! </strong>&nbsp;</span></span></h1>

    <div><em><span style="font-size:18px;font-family: 'EB Garamond 12',serif;">April 15 – May 9</span></em></div>
    <p><span style="font-size:24px">Enjoy savings of 60% off* on our selection of contemporary floor samples. Shop early for best selection.<br>
  Visit the largest showroom of contemporary furnishings in Northern California.&nbsp;</span></p>

    <h2 class="cta">Come in today for best selection! </h2>

    <p><span
        style="font-size:24px;font-family:'EB Garamond 12',serif;letter-spacing: 1px;"><strong>Plus, sign up for&nbsp;$250 off any purchase</strong></span><br><span
        style="font-size:18px;font-style: italic;font-family:'EB Garamond 12',serif">*excludes accessories, coupon delivered by e-mail</span></p>
  </div>

  <div class="row">
    <div class="col-md-6 center-block">
      <div class="content">
        <p>
          <span style="font-size:18px">We’re making room to showcase our new Spring Collection for the</span>
          <br/>
          <span style="color:#000000;font-size:24px">
            <strong class="standout">Grand ReOpening <br/> May 10-12</strong>
          </span>
        </p>
        <p>
          <a href="/promo/grand-reopening" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Find out more!</span></a>
        </p>

      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 center-block webform-section">
      <div class="webform-cta"><!--<h2><strong>Sign Up for Gift Card!</strong></h2>
        <p><strong>Get a $250 Gift Card, </strong>delivered to your inbox.</p>-->
      </div>
      <?php print render($content); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content" style="font-size: 1.35em;line-height: 1.5;">

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>
    <div class="col-xs-12 col-md-5 col-lg-5" >
      <div class="content event-location" style="background-color: #10232A;color:white;padding:45px"><p><strong class="standout">Visit us at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>

  <br/>

  <div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="<?php print $promo_dir ?>/brand-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="<?php print $promo_dir ?>/brand-logos/Alf-Italia-Logo.png"
                                           alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="<?php print $promo_dir ?>/brand-logos/Bauformat-German-Kitchen-Logo.png"
                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="<?php print $promo_dir ?>/brand-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="<?php print $promo_dir ?>/brand-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="<?php print $promo_dir ?>/brand-logos/Incanto-Logo.png"
                                       alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="<?php print $promo_dir ?>/brand-logos/Miele-Logo.png"
                  alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="<?php print $promo_dir ?>/brand-logos/Stressless-Logo.png"
                           alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>
  <br/>

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);

  });
</script>

<script>
  (function ($) {
      $(".slider").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: false,
        slide: 'li',
        cssEase: 'linear',
        centerMode: false,
        centerPadding: "0px",
        slidesToShow: 3,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
          breakpoint: 1200,
          settings: {
            arrows: true,
            centerMode: false,
            //centerMode: false,
            //centerPadding: '40px',
            centerPadding: '0px',
            variableWidth: true,
            slidesToShow: 5,
            dots: false

          }
        }, {
          breakpoint: 800,
          settings: {
            arrows: true,
            //centerMode: false,
            //centerPadding: '40px',
            variableWidth: true,
            slidesToShow: 3,
            dots: false,
            centerMode: false,
            centerPadding: '0px'
          }
        }],
        customPaging: function (slider, i) {
          return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
        }
      });
  })(jQuery);
</script>

