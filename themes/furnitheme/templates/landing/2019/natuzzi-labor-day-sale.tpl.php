<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/natuzzi/labor_day";

?>
<style>

  .row-padded {
    padding-top: 4em;
  }

  .brand-img {
    display: block;
  }
  .separator-small {
    height: 5px;
    border-top: 3px solid #000;
    width: 250px;
    margin: 30px auto;
  }

  #content article, #content article a {
    color: #000;
  }
  a.directions-link.grand-reopening-direction-link {
    position: static;
  }
  article.promo-landing-page {
    font-family: 'Proxima Nova Rg', serif;
  }
  article.promo-landing-page .content {
    padding-left: 4rem;
  }
  article.promo-landing-page a.underline {
    text-decoration: underline;
  }
  article.promo-landing-page p {
    line-height: 1.5;
    margin-bottom: 2em;
  }
  article.promo-landing-page p.sale-intro {
    font-size: 1.5em;
    margin: 0 auto;
    max-width: 57ch;
    font-family: 'EB Garamond 12',serif;
    margin-top: 1em;
  }
  article.promo-landing-page .image-cta {
    position: relative;
  }
  @keyframes slideInFromBottom {
    0% {
      transform: translateY(100%);
    }
    100% {
      transform: translateY(0);
    }
  }
  @keyframes pulse{
    25%  {transform: scale(0.9);}
    75%  {transform: scale(1.1);}
  }

  article.promo-landing-page .image-cta .btn {
    /*animation: 1s ease-out 0s 1 slideInFromBottom;*/
    animation: pulse 0.5s ease-in;
  }
  article.promo-landing-page .image-cta .btn:hover{
    animation: pulse 0.5s ease-in infinite;
  }
  article.promo-landing-page h1 {
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 4em;
    color: #b2cfd4;
    text-transform: uppercase;
    line-height: 150%;
    margin-bottom: 20px;
    margin-left: 0!important;
    margin-top: .2em;
  }
  article.promo-landing-page .sale-subtitle {
    font-size: 2em;
    line-height: 200%;
    text-transform: uppercase;
    letter-spacing: 2px;
  }
  article.promo-landing-page .cta {
    text-transform: uppercase;
    font-weight: bold;
  }
  article.promo-landing-page h2.cta {
    font-size: 2.25em;
    line-height: 150%;
  }
  article.promo-landing-page .promo-block p {
    font-size: 1.5em;
  }

  article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
    background-image: linear-gradient(0deg, rgba(255,82,82,0.16) 25%,transparent 25%);
    font-size: 1.2em;
    padding: .1em 0;
  }
  article.promo-landing-page h3.cta a:hover, article.promo-landing-page a.category:hover {
    text-decoration: none;
    background: rgba(255,82,82,0.16);
  }
  article.promo-landing-page .standout{
    letter-spacing: 3px;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
    text-align: left;
    line-height: 1.5em;
    margin-bottom: 20px;
  }
  article.promo-landing-page .long-text {
    font-size: 20px;
    color: #4d4e4f;
    text-align: left;
    /* font-style: italic; */
    font-family: 'EB Garamond 08', serif;
    line-height: 1.5em;
  }

  ul.get-inspired {
    margin: 0;
    list-style-type: none;
    line-height: 2;
  }

  .webform-section {
    float: none;
  }
  #requestFormContainer form {
    width: 100%;
    box-sizing: border-box;
  }
  .webform-section h3 {
    letter-spacing: 3px!important;
  }

  .bottom-cta p {
    max-width: 50ch;
    margin: auto;
  }

  @media (max-width: 767px){
    body {
      /*overflow-y:scroll;*/
      /*-webkit-overflow-scrolling: touch;*/
      /*position: absolute;*/
      /*top: 0;*/
    }
    article.promo-landing-page {
      font-size: 18px;
      color: #000;
    }
    article.promo-landing-page .image-cta .btn {
      position: static!important;
      margin-top: 1em;
      margin-bottom: 2em;
      cursor: pointer;
    }
    article.promo-landing-page .image-cta .btn:hover{
      animation: none;
    }
    article.promo-landing-page .content {
      padding-left: 1rem;
    }
    article.promo-landing-page .promo-block {
      padding-left:1em;
      padding-right:1em;
    }
    article.promo-landing-page h1 {
      font-size: 1.7em;
    }
    article.promo-landing-page h2.cta {
      font-size: 1.5em;
    }
    article.promo-landing-page .sale-subtitle {
      font-size: 1.35em;
      line-height: 150%;
      margin: 1em 0;
    }
    article.promo-landing-page p.sale-intro {
      text-align: left;
      margin-right: 0.5em;
    }
    #requestFormContainer {
      display: block;
      margin-left: .5em;
      padding-right: 0;
      width: 95vw;
      border: none;
      background: #afddda;
      margin-bottom: 3.5em;
    }
    #requestFormContainer h3.furn-red {
      margin-top: 1em !important;
      font-size: 1.5em!important;
    }

    #requestFormContainer form .form-control {
      width: 90%;
      padding: .7em;
      font-size: 1.2em;
    }

    #requestFormContainer form input.form-submit {
      background-color: #ff0202;
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-size: 18px;
      line-height: 30px;
      border-radius: 20px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border: 0;
      text-shadow: #910909 0 -1px 0;
      padding: .3em 1em;
    }
    article.promo-landing-page h3.cta a, article.promo-landing-page a.category {
      color: #000;
      font-size: 1em;
    }
    ul.get-inspired {
      padding: 0 0 1em 1em;
    }
    .bottom-cta p {
      width: 100%;
    }
  }

</style>

<article class="promo-landing-page clearfix">

  <div class="image-cta text-center">
    <a href="#contact-form">
      <img class="img-responsive center-block"
           src="<?php print $promo_dir;?>/Labor-Day-Natuzzi-Editions-hero-image-v2.jpg"
           alt="Labor Day Sale is on now at Furnitalia!">
    </a>
    <a href="#contact-form" class="btn btn-primary btn-contact" style="
    background: #ba9054;
    border-color: #8e724a;
    color: #fff;
    padding: .75em 2em;
    margin: 1em 0;
    "><span style="font-size:18px;">Get extra 10% Coupon!</span></a>
  </div>

  <div class="content text-center">

    <h1><span style="color:#ba9054"><strong>Labor Day Sale</strong>&nbsp;</span></h1>

    <div><em><span style="font-size:1.5em;font-family: 'EB Garamond 12',serif;">ends September 4, 2019</span></em></div>

    <p class="sale-intro"><span>Enjoy limited-time special savings on our selection of Natuzzi Editions floor samples.
        Create an inviting and modern living room effortlessly with our collection of contemporary sectionals, sofas,
        loveseats, and chairs &mdash; all on sale! Now is the perfect time for a living room makeover!

    <p class="sale-intro"><span>
        Contact us for FREE professional interior design services! Visit our newly-renovated
        37,000 sq. ft. contemporary furnishings showroom
        in Sacramento, the largest Natuzzi and Stressless furniture
        gallery in Northern California.&nbsp;</span></p>

    <p class="sale-intro"><span>Our designers are here to showcase our large selection of sofas, sectionals, recliners,
        dining tables, kitchen cabinets, and bedroom furniture on sale. </span></p>

    <h3 class="cta">Shop early for best selection! </h3>

  </div>

  <div class="row promo-block jumbotron">
    <div class="row">
      <img src="/sites/default/files/brands/natuzzi_editions_logo_165x54.png" alt="Natuzzi Editions"
           class="brand-img center-block">
      <div class="separator-small"></div>
    </div>
    <div class="col-md-6">
      <img class="img-responsive"
           src="<?php print $promo_dir;?>/furnitalia-labor-day-natuzzi-editions.jpg"
           alt="Natuzzi Editions collection - Labor Day Sale!">
    </div>
    <div class="col-md-6 center-block">
      <div class="">
        <h2 class="cta">Natuzzi Editions Sale</h2>
        <p>Choose from a large selection of contemporary Natuzzi Editions
          <a href="/natuzzi-editions/living/sofas" class="underline">sofas</a>,
          <a href="/natuzzi-editions/living/sectionals" class="underline">sectionals</a>, and
          <a href="/natuzzi-editions/living/chairs" class="underline">armchairs</a>, well-known for their
          complete comfort, style, and versatile configurations &mdash; a perfect fit for your living room.
          Available in-stock, quick ship, or in custom configurations. Stop by to see our renovated
          Natuzzi Editions gallery, the largest in Northern California!</p>

        <a href="/brand/natuzzi-editions" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Explore Collection</span></a> |
        <a href="https://view.publitas.com/p222-10363/natuzzi-editions-quck-time-catalog-april-2017-ro4yjqrejry4/" target="_blank" class="btn btn-default" style=""><span style="font-size:18px">Quick Ship PDF Catalog</span></a>
      </div>
    </div>
  </div>

  <!--<div class="row row-padded promo-block jumbotron">
    <div class="row">
      <img src="/sites/default/files/brands/natuzzi_italia_logo_165x54.png" alt="Natuzzi Italia" class="brand-img center-block">
      <div class="separator-small"></div>
    </div>
    <div class="col-md-6">
      <img class="img-responsive"
           src="<?php /*print $promo_dir;*/?>/Natuzzi-Italia-memorial-day-promo.jpg"
           alt="Save up to 20% OFF on Natuzzi Italia!">
    </div>
    <div class="col-md-6">
      <div class="">
        <h2 class="cta">BUY MORE, SAVE MORE ON NATUZZI ITALIA</h2>
        <p>Until May 29th savings up to 20% off <br/>

          10% off up to $6,000 <br/>
          15% off from $6,000 to $10,000 <br/>
          20% off over $10,000 <br/></p>

        <a href="/brand/natuzzi-italia" class="btn btn-primary" style="background: #ea2328;border-color: #77181a;color:#fff"><span style="font-size:18px">Explore Collection</span></a> |
        <a href="https://www.natuzzi.com/italia/cataloghi/catalogo_2017_eng" target="_blank" class="btn btn-default" style=""><span style="font-size:18px">PDF Catalog</span></a>

      </div>
    </div>

  </div>-->


  <div class="row row-padded promo-block">
    <div class="col-md-12 text-center bottom-cta">
      <h2 class="cta">Have a Project in Mind?</h2>
      <p>
        Our Design Consultants are here to help you find the perfect pieces to complete your project
        or update your space with stylish modern Italian and Scandinavian Design.<br/>
        Visit Us Today!</p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 center-block webform-section">
      <a id="contact-form"></a>
      <?php print render($content); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-md-7 col-lg-7">
      <div class="content" style="font-size: 1.35em;line-height: 1.5;">

        <div><span style="font-weight: bold;">GET INSPIRED!</span></div>
        <ul style="" class="get-inspired">
          <li><a href="https://www.furnitalia.com/collections" class="category">Contemporary Furnishings</a></li>
          <li><a
              href="https://www.furnitalia.com/modern-european-kitchen-cabinets" class="category">Luxury Kitchens &amp; Baths</a></li>
          <li><a
              href="https://www.furnitalia.com/miele-appliances" class="category">Appliances</a></li>
        </ul>

      </div>
    </div>
    <div class="col-xs-12 col-md-5 col-lg-5" >
      <div class="content event-location" style="background-color: #10232A;color:white;padding:45px"><p><strong class="standout">Visit us at our Flagship Sacramento Showroom:</strong></p>
        <p>5252 Auburn Boulevard<br>
          Sacramento, California 95841</p>
        <p>Phone: (916) 332-9000<br>
          Hours: Monday – Sunday<br>
          10:00 am – 6:00 pm<br>
          &nbsp;</p>
        <p><a class="directions-link furn-button-text grand-reopening-direction-link"
              href="https://maps.google.com/maps?saddr=&amp;daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&amp;z=11"
              target="_blank">GET DIRECTIONS</a></p>
      </div>
    </div>
  </div>

  <br/>

  <div class="content">
    <h3 class="cta"><a href="/brands" style="">Check out our brands!</a></h3>
    <div class="brands-slideshow">
      <ul class="slider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none;background: #fff"
          data-slick-lazy="false" data-slick-arrows="true" data-slick-nodots="true" data-slick-slidesToShow="3"
      >
        <li>
          <a href="/brand/natuzzi-italia"><img src="/sites/default/files/brands/brand-slider-logos/Natuzzi-Logo.png"
                                               alt="Natuzzi Italia" style=""/></a>
        </li>
        <li>
          <a href="/brand/alf"><img src="/sites/default/files/brands/brand-slider-logos/Alf-Italia-Logo.png"
                                    alt="Alf Italia" style=""/></a>
        </li>
        <li>
          <a href="/modern-european-kitchen-cabinets"><img src="/sites/default/files/brands/brand-slider-logos/Bauformat-German-Kitchen-Logo.png"
                                                           alt="Bauformat German Kitchen Cabinets" style=""/></a>
        </li>
        <li>
          <a href="/brand/bontempi"><img src="/sites/default/files/brands/brand-slider-logos/Bontempi-Logo.png"
                                         alt="Bontempi" style=""/></a>
        </li>
        <li>
          <a href="/brand/calligaris"><img src="/sites/default/files/brands/brand-slider-logos/Calligaris-Logo.png"
                                           alt="Calligaris" style=""/></a>
        </li>
        <li>
          <a href="/brand/incanto"><img src="/sites/default/files/brands/brand-slider-logos/Incanto-Logo.png"
                                        alt="Incanto" style=""/></a>
        </li>
        <li>
          <a href="/miele-appliances"><img src="/sites/default/files/brands/brand-slider-logos//Miele-Logo.png"
                                           alt="Miele" style=""/></a>
        </li>
        <li>
          <a href="/ekornes-stressless-recliners-sofas"><img src="/sites/default/files/brands/brand-slider-logos/Stressless-Logo.png"
                                                             alt="Stressless" style=""/></a>
        </li>
      </ul>
    </div>
  </div>
  <br/>

  <div class="row">
    <div class="map2" style="border:2px solid #981b1e; height: 450px; margin: 0 2em;">
      <?php include_once DRUPAL_ROOT . "/static/map.html";?>
    </div>
  </div>

</article>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = '/sites/all/themes/furnitheme/fonts/natuzzi-landing-fonts.css';
    link.type = 'text/css';
    var godefer = document.getElementsByTagName('link')[0];
    godefer.parentNode.insertBefore(link, godefer);

  });
</script>

<script>
  (function ($) {
    $(".slider").slick({
      dots: false,
      infinite: true,
      speed: 2000,
      fade: false,
      slide: 'li',
      cssEase: 'linear',
      centerMode: false,
      centerPadding: "0px",
      slidesToShow: 7,
      variableWidth: true,
      autoplay: true,
      autoplaySpeed: 5,
      responsive: [{
        breakpoint: 1200,
        settings: {
          arrows: true,
          centerMode: false,
          //centerMode: false,
          //centerPadding: '40px',
          centerPadding: '0px',
          variableWidth: true,
          slidesToShow: 7,
          dots: false,
          // autoplaySpeed:300,
          // speed:300

        }
      }, {
        breakpoint: 800,
        settings: {
          arrows: true,
          //centerMode: false,
          //centerPadding: '40px',
          variableWidth: true,
          slidesToShow: 3,
          dots: false,
          centerMode: false,
          centerPadding: '0px'
        }
      }],
      customPaging: function (slider, i) {
        return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
      }
    });

    $(".btn-contact").on('click touchstart', function () {

      $([document.documentElement, document.body]).animate({
        scrollTop: $("#contact-form").offset().top
      }, 1000);

      return false;

    });
  })(jQuery);
</script>