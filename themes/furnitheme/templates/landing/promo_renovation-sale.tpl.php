<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/renovation_sale";

if ($is_desktop) {
    $img_path = 'furnitalia-renovation-sale-mar2018.mp4';
}
else {
    $img_path = 'furnitalia-renovation-sale-mar2018.mp4';
}
$alt = "Renovation Sale! 20% - 60% OFF select floor models!";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<link href="https://vjs.zencdn.net/5.19.1/video-js.css" rel="stylesheet">
<style>
    .video-js .vjs-big-play-button {
        left: 10.5em;
        top: 6em;
    }
    <?php if(!$is_desktop): ?>
    .video-js .vjs-big-play-button {
        left: 4em;
        top: 2em;
    }
    <?php endif;?>
</style>

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; '>

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 2em;
    font-size: 4em;color:#cd431f;
    margin: 0;
    padding: 0;
    <?php print (!$is_desktop ? 'font-size:3em;line-height:130%;' : ''); ?>">RENOVATION SALE!</h2>

    <div style="    font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 2.5em;
    padding-bottom: 1em;
    <?php print (!$is_desktop ? 'line-height: 100%;margin-top: .5em;' : ''); ?>">20% - 60% OFF SELECT FLOOR MODELS.</div>



    <video id="video_2" class="video-js vjs-fluid mid-video with-audio with-play control-audio"
           preload="auto" muted
           loop playsinline data-setup='{"fluid": true, "autoplay": false, "controls": true}'
           poster="<?php print $promo_dir; ?>/renovation-sale-2-poster.png">
        <source src="<?php print $promo_dir . '/' . $img_path; ?>"
                type='video/mp4'>
    </video>




    <p class="furn-red" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;">For a limited time,</p>

    <p class="" style='font-family: "Signica","Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 1.7em;
    line-height: 1.42858;
    color: #857874;
    font-weight: bold;
    text-align: center;
    margin-top: -1em;
    margin-bottom: 1em;
    '>Extra <strong class="furn-red">25% OFF</strong> already discounted floor models</p>

    <p style="font-size:1.1em; line-height:150%">Furnitalia showroom is your ultimate destination for modern and contemporary leather furniture in Sacramento and Northern California. We offer a large selection of modern furniture collections from Natuzzi Italia, Alf Italia, Bontempi, Stressless, Cattelan, and others.</p>

    <p style="font-size: 1.1em;
    line-height: 150%;">
        Now’s the perfect time to fill your home with comfort. Because now, for a limited time, you can
        get the best selection of specially marked floor models with 20% - 60% discount! <br/>
        Save on Style + Quality throughout the Showroom. <br/>
        Furnitalia - Your Source for Contemporary European Design.
    </p>


    <p style="font-size: 1em;">Exclusions apply. See sales associate for complete details.</p>

    <br/>

    <p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
        Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
    </p>

    <p style="text-align: center">
        <a href="<?php print base_path();?>collections" class="request"  title="Browse collection"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Browse collection
        </a>
    </p>

    <?php print render($content); ?>

</article>

<script src="<?php print $base_folder; ?>/js/videojs.min.js"></script>

<script>

    (function($) {

        var isMobile = false;
        <?php if(!$is_desktop): ?>
            isMobile = true;
        <?php endif; ?>

        handleVideoJSPlugin = function() {
            var o = $.Deferred()
                , e = function() {
                setTimeout(function() {
                    o.resolve()
                }, 1e3)
            };
            $.each($(".video-js"), function(o, t) {
                var i = videojs(this.id);
                i.ready(function() {
                    var s = $(this.el_);
                    if (s.data("bvPlayer", this),
                            s.hasClass("with-audio")) {
                        var a = $(".change_color_label").hasClass("text_white") ? "control-audio-white" : "";
                        s.append('<div class="control-audio ' + a + ' off"></div>')
                    }
                    s.hasClass("with-play") && (s.append('<div class="goplay"></div>'),
                        this.on("play", function() {
                            s.find(".goplay").fadeOut()
                        }),
                        this.on("pause", function() {
                            s.find(".goplay").fadeIn()
                        })),
                        s.on("click tap", function(o) {
                            var e = $(o.target);
                            e.hasClass("control-audio") ? (e.toggleClass("on off"),
                                i.muted(e.hasClass("off"))) : i.paused() ? i.play() : i.pause()
                        }),
                        /*$(window).on("scroll", function() {
                            var o = $(t).first().offset().top
                                , e = $(t).first().height();
                            isMobile || ($(window).scrollTop() > o - e / 2 && $(window).scrollTop() < o + e / 2 ? i.paused() && !$(i.el_).hasClass("video-mobile") && i.play() : i.pause())
                        }),*/
                    "undefined" != typeof _controlVideo && _controlVideo(s),
                    o || e()
                }, !1),
                    i.on("error", function() {
                        container.remove(),
                            e()
                    })
            })
        };

        $(function () {
            //handleVideoJSPlugin();
        });

    })(jQuery);
</script>


