<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/garda_promo";

if ($is_desktop) {
    $img_path = 'Garda-hero-img-desktop.png';
}
else {
    $img_path = 'Garda-hero-img-mobile.png';
}
$alt = "Receive up to $1,500 credit toward Ekornes Stressless purchase!";
?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet">
<!--<link href="https://fonts.googleapis.com/css?" rel="stylesheet">-->

<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; '>

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;
    line-height: 2em;
    font-size: 2.7em;color:#cd431f;     margin: 0;
    padding: 0;">RECEIVE $500 OFF*</h2>

    <div style="    font-family: 'Source Sans Pro', sans-serif;
    text-align: center;
    margin-top: -.2em;
    font-size: 1.5em;
    padding-bottom: 1em;">your purchase of Stressless Garda in select colors.</div>



    <img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>"/>


    <p class="" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;color:#cd431f;">NOW - November 13, 2017.</p>

    <p class="" style='font-family: "Signica","Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 16px;
    line-height: 1.42858;
    color: #857874;
    font-weight: bold;
    text-align: center;
    margin-top: -1em;
    margin-bottom: 2em;
    '>we've cut the cost, not the comfort</p>

    <p style="font-size: 1.1em;
    line-height: 150%;">
        Now’s the perfect time to fill your home with Stressless comfort. Because now, for a limited time, you can
        purchase a Stressless Garda recliner and ottoman with our timeless Classic base in your choice of Paloma Black,
        Chocolate, Light Grey, Metal Grey or Sand leathers with a savings of $500! Then save even more by completing
        your room with an Ekornes Manhattan or Oslo sofa. Right now, they’re on sale in all Paloma colors at up to
        25% OFF. Ultimate comfort awaits you. Don’t miss this opportunity to get it at a great price!

    </p>
    <br/>
<!--    <img src="--><?php //print $promo_dir; ?><!--/GardaClassic-thumb.jpg" alt="Stressless Garda with Classic Base shown in Paloma Sand with Black Base"/>-->
    <?php if ($is_desktop) : ?>
        <img src="<?php print $promo_dir; ?>/save500_banner.png" alt="" style="width: 720px"/>
        <p style="font-size: 1em;
    font-style: italic">
            Stressless Garda with Classic Base shown in Paloma Sand with Black Base.
        </p>
    <?php else: ?>
        <img src="<?php print $promo_dir; ?>/save500_banner_mobile.png" alt=""/>
    <?php endif; ?>

    <ul>
        <li>
            Large - W:33.5", H: 39.4", D: 30.3"</li>
        <li>Medium - W: 29.9", H: 39.4", D: 28.0"</li>
        <li> Small - W: 28.3", H: 37.0", D: 27.6"</li>
    </ul>



    <strong>Stressless Garda – a classic, elegant recliner</strong>

    <p style="font-size: 1em;">
        Stressless Garda is a classic recliner made to fit any home. Soft polyester fibers and molded foam with Comfort Zones give it that extra touch of luxury and an attractive look. Choose between one of 5 of our most popular leather colors and 8 different wood finishes to find the perfect complement to your home.
    </p>
    <br/>
    <p style="font-size: 1em;">Complete your room with <strong>Special Savings</strong> on select sofas.
        <strong>Save up to 25%</strong> on Ekornes Manhattan and Oslo sofas in all Paloma Leather Colors.</strong>
    </p>

    <?php if ($is_desktop) : ?>
        <img src="<?php print $promo_dir; ?>/ekornes_sofas_banner_desktop.png" alt="" style="width: 720px"/>
    <?php else: ?>
        <img src="<?php print $promo_dir; ?>/ekornes_sofas_banner_mobile.png" alt="" style="width: 320px"/>
    <?php endif; ?>



    <p style="font-size: 1em;">Exclusions apply. See sales associate for complete details.</p>

    <br/>

    <p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
        Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
    </p>

    <p style="text-align: center">
        <a href="<?php print base_path();?>brand/stressless-ekornes" class="request"  title="Shop Stressless"
           style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
            Shop Stressless
        </a>
    </p>

    <?php print render($content); ?>

</article>



