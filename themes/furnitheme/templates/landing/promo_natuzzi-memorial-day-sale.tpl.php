<?php
    global $conf;
    if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
        $is_desktop = FALSE;
    } else {
        $is_desktop = TRUE;
    }
    $promo_dir = base_path() . "sites/default/files/promo/memorial_day";

    if ($is_desktop) $img_path = 'natuzzi_memorial_day_desktop_landing.png';
    else $img_path = 'natuzzi_memorial_day_mobile_landing.png';
?>

<h2 class="furn-red" style="text-transform: uppercase;
    letter-spacing: 2px;
    text-align: center;line-height: 2em;"><span style="font-size:1.6em ">Memorial Day Sale. </span><br/>Complete living solutions – now on Sale!</h2>


<img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="Memorial Day Sale - Great Savings - Until June 11th"/>

<p style="font-size: 1.3em;
    line-height: 130%;">
    Take advantage of our Memorial Day sale. We are offering the best of Italian design and craftsmanship at special prices.
    Sofas, armchairs, lamps, rugs, dining tables, chairs, coffee tables, wall units, beds, bed linen and accessories.
    Exceptional quality, innovation, versatility and variety at great prices make Natuzzi Italia and Natuzzi Editions
    the style destination when shopping for your home. Furnish your home with all the creativity of Italian style.
    We look forward to seeing you soon in Furnitalia!
</p>

<p style="font-size: 1em;
    line-height: 130%;
    font-weight: bold;
    color: #5e5757;">
Fill in the form to receive more info, ask for prices of your favorite models or book an appointment.
</p>

*Complete conditions in store. This offer runs until June 11th, 2017.

<p style="text-align: center">
    <a href="<?php print base_path();?>natuzzi-editions/living" class="request"  title="Shop Natuzzi Editions"
       style="padding: 17px 10px;
            background: #981b1e;
            display: inline-block;
            color: white;
            text-decoration: none;
            text-rendering: optimizeLegibility;
            font-size: 22px;
            margin: 10px auto;
            border-radius: 7px;
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;">
        Shop Natuzzi Editions
    </a>
</p>
