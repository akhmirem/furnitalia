<!-- <a href="http://www.mieleconcierge.com/direct-seller/?ma=A9164840333"><img src="http://www.mieleconcierge.com/assets/images/mieledirectseller_150_x_110.png" alt="Shop online with Miele" /></a>
<hr/>
<a href="http://www.mieleconcierge.com/direct-seller/?ma=A9166352504" -->

<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$theme_path = base_path() . drupal_get_path('theme', 'furnitheme');
$promoPathPrefix = base_path() . variable_get('file_public_path', conf_path() . '/files');

?>

<script language="javascript">

  var scrollToIframe = false;

  function scrollToTop() {
    var iframeElem = jQuery(".iframe");
    var target = iframeElem.offset().top;
    var interval = setInterval(function() {
      if (jQuery(window).scrollTop() >= target) {
        console.log("scrolled past it");
        scrollToIframe = true;
        clearInterval(interval);
      }
    }, 250);

    //scroll(0,0);

    if (scrollToIframe) {
      //scroll to newly inserted content
      var offset = iframeElem.offset();
      var scrollTarget = iframeElem;
      while (scrollTarget.scrollTop() === 0 && scrollTarget.parent()) {
        scrollTarget = scrollTarget.parent();
      }
      // Only scroll upward
      scrollTarget.animate({scrollTop: (offset.top - 5)}, 500);
    }
  }
</script>

<div class="container">

<!--    <h1 class="page-title">Kalora Rugs and Furnishings Collection</h1>-->

    <!-- landing page main content -->
    <div class="iframe">
      <iframe id="ekornesIframe" src="https://www.kalora.com/current-collections" class="loading loading-top"
              frameborder="0" width="100%" height="3500"
              scrolling="auto" onload="scrollToTop();"></iframe>
    </div>

    <!-- footer tiles -->
    <div class="row landing-page-footer-tiles">
        <div class="span4 footer-tile">
            <div class="icon">
                <i class="fa fa-envelope"></i>
            </div>
            <div class="tile-content">
                <a href="<?php print base_path();?>request/ajax/miele" class="request" id="request-info" title="Contact Us for more information about Kalora">Contact us</a>
                <p>If you have any questions regarding purchasing Kalora Rugs, please do not hesitate to contact us directly.</p>
            </div>
        </div>

        <div class="span4 footer-tile">         &nbsp;
		<?php if (!$is_desktop) : ?>
		    <section id="requestFormSection"></section>
		<?php endif; ?>
        </div>
    </div>

</div>

