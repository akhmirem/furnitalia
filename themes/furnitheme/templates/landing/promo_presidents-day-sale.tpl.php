<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/presidents_day/2019";
$theme_path = drupal_get_path('theme', 'furnitheme');

if ($is_desktop) {
  $promo_images = array(
      array('url' => 'NE-Presidents-Day-Banner.jpg', 'Save 20% OFF on Natuzzi Editions!'),
      array('url' => 'NI-Presidents-Day-Banner.jpg', 'Save 15% OFF on Natuzzi Italia!'),
  );
} else {
  $promo_images = array(
      array('url' => 'NE-Presidents-Day-Banner-Mobile.jpg', 'Save 20% OFF on Natuzzi Editions!'),
      array('url' => 'NI-Presidents-Day-Banner-Mobile.jpg', 'Save 15% OFF on Natuzzi Italia!'),
  );
}
$alt = "Presidents\' Day Sale - Special Offers!";

$base_folder = base_path() . "sites/all/themes/furnitheme/templates/landing/natuzzi_thanksgiving_sale";

?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Lato|Abril+Fatface" rel="stylesheet">
<style>

  article {
    color: #444;
    font-family: "Lato", Helvetica, Arial, sans-serif;
    padding-left: 4em;
  }

  .sale-caption-section {
    max-width: 1024px;
    color: #6f2e3e;
  }

  .sale-caption-section h2 {
    line-height: 1;
    float: left;
    background: rgb(22, 53, 110);
    padding: 1em 2.5em;
  }
  h2.stressless-orange:before, h2.stressless-orange:after {
    content: "\2605 \2605 \2605 \2605 \2605";
    display: block;
    color: #fff;
    font-size: 1.2em;
    letter-spacing: .75em;
    text-align: center;
    margin-top: .3em;
  }
  h2.stressless-orange:before {
    margin-bottom: .5em;
  }

  .sale-caption-section {
    display: flex;
  }
  .sale-caption-section h2 .sale-title {
    font-size: 1.25em;
    color: #fff;
    font-family: "Source Sans", sans-serif;
    display: block;
    border: 4px solid white;
    border-left: none;
    border-right: none;
    padding: .4em 0;
  }
  .sale-caption-section h2 .sale-title .sale-sale {
    display: block;
    font-size: 3.0em;
  }
  .sale-caption-section section.sale-subtitle {
    margin: 1em 0 0 1.5em;
    float: left;
    display: block;
    font-size: 3.5em;
    line-height: 1.2em;
    font-family: Garamond, serif;
    font-style: italic;
  }

  .sale-caption-section img {
    height: 110px;
    margin-left: 50px;
    float: right;
    padding-top: 1em;
  }

  .sale-caption-section .subcation {
    clear: both;
    margin-bottom: 2em;
    font-size: 1.45em;
  }

  .promo-description-text {
    display: flex;
    max-width: 1400px;
    margin-top: 2em;
  }

  .promo-description-text p {
    border-left: 1px solid #ccc;
    padding-left: 1em;
    padding-right: 1em;
  }

  .promo-description-text p:first-child {
    border: none;
    padding-left: 0;
  }

  .promo-description-container a.promo-cta {
    padding: 17px 20px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    border-radius: 7px;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    justify-self: left;
    margin-top: 1em;
  }

  p {
    font-size: 1.5em;
    line-height: 150%
  }

  .bold {
    font-weight: bold;
  }

  .promo-hero-img {
    max-width: 1400px;
    position: relative;
  }


  .snap-block {
    width: 100%;
    padding: .7em 1em;
    font-size: 3.5em;
    color: #fff;
    background: #851B22;
    background-image: linear-gradient(to right, #851B22, rgb(218, 108, 24));
    font-family: "Source Sans", sans-serif;
    line-height: 1.2em;
  }

  em.underline {
    display: block;
    padding-bottom: .7em;
    /*border-bottom: 1px solid #444;*/
  }

  <?php if(!$is_desktop): ?>

  article {
    padding: 0 1em;
  }

  .promo-hero-img {
    width: auto;
  }

  #requestFormContainer {
    margin-left: 0;
    padding-right: 0;
  }

  .promo-hero-img img {
    width: auto;
    max-width: 100%;
  }

  .promo-description-text {
    margin-bottom: 1em;
    flex-wrap: wrap;
  }

  div.promo-description-container a.request {
    width: 100%;
  }

  #requestFormContainer h3.furn-red {
    margin-top: 1em !important;
  }

  #requestFormContainer form .form-control {
    width: 90%;
    padding: .7em;
    font-size: 1.2em;
  }

  #requestFormContainer form input.form-submit {
    background-color: #ed8223;
    color: #fff;
    font-family: 'Helvetica Neue', sans-serif;
    font-size: 18px;
    line-height: 30px;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border: 0;
    text-shadow: #C17C3A 0 -1px 0;
    padding: .3em 1em;
  }

  <?php endif;?>


  span.separator {
    display: block;
    height: 5px;
    border-top: 5px solid #000;
    width: 100px ;
    margin: 30px auto;
    margin-left: 1.5em;
  }


  @supports (display: grid) {
    span.separator {
      margin: 30px auto;
    }
    div.promo-description-text {
      display: grid;
      grid-template-columns: 1fr 1fr;
    }
    div.promo-description-text p {
      display: grid;
      grid-template-rows: min-content min-content auto min-content;
    }
    div.promo-description-text p > img {
      grid-row: 1/2;
      padding: 2em 0;
      justify-self: center;
    }
    div.promo-description-text p > em {
      grid-row: 2/3;
      margin-bottom: 1.5em;
      justify-self: center;
      margin-top: -1em;
      letter-spacing: 3px;
      font-size: 1.25em;
      font-family: "Source Sans", sans-serif;
      font-style: normal;
    }
    div.promo-description-text p > span {
      grid-row: 3/4;
    }
    div.promo-description-text p > a {
      grid-row: 4/5;
    }

    @media (max-width: 767px) {
      div.promo-description-text {
        grid-template-columns: 1fr;
      }
    }
    
  }


  @media (max-width: 767px) {

    .sale-caption-section {
      flex-wrap: wrap;
    }
    .sale-caption-section section.sale-subtitle {
      margin: .5em 0;
      font-size: 2.5em;
    }
    .promo-description-text p {
      border: none;
      padding: 0;
    }
    .snap-block {
      font-size: 1.8em;
    }
  }




</style>

<article>

  <div class="sale-caption-section clearfix">
    <h2 class="stressless-orange">
      <span class="sale-title" style="">PRESIDENTS' DAY <span class="sale-sale">SALE!</span></span> </span>
    </h2>
    <section class="sale-subtitle">
      Discover new home furnishing inspirations at a special price, now through February 26.
    </section>
  </div>

  <div class="promo-hero-img">


    <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none" data-slick-lazy="true">
      <?php
      foreach ($promo_images as $img) {
        print '<li><img data-lazy="' . $promo_dir . '/' . $img['url'] . '" alt="' . $img['alt'] . '" ></li>';
      }

      ?>
    </ul>

  </div>

  <div class="promo-description-container">

    <div class="snap-block">
      Great deals for every room. Visit us in-store.
    </div>

    <div class="promo-description-text">
      <p>

        <img src="<?php print base_path() . "sites/default/files/brands/natuzzi_editions_logo_165x54.png"; ?>" alt="Natuzzi Editions logo" />

        <em class="bold underline">
          <span class="separator"></span>
          20% OFF
        </em>

        <span>
          Add comfort to your home with great deals during Presidents' Day Sale. <br/>

          Right now, get the best offers on Natuzzi Editions sofas,
          sectionals, and armchairs. <br/>
        </span>

        <a href="<?php print base_path(); ?>natuzzi-editions/living" class="request promo-cta"
           title="Browse Natuzzi Editions collection" style="">
          Browse Natuzzi Editions
        </a>

      </p>

      <p>

        <img src="<?php print base_path() . "sites/default/files/brands/natuzzi_italia_logo_165x54.png"; ?>" alt="Natuzzi Editions logo" />

        <em class="bold underline">
          <span class="separator"></span>
          15% OFF
        </em>

        <span>
          Natuzzi Italia upholstery furniture offers amazing versatility, with 80+ leathers, 130+ fabrics, and
          countless configurations. <br/>

          Contemporary sofas, sectionals, recliners, beds - there are plenty
          of models on display in our Sacramento showroom. Come and test Natuzzi quality in-store!<br/>
        </span>

        <a href="<?php print base_path(); ?>natuzzi-italia/living" class="request promo-cta"
           title="Browse Natuzzi Italia collection" style="">
          Browse Natuzzi Italia
        </a>
      </p>

    </div>


  </div>

  <?php print render($content); ?>

</article>


<script>

  (function ($) {


    $(function () {
      //handleVideoJSPlugin();
    });

  })(jQuery);
</script>


