

<?php
//    global $theme_path;
    $bdi_landing = base_path() . "sites/default/files/promo/bdi/office_sale";
    $theme_path = base_path() . drupal_get_path('theme', 'furnitheme');

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/bdi/office_sale";

if ($is_desktop) {
    $img_path = 'BDI-office-sale-fall-2018-desktop-promo.jpg';
    $promo_images = array(
        array('url' => 'BDI-office-sale-fall-2018-desktop-promo.jpg', 'BDI Sequel Lift Desk - Office Furniture Sales Event'),
        array('url' => 'BDI-office-sale-fall-2018-desktop-promo-corridor.jpg', 'BDI Corridor Desk - Office Furniture Sales Event'),
        array('url' => 'BDI-office-sale-fall-2018-desktop-promo-sequel.jpg', 'BDI Sequal Office Desk - Office Furniture Sales Event'),
    );
}
else {
    $img_path = 'BDI-office-sale-fall-2018-mobile-promo.jpg';
    $promo_images = array(
        array('url' => 'BDI-office-sale-fall-2018-mobile-promo.jpg', 'BDI Sequel Lift Desk - Office Furniture Sales Event'),
        array('url' => 'BDI-office-sale-fall-2018-mobile-promo-corridor.jpg', 'BDI Corridor Desk - Office Furniture Sales Event'),
        array('url' => 'BDI-office-sale-fall-2018-mobile-promo-sequel.jpg', 'BDI Sequal Office Desk - Office Furniture Sales Event'),
    );
}

?>

<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=PT+Sans|PT+Sans+Narrow|Raleway:400,200,300,500,600,100"
      xmlns="http://www.w3.org/1999/html"/>
<style type="text/css">
    .bdi-box {
        font-family: Raleway, sans-serif;
        font-size: 20px;
        letter-spacing: .07em;
        <?php print $is_desktop ? 'width:245px;text-align:justify;' : 'width:100%;text-align:center;'; ?>
        padding: 20px 0;
        float: left;
        margin-right: 15px;
        text-transform: uppercase;
        color: black;
        font-weight: 400;
    }
    .bdi-box .orange {
        color: #F15D2f;
        letter-spacing: 0.05em;
    }
    p.promo-copy {
        font-family: 'PT Sans', sans-serif;
        color: black;
        font-size: 1.2em;
    }
</style>

<h1 style="    text-align: center;
    font-family: Raleway, sans-serif;
    letter-spacing: .05em;
    font-size: 28px;
    margin: 0;"><span class="bolded" style="font-weight: bold;"> <img src="<?php print $theme_path; ?>/images/bdi-logo.png"/></span>
    <span style="        display: inline-block;
    background: #F15D2F;
    color: white;
    position: relative;
    <?php if (!$is_desktop) { print 'line-height:130%;margin: 15px 0 0 0;padding:15px';} ?>
    <?php if ($is_desktop) print 'bottom: 30px;padding: 19px 20px;' ; ?>">Office Furniture Sale</span>
</h1>


<h3 style="text-align: center;margin: 0;font-size: 1.8em;color: black;padding:0.5em 0;line-height:130%">
    <span style="font-size: 2em;display: block;margin: .5em 0;">Save 15%*</span> on ALL BDI Office Furniture
</h3>
<h4 style="text-align: center;margin: 20px 0 30px 0;">August 30 &mdash; September 19</h4>

<aside class="bdi-box">
    <span class="orange">Innovative Designs</span><br/> For Modern Living
</aside>

<?php if (FALSE): ?>
    <!--Your media center is the entertainment hub of your home, and BDI offers an affordable and innovative home theatre and
    media furniture. BDI is an industry leader in media furniture and office systems. Always more than meets the eye, BDI
    merges innovative engineering and original design to seamlessly integrate technology into the home.-->

    <!--BDI’s furniture combines great design with innovative-->
    <!--functionality to seamlessly integrate technology into the home and office environments.-->
    <!--Whether you are looking to house a TV and soundbar or-->
    <!--a full blown multi-channel system, make sure the furniture that you-->
    <!--select meets your needs.-->

<?php endif;?>

<p class="promo-copy" style="font-size:1.7em;line-height:130%">

    For a limited time, SAVE 15% OFF on our entire inventory of BDI office furniture, Bink tables, shelving
    and modular systems. Design your perfect office with BDI! Furnitalia is your destination for contemporary office
    furniture.

</p>


<br/>

<div style="text-align: center;">

    <ul class="bxslider" style="margin:0 0 30px 0;padding:0 0 10px 0;list-style-type:none" data-slick-lazy="true">
    <?php
        foreach($promo_images as $img) {
            print '<li><img data-lazy="' . $promo_dir . '/' . $img['url'] . '" alt="' . $img['alt'] . '" ></li>';
        }

    ?>
    </ul>
    <!--<img src="<?php print $promo_dir . '/' . $img_path; ?>" alt="BDI - Office Furniture Sales Event" /> -->
</div>
<br/>

<div style="text-align: center">`
    <?php if ($is_desktop): ?>
        <iframe width="720" height="405" src="//www.youtube.com/embed/Bu4A5zIlGiI" frameborder="0" allowfullscreen></iframe>
    <?php else: ?>
        <iframe width="320" height="236" src="//www.youtube.com/embed/Bu4A5zIlGiI" frameborder="0" allowfullscreen></iframe>
    <?php endif; ?>
</div>

<br/>

<hr class="grey-sep"/>

<p style="color: black;text-align: center">
    CALL TODAY FOR MORE INFORMATION ABOUT THIS EVENT! <br/>
    <a href="tel:+19163329000" style="border-bottom: 1px dashed #981b1e; color;#981b1e !important; text-decoration: none">916-332-9000</a><br/>
    <a href="<?php print base_path();?>request/ajax/bdi" class="request" id="request-info" title="Request BDI Product Information"
       style="padding: 17px 10px;
        background: #981b1e;
        display: inline-block;
        color: white;
        text-decoration: none;
        text-rendering: optimizeLegibility;
        font-size: 22px;
        width: 170px;
        margin: 10px auto;">
        Request Info
    </a> &nbsp;
    <a href="<?php print base_path();?>office?brand=29" class="request"  title="Shop BDI"
       style="padding: 17px 10px;
        background: #981b1e;
        display: inline-block;
        color: white;
        text-decoration: none;
        text-rendering: optimizeLegibility;
        font-size: 22px;
        width: 170px;
        margin: 10px auto;">
        Shop BDI
    </a>


    <a class="fancybox fancybox.iframe" href="https://view.publitas.com/p222-10363/bdi-product-catalog-2018/page/70-71" title="BDI Furniture Product Catalog" style="color: #a02e31;
    border-bottom: 1px dotted #a02e31;
    text-decoration: none; margin:1em 0 0 .5em;display:inline-block;clear:left;font-size:1.5em" target="_blank">Preview BDI Catalog</a>
    
</p>
<p style="    text-align: center;
    font-size: 0.7em;
    text-transform: uppercase;color:black">
*Website prices are as marked and already include 15% discount.
</p>

<?php print render($content); ?>

<!--<video class="wp-video-shortcode" id="video-10923-1" width="640" height="360" preload="metadata" src="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4?_=1" style="width: 100%; height: 100%;"><source type="video/mp4" src="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4?_=1"><a href="http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4">http://www.lawrance.com/wp-content/uploads/2017/01/BDI-Home-Theater-Furniture-High-Performance-Features.mp4</a></video>-->

<section id="requestFormSection"></section>

