<?php

global $theme_path, $conf;

/*$promo_dir = base_path() . "sites/default/files/promo/stressless";
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
    $img_path = 'Stressless Ekornes Garda mobile.png';
} else {
    $is_desktop = TRUE;
    $img_path = 'Stressless Ekornes Garda.png';
}
$alt = "Stressless by Ekornes - stylish modern living room furniture, recliners, sofas";*/

$promo_dir = base_path() . "sites/default/files/promo/stressless/leather-upgrade";
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
    $img_path = 'stressless-leather-upgarde-mobile-landing.png';
} else {
    $is_desktop = TRUE;
    $img_path = 'stressless-leather-upgarde-desktop-landing.png';
}
$alt = "Find Multiple Ways to Save on Stressless Now.";
?>

<!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400|Signika" rel="stylesheet" async> -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400" rel="stylesheet" async>

<style>
    <?php if ($is_desktop): ?>
        div.stressless-items-link {
            float: right;
            display: inline-block;
            margin-top: 20px;
        }
        div.stressless-items-link a.more-info {
            text-transform: uppercase;
            text-decoration: none;
            float: right;
            line-height: 20px;
            margin-top: 20px;
            font-size: 22px;
            font-weight: bold;
            border-bottom: 2px dotted;
        }
    <?php else : ?>
        div.stressless-items-link {
            padding: 15px 0 30px 0;
        }
        div.stressless-items-link a.more-info {
            border-bottom: thin dotted;
            font-weight: bold;
            text-transform: uppercase;
            line-height: 150%;
        }
    <?php endif; ?>
</style>

<?php /*
 orig h1 style
 color:#981b1e;
    text-align: center;
    font-size: 2em;
    margin: 0;
    letter-spacing: 1.5px;
 */
?>

<h1 style="   color: #ea2328;
    text-align: center;
    font-size: 3em;
    margin: 0;
    letter-spacing: 1.5px;
    font-family: 'Source Sans Pro', sans-serif;
    <?php if (!$is_desktop) : ?>
        line-height: 100%;
        font-size: 2.5em;
    <?php endif; ?>
">Stressless&reg; Ekornes Recliners and Sofas</h1>

<!--<img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>" style="display:block; margin:0 auto;"/>

<aside class="contact" style="text-align: center;
font-size: 1.5em;">
    Call us at <a href="tel:+19163329000">916-332-9000</a> or
    <a href="<?php print base_path();?>request/ajax/stressless" class="request request-info" title="Request Stressless Product Information"
       style="padding: 17px 10px;
background: #981b1e;
display: inline-block;
color: white;
text-decoration: none;
text-rendering: optimizeLegibility;
font-size: 22px;
width: <?php print $is_desktop ? '200px' : '300px'; ?>;
margin: <?php print $is_desktop ? '10px 0 0 15px' : '10px auto'; ?>;
border-radius: 7px;">
        CONTACT US
    </a>

</aside>
<br/> -->

<!-- Promo Text -->
<!--
<article style='color:#444; font-family: "Signika","Helvetica Neue", Helvetica, Arial, sans-serif; margin:0; padding-bottom:3em '>

    <h2 class="stressless-orange" style=" font-family: 'Source Sans Pro', sans-serif;
letter-spacing: -3px;
text-align: center;
line-height: 2em;
font-size: 3.7em;
/*color: #cd431f;*/
color: #c4bcb7;
margin: 0;
padding: 0;
font-weight: 300;
<?php if (!$is_desktop) : ?>
    padding:0.5em 0;
    line-height: 130%;
    font-size: 2.5em;
    letter-spacing: 0px;
<?php endif; ?>

">Turn kindness into savings!</h2>

    <img src="<?php print $promo_dir . '/'. $img_path; ?>" alt="<?php print $alt;?>" style="display:block; margin:0 auto;"/>


    <p class="" style="    text-align: center;
    font-size: 1.8em;line-height: 1.5em;color:#cd431f;">until Jan 15, 2018, <br/> turn a small donation into Stressless® savings of up to $400:</p>

    <div class="description" style="margin:35px auto 0; <?php if ($is_desktop): ?>width:720px <?php endif; ?>">
        <p class="" style= 'font-family:"Signica","Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.35em;
        line-height: 1.42858;
        color: #857874;
        margin-top: -1em;
        margin-bottom: 2em;
        '>- Save <strong>$200</strong> with a minimum donation of $50 to charity (specified by dealer). <br/> Applies to any
            Stressless Recliner, Stressless Office, Stressless Sofa or Ekornes Collection;

        <br/>
        OR
        <br/>

        - Save <strong>$400</strong> off Mayfair Recliner &amp; Ottoman (Classic or Signature Base), Mayfair Office or Mayfair
        Recliner with LegComfort.*</p>


        <p style="font-size: 1.1em;
        line-height: 150%;">
            Enjoy luxurious comfort from head to toe with Stressless recliners. Our sleek Signature Base
            offers the soft, gentle rocking motion of BalanceAdapt™. LegComfort™ recliners feature a
            footrest elegantly hidden under the seat—released with a light touch to just the right length for
            you. In combination with the Glide and Plus™-systems, these recliners provide the perfect
            support for your lower back and neck. Stressless sofas and recliners are available in many types
            of leather grades, colors and fabrics. With more than 140 cover options that can be combined
            with woodwork in several different colors, there is sure to be a perfect Stressless for your
            home.

        </p>

        <p>*See store for full details</p>

    </div>

    <aside class="contact" style="text-align: center;
    font-size: 1.5em;">
        Call us at <a href="tel:+19163329000">916-332-9000</a> or
        <a href="<?php print base_path();?>request/ajax/stressless" class="request request-info" title="Request Stressless Product Information"
           style="padding: 17px 10px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: <?php print $is_desktop ? '200px' : '300px'; ?>;
    margin: <?php print $is_desktop ? '10px 0 0 15px' : '10px auto'; ?>;
    border-radius: 7px;">
            CONTACT US
        </a>

    </aside>

</article> -->
<!-- \End Promo Text -->

<!--<img src="<?php print $theme_path;?>/images/landing/stressless/front_banner_725x530.jpg" alt="Stressless Ekornes Free Leather Upgrade Event"/>
<p style="font-size: 1.2em;
    line-height: 1.5em;
    font-weight: 500;">
    Envision saving up to $500 on Stressless®. During our FREE Leather Upgrade Event you can.
</p>
<p style="font-family:Arial">
During our FREE Leather Upgrade event you can save up to $500 on Stressless®. recliners and sofas. Pay for fabric and receive leather at no extra cost or pay for leather and receive a smoother, softer grade of leather for free. Offer ends March 21.
Imagine a recliner or sofa so comfortable, it takes life’s fast pace down to a speed that relaxes and rejuvenates you. Stressless is the only furniture capable of providing such comfort. You’ll feel it in our patented Plus™-system, which provides exceptional lumbar support by interpreting your every movement. Once you sit in a Stressless chair or a Stressless couch, you’ll wonder how you’ve been able to relax and unwind without it.    
</p>

<aside class="contact" style="text-align:center;">
    Call us at <a href="tel:+19163329000">916-332-9000</a> (Sacramento, CA) | <a href="tel:+19163329000">916-332-9000</a> (Roseville, CA) 
    <a href="<?php print base_path();?>request/ajax/stressless" class="request" id="request-info" title="Request Stressless Product Information"
    style="    padding: 17px 10px;
    background: #981b1e;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: 200px;
    margin: 10px auto;
    border-radius: 7px;">
        CONTACT
    </a>

    <?php if (isset($signup_form)): ?>
        <?php print $signup_form; ?>
    <?php endif; ?>

    <?php if (isset($view_items_link)) : ?>
        <?php print render($view_items_link); ?>
    <?php endif; ?>

</aside>
-->

<script language="javascript">

    var scrollToIframe = false;

    function scrollToTop() {
        var ekornesIframe = jQuery("#ekornesIframe");
        var target = ekornesIframe.offset().top;
        var interval = setInterval(function() {
            if (jQuery(window).scrollTop() >= target) {
                console.log("scrolled past it");
                scrollToIframe = true;
                clearInterval(interval);
            }
        }, 250);

        //scroll(0,0);

        if (scrollToIframe) {
            //scroll to newly inserted content
            var offset = ekornesIframe.offset();
            var scrollTarget = ekornesIframe;
            while (scrollTarget.scrollTop() === 0 && scrollTarget.parent()) {
                scrollTarget = scrollTarget.parent();
            }
            // Only scroll upward
            scrollTarget.animate({scrollTop: (offset.top - 5)}, 500);
        }
    }
</script>

<!-- https://secure.ekornes.com/?d=1&coll=GO&sc_lang=en -->
<!-- https://www.stressless.com/en -->
<iframe id="ekornesIframe" src="https://secure.ekornes.com/?d=1&coll=GO&sc_lang=en" class="loading loading-top"
        frameborder="0" width="100%" height="3500"
        scrolling="auto" onload="scrollToTop();"></iframe>

<aside class="contact" style="text-align: center;
    font-size: 1.5em;">
    Call us at <a href="tel:+19163329000">916-332-9000</a> or
    <a href="<?php print base_path();?>request/ajax/stressless" class="request request-info" title="Request Stressless Product Information"
    style="padding: 17px 10px;
    background: #ea2328;
    display: inline-block;
    color: white;
    text-decoration: none;
    text-rendering: optimizeLegibility;
    font-size: 22px;
    width: 200px;
    margin: 10px 0 0 15px;
    border-radius: 7px;">
        CONTACT US
    </a>

    <?php if (isset($signup_form)): ?>
        <?php print $signup_form; ?>
    <?php endif; ?>

    <?php if (isset($view_items_link)) : ?>
        <?php print render($view_items_link); ?>
    <?php endif; ?>

</aside>

<?php if (!$is_desktop): ?>
    <section id="requestFormSection"></section>
<?php endif; ?>
