<?php
/*// Print out the progress bar at the top of the page.
print drupal_render($form['progressbar']);

// Print out the preview message if on the preview page.
if (isset($form['preview_message'])) {
  print '<div class="messages warning">';
  print drupal_render($form['preview_message']);
  print '</div>';
}

// Print out the main part of the form.
// Feel free to break this up and move the pieces within the array.
print drupal_render($form['submitted']);

// Always print out the entire $form. This renders the remaining pieces of the
// form that haven't yet been rendered above (buttons, hidden elements, etc).
print drupal_render_children($form);

*/
?>

<style>
  .webform-section .form-item label {
    display: block;
    margin: 0;
    font-weight: 600;
    padding-bottom: .3em;
  }
  .dk_fouc select.no-dropkick {
    visibility: visible;
    position: static;
  }
</style>

<div class="jumbotron">
  <div class="container">
  <h2><i class="glyphicon glyphicon-user"></i> Contact Information</h2>
  <div class="row">
    <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['first_name']); ?></div>
    <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['last_name']); ?></div>
  </div>


  <div class="row form-group">
    <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['e_mail']); ?></div>
    <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['zip']); ?></div>
  </div>

  <div class="row form-group">
    <div class="col-xs-12 col-md-6"><?php print drupal_render($form['submitted']['number_of_people_attending']); ?></div>
  </div>

  <!--<div class="row form-group">
  <div class="col-xs-12"><?php /*print drupal_render($form['submitted']['message']); */ ?></div>
</div>-->

  <?php
  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above (buttons, hidden elements, etc).
  print drupal_render_children($form);
  ?>

  </div>

</div>