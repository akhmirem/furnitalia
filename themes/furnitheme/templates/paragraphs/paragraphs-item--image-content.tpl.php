<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<?php if(FALSE): ?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content); ?>
  </div>
</div>

<?php endif; ?>

<?php
  $row_style_attributes = array() ;
  $body_attributes = array() ;
?>

<?php if ($paragraph->field_bg_color['und'][0]['rgb']): ?>
  <?php $bg_color = $paragraph->field_bg_color['und'][0]['rgb'];?>
  <?php $row_style_attributes['style'] = 'background-color:' . $bg_color; ?>
<?php endif; ?>

<?php $row_style_attributes = drupal_attributes($row_style_attributes); ?>


<div data-aos="fade-up" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="component-image-content__container container-fluid">
    <div <?php print $row_style_attributes; ?> class="row">

      <?php
      $col_class = 'col-md-6';
      $col2_class = 'col-md-6';

      if ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '1_4')) {
        $col_class = 'col-md-3';
        $col2_class = 'col-md-9';
      } elseif ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '2_3')) {
        $col_class = 'col-md-4';
        $col2_class = 'col-md-8';
      }

      if ($paragraph->field_alignment['und'][0]['value'] && ($paragraph->field_alignment['und'][0]['value'] == 'right')) {
        if ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '1_4')) {
          $col_class = 'col-md-3 col-md-push-9';
          $col2_class = 'col-md-9 col-md-pull-3';
        } elseif ($paragraph->field_content_layout['und'][0]['value'] && ($paragraph->field_content_layout['und'][0]['value'] == '2_3')) {
          $col_class = 'col-md-4 col-md-push-8';
          $col2_class = 'col-md-8 col-md-pull-4';
        } else {
          $col_class = 'col-md-6 col-md-push-6';
          $col2_class = 'col-md-6 col-md-pull-6';
        }
      }
      ?>

      <div class="component-image-content__col <?php print $col_class; ?>">
        <?php if($content['field_image']): ?>
        <div class="component-image-content__image">
          <?php print render($content['field_image']); ?>
        </div>
        <?php endif; ?>
      </div>
      <div class="component-image-content__col <?php print $col2_class; ?>">

        <?php if ($content['field_paragraphs_reference']): ?>
          <?php if ($paragraph->field_color['und'][0]['rgb']): ?>
            <?php $color_id = 'paragraph-' . $paragraph->item_id . '-color' ?>
            <?php $body_attributes['id'] = $color_id; ?>
            <?php $tags = ['h1','h2','h3','h4','h5','h6','h7','h8','div','span','p','a','blockquote','ul','ol','label','table','tr','td','.base-accordion__header::before'] ?>
            <style type="text/css">
              <?php foreach($tags as $tag): ?>
                <?php print "#" . $color_id. ' ' . $tag; ?><?php if( next( $tags ) ): ?>, <?php endif; ?>
              <?php endforeach; ?>
              {
                color: <?php print $paragraph->field_color['und'][0]['rgb']; ?>;
              }
            </style>
          <?php endif; ?>

          <?php $body_attributes['class'] = ['component-image-content__body--color-override', 'component-image-content__body']; ?>
          <?php $body_attributes = drupal_attributes($body_attributes); ?>
          <div <?php print $body_attributes; ?> data-custom="asd">
            <?php print render($content['field_paragraphs_reference']); ?>
          </div>

        <?php endif; ?>

      </div>
    </div>
  </div>
</div>

