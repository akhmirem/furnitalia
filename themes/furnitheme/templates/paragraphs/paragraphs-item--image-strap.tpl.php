
<?php
  $classes .= 'component-image-strap';
?>

<!-- Background Image. -->
<?php
  if (false && $paragraphs_item->field_image['und'][0]['fid']) {
    $style = "background";
    $derivative_uri = image_style_path($style, $paragraphs_item->field_image['und'][0]['uri']);
    if (!file_exists($derivative_uri)) {
      $display_style = image_style_load($style);
      image_style_create_derivative($display_style, $paragraphs_item->field_image['und'][0]['uri'], $derivative_uri);
    }
    $bg_image = file_create_url($derivative_uri);
  }
?>

<div data-aos="fade-up" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php /* if ($paragraphs_item->field_image['und'][0]['fid']): */?><!--
    <div class="component-image-strap__bg-image">
      <div class="component-image-strap__image">
        <?php /*print theme("image", array(
          "path" => $bg_image,
          "attributes" => array(
            "property" => 'schema:image',
          )
        )); */?>
      </div>
    </div>
  --><?php /*endif; */?>

  <?php print (render($content['field_image']));?>

</div>