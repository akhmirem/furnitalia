
<?php include dirname(__FILE__) . "/header_promo.tpl.php"; ?>


<div id="page" class="clearfix">

  <!-- Header
  ==========================================================================-->
  <header id="header" role="banner">

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img
            src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" style="width:245px;height:auto"/></a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <hgroup id="name-and-slogan">
        <?php if ($site_name): ?>
          <h1 id="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"
               rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>
      </hgroup>
    <?php endif; ?>

    <?php print render($page['header']); ?>

    <section id="header-phone">
      <a href="tel:+19163329000" class="main-phone">(916) 332-9000</a> &nbsp; | &nbsp;
      <a href="#" class="main-phone chat" onclick="jivo_api.open();">CHAT <span id="chat-icon">&nbsp;</span></a>
      <!--<a href="tel:+19164840333">916-484-0333</a> <a href="<?php print url('stores') ?>"> (Sacramento, CA)</a> &nbsp; | &nbsp;
            <a href="tel:+19163329000">916-332-9000</a> <a href="<?php print url('stores') ?>"> (Roseville, CA)</a>	-->
    </section>

  </header>
  <!--/Header -->

  <!--<nav class="nav" id="horizontalNav" role="navigation"><?php /*print render($page['nav']); */?></nav>-->
  <?php if (isset($page['pre_content'])): ?>
    <?php print render($page['pre_content']); ?>
  <?php endif; ?>

  <!-- Main
  ==========================================================================-->
  <div id="main" class="clearfix">

    <?php if ($hamburger_menu) : ?>
      <header id="menuHeader">
        <button class="hamburger">&#9776;</button>
        <button class="cross">&#735;</button>
      </header>
    <?php endif; ?>

    <!-- Content
    ==========================================================================-->
    <div id="content" class="column" role="main">

      <?php print render($page['content_top']); ?>

      <?php if (false && isset($page['description_section'])): ?>
      <p class="description-section-top">
        <?php print render($page['description_section']); ?>
      </p>
      <?php endif; ?>

      <?php print $breadcrumb; ?>

      <a id="main-content"></a>

      <?php print render($title_prefix); ?>

      <?php if ($title && !$is_front && $show_title): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <hr class="title"/>
      <?php endif; ?>

      <?php print render($title_suffix); ?>

      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>

      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php if (isset($page['top_banner']) && $page['top_banner']): ?>
        <?php print render($page['top_banner']); ?>
      <?php endif; ?>

      <?php print render($page['content']); ?>

    </div>
    <!-- /Content -->

    <!-- Left Section
    ==========================================================================-->
    <?php $hamburger_style = ""; ?>
    <?php if ($hamburger_menu ) : ?>
      <?php $hamburger_style = "display:none"; ?>
      <style scoped>
        header#menuHeader {
          background: #ea2328;
          height: 50px;
          line-height: 50px;
          /*position: relative;*/
          top: 60px;
          left: 0;
          z-index: 100;
          position: absolute;
          margin: 25px 0 20px 15px;
          width: 60px;
        }

        .hamburger {
          background: none;
          position: absolute;
          top: 0;
          left: 0;
          /*line-height:45px;*/
          padding: 0 15px 0 15px;
          color: #fff;
          border: 0;
          font-size: 2em;
          font-weight: bold;
          cursor: pointer;
          outline: none;
          z-index: 10000000000000;
        }

        .cross {
          background: none;
          position: absolute;
          top: 0;
          left: 0;
          padding: 0 15px 0 20px;
          color: #fff;
          border: 0;
          font-size: 3em;
          line-height: 65px;
          font-weight: bold;
          cursor: pointer;
          outline: none;
          z-index: 10000000000000;
        }

        #content.full-width {
          width: 100%;
          margin-top: 4em;
        }

        body.page-virtual-showroom #content.full-width {
          margin-top: -1em;
        }
      </style>
    <?php endif; ?>

    <?php if (isset($page['left']) && $page['left']): ?>
    <section id="left-section" class="antialias" style="<?php print $hamburger_style; ?>">
      <?php
      // Render the left sidebar
      $left = isset($page['left']['furn_menu']) ? render($page['left']['furn_menu']) : '';
      ?>

      <?php if ($left): ?>
        <aside class="sidebars menu-left">
          <?php print $left; ?>
          <?php print render($page['left']); ?>
        </aside>
      <?php endif; ?>

      <div id="requestFormContainer" class="dialog-form" style="display:none">
        <p>Javascript must be enabled in your browser to submit a request.</p>
      </div>

      <?php if (isset($page['left_section_extra'])): ?>
        <aside id="left-section-extra">
          <?php print render($page['left_section_extra']); ?>
        </aside>
      <?php endif; ?>

    </section>
    <?php endif; ?>
    <!--  /Left Section -->

    <!--  Promo
   ============================================================================-->
    <?php if ($show_promo) : ?>
      <section id="promo" class="clearfix">
        <?php if (variable_get("promo_type", "default") == "default") : ?>
          <?php include_once("promo-default.tpl.php"); ?>
        <?php else : ?>
          <?php include_once("promo.tpl.php"); ?>
        <?php endif; ?>
      </section>
    <?php endif; ?>
    <!--  /Promo -->

  </div>
  <!--  /Main -->

</div>
<!--  /Page -->

<!--  Footer
============================================================================-->
<footer>

  <!-- Newsletter subscription
  ============================================================================-->
  <!-- Begin Mailchimp Signup Form -->
  <!--<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css"> -->
  <?php //include_once "../css/mailchimp_signup_form.css" ?>
  <div id="mc_embed_signup" class="clearfix">
    <form action="https://furnitalia.us13.list-manage.com/subscribe/post?u=66b55ad437d9bfa2366d0d5e2&amp;id=280637c773" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate with-utm" target="_blank" novalidate>
      <div class="newsletter-block-container" style="
    display: flex;
    align-items: baseline;
">
        <div class="signup-text" style="">Be the first to know about special offers, new products and store events. Plus, get dècor inspiration and advice from our design team.</div>

        <div id="mc_embed_signup_scroll">
          <h2>Get On the List!</h2>
          <div class="mc-field-group">
            <label for="mce-EMAIL" style="display: none">Email Address </label>
            <input type="email" value="" placeholder="E-mail" name="EMAIL" class="required email" id="mce-EMAIL" aria-required="true">
          </div>
          <div class="mc-field-group input-group" style="display:none">
            <ul>
              <li><input type="checkbox" value="512" name="group[2125][512]" id="mce-group[2125]-2125-11" checked=""><label for="mce-group[2125]-2125-11">Newsletter Subscribers</label></li>
            </ul>
          </div>
          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_66b55ad437d9bfa2366d0d5e2_280637c773" tabindex="-1" value=""></div>
          <div class="clear"><input type="submit" value="Sign Me Up" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
        </div>
      </div>
    </form>
  </div>
  <!--End mc_embed_signup-->

  <!-- /Newsletter subscription -->



  <div id="footer-inner" class="clearfix">

    <?php /*print render($page['footer']['webform_client-block-33']); */?><!--
        <div id="newsletterFormContainer"></div>-->


    <!-- VIP Customer Program
    ============================================================================-->
    <div id="vipProgramBlock" style="
            color: white;
    font-size: 1.1em;
    letter-spacing: 1px;
    display: inline-block;
    margin-top: 1em;
        ">
      <h4 style="">
        <?php print l("Join VIP Customer Program", "vip",
            array("attributes" =>
                array(
                    "title" => "Sign up to become a preferred customer for exclusive benefits from Furnitalia!",
                    "style" => "color: white;border: 1px solid #eee;padding: 1em;",
                ))); ?>
      </h4></div> <!-- <span> &#9733;</span> -->
    <!-- /VIP Customer Program -->

    <!-- Footer Menu
    ============================================================================-->
    <nav id="footer-menu">
      <?php print render($footer_menu); ?>
    </nav>
    <!-- /Footer Menu -->

    <!-- SOCIAL ICONS
    ==========================================================================-->
    <section id="social">

      <h3>Connect with Furnitalia</h3>
      <ul>
        <li><a href="https://www.facebook.com/furnitalia.sacramento" class="facebook"
               target="_blank">&nbsp;</a></li>
        <li><a href="https://twitter.com/furnitalia" class="twitter" target="_blank">&nbsp;</a></li>
        <li><a href="https://pinterest.com/furnitalia" class="pinterest " target="_blank">&nbsp;</a></li>
        <li><a href="https://instagram.com/furnitalia" class="instagram" target="_blank">&nbsp;</a></li>
        <li><a href="https://www.houzz.com/pro/furnitalia/furnitalia" class="houzz"
               styled="padding-left:2px;position:relative;top:3px" target="_blank">
            &nbsp;
            <!--<img src="http://st.hzcdn.com/static/badge181_25.png" alt="Contemporary Italian Furniture | Inspiration" border="0" />-->
          </a></li>
      </ul>

      <div class="fb-page" data-href="https://www.facebook.com/furnitalia.sacramento" data-width="230"
           data-small-header="true" data-adapt-container-width="true" data-hide-cover="true"
           data-show-facepile="false" style="margin-top:1em">
        <blockquote cite="https://www.facebook.com/furnitalia.sacramento" class="fb-xfbml-parse-ignore"><a
            href="https://www.facebook.com/furnitalia.sacramento">Furnitalia</a></blockquote>
      </div>

    </section>
    <!-- /SOCIAL ICONS -->


    <!-- Store Locations
   ============================================================================-->
    <section id="footer-addresses">
      <div class="addr-col">
        <span class="store-nm">SACRAMENTO</span><br/>
        5252 Auburn Blvd.<br/>
        Sacramento, CA 95841<br/>
        <a href="tel:+19163329000"><span class="tel">916 332.9000</span></a><br/>
        Hours:<br/>
        Mon-Sun, 10am-6pm
      </div>
      <div class="addr-col col2">
        <span class="store-nm">ROSEVILLE</span><br/>
        1198 Roseville Pkwy. #120<br/>
        Roseville, CA 95678<br/>
        <a href="tel:+19163329000"><span class="tel">916 332.9000</span></a><br/>
        Store temporarily closed due to COVID-19.
        <!--
        Hours:<br/>
        Mon-Sat, 10am-6pm<br/>
        Sun, 11am-6pm<br/>
        closed on Wed.
-->
      </div>
    </section>
    <!-- /Store Locations -->

    <!--<nav id="footer-info-menu"><?php print render($footer_info_menu); ?></nav>
      <nav id="footer-user-menu"><?php print render($footer_user_menu); ?></nav>
      <nav id="footer-policy-menu"><?php print render($footer_policy_menu); ?></nav>
      <?php print  l("Catalogs", "catalogs", array('attributes' => array('id' => 'catalogs'))); ?>  -->

  </div>
</footer>
<!--  /Footer -->

<?php print render($page['bottom']); ?>

<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';fnames[5]='MMERGE5';ftypes[5]='text';fnames[6]='MMERGE6';ftypes[6]='text';fnames[7]='MMERGE7';ftypes[7]='text';fnames[8]='MMERGE8';ftypes[8]='text';fnames[9]='MMERGE9';ftypes[9]='text';fnames[10]='MMERGE10';ftypes[10]='text';fnames[11]='MMERGE11';ftypes[11]='text';fnames[12]='MMERGE12';ftypes[12]='text';fnames[13]='MMERGE13';ftypes[13]='text';fnames[14]='MMERGE14';ftypes[14]='text';fnames[15]='MMERGE15';ftypes[15]='text';fnames[16]='MMERGE16';ftypes[16]='text';fnames[17]='MMERGE17';ftypes[17]='text';fnames[18]='MMERGE18';ftypes[18]='text';fnames[19]='UNIQUECODE';ftypes[19]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>


<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function() {
    let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
    let active = false;

    const lazyLoad = function() {
      if (active === false) {
        active = true;

        setTimeout(function() {
          lazyImages.forEach(function(lazyImage) {
            if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
              lazyImage.src = lazyImage.dataset.src;
              //lazyImage.srcset = lazyImage.dataset.srcset;
              lazyImage.classList.remove("lazy");

              lazyImages = lazyImages.filter(function(image) {
                return image !== lazyImage;
              });

              if (lazyImages.length === 0) {
                document.removeEventListener("scroll", lazyLoad);
                window.removeEventListener("resize", lazyLoad);
                window.removeEventListener("orientationchange", lazyLoad);
              }
            }
          });

          active = false;
        }, 200);
      }
    };

    lazyLoad();

    document.addEventListener("scroll", lazyLoad);
    window.addEventListener("resize", lazyLoad);
    window.addEventListener("orientationchange", lazyLoad);
  });
</script>
