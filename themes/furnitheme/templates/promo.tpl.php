<?php


?>

<?php if(isset($promos) && is_array($promos) && count($promos) > 0) :?>
<ul class="bxslider" style="margin:0;padding:0 0 10px 0;list-style-type:none">
    <?php foreach($promos as $name => $promo)  : ?>
        <?php foreach ($promo['banners'] as $key => $banner)   : ?>
            <?php if (!isset($banner['id'])) $banner['id'] = ''; ?>
            <li>
                <?php print l(
                    theme("image", array("path" => $banner['image'], "title" => $banner['alt'], "attributes" => array("ref" => ""))),
                    $banner['link'],
                    array(
                        "html" => TRUE,
                        "attributes" => array(
                            'title' => $banner['alt'],
                            "class" => array('promoImg'),
                            "id" => $banner['id'],
                        )
                    )
                ); ?>
            </li>
        <?php endforeach; ?>
    <?php endforeach; ?>
</ul>
<?php endif; ?>

<!-- <a href="<?php print url('natuzzi-editions'); ?>" id="bottomBannerImg1" class="promoImg">
        <img src="<?php print base_path(); ?>sites/default/files/promo/memorial_day/NatuzziEditions-MemorialDay-bottom.jpg"
             alt="Memorial Day Sale. Total Comfort, Including the Price!"/>
</a> -->
