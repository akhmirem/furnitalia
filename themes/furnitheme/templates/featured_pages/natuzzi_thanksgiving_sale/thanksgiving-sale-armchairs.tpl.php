<?php
$base = base_path() . "sites/all/themes/furnitheme/templates/featured_pages";
$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";
?>


<link href="<?php print $base; ?>/natuzzi_thanksgiving_sale/css/app_main.css" rel="stylesheet"
      type="text/css" media="all"/>

<style>
    .promo .banner img.title.custom {
        margin-top: 0px;
        position: absolute;
        margin-top: 15px;
        position: static;
    }
</style>


<div class="container-fluid" id="m-top">
    <div class="promo">

        <div class="detail-product"><i class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>
        <div class="banner"
             style="background-image: url(<?php print $base; ?>/natuzzi_thanksgiving_sale/images/bg-trulli-nuovo-122-1920x1080.jpg);">
            <img src="<?php print $base; ?>/natuzzi_thanksgiving_sale/images/png-header-trasparente-123-600x449.png" class="title "/>
            <div class="slideshow-wrapper">
                <div class="cycle-slideshow main-slideshow hidden"
                     data-cycle-log="false"
                     data-cycle-center-horz="true"
                     data-cycle-fx="scrollHorz"
                     data-cycle-swipe="true"
                ><img data-id-product="4516"
                      src="<?php print $base; ?>/natuzzi_summer_sale/images/landing-poltrone-3-900x400-7.png"
                      class="img-responsive center-block"/><img data-id-product="1209"
                                                                src="<?php print $base; ?>/natuzzi_summer_sale/images/landing-poltrone-1-900x400-6.png"
                                                                class="img-responsive center-block"/><img
                            data-id-product="1219"
                            src="<?php print $base; ?>/natuzzi_summer_sale/images/landing-poltrone-2-900x400-7.png"
                            class="img-responsive center-block"/>
                    <div class="slideshow-controls"><a class="arrow-left" href="javascript:;"><img width="16px"
                                                                                                   height="31px"
                                                                                                   src="https://www.natuzzi.us/promotions/images/icons/arrow-left.svg"/></a><a
                                class="arrow-right" href="javascript:;"><img width="16px" height="31px"
                                                                             src="https://www.natuzzi.us/promotions/images/icons/arrow-right.svg"/></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="content" id="m-prod">
            <div class="top">
                <div>Add style in your home with Natuzzi Italia armchairs, entirely Made in Italy. <br><br></div>
                <div>Comfortable, relax, stationary armchairs, Natuzzi Re-vive armchairs and more. <br><br></div>
                <div style="text-transform: uppercase;"><strong>Craftsmanship and technology for you.</strong></div>
            </div>
        </div><!-- prodotti -->
        <section>
            <div class="products row">

                <div class="list-products">
                    <div class="single-product"><a href="javascript:;"
                                                                                        data-id="4516"
                                                                                        data-id-landing="123"><img
                                    src="<?php print $base; ?>/natuzzi_summer_sale/images/viaggio-3-2-1000x500-2.jpg"
                                    class="img-responsive center-block"/><img class="hover-image img-responsive center-block"
                                                                              src="<?php print $base; ?>/natuzzi_summer_sale/images/viaggio-2-1000x500.jpg"/>
                            <div class="prod-info"><h3 class="product-title">Viaggio&nbsp;&nbsp;<i
                                            class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                                <div class="price-details"></div>
                            </div>
                        </a></div>
                    <div class="single-product"><a href="javascript:;"
                                                                                        data-id="14763"
                                                                                        data-id-landing="123"><img
                                    src="<?php print $base; ?>/natuzzi_summer_sale/images/b903-2903-agra-1000x500.jpg"
                                    class="img-responsive center-block "/>
                            <div class="prod-info"><h3 class="product-title">Agra&nbsp;&nbsp;<i
                                            class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                                <div class="price-details"></div>
                            </div>
                        </a></div>
                    <div class="single-product"><a href="javascript:;"
                                                                                        data-id="1203"
                                                                                        data-id-landing="123"><img
                                    src="<?php print $base; ?>/natuzzi_summer_sale/images/test-altea2-1000x500.png"
                                    class="img-responsive center-block "/>
                            <div class="prod-info"><h3 class="product-title">Altea&nbsp;&nbsp;<i
                                            class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                                <div class="price-details"></div>
                            </div>
                        </a></div>
                    <div class="single-product"><a href="javascript:;"
                                                                                        data-id="1204"
                                                                                        data-id-landing="123"><img
                                    src="<?php print $base; ?>/natuzzi_summer_sale/images/intro-amadeus-2422-1000x500.jpg"
                                    class="img-responsive center-block "/>
                            <div class="prod-info"><h3 class="product-title">Amadeus&nbsp;&nbsp;<i
                                            class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                                <div class="price-details"></div>
                            </div>
                        </a></div>
                </div>

                <a href="javascript:;" data-id-landing="123"
                   data-next-offset="4" data-limit="4"
                   class="btn btn-more-products"
                   onclick="trackEvent('products', 'show-more')">OTHER MODELS <i
                            class="fa fa-chevron-down"
                            aria-hidden="true"></i></a></div>
            <div class="products-offers-note">
                <p>
                    <video controls="controls" width="100%">
                        <source src="https://player.vimeo.com/external/229251525.hd.mp4?s=7e7ac1a17b4f262683dab21a53812f64963b5270&amp;profile_id=175"
                                type="video/mp4"/>
                    </video>
                </p>
            </div>
        </section>

        <!-- other promo -->
        <?php include_once dirname(__FILE__) . '/thanksgiving-sale-categories.tpl.php'; ?>

        <!-- why -->
        <section class="why" id="m-why">
            <div class="content">
                <div class="top"><span class="size1">NATUZZI QUALITY</span><br/><span class="size3">IS ALSO REFLECTED IN THE SERVICE</span>
                </div>
                <div class="plus row">
                    <div class="col-5-div no-bt"><img
                                src="<?php print $base_folder; ?>/images/icons/colori.svg"/><br/><br/>360 COLOURS IN
                        LEATHER OR
                        FABRIC
                    </div>
                    <div class="col-5-div no-bt no-br"><img src="<?php print $base_folder; ?>/images/icons/design.svg"/><br/><br/>MADE
                        IN ITALY
                        DESIGN AND TECHNOLOGY
                    </div>
                    <div class="col-5-div"><img src="<?php print $base_folder; ?>/images/icons/quality.svg"/><br/><br/>NATUZZI
                        WARRANTY
                    </div>
                    <div class="col-5-div no-br"><img src="<?php print $base_folder; ?>/images/icons/pointer.svg"/><br/><br/>COME
                        AND VISIT US
                    </div>
                    <div class="col-5-div hidden-sm hidden-xs"><img
                                src="<?php print $base_folder; ?>/images/icons/chiama.svg"/><br/><br/>CALL US FOR
                        MORE INFO
                    </div>
                </div>
            </div>
        </section>
        <section class="legal-notes visible-lg visible-md"><p>
                *All Natuzzi sofas and armchairs have a lifetime warranty on the frame and two years on the upholstery,
                mechanical parts and coverings. All furnishing accessories carry a two year warranty.
            </p>
            <p class="company-info">
                Natuzzi Italia
            </p></section><!-- video -->
    </div>
</div>

<?php include_once dirname(__FILE__) . '/thanksgiving-sale-form.tpl.php'; ?>

<a class="sticky-footer" href="javascript:;" data-scroll-to-mobile="#m-form"
>I WANT TO TAKE ADVANTAGE OF
    <br/><span class="btn-call">BLOCK THE OFFER&nbsp;&nbsp;<i
                class="btn-loading fa fa-chevron-down"
                aria-hidden="true"></i></span></a>
<div class="total-black">
    <div class="popout"><span class="close-popout"></span></div>
</div>


<div class="is-desktop"></div>
<div class="is-tablet"></div>
<div class="is-mobile"></div><!-- vendor cdn -->
<script src="https://use.fontawesome.com/f74604f2dc.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha256-U5ZEeKfGNOja007MMD3YBI0A3OSZOQbeG6z2f2Y0hu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"
        integrity="sha256-NjbogQqosWgor0UBdCURR5dzcvAgHnfUZMcZ8RCwkk8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"
        integrity="sha256-YrHP9EpeNLlYetSffKlRFg8VWcXFRbz5nhNXTMqlQlo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"
        integrity="sha256-4F7e4JsAJyLUdpP7Q8Sah866jCOhv72zU5E8lIRER4w=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"
        integrity="sha256-4O4pS1SH31ZqrSO2A/2QJTVjTPqVe+jnYgOWUVr7EEc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"
        integrity="sha256-jDnOKIOq2KNsQZTcBTEnsp76FnfMEttF6AV2DF2fFNE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"
        integrity="sha256-iMTCex8BQ+iVxpZO83MoRkKBaoh9Dz9h3tEVrM5Rxqo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.center.min.js"
        integrity="sha256-puNNvBbwmmw0WIyDXJ5cTd0W/EAiCArmnjo5gUfDxC4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.swipe.min.js"
        integrity="sha256-Prq5k7iqQbFCbpoBYTw3cqMbUZyKh3Y0BBGPBfcxs20=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"
        integrity="sha256-ffw+9zwShMev88XNrDgS0hLIuJkDfXhgyLogod77mn8=" crossorigin="anonymous"></script>
<script src="https://hammerjs.github.io/dist/hammer.js"></script>
<script>
    WebFont.load({
        google: {
            families: ['Lato:100,300,400,700']
        }
    });
</script>
<script src="<?php print $base; ?>/natuzzi_thanksgiving_sale/js/app_script.js"></script><!-- js -->

<script>

    lang = 'en-US';
    labels = {
        "FORM_ERROR_TITLE": "Attention",
        "FORM_ERROR_TEXT": "Check the required fields",
        "FORM_ERROR_TEXT_GENERAL": "A general error occurred. Please try again later.",
        "NL_STORE_ERROR_GPS": "Your device doesn't support localization",
        "NL_STORE_ERROR_LOCATION": "We couldn't get your position"
    };
    url = '//www.natuzzi.us/promotions';
    landingSlug = 'armchairs';
    userPosition = 0;
    idLanding = 123;

    time = {"from": "900", "to": "1800"};


    $(function () {
        // preload images
        $('.banner').imagesLoaded(function () {
            $('.img-loading').addClass('hidden');
            $('.main-slideshow').removeClass('hidden')
        });

        bindMainSliderControls();
        openDetail();
        getMoreProducts();



        handleVideoControls();
    });

</script><!-- custom track code --><!-- Facebook Pixel Code -->
