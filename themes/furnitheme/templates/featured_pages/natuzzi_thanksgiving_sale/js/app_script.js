// (function($) {
$ = jQuery;
"use strict";
// var natuzziUrl = 'https://www.natuzzi.us/promotions/';
var natuzziUrl = Drupal.settings.basePath + "natuzzi/";
var lang, url, offset = 0, limit = 4, map, bounds, userPosition = !1, numIw = 0, iws = [], points, position, zoom = 0, idProductToShow, idLanding, labels, time, iconDivani = "images/icons/pin-ded.png", iconStore = "images/icons/pin-store.png", iconGallery = "images/icons/pin-gallery.png", iconEssence = "images/icons/pin-essence.png", iconRevive = "images/icons/pin-revive.png", styleMap = [{
    featureType: "all",
    elementType: "geometry.fill",
    stylers: [{
        weight: "2.00"
    }]
}, {
    featureType: "all",
    elementType: "geometry.stroke",
    stylers: [{
        color: "#9c9c9c"
    }]
}, {
    featureType: "all",
    elementType: "labels.text",
    stylers: [{
        visibility: "on"
    }]
}, {
    featureType: "landscape",
    elementType: "all",
    stylers: [{
        color: "#f2f2f2"
    }]
}, {
    featureType: "landscape",
    elementType: "geometry.fill",
    stylers: [{
        color: "#ffffff"
    }]
}, {
    featureType: "landscape.man_made",
    elementType: "geometry.fill",
    stylers: [{
        color: "#ffffff"
    }]
}, {
    featureType: "poi",
    elementType: "all",
    stylers: [{
        visibility: "off"
    }]
}, {
    featureType: "road",
    elementType: "all",
    stylers: [{
        saturation: -100
    }, {
        lightness: 45
    }]
}, {
    featureType: "road",
    elementType: "geometry.fill",
    stylers: [{
        color: "#eeeeee"
    }]
}, {
    featureType: "road",
    elementType: "labels.text.fill",
    stylers: [{
        color: "#7b7b7b"
    }]
}, {
    featureType: "road",
    elementType: "labels.text.stroke",
    stylers: [{
        color: "#ffffff"
    }]
}, {
    featureType: "road.highway",
    elementType: "all",
    stylers: [{
        visibility: "simplified"
    }]
}, {
    featureType: "road.arterial",
    elementType: "labels.icon",
    stylers: [{
        visibility: "off"
    }]
}, {
    featureType: "transit",
    elementType: "all",
    stylers: [{
        visibility: "off"
    }]
}, {
    featureType: "water",
    elementType: "all",
    stylers: [{
        color: "#46bcec"
    }, {
        visibility: "on"
    }]
}, {
    featureType: "water",
    elementType: "geometry.fill",
    stylers: [{
        color: "#c8d7d4"
    }]
}, {
    featureType: "water",
    elementType: "labels.text.fill",
    stylers: [{
        color: "#070707"
    }]
}, {
    featureType: "water",
    elementType: "labels.text.stroke",
    stylers: [{
        color: "#ffffff"
    }]
}], landingSlug, handleVideoJSPlugin = function() {
    var o = $.Deferred()
        , e = function() {
        setTimeout(function() {
            o.resolve()
        }, 1e3)
    };
    $.each($(".video-js"), function(o, t) {
        var i = videojs(this.id);
        i.ready(function() {
            var s = $(this.el_);
            if (s.data("bvPlayer", this),
                    s.hasClass("with-audio")) {
                var a = $(".change_color_label").hasClass("text_white") ? "control-audio-white" : "";
                s.append('<div class="control-audio ' + a + ' off"></div>')
            }
            s.hasClass("with-play") && (s.append('<div class="goplay"></div>'),
                this.on("play", function() {
                    s.find(".goplay").fadeOut()
                }),
                this.on("pause", function() {
                    s.find(".goplay").fadeIn()
                })),
                s.on("click tap", function(o) {
                    var e = $(o.target);
                    e.hasClass("control-audio") ? (e.toggleClass("on off"),
                        i.muted(e.hasClass("off"))) : i.paused() ? i.play() : i.pause()
                }),
                $(window).on("scroll", function() {
                    var o = $(t).first().offset().top
                        , e = $(t).first().height();
                    isMobile() || ($(window).scrollTop() > o - e / 2 && $(window).scrollTop() < o + e / 2 ? i.paused() && !$(i.el_).hasClass("video-mobile") && i.play() : i.pause())
                }),
            "undefined" != typeof _controlVideo && _controlVideo(s),
            o || e()
        }, !1),
            i.on("error", function() {
                container.remove(),
                    e()
            })
    })
}, handleSlideModals = function() {
    $('[data-toggle="slide-modal"]').on("click", function() {
        var o = $(this).data("target");
        $(o).addClass("open"),
            $("body").addClass("modal-open")
    }),
        $(".slide-modal .close").on("click", function() {
            $(this).parents(".slide-modal").removeClass("open"),
                $("body").removeClass("modal-open")
        }),
        $(".slide-modal [data-scroll-to-mobile]").on("click", function() {
            $(this).parents(".slide-modal").removeClass("open"),
                $("body").removeClass("modal-open"),
                animatedScrollTo(isMobile() ? $(this).data("scroll-to-mobile") : $(this).data("scroll-to"))
        })
}, handleVideoControls = function() {
    $.each($("video"), function(o, e) {
        handleVideoStartAndStopOnClick(e),
            $(window).on("scroll", function() {
                var o = $(e).first().offset().top
                    , t = $(e).first().height();
                $(window).scrollTop() > o - t / 2 && $(window).scrollTop() < o + t / 2 ? e.play() : e.pause()
            })
    })
}, handleCustomVideoControls = function(o) {
    $.each($(o + " video"), function(o, e) {
        handleVideoStartAndStopOnClick(e),
        isMobile() || $(window).on("scroll", function() {
            var o = $(e).first().offset().top
                , t = $(e).first().height();
            $(window).scrollTop() > o - t / 2 && $(window).scrollTop() < o + t / 2 ? e.play() : e.pause()
        })
    })
}, handleVideoStartAndStopOnClick = function(o) {
    $(o).on("click", function() {
        $(this).hasClass("playing") ? o.pause() : o.play(),
            $(this).toggleClass("playing")
    })
}, animatedScrollTo = function(o) {
    var e = $(o).offset();
    if (isDesktop())
        TweenMax.to($("html,body"), 1.5, {
            scrollTop: e.top,
            ease: Quint.easeOut
        });
    else {
        var t = $(".nav .head").height() + 40;
        TweenMax.to($("html,body"), 1.5, {
            scrollTop: e.top - t,
            ease: Quint.easeOut
        }),
            $(".scroll").removeClass("open"),
            $("#nav-icon3").removeClass("open")
    }
}, mobileScrollToStores = function() {
    isMobile() && $(window).load(function() {
        "#m-store" === window.location.hash && animatedScrollTo("#m-store-mobile")
    })
}, bindMainSliderControls = function() {
    if ($(".main-slideshow>img").length > 1) {
        var o = $(".main-slideshow");
        $(".slideshow-wrapper .arrow-left").click(function() {
            o.cycle("prev")
        }),
            $(".slideshow-wrapper .arrow-right").click(function() {
                o.cycle("next")
            }),
            isDesktop() && o.hasClass("full-slideshow") ? ($(".slideshow-wrapper .arrow-left").css({
                left: "5px"
            }),
                $(".slideshow-wrapper .arrow-right").css({
                    right: "5px"
                }),
                o.hover(function() {
                    $(".slideshow-wrapper .slideshow-controls").fadeIn()
                }, function() {
                    $(".slideshow-wrapper .slideshow-controls").fadeOut()
                })) : $(".slideshow-wrapper .slideshow-controls").show()
    }
    $(".main-slideshow>img[data-id-product]").click(function() {
        var o = $(this).data("id-product");
        o && showDetailModal(o, idLanding, function() {
            showFooter()
        })
    })
}, getMoreProducts = function () {
    $("body").on("click", ".btn-more-products", function () {
        var e = $(this), o = e.data("next-offset"), t = e.data("limit"),
            i = e.data("id-landing");
        $.ajax({
            url: natuzziUrl + "get-products/" + o + "/" + t + "/" + idLanding,
            method: "GET",
            dataType: "json",
            data: {offset: o, limit: t, idLanding: i, lang: lang},
            success: function (i) {
                var s = $(i.template);
                i.end ? $(".btn-more-products").hide() : e.data("next-offset", o + t), $(".list-products").append(s)
            }
        })
    })
}, closeDetail = function () {
    $("body").on("click", ".btn-close", function () {
        $(".detail-product").fadeOut(function () {
            $(".detail-product").html("")
        }), $(".single-product").removeClass("disabled")
    })
}, bindCoatingSelections = function () {
    $(".coating-btn").click(function () {
        $(".coating-btn.active").removeClass("active"), $(this).addClass("active");
        var e = $(this).data("coatings-ref");
        $(".materials.visible").removeClass("visible"), $(e).addClass("visible")
    })
}, showDetailModal = function (e, o, t) {
    var i = $(window).scrollTop();
    $(".detail-product").css({top: i}), $(".single-product>a[data-id!=" + e + "]").parent().addClass("disabled"), $(".detail-product").html('<i class="loading fa fa-cog fa-spin fa-3x fa-fw"></i>').fadeIn();
    $.ajax({
        url: natuzziUrl + "get-product-detail/" + o + "/" + e, //"get-product-detail",
        method: "GET",
        dataType: "json",
        // data: {idLanding: o, idProduct: e, lang: lang},
        success: function (e) {
            $(".detail-product").html(e.template), bindCoatingSelections(), closeDetail();
            var o = $(".detail-product .product-title").find("h3").text();
            trackEvent("products", "detail-product", o), t && t()
        }
    })
}, openDetail = function () {
    $("body").on("click", ".single-product a[data-id]", function () {
        var e = $(this).data("id"), o = $(this).data("id-landing");
        showDetailModal(e, o)
    }), idProductToShow && showDetailModal(idProductToShow, idLanding)
}, bindFormStoreMobile = function () {
    $("form.form-store-mobile").submit(function (e) {
        e.preventDefault()
    }), $("#pac-input-mobile").focus(function () {
        $(this).val("")
    })
};
$("#nav-icon3").click(function () {
    $(this).toggleClass("open"), $(".scroll").toggleClass("open")
});
var popout = function () {
    $(document).mouseleave(function () {
        Cookies.get("wasa_popup_show") || openPopout()
    })
}, openPopout = function () {
    $(".total-black").fadeIn(), $(".popout").fadeIn(), closePopout()
}, closePopout = function () {
    $(".close-popout").on({
        click: function () {
            $(".total-black").fadeOut(), $(".popout").fadeOut();
            var e = "wasa_popup_show", o = !0, t = 30;
            Cookies.set(e, o, {expires: t})
        }
    })
}, openPopThankyou = function () {
    $(".total-black").fadeIn(), $(".pop-thankyou").fadeIn(), closePopThankyou()
}, closePopThankyou = function () {
    $(".close-pop-thankyou").on({
        click: function () {
            $(".total-black").fadeOut(), $(".pop-thankyou").fadeOut()
        }
    })
}, openFooter = function () {
    bindFooterToScroll()
}, bindCloseFooter = function () {
    $(".sticky-footer").click(function () {
        $(".sticky-footer").fadeOut()
    })
}, isFooterVisible = !1, showFooter = function () {
    $(".sticky-footer").fadeIn(), bindCloseFooter(), isFooterVisible = !0
}, bindFooterToScroll = function () {
    $(window).load(function () {
        var e = !1, o = $($(".single-product").get(1)), t = o.length ? o.offset().top : 0,
            i = $("#m-form"), s = i.offset().top;
        $(window).bind("scroll.checkFooter", function () {
            e = !0
        }), setInterval(function () {
            if (e) {
                e = !1;
                var o = $(window).scrollTop() + $(window).height();
                isFooterVisible ? o > s && $(".sticky-footer").fadeOut() : o > t && showFooter()
            }
        }, 250)
    })
}, sendForm = function (e, o) {
    e.on({
        click: function () {
            var e = $(this), t = e.closest("form"), i = t.serialize();
            e.attr("disabled", !0), e.find(".btn-loading").removeClass("fa-chevron-circle-right"), e.find(".btn-loading").addClass("fa-spinner fa-spin"), $.ajax({
                type: "POST",
                dataType: "json",
                url: o,
                data: {lang: lang, data: i},
                success: function (o) {
                    if (e.attr("disabled", !1), e.find(".btn-loading").addClass("fa-chevron-circle-right"), e.find(".btn-loading").removeClass("fa-spinner fa-spin"), o.error === !0) {
                        switch (o.error_type) {
                            case"fields":
                                bootbox.alert({
                                    title: labels.FORM_ERROR_TITLE,
                                    backdrop: !0,
                                    message: labels.FORM_ERROR_TEXT
                                });
                                break;
                            default:
                                bootbox.alert({
                                    title: labels.FORM_ERROR_TITLE,
                                    backdrop: !0,
                                    message: labels.FORM_ERROR_TEXT_GENERAL
                                })
                        }
                    }
                    else {
                        location.href = url + "/" + lang + "/thankyou-page?slug=" + landingSlug
                    }
                }
            })
        }
    })
}, trackEvent = function (e, o, t) {
    "function" == typeof ga && ga("send", "event", e, o, t)
}, setCookiePolicy = function () {
    var e = "wasa_cookies_policy", o = "ok", t = 100;
    Cookies.set(e, o, {expires: t}), $(".wasa_cookies").hide()
}, getCookiePolicy = function () {
    var e = Cookies.get("wasa_cookies_policy");
    "ok" === e ? $(".wasa_cookies").hide() : $(".wasa_cookies").show(), $(window).scroll(setCookiePolicy)
}, isDesktop = function () {
    return "block" === $(".is-desktop").css("display") ? !0 : !1
}, isTablet = function () {
    return "block" === $(".is-tablet").css("display") ? !0 : !1
}, isMobile = function () {
    return "block" === $(".is-mobile").css("display") ? !0 : !1
};
$(function () {
    if (getCookiePolicy(), isDesktop() ? $("[data-scroll-to]").click(function (e) {
            e.preventDefault();
            var o = $(this).data("scroll-to");
            animatedScrollTo(o)
        }) : $("[data-scroll-to-mobile]").click(function (e) {
            e.preventDefault();
            var o = $(this).data("scroll-to-mobile");
            animatedScrollTo(o)
        }), (isMobile() || isTablet()) && openFooter(), $("body").on("hidden.bs.modal", ".modal", function () {
            $(this).removeData("bs.modal")
        }), isMobile()) {
        var e = new Date, o = e.getHours(),
            t = e.getMinutes() < 10 ? "0" + e.getMinutes() : e.getMinutes(),
            i = (e.getDay(), o.toString() + t.toString());
        time && parseInt(i) >= parseInt(time.from) && parseInt(i) <= parseInt(time.to) ? ($(".call-mobi").show(), console.info("OPEN")) : ($(".call-mobi").hide(), $(".call-desk").show(), console.info("CLOSE"))
    }
});
// })(jQuery);