<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
} else {
    $is_desktop = TRUE;
}
$base = base_path() . "sites/all/themes/furnitheme/templates/featured_pages";
$base_folder = base_path() . "sites/all/themes/furnitheme/templates/featured_pages/natuzzi_thanksgiving_sale";


?>

    <link href="https://vjs.zencdn.net/5.19.1/video-js.css" rel="stylesheet">
    <link href="<?php print $base; ?>/natuzzi_thanksgiving_sale/css/app_main.css" rel="stylesheet"
          type="text/css" media="all"/>

    <style>
        .promo .banner img.title.custom {
            margin-top: 0px;
            position: absolute;
            margin-top: 15px;
            position: static;
        }
    </style>


    <?php if (isset($messages)) : ?>
        <?php print $messages; ?>
    <?php endif; ?>

    <div class="container-fluid" id="m-top">
    <div class="promo">
        <div class="detail-product"><i class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>


        <div id="template-harmony-friday">
            <!-- video -->
            <section class="video visible-lg visible-md start-stop">
                <video id="video_dk" class="video-js vjs-fluid vjs-4-3 mid-video with-play control-audio" preload="auto"
                       loop playsinline data-setup='{"fluid": true,"autoplay": true}'
                       poster="<?php print $base_folder; ?>/images/video_poster.jpg">
                    <source src="https://player.vimeo.com/external/242556287.sd.mp4?s=a42ba88e49e0c5776ffb9bca0e8ed717deac3fa7&profile_id=165"
                            type='video/mp4'>
                </video>
            </section>
            <section class="video visible-sm visible-xs start-stop">
                <video id="video_mb" class="video-js vjs-fluid mid-video with-play control-audio video-mobile"
                       preload="auto" loop playsinline data-setup='{"fluid": true,"autoplay": false}'
                       poster="<?php print $base_folder; ?>/images/video_poster.jpg">
                    <source src="https://player.vimeo.com/external/242556287.sd.mp4?s=a42ba88e49e0c5776ffb9bca0e8ed717deac3fa7&profile_id=165"
                            type='video/mp4'>
                </video>
            </section>
            <section class="text-uppercase font-big text-center boxed harmony-friday-title">
                HARMONY <span class='font-didot'>friday</span></section>
            <section class="" id="m-gif">
                <div class="text-center gif-text font-big boxed">
                    The amazing versatility of our floor sample collection at <strong>20% off</strong> and <strong>FREE local delivery</strong>.
                </div>
                <img class="img-responsive" src="<?php print $base_folder; ?>/images/herman.gif"
                alt="DRESS YOUR home LIKE YOURSELF."/>

                <div class="single-product">
                     <a href="javascript:;"
                        data-id="14564" data-id-landing="121"
                           class="btn btn-discover"><strong>DISCOVER</strong> Herman&nbsp;&nbsp;<i
                            class="btn-loading fa fa-chevron-right" aria-hidden="true"></i>

                     </a>
                </div>
            </section>
            <section class="show-sticky-footer slide-text-image" id="m-services">
                <div class="title">services included</div>
                <a href="javascript:;" class="prev"><img
                            src="<?php print $base_folder; ?>/images/icons/arrow-left.png"/></a><a
                        href="javascript:;" class="next"><img
                            src="<?php print $base_folder; ?>/images/icons/arrow-right.png"/></a>
                <div class="owl-carousel owl-theme">
                    <div class="item dark">
                        <div class="preview"><h3>FREE LOCAL<br/>DELIVERY</h3></div>
                    </div>
                    <div class="item dark">
                        <div class="preview"><h3>TAILORED<br/>PROJECTS</h3></div>
                    </div>
                    <div class="item dark">
                        <div class="preview"><h3>3D INTERIOR<br/> DESIGN</h3></div>
                    </div>
                    <div class="item dark">
                        <div class="preview"><h3>LIFETIME<br/>WARRANTY*</h3></div>
                    </div>
                </div>
                <a href="javascript:;" data-scroll-to="#m-col" data-scroll-to-mobile="#m-col" class="btn btn-discover"><span style="font-style: normal;"><b>LIVING, DINING, BED</b></span>&nbsp;&nbsp;<i class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a>
            </section>
            <section class="slide-video-image" id="m-slide-video">
                    <a href="javascript:;" class="prev"><img src="<?php print $base_folder; ?>/images/icons/arrow-left-white.png"/></a>
                    <a href="javascript:;" class="next"><img src="<?php print $base_folder; ?>/images/icons/arrow-right-white.png"/></a>
                <div class="owl-carousel owl-theme">
                    <div class="item start-stop"><img class="img-responsive"
                                                      src="<?php print $base_folder; ?>/images/herman_ambient.jpg"
                                                      alt="Zero wall system"/>
                        <div class="bg-dark boxed"><span class="text-uppercase font-medium"><div
                                        style='margin-top: 50px;'>ONLY UNTIL DECEMBER 3<sup>rd</sup></div></span>
                            <!-- <br/><br/> --><span class="font-didot font-small subtitle">Exclusive purchasing terms on floor sample sofas, beds and furnishing, entirely made in Italy.</span>
                        </div>
                    </div>
                    <div class="item start-stop">
                        <video id="video_2" class="video-js vjs-fluid mid-video with-play control-audio" preload="auto"
                               loop playsinline data-setup='{"fluid": true,"autoplay": false}'
                               poster="<?php print $base_folder; ?>/images/poster-slide-1.jpg">
                            <source src="https://player.vimeo.com/external/206573382.hd.mp4?s=657581259daf5d79cac8ea33991ea0364474e0d1&profile_id=119"
                                    type='video/mp4'>
                        </video>
                        <!-- <video width="100%" loop poster="images/_tmp/harmony/poster-slide-1.jpg"><source src="https://player.vimeo.com/external/206573382.hd.mp4?s=657581259daf5d79cac8ea33991ea0364474e0d1&profile_id=119" type="video/mp4"></video> -->
                        <div class="bg-dark boxed"><span class="text-uppercase font-medium">LATEST INNOVATIONS</span>
                            <!-- <br/><br/> --><span class="font-didot font-small subtitle">Means adapting relax to your spaces thanks to successful innovations that have changed the world of furniture forever</span><a
                                    data-toggle="slide-modal" data-target="#pop-slide-2"
                                    class="btn btn-discover"><strong>DISCOVER</strong> CRAFTSMANSHIP AND TECHNOLOGY&nbsp;&nbsp;<i
                                        class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a></div>
                    </div>
                    <div class="item start-stop"><img class="img-responsive"
                                                      src="<?php print $base_folder; ?>/images/poster-slide-2.jpg"
                                                      alt="Zero wall system"/>
                        <div class="bg-dark boxed"><span class="text-uppercase font-medium">TOTAL COMFORT</span>
                            <!-- <br/><br/> --><span class="font-didot font-small subtitle">for your dreams</span><a
                                    data-toggle="slide-modal" data-target="#pop-slide-3"
                                    class="btn btn-discover"><strong>DISCOVER</strong> OUR COMMITMENT TO QUALITY&nbsp;&nbsp;<i
                                        class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a></div>
                    </div>
                    <div class="item start-stop"><img class="img-responsive"
                                                      src="<?php print $base_folder; ?>/images/poster-slide-3.jpg"
                                                      alt="Zero wall system"/>
                        <div class="bg-dark boxed"><span class="text-uppercase font-medium">LIFESTYLE OFFER</span>
                            <!-- <br/><br/> --><span class="font-didot font-small subtitle">a way to accompany your day, step by step</span><a
                                    data-toggle="slide-modal" data-target="#pop-slide-4"
                                    class="btn btn-discover"><strong>DISCOVER</strong> FURNISHINGS AND ACCESSORIES&nbsp;&nbsp;<i
                                        class="btn-loading fa fa-chevron-right" aria-hidden="true"></i></a></div>
                    </div>
                </div>
                <div class="slide-modal animated" id="pop-slide-2">
                    <div class="slide-modal-body">
                        <button type="button" class="close" data-dismiss="slide-modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <div class="container">
                            <div class="text-center"><span
                                        class="text-uppercase font-medium">LATEST INNOVATIONS</span><br/>
                                <div class="font-didot font-small">Means adapting relax to your spaces.</div>
                                <br/><br/></div>
                            <div class="text-justify"><p><em>The Zerowall space-saving system eliminates the need to
                                        move the sofa away from the wall when the Soft Touch mechanism simultaneously
                                        opens
                                        the
                                        seat, headrest and footrest.</em></p>
                                <p>The Natuzzi history has been characterised by successful innovations that have
                                    changed the world of furniture forever. Some examples are: the coloured leathers of
                                    the 1980s, the recliner mechanisms of the 1990s, or the <strong>latest
                                        generation</strong> motion sofas of the new millennium. </p>
                                <hr/>
                                <p><strong>Relax mechanism</strong> for seat, headrest, and footrest at your complete
                                    discretion so you can reach your ideal resting position. Features that enhance the
                                    comfort of model. Eco-friendly non-toxic polyurethane padding ensures lasting
                                    comfort and respect for human health and the environment.  </p><img
                                        src="<?php print $base_folder; ?>/images/sofa-relax.jpg"
                                        class="img-responsive visible-md visible-lg" alt="Sofa Relax"/><img
                                        src="<?php print $base_folder; ?>/images/sofa-relax-mobile.jpg"
                                        class="img-responsive visible-sm visible-xs" alt="Sofa Relax"/>
                                <p>Choose the model that best suits your comfort needs, visit one of our Natuzzi stores
                                    or galleries and discover special models with a mix of feathers, memory foam
                                    and polyurethane padding to guarantee extraordinary comfort, offering support and
                                    lasting good looks. </p>
                                <hr/>
                                <p><strong>Sofa-beds</strong> are ideal for those who are looking for an elegant sofa
                                    during the day and a convenient bed at night, preserving a seat of excellent
                                    comfort. The <strong>easy-to-open version</strong>, where the bed can be opened in
                                    few seconds, guarantees elegance while giving a high quality rest, combining
                                    aesthetic with comfort and functionality.  </p><img
                                        src="<?php print $base_folder; ?>/images/sofa-beds.jpg"
                                        class="img-responsive visible-md visible-lg" alt="Sofa Beds"/><img
                                        src="<?php print $base_folder; ?>/images/sofa-beds-mobile.jpg"
                                        class="img-responsive visible-sm visible-xs" alt="Sofa Beds"/>
                                <hr/>
                                <p>Sofas and armchairs now play music.<br/><strong>With integrated audio system</strong>,
                                    sofas like Surround and armchairs like Sound are characterized by great design,
                                    multi-sensorial comfort and a technological soul.<br/>Music notes are like pillows
                                    where to accommodate the silence of loneliness, funny chats and secrets. </p><img
                                        src="<?php print $base_folder; ?>/images/sofa-audio.jpg"
                                        class="img-responsive visible-md visible-lg" alt="Sofa Audio"/><img
                                        src="<?php print $base_folder; ?>/images/sofa-audio-mobile.jpg"
                                        class="img-responsive visible-sm visible-xs" alt="Sofa Audio"/>
                                <p>The desire to push ourselves and imagine things that have never been done before has
                                    always been an important characteristic of our history. We combine research,
                                    creativity, skilled craftsmanship and technology in a process that has been
                                    perfected through our years of dedicated work in furniture.  </p>
                                <p>Only through this, can harmony take shape in our hands.</p></div>
                            <a href="javascript:;" class="btn btn-discover" data-scroll-to-mobile="#footer-addresses"
                               data-scroll-to="#footer-addresses"
                               onclick="trackEvent('store', 'click-visit-from-popup', 'zero wall system')"><strong>COME
                                    AND TEST SOFAS QUALITY</strong>&nbsp;&nbsp;<img
                                        src="<?php print $base_folder; ?>/images/trova-negozio-white.svg" height="19"/></a>
                        </div>
                    </div>
                </div>
                <div class="slide-modal animated" id="pop-slide-3">
                    <div class="slide-modal-body">
                        <button type="button" class="close" data-dismiss="slide-modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <div class="container">
                            <div class="text-center"><span class="text-uppercase font-medium">TOTAL COMFORT</span><br/>
                                <div class="font-didot font-small">for your dreams</div>
                                <br/><br/></div>
                            <div class="text-justify"><p><em>We have always been committed to quality. When it comes to
                                        sleep, quality is the most important thing of all.</em></p>
                                <p>This is why we decided to enter an exclusive partnership with TRECA, leading
                                    manufacturer of highend mattresses. Since 1935, TRECA has controlled the entire
                                    production process, from the choice of raw materials - all strictly of the highest
                                    quality - to the smallest details of the finished mattresses, which are still
                                    hand-sewn today. Their expertise is unique, allowing them to combine natural
                                    materials and cutting-edge technology to ensure the ultimate in comfort, rest and
                                    wellbeing. We could not have made a better choice.</p>
                                <hr/>
                                <p><strong>Beds</strong> - The Natuzzi Bed Collection consists of beds in various sizes
                                    and styles. All of our beds have a European slatted bed suspension and mattresses,
                                    to meet the needs of all markets. We offer the same vast choice of leather and
                                    fabric coverings as the Sofa Collection. The range of functions is also extensive:
                                    beds are available with space-saving storage bases with precision movements; one
                                    model also has an adjustable headboard.</p>
                                <img src="<?php print $base_folder; ?>/images/beds.jpg"
                                     class="img-responsive visible-md visible-lg"
                                     alt="Beds"/><img
                                        src="<?php print $base_folder; ?>/images/beds-mobile.jpg"
                                        class="img-responsive visible-sm visible-xs" alt="Beds"/>
                                <p>We have completed our Beds and Accessories Collection with exclusive bedlinen ranges.
                                    As tactile as <strong>linen</strong> or as sensual as <strong>satin</strong>. From
                                    cold, rational tones to warmer, brighter hues. All embellished with stylish details.
                                </p>
                                <p>To transform the dream into a personal world, we have supplemented the Bed Collection
                                    with accessories for the sleeping area. </p>
                                <hr/>
                                <p>A versatile collections which reflect the idea of quality and customization at the
                                    heart of the Natuzzi Italia world. Dressers and nightstands designed <strong>in
                                        collaboration with major names from the world of design</strong>. These pieces
                                    are designed with flexibility in mind, allowing them also to be used in a living
                                    area. </p></div>
                            <a href="javascript:;" class="btn btn-discover" data-scroll-to-mobile="#footer-addresses"
                               data-scroll-to="#footer-addresses"
                            ><strong>COME
                                    AND TEST BEDS QUALITY</strong>&nbsp;&nbsp;<img
                                        src="<?php print $base_folder; ?>/images/trova-negozio-white.svg" height="19"/></a>
                        </div>
                    </div>
                </div>
                <div class="slide-modal animated" id="pop-slide-4">
                    <div class="slide-modal-body">
                        <button type="button" class="close" data-dismiss="slide-modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <div class="container">
                            <div class="text-center"><span
                                        class="text-uppercase font-medium">LIFESTYLE OFFER</span><br/>
                                <div class="font-didot font-small"> a way to accompany your day, step by step</div>
                                <br/><br/></div>
                            <div class="text-justify"><p>The Natuzzi Italia collection stands out for high quality in
                                    the choice of materials and finishes, as well as for the creativity and details of
                                    its
                                    design.</p>
                                <p>Furniture (wall units, coffee tables, tables, chairs, lamps and
                                    carpets) and accessories (vases, mirrors, clocks, magazines racks, trays, room
                                    fragrances and decorative objects) are designed by Natuzzi Style Center and in
                                    collaboration with major names from the world of design, to offer complete
                                    furnishings and make your Total Living unique and personal.</p><img
                                        src="<?php print $base_folder; ?>/images/lifestyle-1.jpg"
                                        class="img-responsive visible-md visible-lg" alt="lifestyle"/><img
                                        src="<?php print $base_folder; ?>/images/lifestyle-1-mobile.jpg"
                                        class="img-responsive visible-sm visible-xs" alt="lifestyle"/>
                                <p>Always in harmony with you and the things around you, these objects stand out for
                                    their innovative shapes and materials <strong>such as Carrara
                                        marble</strong> and <strong>Murano glass</strong>. Furthermore, all <strong>wood-based
                                        materials</strong> are produced in accordance with the laws governing
                                    formaldehyde content, to ensure our products are completely non-toxic. We only use
                                    wood sourced from renewable forests. </p><img
                                        src="<?php print $base_folder; ?>/images/lifestyle-2.jpg"
                                        class="img-responsive visible-md visible-lg" alt="lifestyle"/><img
                                        src="<?php print $base_folder; ?>/images/lifestyle-2-mobile.jpg"
                                        class="img-responsive visible-sm visible-xs" alt="lifestyle"/></div>
                            <a href="javascript:;" class="btn btn-discover" data-scroll-to-mobile="#footer-addresses"
                               data-scroll-to="#footer-addresses"
                            ><strong>COME
                                    AND TEST LIFESTYLE QUALITY</strong>&nbsp;&nbsp;<img
                                        src="<?php print $base_folder; ?>/images/trova-negozio-white.svg" height="19"/></a>
                        </div>
                    </div>
                </div>
            </section>

        </div>



            <!-- other promo -->
            <?php include_once dirname(__FILE__) . '/thanksgiving-sale-categories.tpl.php'; ?>

            <section class="legal-notes visible-lg visible-md"><p>
                    *All Natuzzi sofas and armchairs have a lifetime warranty on the frame and two years on the
                    upholstery,
                    mechanical parts and coverings. All furnishing accessories carry a two year warranty.
                </p>
            </section>


            <!-- why -->
            <!--<section class="why" id="m-why">
                <div class="content">
                    <div class="top">
                        <p><span
                                    class="size1">COME AND TEST</span><br/>NATUZZI
                            QUALITY
                        </p>
                    </div>
                    <div class="plus row">
                        <div class="col-5-div no-bt"><img
                                    src="https://www.natuzzi.us/promotions/images/icons/colori.svg"/><br/><br/>360
                            COLOURS IN
                            LEATHER OR FABRIC
                        </div>
                        <div class="col-5-div no-bt no-br"><img
                                    src="https://www.natuzzi.us/promotions/images/icons/design.svg"/><br/><br/>MADE
                            IN ITALY
                            DESIGN AND TECHNOLOGY
                        </div>
                        <div class="col-5-div"><img
                                    src="https://www.natuzzi.us/promotions/images/icons/quality.svg"/><br/><br/>NATUZZI
                            WARRANTY
                        </div>
                        <div class="col-5-div no-br"><img
                                    src="https://www.natuzzi.us/promotions/images/icons/pointer.svg"/><br/><br/>COME
                            AND VISIT US
                        </div>
                        <div class="col-5-div hidden-sm hidden-xs"><img
                                    src="https://www.natuzzi.us/promotions/images/icons/chiama.svg"/><br/><br/>CALL
                            US FOR MORE
                            INFO
                        </div>
                    </div>
                </div>
            </section> -->

        </div>
    </div>


<?php include_once dirname(__FILE__) . '/thanksgiving-sale-form.tpl.php'; ?>


<a class="sticky-footer" href="javascript:;" data-scroll-to-mobile="#m-form">
    I WANT TO TAKE ADVANTAGE OF
    <br/><span class="btn-call">BLOCK THE OFFER&nbsp;&nbsp;<i
                class="btn-loading fa fa-chevron-down"
                aria-hidden="true"></i></span></a>
<div class="total-black">
    <div class="popout"><span class="close-popout"></span></div>
</div>
<!-- /.modal -->


<div class="is-desktop"></div>
<div class="is-tablet"></div>
<div class="is-mobile"></div><!-- vendor cdn -->


<script src="https://use.fontawesome.com/f74604f2dc.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha256-U5ZEeKfGNOja007MMD3YBI0A3OSZOQbeG6z2f2Y0hu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"
        integrity="sha256-NjbogQqosWgor0UBdCURR5dzcvAgHnfUZMcZ8RCwkk8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"
        integrity="sha256-YrHP9EpeNLlYetSffKlRFg8VWcXFRbz5nhNXTMqlQlo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"
        integrity="sha256-4F7e4JsAJyLUdpP7Q8Sah866jCOhv72zU5E8lIRER4w=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"
        integrity="sha256-4O4pS1SH31ZqrSO2A/2QJTVjTPqVe+jnYgOWUVr7EEc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"
        integrity="sha256-jDnOKIOq2KNsQZTcBTEnsp76FnfMEttF6AV2DF2fFNE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"
        integrity="sha256-iMTCex8BQ+iVxpZO83MoRkKBaoh9Dz9h3tEVrM5Rxqo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.center.min.js"
        integrity="sha256-puNNvBbwmmw0WIyDXJ5cTd0W/EAiCArmnjo5gUfDxC4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.swipe.min.js"
        integrity="sha256-Prq5k7iqQbFCbpoBYTw3cqMbUZyKh3Y0BBGPBfcxs20=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"
        integrity="sha256-ffw+9zwShMev88XNrDgS0hLIuJkDfXhgyLogod77mn8=" crossorigin="anonymous"></script>
<script src="https://hammerjs.github.io/dist/hammer.js"></script>
<script>
    WebFont.load({
        google: {
            families: ['Lato:100,300,400,700']
        }
    });
</script>

<script src="<?php print $base_folder; ?>/js/app_script.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script src="<?php print $base_folder; ?>/js/videojs.min.js"></script>





<script>

    (function($) {


        lang = 'en-US';
        labels = {
            "FORM_ERROR_TITLE": "Attention",
            "FORM_ERROR_TEXT": "Check the required fields",
            "FORM_ERROR_TEXT_GENERAL": "A general error occurred. Please try again later.",
            "NL_STORE_ERROR_GPS": "Your device doesn't support localization",
            "NL_STORE_ERROR_LOCATION": "We couldn't get your position"
        };
        url = '//www.natuzzi.us/promotions';
        landingSlug = 'sale';
        userPosition = 0;
        idLanding = 158;

        time = {"from": "900", "to": "1800"};

        console.log("outside block");

        $(function () {
            // preload images
            $('.banner').imagesLoaded(function () {
                $('.img-loading').addClass('hidden');
                $('.main-slideshow').removeClass('hidden');
            });

            bindMainSliderControls();

            openDetail();
            getMoreProducts();

            // mobile
            bindFormStoreMobile();
            handleVideoJSPlugin();

            //////////////////////////////// SLICK CAROUSEL
            $('.slide-video-image .owl-carousel').slick({
                infinite: true,
                slidesToShow: 1,
                prevArrow: $('.slide-video-image .prev'),
                nextArrow: $('.slide-video-image .next')
            });
            $('.slide-text-image .owl-carousel').slick({
                infinite: true,
                slidesToShow: 1,
                adaptiveHeight: true,
                prevArrow: $('.slide-text-image .prev'),
                nextArrow: $('.slide-text-image .next')
            });


            handleSlideModals();

        });


        WebFont.load({
            google: {
                families: ['Lato:100,300,400,700']
            }
        });


    })(jQuery);

</script>
