"use strict";
var lang, url, googleMap, idProductToShow, idLanding, labels, time, landingSlug, offset = 0, limit = 4, styleMap = [{
    featureType: "all",
    elementType: "geometry.fill",
    stylers: [{weight: "2.00"}]
}, {featureType: "all", elementType: "geometry.stroke", stylers: [{color: "#9c9c9c"}]}, {
    featureType: "all",
    elementType: "labels.text",
    stylers: [{visibility: "on"}]
}, {featureType: "landscape", elementType: "all", stylers: [{color: "#f2f2f2"}]}, {
    featureType: "landscape",
    elementType: "geometry.fill",
    stylers: [{color: "#ffffff"}]
}, {
    featureType: "landscape.man_made",
    elementType: "geometry.fill",
    stylers: [{color: "#ffffff"}]
}, {featureType: "poi", elementType: "all", stylers: [{visibility: "off"}]}, {
    featureType: "road",
    elementType: "all",
    stylers: [{saturation: -100}, {lightness: 45}]
}, {featureType: "road", elementType: "geometry.fill", stylers: [{color: "#eeeeee"}]}, {
    featureType: "road",
    elementType: "labels.text.fill",
    stylers: [{color: "#7b7b7b"}]
}, {
    featureType: "road",
    elementType: "labels.text.stroke",
    stylers: [{color: "#ffffff"}]
}, {
    featureType: "road.highway",
    elementType: "all",
    stylers: [{visibility: "simplified"}]
}, {featureType: "road.arterial", elementType: "labels.icon", stylers: [{visibility: "off"}]}, {
    featureType: "transit",
    elementType: "all",
    stylers: [{visibility: "off"}]
}, {featureType: "water", elementType: "all", stylers: [{color: "#46bcec"}, {visibility: "on"}]}, {
    featureType: "water",
    elementType: "geometry.fill",
    stylers: [{color: "#c8d7d4"}]
}, {featureType: "water", elementType: "labels.text.fill", stylers: [{color: "#070707"}]}, {
    featureType: "water",
    elementType: "labels.text.stroke",
    stylers: [{color: "#ffffff"}]
}],
    // iconDivani = "images/icons/pin-ded.png",
    // iconStore = "images/icons/pin-store.png",
    // iconGallery = "images/icons/pin-gallery.png",
    // iconEssence = "images/icons/pin-essence.png",
    // iconRevive = "images/icons/pin-revive.png",

    iconDivani = "images/landingpage/map-marker.png",
    iconStore = "images/landingpage/map-marker.png",
    iconGallery = "images/landingpage/map-marker.png",
    iconEssence = "images/landingpage/map-marker.png",
    iconRevive = "images/landingpage/map-marker.png",

GoogleMap = function () {
    function e(e, o) {
        if (document.getElementById("autocomplete-list").innerHTML = "", google.maps.places.PlacesServiceStatus.OK === o)for (var t = 0; t < e.length; t++) {
            var i = document.createElement("li");
            i.setAttribute("data-description", e[t].description), i.setAttribute("data-place-id", e[t].place_id), i.innerHTML = e[t].description, document.getElementById("autocomplete-list").appendChild(i)
        }
    }

    function o(e) {
        var o, i = new google.maps.LatLng(e.geoloc_lat, e.geoloc_lon);
        switch (e.type) {
            case"Natuzzi Store":
                o = iconStore;
                break;
            case"Natuzzi Gallery":
                o = iconGallery;
                break;
            case"Point Essence":
                o = iconEssence;
                break;
            case"Natuzzi Re-vive":
                o = iconRevive;
                break;
            case"Divani e Divani":
                o = iconDivani
        }
        var a = t(i, o), l = '<div class="info-window"><h3>' + e.title + "</h3><p>" + e.description + "</p></div>", s = new google.maps.InfoWindow({content: l});
        d[c++] = s, google.maps.event.addListener(a, "click", function () {
            for (var e = 0; e < c; e++)d[e].close();
            s.open(n, this)
        })
    }

    function t(e, o) {
        var t = new google.maps.Marker({map: n, position: e, icon: o});
        return a.push(t), s || l.extend(e), t
    }

    function i(e) {
        t(new google.maps.LatLng(e.latitude, e.longitude), "images/icons/pin-user.png")
    }

    var n, a, l, s, r, c, d, u, p, f, g, m, h, y;
    return {
        initMap: function () {
            return n || function () {
                function o(o) {

                    var country = window.location.hostname.split('.').slice(-1)[0];
                    var countries = new Array();
                    switch (country) {
                        case 'com':
                            break;
                        default:
                           // countries.push(country);
                            break;
                    }

                    var t, i = o.target.value;

                    var autocomplete_options = {
                        input: i
                    };

                    if (countries.length > 0){
                        autocomplete_options['componentRestrictions'] = {country: countries}
                    }

                    document.getElementById("autocomplete-list").innerHTML = "",
                    i && i.length >= 3 && (t = autocomplete_options, "pac-input" == o.target.id ? h.getPlacePredictions(t, e) : "pac-input-mobile" == o.target.id && y.getPlacePredictions(t, e))
                }

                var t = $(".map")[0], i = {scrollwheel: !1, styles: styleMap};

                n = new google.maps.Map(t, i),
                a = [],
                s = 0,
                r = !1,
                c = 0,
                d = [],
                g = document.getElementById("pac-input"),
                m = document.getElementById("pac-input-mobile"),
                h = new google.maps.places.AutocompleteService(g),
                y = new google.maps.places.AutocompleteService(m),
                f = new google.maps.Geocoder,
                $("body").on("click", "#autocomplete-list li", function () {
                    var e = $(this).parent("#autocomplete-list").prev("input").attr("id"), o = document.getElementById(e);
                    googleMap.placeCallback($(this).data("description"), $(this).data("place-id"), o)
                }), g.addEventListener("keyup", o), m.addEventListener("keyup", o)
            }(), this
        }, getMapInstance: function () {
            return n
        }, getMarkers: function () {
            return a
        }, setCenter: function (e) {
            n.setCenter({lat: e.latitude, lng: e.longitude})
        }, setZoom: function (e) {
            s = e
        }, setUseUserPosition: function (e) {
            r = e
        }, setPoints: function (e) {
            u = e
        }, setPosition: function (e) {
            p = e
        }, setUserPosition: function (e) {
            i(e)
        }, updateMap: function (e, t) {
            if (n.setCenter({
                lat: e.latitude,
                lng: e.longitude
            }), s ? n.setZoom(s) : l = new google.maps.LatLngBounds, a.forEach(function (e) {
                e.setMap(null)
            }), r && i(e), t && t.length) {
                for (var c = 0; c < t.length; c++)o(t[c]);
                s || (n.fitBounds(l), n.panToBounds(l))
            }
        }, placeCallback: function (e, o, t) {
            t.value = e;
            f.geocode({placeId: o}, function (e, o) {
                google.maps.GeocoderStatus.OK == o && ($(".latitude").val(e[0].geometry.location.lat()), $(".longitude").val(e[0].geometry.location.lng()), document.getElementById("autocomplete-list").innerHTML = "", "pac-input-mobile" == t.id && getStoresMobile())
            })
        }
    }
}(),


animatedScrollTo = function (e) {
    var o = $(e).offset();
    if(typeof o !== 'undefined') {
        if (isDesktop())TweenMax.to($("html,body"), 1.5, {scrollTop: o.top, ease: Quint.easeOut}); else {
            var t = $(".nav .head").height() + 40;
            TweenMax.to($("html,body"), 1.5, {
                scrollTop: o.top - t,
                ease: Quint.easeOut
            }), $(".scroll").removeClass("open"), $("#nav-icon3").removeClass("open")
        }
    }
},


closeDetail = function () {
    $("body").on("click", ".btn-close", function () {
        $(".detail-product").fadeOut(function () {
            $(".detail-product").html("")
        }), $(".single-product").removeClass("disabled")
    })
}, bindCoatingSelections = function () {
    $(".coating-btn").click(function () {
        $(".coating-btn.active").removeClass("active"), $(this).addClass("active");
        var e = $(this).data("coatings-ref");
        $(".materials.visible").removeClass("visible"), $(e).addClass("visible")
    })
}, showDetailModal = function (e, o, t) {
    var i = $(window).scrollTop();
    $(".detail-product").css({top: i}), $(".single-product>a[data-id!=" + e + "]").parent().addClass("disabled"), $(".detail-product").html('<i class="loading fa fa-cog fa-spin fa-3x fa-fw"></i>').fadeIn(), $.ajax({
        url: "get-product-detail",
        method: "POST",
        dataType: "json",
        data: {idLanding: o, idProduct: e, lang: lang},
        success: function (e) {
            $(".detail-product").html(e.template), bindCoatingSelections(), closeDetail();
            var o = $(".detail-product .product-title").find("h3").text();
            trackEvent("products", "detail-product", o), t && t()
        }
    })
},

getAutoLocation = function () {
    $(".btn-store-near").click(function () {
        navigator && navigator.geolocation ? navigator.geolocation.getCurrentPosition(successCallback, errorCallback) : swal(labels.FORM_ERROR_TITLE, labels.NL_STORE_ERROR_GPS, "warning");
    })
},

errorCallback = function (e) {
    bootbox.alert({title: labels.FORM_ERROR_TITLE, backdrop: !0, message: labels.NL_STORE_ERROR_LOCATION})
},

getStoresMobile = function () {
    var e = $(".btn-store-near").closest("form").serialize();
    trackEvent("store", "search", e), $.ajax({
        url: "get-stores-mobile",
        method: "POST",
        dataType: "json",
        data: {lang: lang, data: e},
        success: function (e) {
            if (e) {
                $(".slideshow-controls").hide(), $(".store-mobile .results").html(e.template);
                var o = $(".cycle-store");
                o.cycle(), o.find(".single-store").length > 1 && ($(".results-box .slideshow-controls").show(), $(".results-box .slideshow-controls.arrow-left").click(function () {
                    o.cycle("prev")
                }), $(".results-box .slideshow-controls.arrow-right").click(function () {
                    o.cycle("next")
                })), bindStoreResultsButtons(), animatedScrollTo($(".store-mobile .results"))
            }
        }
    })
},

successCallback = function (e) {
    $(".latitude").val(e.coords.latitude), $(".longitude").val(e.coords.longitude), getStoresMobile()
},

bindFormStoreMobile = function () {
    $("form.form-store-mobile").submit(function (e) {
        e.preventDefault()
    }), $("#pac-input-mobile").focus(function (e) {
        $(this).val("")
    })
},

getStores = function () {
    $("form.form-stores").submit(function (e) {
        e.preventDefault(), $(".btn-search").trigger("click")
    }), $("body").on("click", ".btn-search", function () {
        var e = $(".btn-search").closest("form").serialize();
        trackEvent("store", "search", e), $.ajax({
            url: "get-stores",
            method: "POST",
            dataType: "json",
            data: {lang: lang, data: e, source: 'landing'},
            success: function (e) {
                e && (e.setUserPosition && googleMap.setUseUserPosition(!0), e.points ? googleMap.updateMap(e.coords, e.points) : googleMap.updateMap(e.coords, {}), $(".store").html(e.template), bindStoreResultsButtons())
            }
        })
    })
},

bindStoreResultsButtons = function () {
    $(".store-phone-link").click(function (e) {
        var o = $(this).closest(".single-store").find(".store-title").text();
        trackEvent("phone", "call", o)
    }), $(".btn-go-to-store").click(function (e) {
        var o = $(this).closest(".single-store").find(".store-title").text();
        trackEvent("store", "go-to-store", o)
    })
};
$("#nav-icon3").click(function () {
    $(this).toggleClass("open"), $(".scroll").toggleClass("open")
});
var popout = function () {
    $(document).mouseleave(function (e) {
        Cookies.get("wasa_popup_show") || openPopout()
    })
}, openPopout = function () {
    $(".total-black").fadeIn(), $(".popout").fadeIn(), closePopout()
}, closePopout = function () {
    $(".close-popout").on({
        click: function () {
            $(".total-black").fadeOut(), $(".popout").fadeOut();
            Cookies.set("wasa_popup_show", !0, {expires: 30})
        }
    })
},

openPopThankyou = function (message) {
    // $(".total-black").fadeIn(), $(".pop-thankyou").fadeIn(), closePopThankyou()
    swal("", message, "success");

},

openFooter = function () {
    bindFooterToScroll()
},

bindCloseFooter = function () {
    $(".sticky-footer").click(function () {
        $(".sticky-footer").fadeOut()
    })
}, isFooterVisible = !1, showFooter = function () {
    $(".sticky-footer").fadeIn(), bindCloseFooter(), isFooterVisible = !0
}, bindFooterToScroll = function () {
    $(window).load(function () {
        var e = !1;
        if ($(".single-product").length > 0)o = $($(".single-product").get(1)).offset().top; else if ($(".show-sticky-footer").length > 0)o = $(".show-sticky-footer").offset().top; else var o = 1.6 * $(window).height();
        var t = $("#m-form"), i = 0 != t.length ? t.offset().top : 0;
        $(window).bind("scroll.checkFooter", function () {
            e = !0
        }), setInterval(function () {
            if (e) {
                e = !1;
                var t = $(window).scrollTop() + $(window).height();
                isFooterVisible ? t > i && $(".sticky-footer").fadeOut() : t > o && showFooter()
            }
        }, 250)
    })
},

sendForm = function (e, o, t, i) {
    e.on({
        click: function () {
            var e = $(this), t = e.closest("form").serialize();
            e.attr("disabled", !0), e.find(".btn-loading").removeClass("fa-chevron-circle-right"), e.find(".btn-loading").addClass("fa-spinner fa-spin"), $.ajax({
                type: "POST",
                dataType: "json",
                url: o,
                data: {lang: lang, data: t},
                success: function (o) {
                    if (e.attr("disabled", !1), e.find(".btn-loading").addClass("fa-chevron-circle-right"), e.find(".btn-loading").removeClass("fa-spinner fa-spin"), !0 === o.error)switch (o.error_type) {
                        case"fields":
                            swal(labels.FORM_ERROR_TITLE, labels.FORM_ERROR_TEXT, "warning");
                            o.fields_errors.forEach(function (field) {
                                $('.'+field).addClass('error');
                            });
                            break;
                        default:
                            swal(labels.FORM_ERROR_TITLE, labels.FORM_ERROR_TEXT_GENERAL, "warning");

                    } else
                    {
                        if (sl_country == "")
                        {
                            location.href = url + "/" + lang + "/thankyou-page?slug=" + landingSlug
                        }
                        else
                        {
                            location.href = url + "/" + lang + "/thankyou-page?slug=" + landingSlug + "&country=" + sl_country
                        }
                    }
                }
            })
        }
    })
}, trackEvent = function (e, o, t) {
    "function" == typeof gtag ? gtag("event", o, {
        event_category: e,
        event_label: t
    }) : "function" == typeof ga && ga("send", "event", e, o, t), "undefined" != typeof dataLayer && dataLayer.push({
        event: "trackEvent",
        eventCategory: e,
        eventAction: o,
        eventLabel: t
    })
}, setCookiePolicy = function () {
    Cookies.set("wasa_cookies_policy", "ok", {expires: 100}), $(".wasa_cookies").hide()
}, getCookiePolicy = function () {
    "ok" === Cookies.get("wasa_cookies_policy") ? $(".wasa_cookies").hide() : $(".wasa_cookies").show(), $(window).scroll(setCookiePolicy)
},
    isDesktop = function () {
        var w = window.innerWidth;
        return (w > 1000) ? true : false;
    // return "block" === $(".is-desktop").css("display")
},
    isTablet = function () {
    return "block" === $(".is-tablet").css("display")
},
    isMobile = function () {
        var w = window.innerWidth;
        return (w < 1000) ? true : false;
        // return "block" === $(".is-mobile").css("display")
};
$(function () {
    if (getCookiePolicy(), isDesktop() ? $("[data-scroll-to]").click(function (e) {
        e.preventDefault();
        var o = $(this).data("scroll-to");
        animatedScrollTo(o)
    }) : ($("[data-scroll-to-mobile]").click(function (e) {
        e.preventDefault();
        var o = $(this).data("scroll-to-mobile");
        animatedScrollTo(o)
    }), $("#autocomplete-list").insertAfter($("#pac-input-mobile"))), (isMobile() || isTablet()) && openFooter(), $("body").on("hidden.bs.modal", ".modal", function () {
        $(this).removeData("bs.modal")
    }), isMobile()) {
        var e = new Date, o = e.getHours(), t = e.getMinutes() < 10 ? "0" + e.getMinutes() : e.getMinutes(), i = (e.getDay(), o.toString() + t.toString());
        time && parseInt(i) >= parseInt(time.from) && parseInt(i) <= parseInt(time.to) ? $(".call-mobi").show() : ($(".call-mobi").hide(), $(".call-desk").show())
    }
});