
$(document).ready(function () {
    $('.collapse')
        .on('shown.bs.collapse', function() {
            $(this)
                .parent()
                .find(".fa-angle-up")
                .removeClass("fa-angle-up")
                .addClass("fa-angle-down");
        })
        .on('hidden.bs.collapse', function() {
            $(this)
                .parent()
                .find(".fa-angle-down")
                .removeClass("fa-angle-down")
                .addClass("fa-angle-up");
        });

});


$('input').focus(function(){
    $(this).parents('.form-group').addClass('focused');
});

$('input').blur(function(){
    var inputValue = $(this).val();
    if ( inputValue == "" ) {
        $(this).removeClass('filled');
        $(this).parents('.form-group').removeClass('focused');
    } else {
        $(this).addClass('filled');
    }
});

/* FadeIn Scroll */
$(document).ready(function() {

    /* Every time the window is scrolled ... */
    $(window).scroll( function(){

        /* Check the location of each desired element */
        $('.fade').each( function(i){

            var bottom_of_object = $(this).position().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){

                $(this).animate({'opacity':'1'},900);

            }

        });

    });

});

/* scroll to top */

$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});

/* scroll to element */

$('.scrollto').click(function (e) {
    e.preventDefault();
    var element_id_to_scroll = $(this).data('to');
    $("html, body").animate({ scrollTop: $('#'+element_id_to_scroll).offset().top }, "slow");
});

/* hide fullwidth on scrool */

// $(document).ready(function() {
//     var doc = document.documentElement;
//     $(window).scroll( function(){
//         var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
//         if(top>10 && $('#containerfade').is(":visible")) $('#containerfade').fadeOut('fast', function() {});
//         // if(top==0 && $('#containerfade').is(":hidden")) $('#containerfade').fadeIn('fast', function() {});
//     });
// });

/* Form button */

$(document).ready(function() {

    var setIconHeight = function () {
        var text_height = $('.btn-form-footer-mobile .btn-text').outerHeight();
        $('.btn-form-footer-mobile .btn-icon').css({
            'width': text_height+'px',
            'height': text_height+'px',
        });
    };

    setIconHeight();
    $(window).resize(function () {
        setIconHeight();
    });


    $('#form_expand_mobile_footer').on('show.bs.collapse', function () {
        $('.button-form-footer-mobile').css({
            'top': 0,
            // 'bottom': '0',
            'height': '100%',
        });
    });

    $('#form_expand_mobile_footer').on('hide.bs.collapse', function () {
        $('.button-form-footer-mobile').css({
            'top': 'auto',
            // 'bottom': 0
            'height': 'auto',
        });
    });

    // FORM CONTENT WIDTH

    var setFormContentWidth = function () {
        $('.btn-form-collapsed').each(function (i, form_button) {
            var width = $(form_button).outerWidth();
            $('#form_expand').css('width', width+'px');
            $('#form_expand_footer').css('width', width+'px');
        });
    };

    setFormContentWidth();
    $(window).resize(function () {
        setFormContentWidth();
    });

});

// MOBILE DETECT

var $isMobile;

$(document).ready(function() {
    $(window).resize(function(){
        onResize()
    });
    onResize()
});

function onResize(){
    var $isMobile = (window.innerWidth <= 1000);
}

