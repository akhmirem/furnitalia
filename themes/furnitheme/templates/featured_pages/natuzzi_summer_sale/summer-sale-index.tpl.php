
<?php
global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
}
else {
  $is_desktop = TRUE;
}
$promo_dir = base_path() . "sites/default/files/promo/stressless/spring_sale";
$base = base_path() . "sites/all/themes/furnitheme/templates/featured_pages";

if ($is_desktop) {
  $img_path = 'SpringIntoComfort_promo_img.jpg';
}
else {
  $img_path = 'SpringIntoComfort_promo_img_mobile.jpg';
}


  $promo_url = base_path() . 'promo/natuzzi-summer-sale';

?>

<link href="<?php print $base; ?>/natuzzi_summer_sale/css/app_main.css" rel="stylesheet"
      type="text/css" media="all"/>

<style>
  .promo .banner img.title.custom {
    margin-top: 0px;
    position: absolute;
    margin-top: 15px; position: static;
  }
</style>


<div class="container-fluid" id="m-top">
  <div class="promo">
    <div class="detail-product"><i
        class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>
    <div class="banner big-slider" style="min-height: 150px">
      <!-- <img src="images/logo.svg" class="img-loading" width="300" height="91"/> --><img
        src="<?php print $base; ?>/natuzzi_summer_sale/images/n-loading.png" class="img-loading"
        style="max-width: 100%" align="middle"/>
      <div class="slideshow-wrapper">
        <div class="cycle-slideshow main-slideshow full-slideshow hidden"
             data-cycle-log="false"
             data-cycle-center-horz="true"
             data-cycle-fx="scrollHorz"
             data-cycle-swipe="true"
        >
          <img src="<?php print $base; ?>/natuzzi_summer_sale/images/sale-ch-de-3-21973-1920x1080.jpg"
               class="img-responsive center-block"/><img
            src="<?php print $base; ?>/natuzzi_summer_sale/images/sale-ch-de-2-21972-1920x1080.jpg"
            class="img-responsive center-block"/>
          <div class="slideshow-controls"><a class="arrow-left"
                                             href="javascript:;"><img
                width="16px"
                height="31px"
                src="https://www.natuzzi.us/promotions/images/icons/arrow-left.svg"/></a><a
              class="arrow-right" href="javascript:;"><img
                width="16px" height="31px"
                src="https://www.natuzzi.us/promotions/images/icons/arrow-right.svg"/></a>
          </div>
        </div>
      </div>
    </div>
    <div class="content" id="m-prod">
      <div class="top"><b>THE COLLECTION OF SOFAS, BEDS, FURNISHINGS AND
          ACCESSORIES ENTIRELY MADE IN ITALY</b>,
        BLENDS DESIGN, FUNCTIONS, MATERIALS AND COLOURS. INNOVATION,
        COMFORT AND VERSATILITY AT SPECIAL PRICES.
        <br/><br/>DISCOVER THE COLLECTION ONLINE AND TAKE ADVENTAGE OF
        OUR PROMOTION IN THE STORE.
      </div>
    </div><!-- prodotti -->
    <section>
      <div class="products row">
        <div class="list-products">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/arioso-2898--slide1-1000x500-4-9476-1000x500-4.jpg"
                class="img-responsive center-block"/><img
                class="hover-image img-responsive center-block"
                src="<?php print $base; ?>/natuzzi_summer_sale/images/arioso-2898--slide3-1000x500-6.jpg"/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/sofa-beds">SOFA BEDS <i
                  class="fa fa-chevron-right"
                  aria-hidden="true"></i></a></div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/brera-1000x500-2.jpg"
                class="img-responsive center-block "/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/chairs">CHAIRS <i
                  class="fa fa-chevron-right"
                  aria-hidden="true"></i></a></div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/e015lt0-hex-scont-2-web-1000x500.jpg"
                class="img-responsive center-block "/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/tables">TABLES <i
                  class="fa fa-chevron-right"
                  aria-hidden="true"></i></a></div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/ido-2994--daac20161006095640047-1000x500.jpg"
                class="img-responsive center-block"/><img
                class="hover-image img-responsive center-block"
                src="<?php print $base; ?>/natuzzi_summer_sale/images/ido-2994--daac20161006095628346-14533-1000x667.jpg"/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/sofas">SOFAS <i
                  class="fa fa-chevron-right"
                  aria-hidden="true"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/landing-letto-piuma-1000x500-2.jpg"
                class="img-responsive center-block "/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/beds">BEDS <i
                  class="fa fa-chevron-right"
                  aria-hidden="true"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/plisse-58b9418d667834-3-1000x500.jpg"
                class="img-responsive center-block "/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/furnishings-and-accessories">FURNISHINGS
                <i class="fa fa-chevron-right"
                   aria-hidden="true"></i></a></div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/a842v6v-pool-sc309smk-gemaa-3-1000x500.jpg"
                class="img-responsive center-block"/><img
                class="hover-image img-responsive center-block"
                src="<?php print $base; ?>/natuzzi_summer_sale/images/a84203v-pool-sc309amb-gema-4-1000x500.jpg"/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/furnishings-and-accessories">ACCESSORIES
                <i class="fa fa-chevron-right"
                   aria-hidden="true"></i></a></div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;"
               class="cursor-default"><img
                src="<?php print $base; ?>/natuzzi_summer_sale/images/viaggio-3-2-1000x500-2.jpg"
                class="img-responsive center-block"/><img
                class="hover-image img-responsive center-block"
                src="<?php print $base; ?>/natuzzi_summer_sale/images/viaggio-2-1000x500.jpg"/></a>
            <div class="prod-info layout-version-3"><a
                href="<?php print $promo_url; ?>/armchairs">ARMCHAIRS <i
                  class="fa fa-chevron-right"
                  aria-hidden="true"></i></a></div>
          </div>
        </div>
      </div>
      <div class="products-offers-note"><p><b>Come and visit us. Discover
            the floor sample sale models.<br/>Until
            September, the 17th.</b><br/>
        </p></div>
    </section><!-- stores -->


    <!--<section class="stores visible-lg visible-md" id="m-store">
        <div class="content"><p><span
                        class="size1">COME AND TEST</span><br/>NATUZZI
                QUALITY
            </p>
            <form class="form-stores"><input type="hidden" class="latitude"
                                             name="latitude"
                                             value=""/><input
                        type="hidden" class="longitude" name="longitude"
                        value=""/>
                <div class="search row">
                    <div class="col-sm-3"><label>ZIP/City</label><input
                                type="text" name="city" id="pac-input"
                                class="form-control"></div>
                    <div class="col-sm-3"><label>Search Radius</label>
                        <div class="form-control-select"><select
                                    name="distance">
                                <option value="">-- No radius --</option>
                                <option value="25">25Km</option>
                                <option selected="selected" value="50">
                                    50Km
                                </option>
                                <option value="100">100Km</option>
                            </select></div>
                    </div>
                    <div class="col-sm-3"><label>Store Type</label>
                        <div class="form-control-select"><select
                                    name="type">
                                <option value="">-- ALL --</option>
                                <option>Natuzzi Store</option>
                                <option>Natuzzi Gallery</option>
                                <option>Point Essence</option>
                                <option>Natuzzi Re-Vive</option>
                            </select></div>
                    </div>
                    <div class="col-sm-3"><label>&nbsp;</label>
                        <button type="button" class="btn-search">SEARCH
                            STORE&nbsp;<i class="fa fa-chevron-right"
                                          aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="container2">
            <div class="row">
                <div class="col-sm-9">
                    <div class="map"><!-- js load --><!--</div>
                    </div>
                    <div class="col-sm-3 store"><!-- load via ajax --><!--<strong>Use
                            the filters to improve your
                            search</strong></div>
                </div>
            </div>
        </section>-->

    <!-- video --><!-- store mobile -->
    <!--
    <section class="store-mobile" id="m-store-mobile"><p>FIND A <strong>NATUZZI
                ITALIA STORE</strong></p>
        <form class="form-store-mobile"><input type="hidden" name="latitude"
                                               class="latitude"/><input
                    type="hidden"
                    name="longitude"
                    class="longitude"/><a
                    href="javascript:;" class="btn-store-near"
                    onclick="trackEvent('store', 'near-store')">LOOK A STORE
                NEAR YOU&nbsp;&nbsp;<i class="fa fa-map-marker"
                                       aria-hidden="true"></i></a>
            <p>OR</p><input type="text" name="city" placeholder="ZIP/City"
                            id="pac-input-mobile"
                            class="form-control"></form>
        <div class="results-box">
            <div class="results"><!-- load via ajax --><!--</div>
                <a class="slideshow-controls arrow-left"
                   href="javascript:;"><img width="16px" height="31px"
                                            src="https://www.natuzzi.us/promotions/images/icons/arrow-left-black.svg"/></a><a
                        class="slideshow-controls arrow-right"
                        href="javascript:;"><img width="16px" height="31px"
                                                 src="https://www.natuzzi.us/promotions/images/icons/arrow-right-black.svg"/></a>
            </div>
        </section>
        -->

    <!-- why -->
    <section class="why" id="m-why">
      <div class="content">
        <div class="top">
          <!--<span class="size1">NATUZZI QUALITY</span><br/><span
                  class="size3">IS ALSO REFLECTED IN THE SERVICE</span>-->
          <p><span
              class="size1">COME AND TEST</span><br/>NATUZZI
            QUALITY
          </p>
        </div>
        <div class="plus row">
          <div class="col-5-div no-bt"><img
              src="https://www.natuzzi.us/promotions/images/icons/colori.svg"/><br/><br/>360
            COLOURS IN
            LEATHER OR FABRIC
          </div>
          <div class="col-5-div no-bt no-br"><img
              src="https://www.natuzzi.us/promotions/images/icons/design.svg"/><br/><br/>MADE
            IN ITALY
            DESIGN AND TECHNOLOGY
          </div>
          <div class="col-5-div"><img
              src="https://www.natuzzi.us/promotions/images/icons/quality.svg"/><br/><br/>NATUZZI
            WARRANTY
          </div>
          <div class="col-5-div no-br"><img
              src="https://www.natuzzi.us/promotions/images/icons/pointer.svg"/><br/><br/>COME
            AND VISIT US
          </div>
          <div class="col-5-div hidden-sm hidden-xs"><img
              src="https://www.natuzzi.us/promotions/images/icons/chiama.svg"/><br/><br/>CALL
            US FOR MORE
            INFO
          </div>
        </div>
      </div>
    </section><!-- video --></div>
</div>
<div class="form" id="m-form"><p><span class="size2">FLOOR SAMPLE SALE  UP TO 70%</span><br/>
    on sofas, beds,
    furnishings and accessories
  </p><!--
  <form class="form-data" method="post"><input type="hidden" name="id_landing"
                                               value="158"/><input
      type="hidden"
      name="title_landing"
      value="Sale"/>
    <div class="form-group"><input type="text" class="form-control"
                                   name="name" required
                                   placeholder="First Name"
                                   value=""></div>
    <div class="form-group"><input type="text" class="form-control"
                                   name="surname" required
                                   placeholder="Last Name"
                                   value=""></div>
    <div class="form-group"><input type="text" class="form-control"
                                   name="city" required placeholder="City"
                                   value=""></div>
    <div class="form-group"><input type="tel" class="form-control"
                                   name="phone" placeholder="Phone number"
                                   value="">
    </div>
    <div class="form-group"><input type="email" class="form-control"
                                   name="email" required
                                   placeholder="E-mail"
                                   value=""></div>
    <div class="form-group"><textarea class="form-control" name="message"
                                      placeholder="Comments"></textarea>
    </div>
    <div class="form-group noBg">
      <div class="checkbox privacy"><label><input type="checkbox" required
                                                  name="privacy"
                                                  value="Y"><a href="#"
                                                               data-toggle="modal"
                                                               data-target="#pop-privacy-form">Privacy
            Policy (read)</a></label></div>
    </div>
    <div class="form-group noBg text-center">
      <button type="button" class="btn btn-primary form-btn"
              onclick="trackEvent('phone', 'call')">YES, CONTACT ME
        <i class="btn-loading fa fa-chevron-right"
           aria-hidden="true"></i></button>
    </div>
  </form>-->
    <?php print render($content); ?>
</div>
<a class="sticky-footer" href="javascript:;" data-scroll-to-mobile="#m-form"
   onclick="trackEvent('popup-mobile', 'click')">I WANT TO TAKE ADVANTAGE OF
  <br/><span class="btn-call">BLOCK THE OFFER&nbsp;&nbsp;<i
      class="btn-loading fa fa-chevron-down"
      aria-hidden="true"></i></span></a>
<div class="total-black">
  <div class="popout"><span class="close-popout"></span></div>
</div>
<div class="modal fade" id="pop-cookie" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"> Privacy Policy</h4></div>
      <div class="modal-body"><p>The methods used to manage this website
          are described on this page, in reference
          to processing of the personal data of users who consult the
          website. This information is provided
          pursuant to art. 13 of legislative decree 196/03
          (hereinafter the Privacy Law) for users of the services
          of our website, provided via the Internet. The information
          does not apply to other websites consulted
          via our links, for which Natuzzi S.p.A. is not liable in any
          way. The information is also based on
          Recommendation no. 2/2001, which the European authorities
          for protection of personal data, combined in
          the group created by art. 29 of directive no. 95/46/CE,
          adopted on 17 May 2001 to identify the minimum
          requirements for collection of personal data on line: the
          methods, times and nature of the information
          which data controllers must provide to users when they visit
          the website.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>The Data Controller</strong><span></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The Data Controller of the data processed when our website is
          consulted is Natuzzi S.p.A., with
          registered office in Via Iazzitiello, 47 - Santeramo in
          Colle (BA)</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>The Data Processor</strong><span></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The external Data Processor is Protection Trade S.r.l., with
          registered office in Via Giorgio Morandi
          - 22, Itri (LT). An up-to-date list of the other data
          processors may be obtained by writing to<a
            href="mailto:natuzzi@protectiontrade.it"> natuzzi@protectiontrade.it</a>
        </p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>Location, purposes and disclosure of the
            data</strong><span></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The processing connected with the services of this website is
          performed at the aforementioned
          registered office of the Company, by employees and
          collaborators of Natuzzi S.p.A. responsible for
          processing and by any other people assigned to this. The
          personal data provided by users submitting
          requests for services is used for the sole purpose of
          providing the service and is not disclosed to
          third parties, unless disclosure is required by law or is
          strictly necessary to fulfil the requests.
          The data, which will not be disclosed, may be communicated
          to third parties exclusively for
          management of the services connected with our website.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>Types of data processed</strong><span></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><em>Browsing data</em><em></em></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The computer systems and software procedures used by this
          website acquire certain personal data,
          during normal functioning, which is transmitted implicitly
          through use of the Internet communication
          protocols.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>This information is not collected to be associated with
          identified individuals, but could, by its
          nature, through processing and association with data held by
          third parties, allow users to be
          identified.</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>This data includes IP addresses and domain names of the
          computers used to connect to the website, the
          URI (Uniform Resource Identifier) addresses of the resources
          requested, the time of the request, the
          method used to submit the request to the server, the size of
          the file obtained in response, the
          digital code indicating the status of the response from the
          server (successful, error, etc.) and
          other parameters relating to the operating system and the
          user's computer.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>This data is used solely to obtain anonymous statistical
          information on use of the website and to
          check correct functioning and is deleted immediately after
          processing. The data could be used to
          check for liability in the case of possible computer crimes
          damaging the website.</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><em>Data provided optionally by the user</em><em></em></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Optional, explicit and voluntary sending of e-mails to the
          addresses indicated on this website leads
          to subsequent acquisition of the sender's address, in order
          to respond to the requests, and also any
          other data contained in the e-mail.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Specific summary information is progressively reported or
          displayed on the pages of the website
          dedicated to particular services on request.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><em>Cookies</em><em></em></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>This website uses cookies and similar technologies to
          guarantee correct functioning of the procedures
          and to improve the experience of using on-line applications.
          Cookies are small text files stored on
          the hard disk of a computer. They simplify analysis of web
          traffic or indicate when a specific
          website is being visited, as well as allowing web
          applications to send information to individual
          users.</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Detailed information on use of cookies and similar
          technologies, how they are used by Natuzzi S.p.A.
          and how to manage them is provided below.</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Natuzzi S.p.A. uses the following types of cookies:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>- <span><span>"technical" cookies</span></span><span>:</span><span></span>
        </p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>These cookies are needed for browsing and guarantee secure
          and efficient browsing of the website.
          Google Analytics cookies are also used solely to collect
          information, in aggregate form, on the
          number of users and how they visit the website. These are
          third-party cookies collected and managed
          anonymously to monitor and improve the services of the host
          website (performance cookies).</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>For further information on the Google cookies policy, refer
          to the link indicated below: <a
            href="http://www.google.com/intl/it_ALL/analytics/learn/privacy.html">http://www.google.com/intl/it_ALL/analytics/learn/privacy.html</a>
        </p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>
          - <span>third-party “social plugin” cookies:</span><span></span>
        </p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>If you visit a website, you receive cookies from that
          particular website (“proprietary”) and also
          from websites managed by other organisations (“third
          parties”). These third-party cookies relate to
          the “social plugins” for Facebook, Twitter, Google. They are
          parts of the page generated directly by
          those websites and included on the host page. The most
          common use of social plugins is sharing the
          contents on social networks.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The presence of these plugins means cookies are transmitted
          to and from the websites managed by third
          parties. Please refer to the relative information on
          management of information collected by "third
          parties".</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>For greater transparency and simplicity, the addresses where
          said information and details on the
          methods of managing cookies may be obtained are provided
          below:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Facebook information: <a
            href="https://www.facebook.com/help/cookies/">https://www.facebook.com/help/cookies/</a>
        </p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Twitter information: <a
            href="https://support.twitter.com/articles/20170514">https://support.twitter.com/articles/20170514</a>
        </p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Yuotube information: <a
            href="http://www.google.it/intl/it/policies/technologies/cookies/">http://www.google.it/intl/it/policies/technologies/cookies/</a>
        </p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Instagram information:
          https://instagram.com/legal/cookies/</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>- <span>“Remarketing” cookies:</span></p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>These are persistent cookies, i.e. fragments of information
          which remain for longer and are stored on
          the hard disk of the user's computer until the cookies are
          deleted. “Persistent cookies” only store
          the fact that an unidentified visitor has browsed on the
          website and whether or not the visitor has
          accepted the summary information. The website uses other
          persistent cookies of third parties to
          monitor visits relating to the digital activities
          performed.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The Google Remarketing cookies are used.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The user may decide whether or not to accept the cookies
          using their own <em>browser</em> settings.
        </p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Total or partial disabling of cookies:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Total or partial disabling of technical cookies could prevent
          use of certain functions of the website
          reserved to registered users. However, use of public
          contents is possible even when cookies are
          completely disabled.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Disabling "third-party" cookies does not affect browsing in
          any way.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The setting must be defined specifically for the different
          websites and web applications.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The best <em>browsers</em> allow different settings to be
          defined for “proprietary” cookies and
          "third-party" cookies.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The procedures for disabling cookies of several browsers are
          shown below.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><span>Firefox:</span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>1. Open Firefox</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>2. Press “Alt” on the keyboard</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>3. On the instruments bar at the top of the browser, select
          “Instruments”, then “Options”</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>4. Select the “Privacy” option</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>5. Go to “Chronology settings”, then “Use customised
          settings”. Deselect “Accept cookies from
          websites" and save the preferences.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><span>Internet Explorer:</span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>1. Open Internet Explorer</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>2. Click on “Instruments” and then on “Internet Options”</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>3. Select the “Privacy” option and scroll to the desired
          privacy level (up to block all cookies and
          down to allow all cookies)</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>4. Then click on OK</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><span>Google Chrome:</span></p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>1. Open Google Chrome</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>2. Click on “Instruments”</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>3. Select “Settings” and then “Advanced settings”</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>4. Select “Content settings” under “Privacy”</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>5. On the “Cookies” option, it is possible to deselect the
          cookies and save the preferences</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><span>Safari:</span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>1. Open Safari</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>2. Choose “Preferences” on the instruments bar, then select
          the "Security" panel in the dialogue
          window which follows</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>3. In the “Accept cookies” section, it is possible to specify
          if and when Safari saves cookies from
          the websites. Click on the Help button (question mark) for
          further information</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>4. For more information on the cookies stored on your
          computer, click on "Show cookies”</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p> </p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>Optional providing of data</strong><span></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Other than what is specified for browsing data, the user is
          free to provide personal data to request
          the services offered by the company. It may be impossible to
          obtain the service requested if the
          data is not provided.</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>Data processing methods</strong><span></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The personal data is processed manually or automatically, for
          the time necessary to achieve the
          purpose for which it has been collected.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The personal data processed is kept in a manner to reduce the
          risks of destruction or loss of the
          data, even accidental, unauthorised access or illegal
          processing or processing unrelated to the
          purposes of collection to a minimum, by adopting appropriate
          and preventive security measures.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><strong>Rights of the parties concerned</strong><span></span>
        </p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>The people to whom the data refers have the rights envisaged
          by art. 7 of the Privacy Law, which is
          shown below:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>1. The person concerned is entitled to obtain confirmation of
          whether or not their own personal data
          exists, even if not registered, and whether it is
          communicated in intelligible form.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>2. The person concerned is entitled to obtain:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>a) the origin of the personal data;</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>b) the processing purposes and methods;</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>c) the logic applied when processing is performed using
          electronic instruments;</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>d) the identity of the data controller, the data processors
          and the representative appointed pursuant
          to article 5, paragraph 2;</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>e) the parties or the categories of parties to whom the data
          may be communicated or who may learn it
          as the appointed representative in the territory of the
          State, managers or processors.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>3. The person concerned is entitled to obtain:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>a) revision, correction or, if desired, supplementing of the
          data;</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>b) deletion, conversion into anonymous form or blocking of
          data processed illegally, including data
          which does not need to be kept in relation to the purposes
          for which it has been collected and
          subsequently processed;</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>c) certification that the operations of letters a) and b)
          have been explained, even only in terms of
          their content, to anyone to whom the data itself has been
          communicated or disclosed, unless this is
          impossible or would involve a use of resources which is
          manifestly disproportionate to the right
          being protected.</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>4. The party concerned may partially or fully oppose:</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>a) for legitimate reasons, processing of their own personal
          data, even for the reasons for which it
          has been collected;</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>b) processing of their personal data in order to send
          advertising or direct sales material or to
          conduct market surveys or make business communications.</p>
        <br/><br/><br/><br/><br/><br/><span>The person concerned can exercise these rights by sending an e-mail to </span><a
          href="mailto:natuzzi@protectiontrade.it">natuzzi@protectiontrade.it</a><br/><br/><br/><br/><br/><br/><br/><br/>
        <p><span><br/><strong>Company information</strong></span></p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>NATUZZI S.P.A.</p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <p>Registered office Via Iazzitiello, 47 – 70029 Santeramo in
          Colle (Ba)<br/>Tax Code and VAT and
          Companies Registration Office of Bari 03513760722<br/>Tel
          080 8820111 Fax 080 8820241<br/>Fully paid
          up capital € 54.853.045,00<br/>Company Subject to the
          direction and coordination of Invest 2003 Srl
        </p></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning"
                data-dismiss="modal">Close
        </button>
      </div>
    </div><!-- /.modal-content --></div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="pop-privacy-form" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Privacy Policy (read)</h4></div>
      <div class="modal-body"><p>Subject: Disclosure under the terms of
          Art. 13 of Italian Legislative Decree 196
          of 2003. Under the terms of Article 13 of Italian
          Legislative Decree 196 of 2003 (henceforth the Privacy
          Code), the Data Controller of Natuzzi S.p.A., whose
          registered office is at Via Iazzitiello, 47 –
          Santeramo in colle (BA), and with administrative
          headquarters at Via Iazzitiello, 47 - Santeramo in
          Colle (BA), informs you that your personal data is
          processed, by manual and automatic means: a. to carry
          out your requests b. to send you newsletters Supplying the
          data is optional, but without such data we
          may not be able to pursue the above purposes. The data will
          not be communicated or disseminated. The
          Marketing Department Manager and our staff who work there
          may access your personal data exclusively for
          the aforementioned purposes. The Data Processor is
          Protection Trade S.r.l. whose registered office is at
          Via Giorgio Morandi - 22, Itri (LT); a list of other
          processors may be requested by writing to the
          following e-mail address: natuzzi@protectiontrade.it. We
          inform you, finally, that you may exercise the
          rights envisaged in Article 7 of the Legislative Decree
          196/03; including the right to obtain, from the
          Data Controller or Processor, confirmation of the existence
          or otherwise of your personal data and to
          permit you, if they do exist, to gain access to them.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"
                data-dismiss="modal">Close
        </button>
      </div>
    </div><!-- /.modal-content --></div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="is-desktop"></div>
<div class="is-tablet"></div>
<div class="is-mobile"></div><!-- vendor cdn -->
<script src="https://use.fontawesome.com/f74604f2dc.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha256-U5ZEeKfGNOja007MMD3YBI0A3OSZOQbeG6z2f2Y0hu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"
        integrity="sha256-NjbogQqosWgor0UBdCURR5dzcvAgHnfUZMcZ8RCwkk8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"
        integrity="sha256-YrHP9EpeNLlYetSffKlRFg8VWcXFRbz5nhNXTMqlQlo="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"
        integrity="sha256-4F7e4JsAJyLUdpP7Q8Sah866jCOhv72zU5E8lIRER4w="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"
        integrity="sha256-4O4pS1SH31ZqrSO2A/2QJTVjTPqVe+jnYgOWUVr7EEc="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"
        integrity="sha256-jDnOKIOq2KNsQZTcBTEnsp76FnfMEttF6AV2DF2fFNE="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"
        integrity="sha256-iMTCex8BQ+iVxpZO83MoRkKBaoh9Dz9h3tEVrM5Rxqo="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.center.min.js"
        integrity="sha256-puNNvBbwmmw0WIyDXJ5cTd0W/EAiCArmnjo5gUfDxC4="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.swipe.min.js"
        integrity="sha256-Prq5k7iqQbFCbpoBYTw3cqMbUZyKh3Y0BBGPBfcxs20="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script>
  WebFont.load({
    google: {
      families: ['Lato:300,400,700']
    }
  });
</script>
<script src="<?php print $base; ?>/natuzzi_summer_sale/js/app_script.js"></script><!-- js -->

<script>

  (function($) {


    lang = 'en-US';
    labels = {
      "FORM_ERROR_TITLE": "Attention",
      "FORM_ERROR_TEXT": "Check the required fields",
      "FORM_ERROR_TEXT_GENERAL": "A general error occurred. Please try again later.",
      "NL_STORE_ERROR_GPS": "Your device doesn't support localization",
      "NL_STORE_ERROR_LOCATION": "We couldn't get your position"
    };
    url = '//www.natuzzi.us/promotions';
    landingSlug = 'sale';
    userPosition = 0;
    idLanding = 158;

    time = {"from": "900", "to": "1800"};

    console.log("outside block");

    $(function () {
      // preload images
      $('.banner').imagesLoaded(function () {
        $('.img-loading').addClass('hidden');
        $('.main-slideshow').removeClass('hidden');
      });

      console.log("inside block");

      //mobileScrollToStores();
      bindMainSliderControls();

      // send form
//      sendForm($('.form-btn'), 'send-request', 'send-contact');

      openDetail();
      getMoreProducts();

      // geo
//        getCoordsByAddress('#pac-input', function () {
//        });
//        initMap(position, points);
//        getStores();

      // mobile
      bindFormStoreMobile();
//        getCoordsByAddress('#pac-input-mobile', getStoresMobile);
//        getAutoLocation();

    });

  })(jQuery);

</script>
