<?php
//    $promo_url = base_path() . current_path();
    $promo_url = base_path() . 'promo/natuzzi-summer-sale';
?>

<!-- other promo -->
<section class="other-promo" id="m-col">
    <div class="content">
        <div class="top">
            <div><br/><br/>DO YOU WANT TO COMPLETE THE PERFECT
                MATCH?<br/><span
                    class="size2">BROWSE THE COLLECTION</span></div>
        </div>
    </div><!-- DESKTOP -->
    <div class="grid visible-lg visible-md">
        <div class="row">
            <div class="col-md-4"><a href="<?php print $promo_url; ?>/sofas"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x641-0007-divani-v-6-500x641-5.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Sofas</span></div>
                </a></div>
            <div class="col-md-4"><a href="<?php print $promo_url; ?>/sofa-beds"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x641-0008-divani-letto-v-7-500x641-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Sofa beds</span></div>
                </a></div>
            <div class="col-md-4"><a href="<?php print $promo_url; ?>/armchairs"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-poltrone-h-8-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Armchairs</span></div>
                </a><a href="<?php print $promo_url; ?>/beds"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-letti-h-25-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Beds</span></div>
                </a></div>
        </div>
        <div class="row">
            <div class="col-md-4"><a href="<?php print $promo_url; ?>/tables"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-tavoli-h-24-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Tables</span></div>
                </a></div>
            <div class="col-md-4"><a href="<?php print $promo_url; ?>/chairs"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-sedie-h-26-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Chairs</span></div>
                </a></div>
            <div class="col-md-4"><a
                    href="<?php print $promo_url; ?>/furnishings-and-accessories"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-librerie-h-27-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Furnishings and accessories</span>
                    </div>
                </a></div>
        </div>
    </div><!-- TABLET & MOBILE -->
    <div class="grid visible-sm visible-xs">
        <div class="row">
            <div class="col-sm-6 col-xs-6"><a href="<?php print $promo_url; ?>/sofas"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-divani-h-6-500x313-5.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Sofas</span></div>
                </a></div>
            <div class="col-sm-6 col-xs-6"><a href="<?php print $promo_url; ?>/sofa-beds"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-letti-h-7-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Sofa beds</span></div>
                </a></div>
            <div class="col-sm-6 col-xs-6"><a href="<?php print $promo_url; ?>/armchairs"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-poltrone-h-8-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Armchairs</span></div>
                </a></div>
            <div class="col-sm-6 col-xs-6"><a href="<?php print $promo_url; ?>/beds"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-letti-h-25-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Beds</span></div>
                </a></div>
            <div class="col-sm-6 col-xs-6"><a href="<?php print $promo_url; ?>/tables"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-tavoli-h-24-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Tables</span></div>
                </a></div>
            <div class="col-sm-6 col-xs-6"><a href="<?php print $promo_url; ?>/chairs"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-sedie-h-26-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Chairs</span></div>
                </a></div>
            <div class="col-sm-6 col-xs-6"><a
                    href="<?php print $promo_url; ?>/furnishings-and-accessories"><img
                        src="<?php print $base; ?>/natuzzi_summer_sale/images/500x313-librerie-h-27-500x313-4.jpg"
                        width="100%"
                        class="img-responsive center-block"/>
                    <div class="overlay"><span>Furnishings and accessories</span>
                    </div>
                </a></div>
        </div>
    </div>
</section>
<!-- why -->
<section class="why" id="m-why">
    <div class="content">
        <div class="top"><span class="size1">NATUZZI QUALITY</span><br/><span
                class="size3">IS ALSO REFLECTED IN THE SERVICE</span>
        </div>
        <div class="plus row">
            <div class="col-5-div no-bt"><img
                    src="https://www.natuzzi.us/promotions/images/icons/colori.svg"/><br/><br/>360
                COLOURS IN LEATHER OR FABRIC
            </div>
            <div class="col-5-div no-bt no-br"><img
                    src="https://www.natuzzi.us/promotions/images/icons/design.svg"/><br/><br/>MADE
                IN ITALY DESIGN AND TECHNOLOGY
            </div>
            <div class="col-5-div"><img
                    src="https://www.natuzzi.us/promotions/images/icons/quality.svg"/><br/><br/>NATUZZI
                WARRANTY
            </div>
            <div class="col-5-div no-br"><img
                    src="https://www.natuzzi.us/promotions/images/icons/pointer.svg"/><br/><br/>COME
                AND VISIT US
            </div>
            <div class="col-5-div hidden-sm hidden-xs"><img
                    src="https://www.natuzzi.us/promotions/images/icons/chiama.svg"/><br/><br/>CALL
                US FOR MORE INFO
            </div>
        </div>
    </div>
</section><!-- video -->
