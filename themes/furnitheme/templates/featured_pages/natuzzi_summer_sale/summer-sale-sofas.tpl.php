<?php
    $base = base_path() . "sites/all/themes/furnitheme/templates/featured_pages";
?>


<link href="<?php print $base; ?>/natuzzi_summer_sale/css/app_main.css" rel="stylesheet"
      type="text/css" media="all"/>

<style>
    .promo .banner img.title.custom {
        margin-top: 0px;
        position: absolute;
        margin-top: 15px; position: static;
    }
</style>


<div class="container-fluid" id="m-top">
  <div class="promo">

    <div class="detail-product"><i
      class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>
    <div class="banner"
         style="background-image: url(<?php print $base; ?>/natuzzi_summer_sale/images/bg-trulli-nuovo-121-1920x1080.jpg);">
      <img
        src="<?php print $base; ?>/natuzzi_summer_sale/images/ch-de-sale-121-600x449.png"
        class="title "/>
      <div class="slideshow-wrapper">
        <div class="cycle-slideshow main-slideshow hidden"
             data-cycle-log="false"
             data-cycle-center-horz="true"
             data-cycle-fx="scrollHorz"
             data-cycle-swipe="true"
        ><img data-id-product="1121"
              src="<?php print $base; ?>/natuzzi_summer_sale/images/prod-slide-1-900x400-18.png"
              class="img-responsive center-block"/><img
          data-id-product="11203"
          src="<?php print $base; ?>/natuzzi_summer_sale/images/prod-slide-2-900x400-17.png"
          class="img-responsive center-block"/>
          <div class="slideshow-controls"><a class="arrow-left"
                                             href="javascript:;"><img
            width="16px" height="31px"
            src="https://www.natuzzi.us/promotions/images/icons/arrow-left.svg"/></a><a
            class="arrow-right" href="javascript:;"><img
            width="16px" height="31px"
            src="https://www.natuzzi.us/promotions/images/icons/arrow-right.svg"/></a>
          </div>
        </div>
      </div>
    </div>
    <div class="content" id="m-prod">
      <div class="top"><b>THE COLLECTION OF SOFAS, BEDS, FURNISHINGS AND
        ACCESSORIES ENTIRELY MADE IN ITALY</b>, BLENDS DESIGN,
        FUNCTIONS, MATERIALS AND COLOURS. INNOVATION, COMFORT AND
        VERSATILITY AT SPECIAL PRICES. <br/><br/>SEE SOME EXAMPLES:
      </div>
    </div><!-- prodotti -->
    <section>
      <div class="products row">
        <div class="list-products">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;" data-id="9476"
               data-id-landing="121"><img
              src="<?php print $base; ?>/natuzzi_summer_sale/images/arioso-2898--slide1-1000x500-4-9476-1000x500-4.jpg"
              class="img-responsive center-block"/><img
              class="hover-image img-responsive center-block"
              src="<?php print $base; ?>/natuzzi_summer_sale/images/arioso-2898--slide3-1000x500-6.jpg"/>
              <div class="prod-info"><h3 class="product-title">
                Arioso&nbsp;&nbsp;<i
                class="fa fa-chevron-right hidden-lg hidden-md"
                aria-hidden="true"></i></h3>
                <div class="price-details"></div>
              </div>
            </a></div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;" data-id="1109"
               data-id-landing="121"><img
              src="<?php print $base; ?>/natuzzi_summer_sale/images/brio-sofa-relax-01-1000x500.jpg"
              class="img-responsive center-block"/><img
              class="hover-image img-responsive center-block"
              src="<?php print $base; ?>/natuzzi_summer_sale/images/brio-sofa-relax-03-1000x500-2.jpg"/>
              <div class="prod-info"><h3 class="product-title">
                Brio&nbsp;&nbsp;<i
                class="fa fa-chevron-right hidden-lg hidden-md"
                aria-hidden="true"></i></h3>
                <div class="price-details"></div>
              </div>
            </a></div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;" data-id="11203"
               data-id-landing="121"><img
              src="<?php print $base; ?>/natuzzi_summer_sale/images/iago-1000x500.jpg"
              class="img-responsive center-block"/><img
              class="hover-image img-responsive center-block"
              src="<?php print $base; ?>/natuzzi_summer_sale/images/iago-2954-1000x500-2.jpg"/>
              <div class="prod-info"><h3 class="product-title">
                Iago&nbsp;&nbsp;<i
                class="fa fa-chevron-right hidden-lg hidden-md"
                aria-hidden="true"></i></h3>
                <div class="price-details"></div>
              </div>
            </a></div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
            <a href="javascript:;" data-id="14533"
               data-id-landing="121"><img
              src="<?php print $base; ?>/natuzzi_summer_sale/images/ido-2994--daac20161006095640047-1000x500.jpg"
              class="img-responsive center-block"/><img
              class="hover-image img-responsive center-block"
              src="<?php print $base; ?>/natuzzi_summer_sale/images/ido-2994--daac20161006095628346-14533-1000x667.jpg"/>
              <div class="prod-info"><h3 class="product-title">Ido&nbsp;&nbsp;<i
                class="fa fa-chevron-right hidden-lg hidden-md"
                aria-hidden="true"></i></h3>
                <div class="price-details"></div>
              </div>
            </a></div>


            <!--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="14594" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/jeremy-natuzzi-italia-1--1--2-1000x500-4.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/jeremy-natuzzi-italia-1--2--1000x500-2.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Jeremy&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="14502" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/agora--sofa-natuzziitalia-1-rgb-14461-1000x667.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/agora--sofa-natuzziitalia-2-rgb-2-1000x500.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Agorà&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="12978" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/armonia-2788-intro-566-1000x1000.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Armonia&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1235" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/avana-2570-intro-1000x500-2.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/2570-25fc-009-d-1000x500.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Avana&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1108" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2826-009-15cq-4-borghese-425-1000x500.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/2826-009-15cq-7-borghese-425-1000x500.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Borghese&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1240" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2638-camp-001000002-a-4-1000x500-2.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/cambre-2638-intro-1000x500.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Cambrè&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="9443" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/cult-2575-intro-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Cult&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1111" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/dado-2-1000x500.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/dado-3-429-1000x833.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Dado&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1111" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/dado-2-1000x500.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/dado-3-429-1000x833.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Dado&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1248" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/diesis-2828-intro-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Diesis&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1249" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/domino-2226-r226-intro-1000x500.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/rs-2226-20ce-225198049-d-1000x500.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Domino&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="9560" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/dongiovanni-2906-slide1-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Don Giovanni&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="9588" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/dorian2904-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Dorian&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="9616" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/natuzzi-italia-fidelio--1--1000x500.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/fidelio-2907-446-60db-008-9400-1000x800.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Fidelio&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="14564" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/herman-natuzzi-italia-1--3--1000x500.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/herman-natuzzi-italia-1--1--1000x500.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">Herman&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="13038" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2102-ritoc1-intro-2-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">King&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="14471" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/3000-1-064-rgb-2-1000x500-2.jpg" class="img-responsive center-block">
                    <img class="hover-image img-responsive center-block" src="https://www2.natuzzi.com/cms/uploads/compressed/3000-2-064-rgb-2-1000x500-2.jpg">
                    <div class="prod-info">
                        <h3 class="product-title">La Scala&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="9672" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2911-272-274-30il230715-001-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Long Beach&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1104" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/nicolaus-2402-intro-2-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Nicolaus&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1227" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/preludio-2782-intro-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Preludio&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1120" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/surround-v571-25fp-009-a-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Surround&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="11217" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2964-064-pelle-campione-091215-001-intro-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Svevo&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1121" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2834-009-new-7-tempo-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Tempo&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1277" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/2787-088-40bm-85003208-a-2-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Tenore&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1122" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/tratto-2811-intro-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Tratto&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-product">
                <a href="javascript:;" data-id="1124" data-id-landing="121">
                    <img src="https://www2.natuzzi.com/cms/uploads/compressed/volo2821-intro-1000x500.jpg" class="img-responsive center-block product-image">
                    <div class="prod-info">
                        <h3 class="product-title">Volo&nbsp;&nbsp;<i class="fa fa-chevron-right hidden-lg hidden-md" aria-hidden="true"></i></h3>
                        <div class="price-details">
                        </div>
                    </div>
                </a>
            </div>-->






        </div>
        <a href="javascript:;" data-id-landing="121"
           data-next-offset="4" data-limit="4"
           class="btn btn-more-products"
           onclick="trackEvent('products', 'show-more')">OTHER MODELS <i
          class="fa fa-chevron-down"
          aria-hidden="true"></i></a></div>
      <div class="products-offers-note"><p><b>Come and visit us. Discover
        the floor sample sale models.<br/>Until September, the
        17th.</b><br/>
      </p></div>
    </section>

        <?php include_once dirname(__FILE__) . '/summer-sale-page-bottom.tpl.php';?>

  </div>
</div>

    <?php include_once dirname(__FILE__) . '/summer-sale-form.tpl.php'; ?>

<a class="sticky-footer" href="javascript:;" data-scroll-to-mobile="#m-form"
   onclick="trackEvent('popup-mobile', 'click')">I WANT TO TAKE ADVANTAGE OF
  <br/><span class="btn-call">BLOCK THE OFFER&nbsp;&nbsp;<i
    class="btn-loading fa fa-chevron-down"
    aria-hidden="true"></i></span></a>
<div class="total-black">
  <div class="popout"><span class="close-popout"></span></div>
</div>


<div class="is-desktop"></div>
<div class="is-tablet"></div>
<div class="is-mobile"></div><!-- vendor cdn -->
<script src="https://use.fontawesome.com/f74604f2dc.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"
  integrity="sha256-U5ZEeKfGNOja007MMD3YBI0A3OSZOQbeG6z2f2Y0hu8="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"
  integrity="sha256-NjbogQqosWgor0UBdCURR5dzcvAgHnfUZMcZ8RCwkk8="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"
  integrity="sha256-YrHP9EpeNLlYetSffKlRFg8VWcXFRbz5nhNXTMqlQlo="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"
  integrity="sha256-4F7e4JsAJyLUdpP7Q8Sah866jCOhv72zU5E8lIRER4w="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"
  integrity="sha256-4O4pS1SH31ZqrSO2A/2QJTVjTPqVe+jnYgOWUVr7EEc="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"
  integrity="sha256-jDnOKIOq2KNsQZTcBTEnsp76FnfMEttF6AV2DF2fFNE="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"
  integrity="sha256-iMTCex8BQ+iVxpZO83MoRkKBaoh9Dz9h3tEVrM5Rxqo="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.center.min.js"
  integrity="sha256-puNNvBbwmmw0WIyDXJ5cTd0W/EAiCArmnjo5gUfDxC4="
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.swipe.min.js"
  integrity="sha256-Prq5k7iqQbFCbpoBYTw3cqMbUZyKh3Y0BBGPBfcxs20="
  crossorigin="anonymous"></script>
<script
  src="https://unpkg.com/imagesloaded@4.1.3/imagesloaded.pkgd.min.js"></script>
<script>
  WebFont.load({
    google: {
      families: ['Lato:300,400,700']
    }
  });
</script>
<script src="<?php print $base; ?>/natuzzi_summer_sale/js/app_script.js"></script><!-- js -->

<script>

  lang = 'en-US';
  labels = {
    "FORM_ERROR_TITLE": "Attention",
    "FORM_ERROR_TEXT": "Check the required fields",
    "FORM_ERROR_TEXT_GENERAL": "A general error occurred. Please try again later.",
    "NL_STORE_ERROR_GPS": "Your device doesn't support localization",
    "NL_STORE_ERROR_LOCATION": "We couldn't get your position"
  };
  url = '//www.natuzzi.us/promotions';
  landingSlug = 'sofa';
  userPosition = 0;
  idLanding = 121;

  time = {"from": "900", "to": "1800"};


  $(function () {
    // preload images
    $('.banner').imagesLoaded(function () {
      $('.img-loading').addClass('hidden');
      $('.main-slideshow').removeClass('hidden')
    });

//    mobileScrollToStores();
//    bindMainSliderControls();

    // send form
//    sendForm($('.form-btn'), 'send-request', 'send-contact');

    openDetail();
    getMoreProducts();

    // geo
//    getCoordsByAddress('#pac-input', function () {
//    });
//    initMap(position, points);
//    getStores();

    // mobile
//    bindFormStoreMobile();
//    getCoordsByAddress('#pac-input-mobile', getStoresMobile);
//    getAutoLocation();

  });

</script><!-- custom track code --><!-- Facebook Pixel Code -->
