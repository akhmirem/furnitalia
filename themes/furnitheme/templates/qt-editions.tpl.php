<?php
    $base = base_path() . "sites/all/themes/furnitheme/";
    $is_ajax = isset($ajax) && !empty($ajax);
?>

<?php if ($is_ajax) : ?>
    <link href="<?php print $base; ?>css/qt_page_responsive.css" rel="stylesheet"
        type="text/css" media="all"/>
<?php endif; ?>

<div class="container-fluid" id="m-top">
    <div class="promo">
        <div class="detail-product"><i
                    class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>
        <div class="content" id="m-prod">
            <div class="top">
                <b>CHOOSE FROM THE NATUZZI EDITIONS <br/>
                    QUICKTIME COLLECTIONS</b>
                <br/><br/>
                <span style="font-size:0.8em">and start enjoying the best of Italian comfort
                    in your home in just a couple of weeks. </span><br/>

                <span style="font-size:0.8em">With QuckTime Program, the best-selling models and covers
                are stored in the USA for immediate delivery. The variety of styles and
                    configurations will help you pick the best from the pure Italian design.</span>

            </div>
        </div>
        <section>
            <div class="products row">
                <div class="list-products">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                                src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon1.jpg"
                                class="img-responsive center-block"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                                src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon2.jpg"
                                class="img-responsive center-block"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                                src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon3.jpg"
                                class="img-responsive center-block"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                                src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon4.jpg"
                                class="img-responsive center-block"/>
                    </div>
                </div>

                <img src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-constanza.jpg"
                     class="img-responsive"
                     alt="Constanza Sectional - Natuzzi Editions Quick Delivery Program" />

                <?php if(!$is_ajax): ?>
                    <br/> <br/>
                    <div class="products-offers-note">
                        <p>
                            <span style="">See more Natuzzi Editions Quick Delivery items </span><br/><br/>
                            <a href="/brand/natuzzi-editions?availability=quicktime" title="Browse Natuzzi Editions Quick Delivery items" class="btn btn-primary">Browse</a>
                        </p><br/>
                    </div>
                <?php endif; ?>
            </div>
        </section>

    </div>
</div>


