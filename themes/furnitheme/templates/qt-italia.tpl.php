<?php
    $base = base_path() . "sites/all/themes/furnitheme/";
    $is_ajax = isset($ajax) && !empty($ajax);
?>

<?php if ($is_ajax) : ?>
    <link href="<?php print $base; ?>css/qt_page_responsive.css" rel="stylesheet"
          type="text/css" media="all"/>
<?php endif; ?>

<div class="container-fluid" id="m-top">
    <div class="promo">
        <div class="detail-product"><i
                class="loading fa fa-cog fa-spin fa-3x fa-fw"></i></div>
        <div class="content" id="m-prod">
            <div class="top">
                <b><span class="caps" style="font-size: 1.5em;">Quick Delivery.</span> <br/>
                    Made in Italy, now in stock.</b>
                <br/><br/>
                <span style="font-size:0.8em">Natuzzi Quick Delivery program offers a selection of popular and best-selling models featured
                    in select configurations in stock &mdash; available in a couple of weeks. We are commited to help you
                    design and furnish a complete living space in the shortest amount of time – This is the Quick Delivery
                    program by Natuzzi Italia.  </span>

                <br/>
                <br/>

                <span style="font-size:0.8em">Natuzzi Italia models are entirely designed and made in Italy, created by Natuzzi designers in
                    our style center and manufactured by expert craftsman.  Production takes place at the Italian facilities in full
                    compliance with ergonomic principles to ensure unique comfort.  Strict quality control tests are performed on all
                    of Natuzzi materials to ensure safety and durability.  Natuzzi Group and 50+ Years of Italian design, now available for
                    immediate order.</span>

            </div>
        </div>
        <section>
            <div class="products row">
                <div class="list-products">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                            src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon1.jpg"
                            class="img-responsive center-block"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                            src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon2.jpg"
                            class="img-responsive center-block"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                            src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon3.jpg"
                            class="img-responsive center-block"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 single-product">
                        <img
                            src="<?php print $base; ?>/images/quick-delivery/natuzzi-editions-qt-icon4.jpg"
                            class="img-responsive center-block"/>
                    </div>
                </div>

                <img src="<?php print $base; ?>/images/quick-delivery/natuzzi-italia-quick-delivery.png"
                     class="img-responsive"
                     alt="Natuzzi Italia Quick Delivery Program - best-selling models available in couple of weeks" />

                <?php if (!$is_ajax) : ?>
                    <br/> <br/>
                    <div class="products-offers-note">
                        <p>
                            <span style="">See more Natuzzi Italia Quick Delivery items </span><br/><br/>
                            <a href="/brand/natuzzi-italia?availability=quicktime" title="Browse Natuzzi Italia Quick Delivery items" class="btn btn-primary">Browse</a>
                        </p><br/>
                    </div>
                <?php endif; ?>
            </div>
        </section>

    </div>
</div>


