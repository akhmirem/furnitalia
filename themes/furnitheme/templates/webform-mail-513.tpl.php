<?php

/**
 * @file
 * Customize the e-mails sent by Webform after successful submission.
 *
 * This file may be renamed "webform-mail-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-mail.tpl.php" to affect all webform e-mails on your site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The webform submission.
 * - $email: The entire e-mail configuration settings.
 * - $user: The current user submitting the form.
 * - $ip_address: The IP address of the user submitting the form.
 *
 * The $email['email'] variable can be used to send different e-mails to different users
 * when using the "default" e-mail template.
 */
?>

<?php

global $base_url, $theme_path, $conf;

//$data = '<pre>' . print_r($submission, TRUE) .'</pre>';
//$data .= '<br/><br/><pre>Email var:<br/>' . print_r($email, TRUE) .'</pre>';

$is_user = $email['email'] == 3;

$theme = $base_url . '/' . $theme_path;

// TEMP
$theme = 'https://www.furnitalia.com/sites/all/themes/furnitheme';
// TEMP

$site = 'desktop site';
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $site = 'mobile site';
}

$is_grand_reopening_voucher = false;
if (isset($submission->data['11']) && isset($submission->data['11'][0]) &&
  stristr($submission->data['11'][0], "promo/grand-reopening")) {
  $is_grand_reopening_voucher = true;
}

$is_10_percent_voucher = false;
if (isset($submission->data['11']) && isset($submission->data['11'][0]) &&
  stristr($submission->data['11'][0], "promo/natuzzi-labor-day-sale")) {
  $is_10_percent_voucher = true;
}



//------------------------
// ATTACHMENTS
/*
global $drupal_hash_salt;
$hash = md5($drupal_hash_salt);
$mime_boundary = "==Multipart_Boundary_x{$hash}x";
ob_start();
print "--" . $mime_boundary . "\r\n" . "Content-Type: text/html; charset=\"UTF-8\"; format=flowed; delsp=yes" . "\r\n";*/
//------------------------

?>

<?php if ($is_user) : ?>

    <div>

        <table cellpadding="0" cellspacing="0" width="600" align="center" style="border-collapse:collapse;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="100%"
                           style="border-collapse:collapse;border-spacing:0;">

                        <!-- LOGO -->
                        <tr>
                            <td style="text-align:center;"><span><a href="http://www.furnitalia.com" target="_blank"
                                                                    rel="nofollow" title="Furnitalia"
                                                                    style="text-decoration:none"><img
                                                src="<?php print $theme; ?>/images/webmail/furnitalia_logo.jpg" alt="Furnitalia"
                                                border="0" width="599" height="100"/></a></span></td>
                        </tr>

                        <tr>
                            <td style="height:15px">
                                <span><img src="<?php print $theme; ?>/images/webmail/space.gif" style="height:15px;"
                                           border="0"/></span>
                            </td>
                        </tr>
                        <!-- CONTENT -->

                        <tr>
                            <td align="center"
                                style="text-align:center;width:688px;border:2px solid #CCC;padding:10px;">

                                <table width="600" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td width="600" align="left" valign="top">
                                            <table width="600" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                <tr>
                                                    <td width="600" align="left" valign="top"
                                                        style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:14px;line-height:130%;font-weight:normal;text-decoration:none;color:#000;">

                                                      <?php if($is_grand_reopening_voucher): ?>
                                                        <img src="<?php print $theme; ?>/images/webmail/grand_reopening_sale_voucher.png"
                                                               alt="Here is your exclusive $250 discount voucher!"/>
                                                      <?php elseif($is_10_percent_voucher): ?>
                                                        <img src="<?php print $theme; ?>/images/webmail/FurnLbrDy10pctVchr.png"
                                                           alt="Here is your exclusive 10% OFF discount voucher!"/>
                                                      <?php else : ?>
                                                        <img src="<?php print $theme; ?>/images/webmail/spring-sale-coupon.png"
                                                             alt="Here is your exclusive $250 discount coupon!"/>
                                                      <?php endif; ?>

                                                      <br/>
                                                      <p>[submission:values:first_name],</p>

                                                      <?php if($is_grand_reopening_voucher): ?>
                                                        <p>Thank you for your interest in Furnitalia Grand ReOpening Sale!
                                                          We are looking forward to seeing you in our showroom. Present
                                                          this voucher for an exclusive $250 OFF discount on any furniture
                                                          purchase*. </p>
                                                      <?php elseif($is_10_percent_voucher): ?>
                                                        <p>Thank you for your interest in Furnitalia Sale! We are
                                                          looking forward to seeing you in our showroom. Present this
                                                          voucher for an exclusive 10% OFF discount on Natuzzi Editions
                                                          furniture loor sample purchase*. </p>
                                                      <?php else: ?>
                                                        <p>Thank you for your interest in Furnitalia Spring Sale! We are
                                                          looking forward to seeing you in our showroom. Present this
                                                          voucher for an exclusive $250 OFF discount on any furniture
                                                          purchase*. </p>
                                                      <?php endif; ?>

                                                      <p style="font-size: 12px;color:#555555">*Limited-time offer. Voucher does
                                                        not apply towards accessories purchase. Contact store for
                                                        details. </p>


                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>

                        <!-- \CONTENT -->
                        <tr>
                            <td style="height:15px">
                                <span><img src="<?php print $theme; ?>/images/webmail/space.gif" style="height:40px;"
                                           border="0"/></span>
                            </td>
                        </tr>
                        <!-- STORES -->
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%"
                                       style="border-collapse:collapse; border-spacing:0;">
                                    <tr style="text-align:center">
                                        <td>
                  <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:13px;">
                    <a href="http://www.furnitalia.com/contact" rel="nofollow" title="Locate Furnitalia Stores"
                       target="_blank"
                       style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-weight:bold; font-size:14px;text-decoration:none;">Furnitalia Main Store</a>
                  </span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial,Verdana,sans-serif; font-size:11px;">5252 Auburn Blvd.</span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial,Verdana,sans-serif; font-size:11px;">Sacramento, CA 95841</span><br/>
                                            <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:14px; font-weight:bold;">916.332.9000</span>
                                        </td>
                                        <td>
                  <span>
                    <a href="http://www.furnitalia.com" title="Furnitalia" rel="nofollow" target="_blank"
                       style="text-decoration:none"><img src="<?php print $theme; ?>/images/webmail/website.jpg"
                                                         style="height:41px;width:181px" border="0"
                                                         alt="Furnitalia.com"/></a>
                  </span>
                                        </td>
                                        <td>
                  <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:13px">
                    <a href="http://www.furnitalia.com/contact" rel="nofollow" title="Locate Furnitalia Stores"
                       target="_blank"
                       style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-weight:bold; font-size:14px; text-decoration:none;">Fountains at Roseville</a>
                  </span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial, Verdana, sans-serif; font-size:11px;">1198 Roseville Pkwy</span><br/>
                                            <span style="color:#6A6A6A; font-family:Arial, Verdana, sans-serif; font-size:11px;">Roseville, CA 95678</span><br/>
                                            <span style="color:#AB0000; font-family:Arial, Verdana, sans-serif; font-size:14px; font-weight:bold;">916.332.9000</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- \STORES -->
                        <tr>
                            <td>&nbsp;</td>
                        </tr>


                    </table>
                </td>
            </tr>
        </table>


    </div>

    <?php
    //------------------------
    //  ATTACHMENTS
    /*print "--" . $mime_boundary . "\r\n"
      ."Content-Type: application/pdf; name=\"Furnitalia_moving_sale_coupon.pdf\"" . "\r\n"
      ."Content-Disposition: attachment; filename=\"Furnitalia_moving_sale_coupon.pdf" ."\"\r\n"
      ."Content-Transfer-Encoding: base64\r\n\r\n"
      .chunk_split(base64_encode(file_get_contents($theme . '/files/landing/Furnitalia_moving_sale_coupon.pdf')))."\r\n";
   print "--" . $mime_boundary . "--\r\n";
   print(ob_get_clean());*/
    //------------------------
    ?>

<?php else: ?>

    <div class="request">

        <p><b>A new Renovation Sale webform registration was received on <?php print $site; ?> on [submission:date:long]</b></p>

        <p>[submission:values:first_name]</p>
        <p>[submission:values:email]</p>
        <p>[submission:values:address]</p>
        <p>[submission:values:phone]</p>
        <p>[submission:values:city]</p>
        <p>[submission:values:state]</p>
        <p>[submission:values:zip]</p>
        <p>[submission:values:utm]</p>
        <p>[submission:values:submitted_from]</p>
        <p>[current-user:ip-address]</p>

      <?php //print $data; ?>

    </div>

    <?php
    //------------------------
    //print(ob_get_clean());
    //------------------------
    ?>


<?php endif; ?>


