<?php
    global $conf;
    if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
        $is_desktop = FALSE;
    } else {
        $is_desktop = TRUE;
    }

	$theme_path = base_path() . drupal_get_path('theme', 'furnitheme');
	$pdf_icon = '<img src="' . $theme_path . '/images/pdf-icon.png" />';
	$files = base_path() . 'sites/default/files';
?>

<style>
    <?php if (!$is_desktop): ?>

        article.catalogs h4 {
            text-align: center;
        }
        article.catalogs h4 a {
            color: #444;
        }
        article.catalogs h4 a span {
            display: block;
            text-align: center;
        }
        article.catalogs ul {
            font-size:1.35em;
        }
        article.catalogs ul li {
            line-height: 130%;
            margin: 1em 0;
        }
        article.catalogs ul li a {
            color: #a02e31;
            border-bottom: 1px dotted #a02e31;
            text-decoration: none;
        }
    <?php else: ?>
        article.catalogs ul {
        font-size:1.35em;
        }
        article.catalogs ul li {
        line-height: 130%;
        margin: 1em 0;
        }
        article.catalogs ul li a {
        color: #a02e31;
        border-bottom: 1px dotted #a02e31;
        text-decoration: none;
        }
    <?php endif; ?>
</style>

<?php if (strstr(drupal_get_path_alias(), "stressless")) : ?>

    <article class="catalogs">
        <img src="<?php print $theme_path;?>/images/stressless/stressless-catalog-2017.jpg"/>
        <div style="font-weight: bold;font-size: 1.2em;margin: 1em 0 0.5em 0;">View or download the</div>
        <div> <a href="http://stressless.ekornes.com/US/totalus/" target="_blank" style="color: #a02e31;border-bottom: 1px dotted #a02e31;text-decoration: none;font-size:1.2em">Stressless Collection Catalog</a></div>
    </article>

<?php else : ?>
    <article class="catalogs">
        <section class="clearfix">
            <h4><a href="<?php print base_path(); ?>/brands/natuzzi-italia">
                    <img src="<?php print $files?>/brands/brand_page/natuzzi_italia_logo_color.jpg" />
                    <span>(Natuzzi Italia)</span>
                </a>
            </h4>
            <ul style="font-size:1.35em">
                <li>View the <a class="fancybox fancybox.iframe"  href="https://www.natuzzi.com/italia/cataloghi/catalogo_2017_eng" target="_blank" style="    color: #a02e31;
        border-bottom: 1px dotted #a02e31;
        text-decoration: none;">Natuzzi Italia Interactive Catalog</a> online</li>
                <li>Browse the <a class="fancybox fancybox.iframe" href="https://view.publitas.com/p222-10363/natuzzi-italia-quicktime-catalog-2017/" title="Natuzzi Italia  QuickTime Collections" target="_blank" style="    color: #a02e31;
        border-bottom: 1px dotted #a02e31;
        text-decoration: none;">Natuzzi Italia QuickTime Program Collections </a><br/><span>2-week availability of best-selling models in select confiurations</span><br/><br/></li>	
                <?php if (FALSE): ?>

                <li><a href="https://www.dropbox.com/s/6y8t7go61867dl3/benvenuti-a-casa-catalog2012.pdf" title="The Culture of Beauty"><?php print $pdf_icon?>Natuzzi Italia: The Culture of Beauty</a></li>
                <li><a href="https://www.dropbox.com/s/n3heanw5h3edg4i/NATUZZI%20ITALIA%20_Catalogue%20EN%202011-2012.pdf" title="Natuzzi Italia Catalog"><?php print $pdf_icon?>Natuzzi Italia Catalog</a></li>
                <li><a href="https://www.dropbox.com/s/sai9hqcxoakt1xl/NATUZZI%20ITALIA%20-%20Upholstery%20Product%20Catalog%20-%20October%202013.pdf" title="Natuzzi Italia Upholstery Catalog"><?php print $pdf_icon?>Natuzzi Italia Upholstery Product Catalog</a></li>	
                <li><a href="https://www.dropbox.com/s/ikc6q3m4ftkhx7k/NATUZZI%20ITALIA_FURNISHINGS_COLLECTION_2012_ed11_with_pillows_low2.pdf" title="Natuzzi Italia Furnishings Catalog"><?php print $pdf_icon?>Natuzzi Italia Furnishings</a></li>		
                <li><a href="https://www.dropbox.com/s/u7s7rzypp818mjg/NATUZZI%20ITALIA_Proxima_AND_NOVECENTO%20900_Wall%20Units.pdf" title="Natuzzi Italia Wall Units"><?php print $pdf_icon?>Natuzzi Italia Wall Units</a></li>
                <li><a href="https://www.dropbox.com/s/uztizcfyt6l9rw3/NATUZZI%20ITALIA-%20QT%20Brochure_Fall%202012.pdf" title="Natuzzi Italia QuickTime"><?php print $pdf_icon?>Natuzzi Italia QuickTime Program Catalog</a></li>	
            </ul>
            
            <ul>
                <li><a href="https://www.dropbox.com/s/mrjjq9orw4x7kbt/NATUZZI%20GROUP%20-%20WARRANTY%20INFORMATION.pdf" title="Natuzzi Group Warranty Information"><?php print $pdf_icon?>Natuzzi Group Warranty Information</a></li>	
                <li><a href="https://www.dropbox.com/s/e8muph1qvflfojy/Natuzzi_Information_Care_Manual_EN.pdf" title="Natuzzi Product Care Information Manual"><?php print $pdf_icon?>Natuzzi Product Care Information Manual</a></li>		
            <?php endif; ?>
            </ul>
        </section>

        <hr class="gradient"/>
        
        <section class="clearfix">
            <h4><a href="<?php print base_path(); ?>/brands/natuzzi-editions">
                    <img src="<?php print $files?>/brands/brand_page/natuzzi_editions_logo_color.jpg" />
                    <span>(Natuzzi Editions)</span>
                </a>
            </h4>
            <ul>
                <li>Browse the <a class="fancybox fancybox.iframe" href="https://view.publitas.com/p222-10363/natuzzi-editions-quck-time-catalog-april-2017-ro4yjqrejry4/" title="Natuzzi Edtions QuickTime Collections" target="_blank" style="    color: #a02e31;
        border-bottom: 1px dotted #a02e31;
        text-decoration: none;">Natuzzi Edtions QuickTime Collections </a><br/><span>2-weeks availability for best-selling models in select configurations<br/><br/></li>	
                <li><a href="https://www.dropbox.com/s/vvc3pkv3i57zle6/NATUZZI%20EDITIONS_Information_Care_Manual_EN.pdf" title="Natuzzi Edtions Product Care Information Manual"><?php print $pdf_icon?>Natuzzi Edtions Product Care Information Manual</a></li>	
                <li><a href="https://www.dropbox.com/s/d0fmep9do2e8jcc/The%20Value%20of%20a%20Unique%20Material%20Brochure.pdf" title="The Value of a Unique Material Brochure"><?php print $pdf_icon?>The Value of a Unique Material Brochure</a></li>
            </ul>
        </section>

        <hr class="gradient"/>

        <section class="clearfix">
            <h4><a href="<?php print base_path(); ?>/brands/bdi">
                    <img src="<?php print $files?>/brands/brand_page/bdi-brand-logo-new_0.jpg" />
                    <span>(BDI)</span>
                </a>
            </h4>
            <ul style="font-size:1.35em">
                <li>Preview or download<br/>
                    <a class="fancybox fancybox.iframe" href="https://view.publitas.com/p222-10363/bdi-product-catalog-2018/" title="BDI Furniture Product Catalog" style="color: #a02e31;
    border-bottom: 1px dotted #a02e31;
    text-decoration: none;">BDI Furniture Catalog</a>

                </li>
            </ul>
        </section>

        <hr class="gradient"/>

        <section class="clearfix">
            <h4>
                <a href="<?php print base_path(); ?>/brands/bdi">
                    <img src="<?php print $files?>/brands/brand_page/stressless_ekornes_brand-logo_0_0.jpg" />
                    <span>(Stressless by Ekornes)</span>
                </a>
            </h4>
            <ul style="font-size:1.2em; line-height:1.5em;">
                <li style="list-style-type:none">
                    <div style="font-weight: bold;font-size: 1.2em;margin: 1em 0 0.5em 0;">View or download the</div>
                    <div> <a href="http://stressless.ekornes.com/US/totalus/" target="_blank" style="color: #a02e31;border-bottom: 1px dotted #a02e31;text-decoration: none;font-size:1.2em">Stressless Collection Catalog</a></div>
                </li>
            </ul>
        </section>

        <?php if (false) : ?>
            <iframe width="720px" height="450px" src="https://www.yumpu.com/en/embed/view/jvgfKDbxyoBh4ROX" frameborder="0" allowfullscreen="true" allowtransparency="true"></iframe>

            <div id="publitas-embed-621xh3zl5ajmt7oeusg48zd7vi"></div><script publitas-embed data-wrapperId="publitas-embed-621xh3zl5ajmt7oeusg48zd7vi" data-cfasync="false" data-publication="https://view.publitas.com/p222-10363/natuzzi-editions-quicktime-program-brochure-2017/" data-responsive="true" type="text/javascript" src="https://view.publitas.com/embed.js"></script>

        <?php endif; ?>
    </article>
<?php endif; ?>
