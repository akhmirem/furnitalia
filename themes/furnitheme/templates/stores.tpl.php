<span class="field-label phone"><a href="tel:+19163329000" class="main-phone">1-916-332-9000</a></span>
<hr class="grey" />

<?php if (FALSE) :?>
<section class="hours-update" style="padding:10px 5px;font-size:1em;color:#981b1e;text-transform:uppercase;text-align:center">
Our stores are closed on <strong>December 24-25, 2017</strong> for Christmas holidays and <strong>December 31, 2017 and January 1, 2018</strong> for New Year observance.
<!--<ul style="margin:0"><li>Dec. 24, 2013 and Dec. 25, 2013</li>
<li>Dec. 30-31, 2013 and Jan. 1, 2014.</li>
</ul>-->
</section>
<?php endif; ?>

<?php $marker_img_path = base_path() . "sites/all/themes/furnitheme/images/gmap-marker.png"; ?>
<h2 class="block-title furn-red">Furnitalia Showroom Locations</h2>
<section class="store-info clearfix">
  <!--<div class="map"><iframe width="295" height="223" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=211487104845968262296.0004968861c94e20ef05a&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=38.664335,-121.349316&amp;spn=0.014945,0.025234&amp;z=14&amp;output=embed"></iframe></div>-->

  <div id="map-sac" class="map">&nbsp;</div>

  <div class="store-descr">
    <h2 class="field-label">Furnitalia Sacramento (main store)</h2>
    <div class="location-label">37k sq ft contemporary furniture showroom</div>

    <p class="info">
      5252 Auburn Boulevard <br/>
      Sacramento, California 95841<br/>
      – located close to the corner of Madison Avenue and
      Auburn Boulevard (next to the U-haul Rentals on Auburn Blvd).
    </p>

    <p class="info">
      Phone: <a href="tel:+19163329000" class="phone">(916) 332–9000</a> <br/>
      Hours: Monday – Sunday <br/>
      <!--<span class="hours-indent">10:00 am – 6:00 pm</span> -->
      <span class="hours-indent">10:00 am – 6:00 pm</span>
    </p>
    <a href="https://maps.google.com/maps?saddr=&daddr=Furnitalia,+5252+Auburn+Boulevard,+Sacramento,+CA+95841&z=11" target="_blank" class="directions-link">GET DIRECTIONS</a>
  </div>
</section>

<hr class="grey" style="margin:15px 0"/>

<section class="store-info clearfix">

  <div id="map-ros" class="map">&nbsp;</div>

  <!--<div class="map"><iframe width="295" height="223" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=211487104845968262296.0004968861c94e20ef05a&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=38.769543,-121.266747&amp;spn=0.007462,0.012617&amp;z=15&amp;output=embed"></iframe></div>-->
  <div class="store-descr">
    <h2 class="field-label">Furnitalia Roseville Boutique</h2>
    <div class="location-label">Located at the Fountains at Roseville</div>

    <p class="info">
      1198 Roseville Parkway, #120 <br/>
      Roseville, California 95678<br/>
      – located between New Balance and Aveda Salon.
    </p>

    <p class="info">
      Phone: <a href="tel:+19163329000" class="phone">(916) 332-9000</a> <br/>
      <strong>Store is temporarily closed due to COVID-19 situation</strong>
      <!--
      Hours: Monday – Saturday <br/>
      <span class="hours-indent">10:00 am – 6:00 pm</span> <br/>
      <span class="hours-indent">Sunday</span> <br/>
      <span class="hours-indent">11:00 am – 6:00 pm</span> <br/>
-->
    </p>
    <a href="https://maps.google.com/maps?saddr=&daddr=Furnitalia,+1198+Roseville+Pkwy+#120,+Roseville,+CA+95678&z=11" target="_blank" class="directions-link">GET DIRECTIONS</a>
  </div>
</section>

<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyB1-So6UFlqJuy0Byg833vwrPgPX1Qc9gc&sensor=false&extension=.js'></script>
<script>
  google.maps.event.addDomListener(window, 'load', InitMaps());
  var map;

  function InitMaps() {
    var sac_loc = [
      ['Furnitalia - Sacramento', 'undefined', '916-332-9000', 'info@furnitalia.com', 'https://www.furnitalia.com', 38.6645294, -121.3420888, '<?php print $marker_img_path; ?>']
    ];
    var map_center_sac = [38.660448,-121.345683];
    showMap(sac_loc, map_center_sac, 'map-sac', 14);

    var ros_loc = [
      ['Furnitalia - Roseville', 'undefined', '916-332-9000', 'info@furnitalia.com', 'https://www.furnitalia.com', 38.769543,-121.266747, '<?php print $marker_img_path; ?>']
    ];
    var map_center_ros = [38.769543,-121.266747];
    showMap(ros_loc, map_center_ros, 'map-ros', 16);
  }

  function showMap(locations, center, containerId, zoom) {
    var mapOptions = {
      center: new google.maps.LatLng(center[0], center[1]),
      zoom: zoom,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
      },
      disableDoubleClickZoom: true,
      mapTypeControl: false,
      scaleControl: false,
      scrollwheel: true,
      panControl: true,
      streetViewControl: false,
      draggable : true,
      overviewMapControl: false,
      overviewMapControlOptions: {
        opened: false,
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    }
    var mapElement = document.getElementById(containerId);
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker;
    for (i = 0; i < locations.length; i++) {
      if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
      if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
      if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
      if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
      if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
      if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
      marker = new google.maps.Marker({
        icon: markericon,
        position: new google.maps.LatLng(locations[i][5], locations[i][6]),
        map: map,
        title: locations[i][0],
        desc: description,
        tel: telephone,
        email: email,
        web: web
      });
      link = '';
    }
  }
</script>

<style>
  #map-sac, #map-ros {
    flex: 1 1 50vw;
    height: 350px;
  }
  .gm-style-iw * {
    display: block;
    width: 100%;
  }
  .gm-style-iw h4, .gm-style-iw p {
    margin: 0;
    padding: 0;
  }
  .gm-style-iw a {
    color: #4272db;
  }
</style>
