<?php
  $files_dir = variable_get('file_public_path', conf_path() . '/files');
  $slide_img_path = "/" . $files_dir . "/front_slider";
  $promo_dir = $files_dir . "/promo";

  global $conf;
  if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
    $is_desktop = FALSE;
  } else {
    $is_desktop = TRUE;
  }
?>

<style>
  .slide-caption-mobile {
    background: rgba(254, 254, 254, .85);
    color: #050601;
    font-size: 1.5em;
    top: 1em;
    left: 1em;
    width: 12em;
  }
</style>

<?php if (FALSE): ?>

  <!--
    <li>
    <a href="/promo/memorial-day-sale" title="Memorial Day SALE - up to 40% OFF">
      <img src="" data-lazy="<?php print $slide_img_path; ?>/memorial-day-homepage-slider.jpg"
           alt="Memorial Day Sale is ON NOW"
           style="width:11em" />
    </a>
    <?php //if (!$is_desktop): ?>
      <span class="slide-caption" style="    background: rgb(68, 150, 86, 0.95);
      font-size: 1.5em;
      top: 3em;">
        Memorial Day SALE <br/> Save BIG on modern furniture!
        <a href="/promo/memorial-day-sale" class="shop-now" style="color:#fff">Shop Now</a>
      </span>
    <?php //endif; ?>
  </li>
  -->
  <!--
  <li>
    <a href="/promo/fathers-day-sale" title="Fathers Day Sale! Get additional 10% OFF on recliners!">
      <img src="" data-lazy="<?php print $slide_img_path; ?>/Fathers-Day-Sale--home-page-slider.jpg"
           alt="Fathers Day Sale - Get additional 10% OFF on recliners!"
           style="" />
    </a>
    <?php if (!$is_desktop): ?>
    <span class="slide-caption" style="
    background: rgba(254, 254, 254, .85);
    color: #050601;
    font-size: 1.5em;
    top: 1em;
    left: 1em;
    width: 12em;
    "><span style="font-weight:bold"> Father's Day Sale</span> <br>Get extra 10% OFF on recliners! <a href="/promo/fathers-day-sale" class="shop-now" tabindex="0">Learn More</a></span>
    <?php endif; ?>
  </li>
    <li>
    <a href="/promo/holiday-suite-deals" title="Holiday Suite Deals! Up to 80% OFF!">
      <img src="" data-lazy="<?php print $promo_dir; ?>/holiday_suite_deals/Furnitalia-Holiday-suite-deals-2019-landing.jpg"
           alt="Holiday Suite Deals! Up to 80% OFF!"
           style="width:11em" />
    </a>
    <?php if (!$is_desktop): ?>
      <span class="slide-caption" style="background: rgb(68, 150, 86, 0.95);font-size: 1.5em;top: 3em;">
        Holiday Suite Deals! Up to 80% OFF!
        <a href="/promo/holiday-suite-deals" class="shop-now" style="color:#fff">Learn More</a>
      </span>
    <?php endif; ?>
  </li>
-->
<?php endif; ?>
<div class="slider-container">
<ul class="bxslider" style="margin:auto;padding:0;list-style-type:none" data-slick-lazy="true">

  <?php if (false): ?>
  <li>
  <a href="/living/sofas" title="Spring Sale!">
    <img src="" data-lazy="<?php print $slide_img_path; ?>/spring-sale-promo-home-page.jpg"
         alt="Spring Sale - up to 60% OFF!"
         style="width:11em" />
  </a>
  <?php if (!$is_desktop): ?>
    <span class="slide-caption" style="background: rgb(68, 150, 86, 0.95);font-size: 1.5em;top: 3em;">
        Spring Sale!
        <a href="/living/sofas" class="shop-now" style="color:#fff">Browse collection</a>
      </span>
  <?php endif; ?>
  </li>
  <?php endif; ?>

  <li>
    <a href="/natuzzi-italia/living" title="Enjoy the unique taste of Italian design and Made in Italy handcraft furniture.">
      <img src="" data-lazy="<?php print $slide_img_path; ?>/Natuzzi-Italia-Summer-Sale-2020.jpg"
           alt="Enjoy the unique taste of Italian design and Made in Italy handcraft furniture."
           style="width:11em" />
    </a>
    <?php if (!$is_desktop): ?>
      <span class="slide-caption" style="background: rgb(68, 150, 86, 0.95);font-size: 1.5em;top: 3em;">
        Enjoy the unique taste of Italian design and Made in Italy handcraft furniture.
        <a href="/natuzzi-italia/living" class="shop-now" style="color:#fff">Shop Now</a>
      </span>
    <?php endif; ?>
  </li>

  <?php if (false): ?><li>
    <a href="/promo/natuzzi-italia-top-sellers" title="Natuzzi Italia: shop best-sellers, now up to 40% off">
      <img src="" data-lazy="<?php print $slide_img_path; ?>/Releve-sectional-Italia-slide-imagev2.jpg"
                             alt="Natuzzi Italia best-sellers in-stock and at a special price!" />
    </a>
    <span class="slide-caption" style="
    background: rgba(254, 254, 254, .85);
    color: #050601;
    font-size: 1.5em;
    top: 1em;
    left: 1em;
    width: 12em;
    "><span style="font-weight:bold">Natuzzi Italia</span> <br>Best-seller in-stock items - now on sale! <a href="/promo/natuzzi-italia-top-sellers" class="shop-now" tabindex="1">Shop Now</a></span>
  </li>
  <?php endif; ?>
</ul>
</div>
<h2 class="home-subcaption">Modern & Contemporary Furniture</h2>
<?php print render($categories); ?>
