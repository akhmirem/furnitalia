
<!-- Header Promo Top -->
<!-- ========================================= -->
<style>

  #header_promo_top {
    min-height: 25px;
    margin-bottom: 10px;
  }
  #header_promo_top .header_promo_top__container  {
    text-align: center;
  }
  #header_promo_top a.hp-header {
    /*color: #ac0202;*/
    color: #ea2328;
    font-family: 'Source Sans Pro', sans-serif;
    background-color: rgba(153, 153, 153, 0.23);
    display: block;
    padding: 7px 0px;
    font-weight: 500;
    text-decoration: none;
    letter-spacing: .1em
  }
  #header_promo_top a.hp-header .em {
    font-weight:bold;
  }
  #header_promo_top a.hp-header:hover {
    text-decoration: underline;
  }

</style>
<?php if ($header_promo): ?>
  <div id="header_promo_top" style="">
    <div class="header_promo_top__container" style="">
      <a href="<?php print $header_promo['link']; ?>" class="hp-header" style="">
        <?php print $header_promo['title']; ?>
      </a>
    </div>
  </div>
<?php endif; ?>
<!-- \Header Promo Top -->