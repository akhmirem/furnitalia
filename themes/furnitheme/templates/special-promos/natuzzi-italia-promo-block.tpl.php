<?php


$files_dir = variable_get('file_public_path', conf_path() . '/files');
?>

<style>
  section.promo-section-top {
    border: 2px dashed #2ac07d;
    position: relative;
    margin-top: 4em;
    margin-bottom: 2em;
    text-align: center;
  }
  section.promo-section-top a.promo-link {
    text-decoration: none;
  }

  section.promo-section-top h2.promo {
    position: absolute;
    top: -4rem;
    padding: 0 1em;
    font-size: 2em;
    font-family: 'Source Sans Pro', sans-serif;
    font-style: italic;
    color: #07a556;
    left: auto;
    width: 100%;
    background: transparent;
  }
  h2.promo > span {
    background: #fff;
    padding: .5em 0.75em 0.1em;
  }

  div.sale-offer {
    display: flex;
    height: 200px;
    background: transparent url(/sites/default/files/promo/natuzzi-editions/spring-sale/spring_flower_editions2.png) no-repeat;
    background-position-x: -66px;
    align-items: center;
  }

  .sale-offer > div {
    justify-content: center;
    flex-direction: column;
    margin-right: 1.5em;
    align-items: center;
  }
  .brand-logo {
    order: 1;
    align-self: center;
  }

  .sale-offer div.promo-desc-block {
    flex: 1;
    /* margin-left: 3em; */
    color: #981b1e;
  }

  .sale-offer div.promo-desc-block .vertical-line {
    width: 1px;
    height: 60%;
    position: absolute;
    background: #aaa;
    left: 31em;
    top: 3em;
  }

  .sale-offer div.promo-desc-block .line-1 {
    font-size: 3rem;
    padding-left: 3rem;
    margin-top: 2rem;
    display: flex;
    justify-content: center;
  }

  span.upto {
    display: flex;
    flex-direction: column;
    font-size: 1rem;
    padding-right: .75rem;
    font-weight: bold;
    line-height: 100%;
  }

  .sale-offer div.promo-desc-block .line-2 {
    padding-left: 3rem;
    font-size: 1.7em;
    margin-top: 1rem;
    line-height: 100%;
    color: #222222;
  }
  span.plus {
    /* display: block; */
  }

  .sale-offer div.promo-desc-block .line-3 {
    color: black;
    margin-left: 3rem;
    margin-top: .7rem;
    font-size: .8em;
    letter-spacing: 1.3px;
    text-align: center;
  }

  @media all and (max-width: 767px){
    section.promo-section-top h2.promo {
      top: -1.0rem;
      background: transparent;
      padding: 0.25em .5em;
      font-style: normal;
      text-align: center;
      width: 100%;
      margin: auto;
      left: auto;
    }
    section.promo-section-top h2.promo > span {
      background: #e6e7e8;
      padding: 0.15em 0.5em;
    }

    span.upto {
      justify-content: center;
    }

		div.sale-offer {
			flex-direction: column;
			align-items: center;
			height: auto;
		}
		/*.sale-offer > div {
			display: block;
			margin-left: 0
    }*/
		.brand-logo {
      margin-top: 4.2em;
      width: 130px;
      order: 0;
      margin-left: 4.5em;
		}
    .sale-offer div.promo-desc-block {
      margin-left: 4.5em;
    }
		.sale-offer div.promo-desc-block .vertical-line {
      height: 1px;
      width: 3em;
      margin-top: 1em;
      top: 8em;
      left: 12.8em;
      background: #000;
		}
		.sale-offer div.promo-desc-block .line-1 {
      font-size: 2.5rem;
      padding-left: 0;
      margin-top: 2rem;
      line-height: 110%;
      text-align: left;
      font-weight: bold;
		}
		span.save {
			display:none;
		}
		.sale-offer div.promo-desc-block .line-2 {
      padding-left: 0;
      font-size: 2.5rem;
      margin-top: 1rem;
      line-height: 1.15;
      max-width: 7em;
		}
    span.additional {
      font-weight: bold;
      font-size: .55em;
      display: block;
      letter-spacing: .25em;
  }
		span.plus {
			display: block;
			line-height: 1;
			margin-top: -0.25em;
		}
    .sale-offer div.promo-desc-block .line-2 > span.smaller {
      font-size: .55em;
      display: block;
    }
		.sale-offer div.promo-desc-block .line-3 {
      margin: 1em auto;
		}
  }

</style>


<section class='promo-section-top'>
  <a href="/promo/natuzzi-italia-top-sellers" class="promo-link">
    <h2 class='promo'><span>SHOWROOM SALE</span></h2>
    <div class='sale-offer'>
      <div class='brand-logo'><img src="/<?php print $files_dir; ?>/brands/natuzzi_italia_logo_165x54.png"
                                   alt='Natuzzi Italia'/></div>
      <div class='promo-desc-block'>
        <div class='vertical-line'></div>
        <div class='line-1'><span class="upto"><span class="up">UP</span><span class="to"> TO</span></span> 40% OFF
        </div>
        <div class='line-2'>overstock & floor models</div>
        <div class='line-3'>*limited stock, contact for details</div>
      </div>
    </div>
  </a>
</section>
