<?php

global $conf;
if (isset($conf['SITE_ID']) && $conf['SITE_ID'] == 'mobile') {
  $is_desktop = FALSE;
} else {
  $is_desktop = TRUE;
}
$files_dir = variable_get('file_public_path', conf_path() . '/files');
$img_path = "/" . $files_dir . "/promo/calligaris/taxes-on-us";
?>

<style>

  .promo-desc-block {
    color: #000;
    border-bottom: 1px solid;
    padding: 1em 1em 1em 3.5em;
  }
  .promo-desc-block .save {
    font-weight: bold;
    color: #e43f44;
  }
  .promo-desc-block .smaller {
    font-size: .7em;
    color: #7b7a7a;
    letter-spacing: 1px;
  }

  @media all and (max-width: 767px){
    .promo-desc-block {
      color: #000;
      border-bottom: 1px solid;
      padding: 1em 1em 1em 1em;
      text-align: left;
      font-size: 1.2em;
      line-height: 1.5;
    }
  }

</style>


<section class='promo-section-top'>
  <div class='sale-offer'>

    <?php if ($is_desktop): ?>
      <img src="<?php print $img_path; ?>/US_taxes_on_us_desktop.jpg"  alt="Taxes on us + 25% OFF Calligaris!">
    <?php else: ?>
      <img src="<?php print $img_path; ?>/US_taxes_on_us_mobile.jpg"  alt="Taxes on us + 25% OFF Calligaris!">
    <?php endif; ?>

    <div class='promo-desc-block'>
      <div class='line-1'><span class="save">TAXES ON US + Up to 25% Off</span></div>
      <div class="line-2">
        Get a Tax break on Calligaris in addition to up to 25% off on entire Italian Design
        selection. <br/> <span class="smaller">Offer valid through April 1.</span>
      </div>
    </div>
  </div>
</section>
